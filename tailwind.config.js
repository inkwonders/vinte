const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    // mode: 'jit',
    purge: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.vue',
    ],

    theme: {
        extend: {
            // fontSize: {
            //     xxs: ['8px'],

            // },
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
            },
            backgroundImage: theme => ({
                'fondo-inicio': "url('/assets/images/svg/fondo_item.svg')",
                'fondo-back': "url('/assets/images/png/back_compra.jpg')",
                'fondo-desc': "url('/assets/images/png/fondo_desc.png')",
                'fondo-back-movil': "url('/assets/images/png/back_compra_movil.jpg')",
                'fondo-desc-prop': "url('/assets/images/png/fondo_desc_p.png')",
                'fondo-home': "url('/assets/images/fondo_home.png')",
                'fondo-home2': "url('/assets/images/home2.png')",
                'fondo-end': "url('/assets/images/home3.png')"
            }),
            colors: {
                'dorado': '#e89700',
                'amarillo-inicio': '#fbad3b',
                'amarillo-fin': '#fb803b',
                'amarillo-vinte': '#ff9420',
                'amarillo-texto': '#fbb03b',
                'amarillo-estrella': '#ead102',
                'naranja_desc': '#ff7130',
                'gris': '#b3b3b3',
                'gris-texto': '#4d4d4d',
                'gris-texto-claro': '#999999',
                'gris-texto-footer': '#808080',
                'gris-texto-desc': '#8a8a8a',
                'azul-vinte': '#255180',
                'azul-vinte-texto': '#0071cf',
                'azul-cian': '#29abe2',
                'azul-footer': '#3fa9f5',
                'azul-fondo': '#cdd6e0',
                'azul-footer_index': '#1C71C8',
                'blanco-fondo': '#f2f2f2',
                'amarillo-barra': '#F8B133',
                'morado': '#312782',
                'verde-landing': '#aacd08',
                'verde-boton': '#b3e200',
                'verde-desc': '#b9cd39',
                'gris-landing': '#A09E9E',
                'morado-xante': '#60338a',
                'morado-xante_claro': '#ede6f6',
                'fondo-xante_landing': '#fbfafd',
                'fondo-xante_gris': '#f8f8f8',
                'fondo-xante_morado': '#49217c',
                'fondo-xante_palabras': '#f5f3f7',
                'fondo-xante_cuadros': '#faf9fb',
                'naranja-landing': '#ff8b5d',
                'purpura-xante': '#9075b3',

                'morado-claro': '#554d89',
                'verde': '#b8d057',
                'morado-fuerte': '#583487',
                'purpura': '#b244fb'


            },
            fontFamily:{
                'roboto-thin': ['Roboto-thin'],
                'roboto-light': ['Roboto-light'],
                'roboto-normal': ['Roboto-normal'],
                'roboto-bold': ['Roboto-bold'],
                'roboto-black': ['Roboto-black'],
                'oswald-medium': ['Oswald-medium'],
                'titillium-semibold': ['SemiBold'],
                'titillium-bold': ['Bold'],
                'semibolditalic': ['SemiBoldItalic'],
                'montserrat-bold': ['Montserrat-Bold'],
                'montserrat-semibold': ['Montserrat-SemiBold'],
                'montserrat-regular': ['Montserrat-Regular'],
                'nunito-bold': ['nunito-bold'],
                'poppins-bold': ['Poppins-Bold'],
                'poppins-regular': ['Poppins-Regular'],
                'poppins-semibold': ['Poppins-SemiBold'],
                'poppins-light': ['Poppins-Light']
            },
            fontSize: {

                'xxs': '.50rem',
                'tiny': '8px',


            },
            opacity: {
                '222': '.222',
            },
            borderWidth:{
                DEFAULT: '1px',
            },
            height: {
                '120'  :'120%',
                '80vh' : '80vh',
                '85vh' : '85vh',
                'tres' : 'calc(100% - 240px)'
            },minHeight: {
                'mitad'          :       '50vh'
            },
            spacing: {
                '1/7': '14.28%',
                '3/7': '42.85%',
                '4/7': '57.14%',
                'card': '24rem',
            },
            backgroundSize: {
                'completo': '100% 100%',
            }
        },
    },

    plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography')],

};
