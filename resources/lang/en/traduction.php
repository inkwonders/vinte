<?php
return [
    //LOGIN
    'login' => 'Login',
    'email' => 'Email',
    'password' => 'Password',
    'forgot_pass' => 'Forgot the password',
    'enter' => 'Login',
    'logout' => 'Logout',
    'lang' => 'English',

    //TABLA PROPIEDADES
    'registration_date' => 'Registration date',
    'name' => 'Name',
    'phone' => 'Phone',
    'dev_name' => 'Development',
    'property_type' => 'House or Department',
    'state' => 'State',
    'zip_code' => 'Zip Code',
    'price' => 'Price',
    'see' => 'See More',
    'download_photos' => 'Download photos',
    'no_date' => 'No registration date',
    'no_images' => 'No images',
    'download_excel' => 'Download Excel',
    'empty' => 'No rows found',
    'status' => 'Status',

    //INFO DE LA PROPIEDAD
    'info_prop' => 'Property info',
    'type_prop' => 'Property type',
    'street' => 'Street',
    'num_ext' => 'Outdoor Number',
    'area' => 'Area',
    'levels' => 'Floors',
    'rooms' => 'Number of rooms',
    'baths' => 'Number of baths',
    'med_baths' => 'Number of half baths',
    'parkins' => 'Parking lots',
    'years' => 'Years',
    'price' => 'Price',
    'mortgage' => 'Mortgage',
    'mortgage_type' => 'Mortgage type',
    'reason_sale' => 'Reason for sale',
    'phone_number' => 'Cell phone number',
    'email_table' => 'Email',
    'garden' => 'Does the property have a terrace or garden?',
    'equipment' => 'Equipment',
    'equipment_type' => 'Equipment type',

    //MOTIVOS
    'mov_1' => 'Change house or apartment',
    'mov_2' => 'I\'m looking for liquidity',
    'mov_3' => 'Bank auction',
    'mov_4' => 'Move to another city',
    'mov_5' => 'Other',

    //EQUIPO
    'cooking' => 'Integral kitchen',
    'cancel_bath' => 'Bathroom furniture',

    //SINGLE WORDS
    'house' => 'house',
    'department' => 'department',
    'search' => 'Search',
    'floor' => 'Floor',
    'yes' => 'YES',
    'active' => 'Active',
    'discarded' => 'Discarded',
    'change' => 'Change Status',


    //tabla usuarios
    'new_user' => 'New User',
    'user_type' => 'User Type',

    //MENU
    'new_property'  =>  'Properties (Buy)',
    'new_property_old'  =>  'Properties (Sell Old)',
    'properties'    =>  'Properties (Sell)',
    'users'         =>  'Users',
    'neighborhoods' =>  'Neighborhoods',

    'acciones'      =>   'Actions',


    //colonias
    'new_neighborhood' => 'New Neighborhood',
    'municipality'     => 'Municipality',
    'neighborhood'     => 'Neighborhood',
    'cp'               => 'Zip Code',
    'neighborhood_type'=> 'Type',
    'neighborhood_active' => 'Active',


];
