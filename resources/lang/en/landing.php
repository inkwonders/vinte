<?php
return[
    'sell_h1'           => 'Sell your house <strong>fast,</strong><br /> also with mortgage.<br> Receive an offer in <strong>10 days</strong>*',
    // 'sell_h1'           => 'Sell your house <strong>fast,</strong><br /> receive an offer in <strong>10 days</strong>*',
    'w_sell'            => 'I want to sell',
    'w_buy'             => 'I want to buy',
    'policy'            => 'Privacy policy',
    'buy'               => 'Buy',
    'sell'              => 'Sell',
    'company'           => 'Company',
    'sell_h'            => 'Sell your house',
    'buy_h'             => 'Buy your house',
    'help'              => 'Help',
    'faq'               => 'Frequently asked questions',
    'contact'           => 'Contact Us',
    'h2_p'                => '<b class="text-morado-Xante font-montserrat-bold"> Selling or buying </b> <b class="font-montserrat-bold"> your house is now very easy and 100%<br class="hidden md:block"> safe </b> we advise you free of charge throughout the process',
    // 'time_'             => '*The stipulated time depends on the process required by the house or apartment.',
    'time_'             => '*The stipulated time will depend on the process that your house or apartment requires.',
    'time_1_nw'         => '*The stipulated time will depend on the process that your property requires. Additional expenses such as ISR, certifications (freedom from liens) and/or mortgage cancellation are expenses that you will have to pay as the owner. In the event that the home is mortgaged, the process times may vary depending on the entity where it is located.',
    'time_2_nw'         => '**The stipulated time will depend on the process that your property requires. Additional expenses such as ISR, certifications (freedom from liens) and/or mortgage cancellation are expenses that you will have to pay as the owner. In the event that the home is mortgaged, the process times may vary depending on the entity where it is located.',
    'bills'             => '** Additional expenses such as ISR, certifications (freedom from liens) and/or mortgage cancellation are expenses that the owner of the house will have to pay. In the event that the home is mortgaged, the process times may vary according to the entities.',
    'sell_h_p'          => 'When you register on our portal, we will contact you to visit your home. You will receive an <b class="font-montserrat-bold"> offer </b> and if you accept, receive your <b class="font-montserrat-bold"> money </b> in <b class="font- montserrat-bold"> 4 business days!</b>*',
    'buy_h_p'           => 'We have the best options to acquire your <b class="font-montserrat-bold"> used home</b>! <br><br> ¡Choose your house, set it aside and within 45 days you will have it <b class="font-montserrat-bold">deeded </b> and <b class="font-montserrat-bold"> immediate delivery!</b>**',
    'buy_h_p_2'           => '**The time will depend on the type of financing with which the home was purchased and that it is 100% liquidated
    ',
    'why_sell'          => 'Why sell your house with xante?',
    'days_10'           => 'Receive an offer in 10 days.*',
    'wr_p'              => 'Without so much trouble we make you an offer for your house, an easy, fast and 100% safe process. Register your house, we will carry out a technical visit and in 10 days you will receive an offer from Xante.',
    'accept_offer'      => 'If you accept the offer, we will pay for your house in 4 business days.*',
    'accept_offer_p'    => 'Once the offer is accepted, you deliver all your documentation, we review it, we sign a contract and you receive your money in 4 business days. So easy and safe.',
    'attention'         => 'Personalized and free attention.',
    'attention_p'       => 'We advise you throughout the process at no cost. You will not have to worry about anything, we solve all the doubts you have.',
    'experience'        => 'Xante experience',
    'easy'              => 'Easy, fast and 100% secure',
    'meet_h'            => 'We want to know details of your house!',
    'meet_h_p'          => 'Register your house or apartment <a href="/form" class="text-blue-400 underline font-montserrat-semibold"> here, </a> fill in the form with all possible information to make a first offer.',
    'visit_h'           => 'We will visit your house',
    'visit_h_p'         => 'If the house or apartment meets what we are looking for, we will schedule a technical visit with one of our advisors for its assessment.',
    'offer_h'           =>  'We will make you an offer!',
    'offer_h_p'         =>  'In a period of 10 days* you will be receiving a purchase offer for your house.',
    'money_72'          =>  'Receive your money in 4 business days*',
    'money_72_p'        =>  'If you agree with the offer, we will take care of all the process of for free and you get paid.',
    'partners'          => ' Xante <br class="lg:hidden"> Partners ',
    'adviser'           => 'Are you a real estate consultant?',
    'adviser_p1'        =>  'Are you a real estate advisor? We are your best ally. We have locations
    unique and immediate delivery.',
    'adviser_p2'        =>  'Your client visits the house, sets it aside,
    deliver his file, we start the deed process and deliver
    his house. Sign up with us and earn attractive commissions!',
    // 'buy_p'             =>  'Find the home you dreamed of, they are <br /> <b> unique locations</b> and <b>immediate delivery.**</b>',
    'buy_p'             =>  'Find the home you dreamed of, they are <br /> <b> unique locations</b> and <b>
    immediate delivery.**</b>',
    'delivery'          =>  'Immediate Delivery',
    'delivery_2'          =>  'Writing and immediate <br> delivery',

    'desc_1'            =>  'Like new, full kitchen and closets!',
    'desc_2'            =>  'With excellent finishes and equipment!',
    'desc_3'            =>  'At a great price!',
    'desc_4'            =>  'Excellent location',
    'desc_5'            =>  'Only price',
    // 'apart'             =>  '*Save your home, we accompany you throughout the process and we deliver your home in just 2 days',
    'apart'             =>  '**The time will depend on the type of financing with which the home was purchased and that it is 100% liquidated.',
    'client'            =>  'Learn about the experiences',
    'client_p'          =>  'of those who have sold or bought a property from us with Xante.',
    'opinion_1'         =>  '<p>The sale was quick and the value was fair. They helped me with all the paperwork and the advice was good. <br><br> Thanks! <br><br></p>',
    'opinion_2'          =>  "<p>I thank Xante for helping me sell my home in Querétaro, I live in the US and I don't have the time and opportunity to focus on doing the whole process. Very comfortable with the attention and facilities for the entire process, making it easy and comfortable. <br><br> Thank you for your help, I highly recommend you. <br><br> </p>",
    'opinion_3'         =>  '<p>My experience was very good because they helped me quickly resolve the process of buying my apartment without the need to use traditional methods that are more expensive in time and money. I thank you for accompanying me along the way at all times. <br><br> </p>',
    'opinion_4'         =>  '<pExcellent service, they serve you quickly and are cordial. Their times are very good and they help you solve all your doubts during the process, from the visit to the signing of the contract.
    <br><br> </p>',
    'opinion_5'         =>  '<p>It was very fast, in time and form. Friendly and excellent treatment. <br><br> </p>',
    'opinion_6'         =>  '<p>Thank you for your follow-up and support throughout this process. <br><br> </p>',
    'opinion_7'         =>  '<p>Excellent service for the purchase of real estate. The treatment is reliable and they take care of all the credit and deed procedures, you can also request that they show you the house.<br>
    The purchase was very fast and the documents are in order. <br>100% recommended.<br><br> </p>',
    'opinion_8'         =>  '<p>The process of selling my house was very simple, the steps to follow are easy, the process did not take more than 15 days and the payment was exact after accepting the proposal, it all depends on having the papers in order and up to date. The staff is friendly and the follow-up is in a personal way. Excellent service. Super recommended. <br><br> </p>',

    'opinion_9'         =>  '<p>Hello, good morning, yes, of course, the truth is very professional, they promise what they say about the payment in 4 days, everything is fine, I was very satisfied with the process, thank you. <br><br> </p>',
    'opinion_10'         =>  '<p>The truth is very satisfactory, professional in time and form. Above all, honest and honorable. I really appreciate the attention. <br><br> </p>',
    'opinion_11'         =>  '<p>It is a company that performs its services with efficiency, speed and transparency. I recommend it. <br><br> </p>',
    'opinion_12'         =>  '<p>Yovanna did excellent follow-up and helped us at all times. Thank you! <br><br> </p>',
    'opinion_13'         =>  '<p>Clear, reliable process and at a fair price.<br><br> </p>',


    'faq_p'             => 'Frequently asked questions about <br> how to sell my property',

    'faq_q1'            => 'How does the Xante sales service work?',
    'faq_q1_a'          => 'To sell your home to Xante, request a free offer by providing a few details about the property in our online form. <br><br>If the property is in our areas of influence, we will contact you
    to schedule a technical visit to see your house and take some pictures.<br><br>  After the visit, if your house meets our requirements, we will make you an offer and if you accept it, we will close the operation shortly. time and you can receive your money in 4 business days. *',
    'faq_q2'            => 'Does the valuation of my property cost?',
    'faq_q2_a'          =>  'No. It is totally free.',
    'faq_q3'            => 'What requirements must my house meet in order to sell it?',
    'faq_q3_a'          => 'You must be the owner of the property, have all the legal documentation of the property. And your service payments must be up to date (water, electricity, property and maintenance). It is important that your home is at least 5 years old, this will allow us to make you a better offer. Otherwise, we may not be able to make a proposal to you.',
    'faq_q4'            =>  'Can I sell my house if it is still mortgaged?',
    'faq_q4_a'          =>  'Regardless of whether you have a mortgage or not, we can make a
    offer for your house.',
    'faq_q5'            =>  'What expenses do I incur when I sell my house?',
    'faq_q5_a'          =>  'When selling a property, Income Tax (ISR) is generated, which you must pay. <br><br>And if you have a mortgage, you must consider the payment for its cancellation.',
    'faq_q6'            =>  'What are notarial expenses?',
    'faq_q6_a'          =>  'These are the fees received by Notaries Public for the
    formalization and completion of the procedures for the deed and
    titling of the property.',
    'faq_q7'            =>  'What does Xante take into account to value your home?',
    'faq_q7_a'          =>  'With our experience in the areas where we are present,
    we have data to make comparisons between new housing and
    used; we analyze the behavior of demand and supply; Y
    We review the condition of the property to be sold.',
    'faq_q8'            =>  'Who is Xante?',
    'faq_q8_a'          =>  "We are Vinte's Proptech focused on the purchase and sale of housing
    almost new, where through the use of technology we speed up,
    We automate and optimize the entire real estate process.<br><br>Vinte is a developer with more than 20 years of experience, who has
    developed 28 projects, today it is considered a leader in
    innovation and sustainability of housing in Mexico.<br><br>To date, more than 60 thousand families live in a Vinte community in
    State of Mexico, Hidalgo, Puebla, Querétaro, Quintana Roo and New
    Lion.",

    'see_all_q'         =>  'See all questions',

    'follow'            =>  'Just follow these simple steps:',
    'upload'            =>  'Upload your house',
    'offer'             =>  'We\'ll make you an offer',
    'free_fast'         =>  'Offer your home for free,<br> simply and quickly.',
    'begin'             =>  'Begin',
    'dev'               =>  'Developed by MOMENTUM',
    'partnes_uno'       =>  'I rent your house',
    'partnes_dos'       =>  'I rent you the house to buy',
    'partnes_tres'      =>  'lends you to buy a house',
    'partnes_cuatro'    =>  'Equip the house',
    'partnes_cinco'     =>  'sells you a new house',
    'buy_d_h'           => 'Find the home you dreamed of, they are <br> <b class="font-montserrat-semibold">  unique locations </b> and <b class="font-montserrat-semibold">immediate delivery.*</b>',
    'apart_c'           => 'Reserve your home, we accompany you throughout the process and we deliver your home in just 2 days. <br> The delivery time of the home may vary if the client acquired some type of credit to acquire it.',
    'desc_contact'       => 'Do you want to meet him?',
    'xan_medios'     =>  'Xante in the media',
    'xan_discraimer'     =>  '*The prices of the houses can vary, it will depend on whether it is acquired in cash or with some credit, in addition to the notary expenses.  <br> **Reserve your home. We advise you and accompany you throughout the process. We sign the deed and deliver your house in a maximum of 5 days. <br>  this time may vary, since any difference that exists must be settled before delivery.',
    'disponible'     =>  'AVAILABLE',
    'escritura_3_dias'     =>  'Immediate writing <br> and we deliver to you in 3 days',
    'sell_c'     =>  'SOLD',
    'sell_apar'     =>  'PULLED APART',
    'sell_escr'     =>  'DEED',
    'sell_entr'     =>  'DELIVERED',
    'sell_disp'     =>  'AVAILABLE',
    'xan_c_desc_1'     =>  'Incredible offer with 70 thousand pesos equipment!',
    'xan_c_desc_2'     =>  'Ready to debut! <br> With integral kitchen, closet and protections.',
    'xan_c_desc_3'     =>  'Almost like new! <br> With integral kitchen and closetss',
    'xan_c_desc_4'     =>  'Unique price and location!',
    'xan_c_desc_5'     =>  'Unique location on the ground floor <br> and with equipment!',
    'xan_c_desc_6'     =>  'Excellent location! <br> At a super price Premiere now!',
    'xan_c_desc_7'     =>  'Take advantage of beautiful house! <br> With floor, integral kitchen, closets and more',
    'xan_c_desc_8'     =>  'Ready to debut! <br> With integral kitchen, closets and cancel in the bathroom',
    'xan_t_prox'     =>  'Your next home is here!',

    'xan_d_p_t_v_1'     =>  'Incredible offer',
    'xan_d_p_t_g_1'     =>  'with equipment of <b class="font-montserrat-bold">  70 thousand pesos!</b>',
    'xan_d_p_t_n_1'     =>  'Apartment in excellent condition <br> Ready to move in!',
    'xan_d_p_d_g_1'     =>  'It has a full kitchen, closets in bedrooms and a cancel in the bathroom. It is located in Real Segovia, a development with controlled access, parks, a dog park, a baseball field and a 7-a-side soccer field, shops and a bicycle path. Very close to VW, universities and 15 minutes from Cholula.',

    'xan_d_p_t_v_2'     =>  'Ready to debut!',
    'xan_d_p_t_g_2'     =>  'With integral kitchen, closets and protections',
    'xan_d_p_t_n_2'     =>  'Apartment in excellent condition, ready to move in!',
    'xan_d_p_d_g_2'     =>  'It has a full kitchen, closets in bedrooms and protections. It is located in Real Solare, a development with controlled access, playgrounds, dog park, multi-courts, outdoor gym, bike path, schools and shops. Very close to industrial parks in the El Marqués area and 15 minutes from downtown Querétaro.',

    'xan_d_p_t_v_3'     =>  'Almost like new!',
    'xan_d_p_t_g_3'     =>  'With full kitchen and closets',
    'xan_d_p_t_n_3'     =>  'Beautiful house, like new!',
    'xan_d_p_d_g_3'     =>  'It has a full kitchen and closets in excellent condition. It is located in Real Toledo, a development with controlled access, playgrounds, multi-courts, a bike path and shops. A few steps from Esplanada Pachuca, near schools, universities, hospitals and the Tuzobus.',

    'xan_d_p_t_v_4'     =>  'Unique price and location!',
    'xan_d_p_t_g_4'     =>  '',
    'xan_d_p_t_n_4'     =>  '2 level home in excellent condition ready to move in!',
    'xan_d_p_d_g_4'     =>  'It has a full kitchen (does not include a stove) and a large service patio. It is located in Real Segovia, a development with controlled access, parks, a dog park, a baseball field and a 7-a-side soccer field, shops and a bicycle path. Very close to VW, universities and 15 minutes from Cholula.',

    'xan_d_p_t_v_5'     =>  'unique location',
    'xan_d_p_t_g_5'     =>  'on the ground floor and with equipment!',
    'xan_d_p_t_n_5'     =>  'Ground floor corner apartment in excellent condition, ready to move into!',
    'xan_d_p_d_g_5'     =>  'It has a full kitchen, closets, cancel in the bathroom and protections. It is located in Real Granada, a development with controlled access, parks, a dog park, multi-courts, schools, shops and a bicycle path. 15 minutes from AIFA and 45 minutes from CDMX.',

    'xan_d_p_t_v_6'     =>  'Excellent location',
    'xan_d_p_t_g_6'     =>  'at a super price! Premiere now!',
    'xan_d_p_t_n_6'     =>  'Beautiful house at a super price, in excellent condition, ready to move into!',
    'xan_d_p_d_g_6'     =>  'It has a floor, cancel in the bathroom and protections. It is located in Real del Sol, a development with controlled access, playgrounds, multi-courts, schools, shops and a bicycle path. In the best area of Ojo de Agua, minutes from shopping centers, schools and 45 minutes from CDMX.',

    'xan_d_p_t_v_7'     =>  'Take advantage of beautiful house!',
    'xan_d_p_t_g_7'     =>  'With floor, integral kitchen, closets and more.',
    'xan_d_p_t_n_7'     =>  'Beautiful house at a super price, in excellent condition, ready to move into!',
    'xan_d_p_d_g_7'     =>  'It has a floor, integral kitchen, cancel in the bathroom and protections. It is located in Real del Cid, a development with controlled access, playgrounds, multi-courts, shops and a bicycle path. In the best area of Ojo de Agua, minutes from shopping centers, schools and 45 minutes from CDMX.',

    'xan_d_p_t_v_8'     =>  'Ready to debut!',
    'xan_d_p_t_g_8'     =>  'With integral kitchen, closets and cancel in the bathroom',
    'xan_d_p_t_n_8'     =>  'Beautiful 2-level house, ready to move into!',
    'xan_d_p_d_g_8'     =>  'It has a full kitchen, closets in bedrooms, bathroom doors and protections. It is located in Real Solare, a development with controlled access, playgrounds, dog park, multi-courts, outdoor gym, bike path, schools and shops. Very close to industrial parks in the El Marqués area and 15 minutes from downtown Querétaro.',

    'xan_d_iconos_1'     =>  'of construction',
    'xan_d_iconos_2'     =>  'bedrooms',
    'xan_d_iconos_3'     =>  'bath',
    'xan_d_iconos_4'     =>  'closets',
    'xan_d_iconos_5'     =>  'Kitchen <br> Comprehensive',
    'xan_d_iconos_6'     =>  'Protections',
    'xan_d_iconos_7'     =>  'Cancel',
    'xan_d_iconos_8'     =>  'Kitchenette <br> without stove',
    'xan_d_iconos_9'     =>  'Floor',

    'xan_d_estrena'     =>  'PREMIERE NOW!',

    'xan_d_estrena_d'     =>  '*The prices of the houses can vary, it will depend on whether it is acquired in cash or with some credit, in addition to the notary expenses.',

    'xan_nuestras_prop'  => 'Know our properties',

    'xan_ver_mas'  => 'See more photos',

    'xan_precio'  => 'Price',

    'xan_sol_info'  => 'REQUEST INFO',

    'xan_send'  => 'Send',

    'xan_descripcion'  => 'Description',

    'xan_mapa'  => 'Map',

    'xan_horario'  => 'Business hours',

    'xan_horario_lun'  => 'Monday to Friday from 10:00 a.m. to 8:00 p.m.',

    'xan_horario_dom'  => 'Saturday 10:00 a.m. to 3:00 p.m.',


    //news

    'send_house'        =>  'Do you want to sell your house?',
    'share_direction'   =>  'Share us the address to perform the search.',
    'other'             =>  'Other',
    'send_button'       =>  'Send',
    'buy_house'         =>  'Buy your house',
    'buy_house_message' =>  'With the confidence and speed you are looking for at a better price. We have the best options for',
    'buy_house_message_2' => 'buy your used home.',
    'buy_button'        =>  'Buy',
    'message1'          =>  'Find the best offer to sell your house',
    'message2'          =>  'with us in a way',
    'message3'          =>  'fast, safe and reliable.',
    'send_yout_house'   =>  'Sell your house',
    'message_send1'     =>  'Quick, cash, with or without a mortgage.',
    'message_send2'     =>  'Receive an offer in 10 days.*',
    'vender_button'     =>  'Sell',


];
