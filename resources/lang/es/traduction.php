<?php
return[
    //LOGIN
    'login' => 'Iniciar sesión',
    'email' => 'Correo',
    'password' => 'Contraseña',
    'forgot_pass' => 'He olvidado la contraseña',
    'enter' => 'Entrar',
    'logout' => 'Cerrar Sesión',
    'lang' => 'Español',

    //TABLA PROPIEDADES
    'registration_date' => 'Fecha de registro',
    'name' => 'Nombre',
    'phone' => 'Teléfono',
    'dev_name' => 'Nombre del desarrollo',
    'property_type' => 'Casa o departamento',
    'state' => 'Estado',
    'zip_code' => 'Código Postal',
    'price' => 'Precio',
    'see' => 'Ver Más',
    'download_photos' => 'Descargar Fotos',
    'no_date' => 'Sin fecha de registro',
    'no_images' => 'Sin imágenes',
    'download_excel' => 'Descargar Excel',
    'empty' => 'Sin registros',
    'status' => 'Estatus',

    //INFO DE LA PROPIEDAD
    'info_prop' => 'Información de la propiedad',
    'type_prop' => 'Tipo de inmueble',
    'street' => 'Calle o privada',
    'num_ext' => 'Número exterior',
    'area' => 'Medidas',
    'levels' => 'Niveles',
    'rooms' => 'Número de recámaras',
    'baths' => 'Número de baños',
    'med_baths' => 'Número de medios baños',
    'parkins' => 'Número de estacionamientos',
    'years' => 'Años de antigüedad',
    'price' => 'Precio estimado',
    'mortgage' => 'Hipotecada',
    'mortgage_type' => 'Tipo de hipoteca',
    'reason_sale' => 'Motivo de la venta',
    'phone_number' => 'Teléfono celular',
    'email_table' => 'Correo electrónico',
    'garden' => '¿La propiedad cuenta con terraza o jardín?',
    'equipment' => 'Equipada',
    'equipment_type' => 'Tipo de equipamiento',

    //MOTIVOS
    'mov_1' => 'Cambiar de casa o departamento',
    'mov_2' => 'Busco liquidez',
    'mov_3' => 'Remate bancario',
    'mov_4' => 'Cambiar de ciudad',
    'mov_5' => 'Otro',

    //EQUIPO
    'cooking' => 'Cocina Integral',
    'cancel_bath' => 'Cancel en el baño',


    //SINGLE WORDS
    'house' => 'casa',
    'department' => 'departamento',
    'search' => 'Buscador',
    'floor' => 'Piso',
    'yes' => 'SI',
    'active' => 'Activo',
    'discarded' => 'Descartado',
    'change' => 'Cambiar Estatus',

    //tabla usuarios
    'new_user' => 'Nuevo Usuario',
    'user_type' => 'Perfil',

    //MENU
    'new_property'  =>  'Propiedades (Comprar)',
    'new_property_old'  =>  'Propiedades (Vender Old)',
    'properties'    =>  'Propiedades (Vender)',
    'users'         =>  'Usuarios',
    'neighborhoods' =>  'Colonias',

    'acciones'      =>   'Acciones',

    //colonias
    'new_neighborhood' => 'Nueva Colonia',
    'municipality'     => 'Municipio',
    'neighborhood'     => 'Colonia',
    'cp'               => 'Cp',
    'neighborhood_type'=> 'Tipo',
    'neighborhood_active' => 'Activo',
];
