

@extends('layouts.base')

@section('title')
    Xante.mx
@endsection

@section('css')


    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />

@endsection

@section('contenido')

    <div class="flex flex-col items-center w-full h-screen pb-8 bg-white bg-bottom bg-no-repeat bg-cover md:pb-16 comprar_fondo">
        <div class="relative flex flex-col items-center justify-center w-10/12 top-14 h-2/4">
            <div class="hidden md:block">
                <img src="/assets/images/svg/vente_tucasa_dos.png" alt="" class="mt-10 h-36 xl:mt-16 xl:h-36 2xl:mt-20 2xl:h-48">
            </div>
            <br>
           <h2 class=" mt-96  md:mt-8 2xl:mt-20 2xl:h-48 font-semibold bg-center bg-cover md:text-2xl xl:text-4xl 2xl:text-5xl text-center" style="color: #60338a;"> ¡Muchas gracias por tu interés!</h2>
           <br>
           <h2 class=" 2xl:mt-20 2xl:h-48 font-semibold bg-center bg-cover md:text-xl xl:text-2xl xl:text-3xl text-center" style="color: #60338a;"> Pronto un asesor se pondrá en contacto contigo.</h2>
            <a href="/comprar/1" class="cursor-pointer">
                <div class="mt-60 md:mt-8">
                        <div
                        class="relative flex justify-center w-full px-8 py-4 font-semibold bg-center bg-cover rounded-full e md:rounded-xl md:text-2xl xl:text-4xl 2xl:text-5xl " style="color: #60338a; background-color: #b8d057">
                        Regresar
                        <img src="/assets/images/svg/click_icon.png" alt="" class="absolute w-8 md:w-8 2xl:w-10 btn_click">
                    </div>
                </div>
            </a>
        </div>
    </div>



    @include('footer')

@endsection

@section('js-usable')

    <script>

        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-227859785-1');

    </script>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

    <script type="text/javascript">

        $('.slider_propiedades').slick({

            slidesToShow: 1,
            arrows:false,
            dots:false,
            centerMode: true,
            centerPadding: '10px',

        });

        $(document).ready(function() {
            $('.acc-container .acc:nth-child(1) .acc-head').addClass('active');
            $('.acc-container .acc:nth-child(1) .acc-content').slideDown();
            $('.acc-head').on('click', function() {
                if ($(this).hasClass('active')) {
                    $(this).siblings('.acc-content').slideUp();
                    $(this).removeClass('active');
                } else {
                    $('.acc-content').slideUp();
                    $('.acc-head').removeClass('active');
                    $(this).siblings('.acc-content').slideToggle();
                    $(this).toggleClass('active');
                }
            });
        });

        function abrir_menu() {
            $('.menu').toggleClass('opened');
            $('#cont_menu').toggleClass(' menu_visible');
        }

    </script>
@endsection
