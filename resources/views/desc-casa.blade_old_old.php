@php

    switch ($id_prop) {

        case "real-toledo-pachuca-52":

            $texto_titulo               =       'Casa equipada';

            $subtitulo                  =       'de 2 niveles';

            $ubicacion_propiedad        =       'Real Toledo - Pachuca';

            $precio                     =       '990000';

            $equipamento                = array(

                'metros'             =>     '69 m<sup>2</sup>',

                'cuartos'            =>     '2',

                'banios'             =>     '1 <small>1/2</small>',

                'estacionamiento'    =>     '1',
            );

            $descripcion                =       ' Preciosa casa ¡como nueva!  <br><br> Cuenta con cocina integral y clósets en excelente estado. Está ubicada en Real Toledo, un desarrollo con acceso controlado, parques infantiles, multicanchas, ciclopista y comercio. A unos pasos de Explanada Pachuca, cerca de escuelas, universidades, hospitales y el Tuzobus.';

            $mapa                       =       'https://goo.gl/maps/NXM1ndVXF4UT2FMR8';

            $embed                       =       '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3748.4465004461135!2d-98.7860391!3d20.03173225!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1a170ccd620b1%3A0xcef29b9790600b18!2sReal%20de%20Toledo%2C%2042119%20Pachuca%20de%20Soto%2C%20Hgo.!5e0!3m2!1ses-419!2smx!4v1666625180063!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';

            $ubicacion                  =       'Calle Paseo Toledo, <br/> Fraccionamiento Real Toledo,<br/>  Pachuca de soto, <br/>   Hidalgo, México, Cp. 42119.';

            $img                        =       'real_toledo_pachuca_52';

            $cantidad_img               =       8;

        break;


        case "real-del-sol-inv":

            $texto_titulo               =       '¡Excelente ubicación';

            $subtitulo                  =       'a un súper precio! ¡Estrena ya!';

            $ubicacion_propiedad        =       'Real del Sol - Tecámac';

            $precio                     =       '1595000';

            $equipamento                = array(

                'metros'             =>     '95 m<sup>2</sup>',

                'cuartos'            =>     '3',

                'banios'             =>     '1 <small>1/2</small>',

                'estacionamiento'    =>     '2',
            );

            $descripcion                =       ' Preciosa casa a un súper precio, en excelentes condiciones ¡lista para estrenarse!   <br><br> Cuenta con piso, cancel en el baño y protecciones. Está ubicada en Real del Sol, un desarrollo con acceso controlado, parques infantiles, multicanchas, escuelas, comercio y ciclopista. En la mejor zona de Ojo de Agua, a minutos de centros comerciales, escuelas y a 45 min de CDMX.';

            $mapa                       =       'https://goo.gl/maps/4Qk8m4gyFwYk1GJg7';

            $embed                       =       '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3757.119816755542!2d-99.0193594!3d19.664893!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1f3f778865d77%3A0x4382813865ca4cb9!2sREAL%20DEL%20SOL!5e0!3m2!1ses-419!2smx!4v1665009857223!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';

            $ubicacion                  =       'Ojo de Agua, <br> Cp. 55767 Ojo de Agua, <br>  Méx.';

            $img                        =       'real_del_sol_tecamac_inv_vinte';

            $cantidad_img               =       4;

        break;

        case "real-del-cid-96":

            $texto_titulo               =       '¡Aprovecha preciosa casa! ';

            $subtitulo                  =       'Con piso, cocina integral, clósets y más.';

            $ubicacion_propiedad        =       'Real del Cid - Tecámac';

            $precio                     =       '1200000';

            $equipamento                = array(

                'metros'             =>     '77 m<sup>2</sup>',

                'cuartos'            =>     '3',

                'banios'             =>     '1 <small>1/2</small>',

                'estacionamiento'    =>     '2',
            );

            $descripcion                =       ' Preciosa casa a un súper precio, en excelentes condiciones ¡lista para estrenarse!    <br><br> Cuenta con piso, cocina integral, cancel en el baño y protecciones. Está ubicada en Real del Cid, un desarrollo con acceso controlado, parques infantiles, multicanchas, comercio y ciclopista. En la mejor zona de Ojo de Agua, a minutos de centros comerciales, escuelas y a 45 min de CDMX.';

            $mapa                       =       'https://goo.gl/maps/1q8e6tjFQepy7HN57';

            $embed                       =       '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3757.5559076929057!2d-99.0230701!3d19.646274899999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1f3fb7d5ebe27%3A0xed931f1ac430834!2sReal%20del%20cid!5e0!3m2!1ses-419!2smx!4v1666626103753!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';

            // <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1127.9354028322734!2d-99.0232615610812!3d19.646172367672047!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1f3fb7d5ebe27%3A0xed931f1ac430834!2sReal%20del%20cid!5e0!3m2!1ses-419!2smx!4v1665785704803!5m2!1ses-419!2smx" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>

            $ubicacion                  =       'Calle Albarracín <br> Conjunto Urbano  Real del Cid,<br> Cp. 55767, Tecámac, <br> Estado de México';

            $img                        =       'real_del_cid_tecamac_96';

            $cantidad_img               =       4;

        break;

        case "real-navarra-pachuca-12":

            $texto_titulo               =       'Casa equipada';

            $subtitulo                  =       'de 2 niveles ';

            $ubicacion_propiedad        =       'Real Navarra - Pachuca';

            $precio                     =       '1650000';

            $equipamento                = array(

                'metros'             =>     '134 m<sup>2</sup>',

                'cuartos'            =>     '3',

                'banios'             =>     '2 <small>1/2</small>',

                'estacionamiento'    =>     2,
            );

            $descripcion                =       ' Cuenta con cocina integral y clósets en recámaras  <br><br> Está ubicada en Real Navarra, un desarrollo con acceso controlado, parques infantiles, dog park, multicanchas, gym al aire libre, ciclopista y comercio.  Muy cerca del Tuzobus, asi como de escuelas y avenidas principales.';

            $embed                       =       '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1874.5420645182717!2d-98.8026479!3d20.0049835!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1a187dbea2dbf%3A0x1f4009b1b0963009!2sReal%20Navarra!5e0!3m2!1ses-419!2smx!4v1666626419784!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';

            $ubicacion                  =       'Carretera Pachuca-Téllez <br> Real Navarra <br> Zempoala, Hidalgo. <br> A 3 minutos del Tuzobús.';

            $img                        =       'xa0012';

            $cantidad_img               =       11;

        break;

        case "real-segovia-puebla-15":

            $texto_titulo               =       'Departamento';

            $subtitulo                  =       'en Planta Baja ';

            $ubicacion_propiedad        =       'Real Segovia - Puebla';

            $precio                     =       '607000';

            $equipamento                = array(

                'metros'             =>     '54 m<sup>2</sup>',

                'cuartos'            =>     '3',

                'banios'             =>     '1',

                'estacionamiento'    =>     1,
            );

            $descripcion                =       ' Cuenta con 2 recámaras y una alcoba, además de cocina integral y protecciones.   <br><br> Está ubicada en Real Segovia, un desarrollo con acceso controlado, parques infantiles, parque canino, multicanchas, gimnasio al aire libre y comercio. A solo 15 min se encuentra Cholula, además de estar rodeado de Parques Industriales.';

            $mapa                       =       'https://g.page/RealSegoviaPuebla?share';

            $embed                       =       '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3768.315476302286!2d-98.38452798509724!3d19.1814184870275!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cfcde7f5db286f%3A0x235339753d402237!2sReal%20Segovia!5e0!3m2!1ses-419!2smx!4v1666626675317!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';

            $ubicacion                  =       'Calle Paseo Segovia, <br> Fracc. Real Segovia, <br> C.P. 74160. Huejotzingo, Puebla';

            $img                        =       'xa0015';

            $cantidad_img               =       9;

        break;

        case "real-verona-tecamac-13":

            $texto_titulo               =       'Casa';

            $subtitulo                  =       'de 2 niveles ';

            $ubicacion_propiedad        =       'Real Verona - Tecámac';

            $precio                     =       '1115000';

            $equipamento                = array(

                'metros'             =>     '77 m<sup>2</sup>',

                'cuartos'            =>     '3',

                'banios'             =>     '1 <small>1/2</small>',

                'estacionamiento'    =>     '1',
            );

            $descripcion                =       ' Cuenta con 3 recámaras y 2 lugares de estacionamiento. <br><br> Está ubicada en Real Verona, un desarrollo con acceso controlado, parques infantiles, parque canino, multicanchas, gimnasio al aire libre, comercio y escuelas desde jardin de niños hasta secundaria. Muy cerca de avenidas principales y plazas comerciales.';

            $mapa                       =       'https://goo.gl/maps/74gT7gSWoAeBDCbW9';

            $embed                       =       '<iframe class="w-full h-full rounded-xl" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8936.766638552726!2d-99.02702702536774!3d19.650904101937094!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1f2135a3e68f7%3A0x2e64832cbc973d2c!2sFraccionamiento%20Real%20Verona%2C%20Ojo%20de%20Agua%2C%20M%C3%A9x.!5e0!3m2!1ses-419!2smx!4v1666627006575!5m2!1ses-419!2smx"  style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';

            $ubicacion                  =       'Av. Paseo castilla, <br> C.P 55767 Ojo de agua  <br> Edo. México';

            $img                        =       'real-verona-tecamac-13';

            $cantidad_img               =       11;

        break;

        case "real-bilbao-playa-del-carmen-17":

            $texto_titulo               =       'Departamento';

            $subtitulo                  =       'en 2do piso';

            $ubicacion_propiedad        =       'Real Bilbao - Playa del Carmen';

            $precio                     =       '1202000';

            $equipamento                = array(

                'metros'             =>     '62 m<sup>2</sup>',

                'cuartos'            =>     '3',

                'banios'             =>     '1',

                'estacionamiento'    =>     '1',
            );

            $descripcion                =       ' Departamento que cuenta con 3 recámaras y cocina integral.  <br><br> Está ubicada en Real Bilbao, un desarrollo con acceso controlado donde encontrarás parques infantiles, parque canino, casa club con alberca, multicanchas, gimnasio al aire libre y comercio.';

            $mapa                       =       'https://goo.gl/maps/2XG2ConsGqGcJFabA';

            $embed                       =       '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d29863.403219486885!2d-87.1222638!3d20.6726137!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f4e4365a8f0a989%3A0x6f97a25427373ba8!2sFRACC.%20REAL%20BILBAO!5e0!3m2!1ses-419!2smx!4v1668722133215!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';
            $ubicacion                  =       'Av Lilis , <br> lote 001, Manzana 151, C.P 77712 <br>Playa del Carmen, Quintana Roo.
';

            $img                        =       'xa0018';

            $cantidad_img               =       8;

        break;

        case "real-ibiza-playa-del-carmen-18":

            $texto_titulo               =       'Departamento ';

            $subtitulo                  =       ' en 2do piso';

            $ubicacion_propiedad        =       'Real Ibiza Plus - Playa del Carmen';

            $precio                     =       '1020000';

            $equipamento                = array(

                'metros'             =>     '52 m<sup>2</sup>',

                'cuartos'            =>     '2',

                'banios'             =>     '1',

                'estacionamiento'    =>     '1',
            );

            $descripcion                =       ' Departamento que cuenta con 3 recámaras, cocina integral y clósets.  <br><br> Está ubicada en Real Ibiza Plus, un desarrollo con acceso controlado, alberca, parques, area de usos multiples, parque canino, multicanchas, gimnasio al aire libre. A 4km de la playa  y cercanía con hospitales, escuelas y comercio.';

            $mapa                       =       'https://goo.gl/maps/SsLLuYrA4cLg5bkv7';

            $embed                       =       '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3733.286233215674!2d-87.0958857!3d20.6579308!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f4e42f0e2d09881%3A0x3115d8d185c8b4cd!2sReal%20Ibiza%20Plus%20Apartment!5e0!3m2!1ses-419!2smx!4v1665011872472!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';

            $ubicacion                  =       'Av Flor de Ciruelo, <br/> C.P 77730 <br/> Playa del Carmen, Q.R.';

            $img                        =       'xa0017';

            $cantidad_img               =       6;

        break;
        // case "real-segovia-puebla-1":

        //     $nombre_propiedad = 'Real Segovia - Puebla';
        //     $precio_prop = '599,000';
        //     $texto_titulo_verde = __('landing.xan_d_p_t_v_1');
        //     $texto_titulo_gris = __('landing.xan_d_p_t_g_1');
        //     $metros = '54';
        //     $camas = '2';
        //     $banios = '1';
        //     $equipamento = array(
        //         'closets' => 1,
        //         'cocina_integral' => 1,
        //         'protecciones' => 1,
        //         'cancel' => 1,
        //         'cocineta_sin_estufa' => 0,
        //         'piso' => 1
        //     );
        //     $titulo_prop = __('landing.xan_d_p_t_n_1');
        //     $desc_porp = __('landing.xan_d_p_d_g_1');
        //     $mapa_frame = "<iframe  class='h-40 mx-auto shadow-xl w-60 2xl:h-52 2xl:w-96 rounded-xl' src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3768.3154760054817!2d-98.38233930000001!3d19.1814185!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cfcde7f5db286f%3A0x235339753d402237!2sReal%20Segovia!5e0!3m2!1ses-419!2smx!4v1656517741931!5m2!1ses-419!2smx'  style='border:0;' allowfullscreen='' loading='lazy' referrerpolicy='no-referrer-when-downgrade'></iframe>" ;
        //     $mapa = 'https://goo.gl/maps/MGAYoAAT8nzFfcMi6';
        //     $img = 'real_segovia_puebla_1';
        //     $vendida = 1;

        // break;

        case "real-solare-queretaro-2":

            $texto_titulo               =       'Casa equipada de 2 niveles';

            $subtitulo                  =       'de 2 niveles';

            $ubicacion_propiedad        =       'Real Solare - Querétaro';

            $precio                     =       '1021000';

            $equipamento                = array(

                'metros'             =>     '78 m<sup>2</sup>',

                'cuartos'            =>     '2',

                'banios'             =>     '2',

                'estacionamiento'    =>     '2',
            );

            $descripcion                =       ' Cuenta con cocina integral, clósets en recámaras, cancel de baño y protecciones. <br><br>
Está ubicada en Real Solare, un desarrollo con acceso controlado, parques infantiles, dog park, multicanchas, gym al aire libre, ciclopista, escuelas y comercio.
Muy cerca de parques industriales de la zona El Marqués y a 15 min del centro de Querétaro.';

            $mapa                       =       'https://goo.gl/maps/voh55dedGjsagY8t8';

            $embed                       =       '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3734.936271138694!2d-100.28735404999999!3d20.59066!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d342cc112abe65%3A0xcb0cd5063e7b5675!2sReal%20Solare%2C%2076246%20Qro.!5e0!3m2!1ses-419!2smx!4v1666216763278!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';

            $ubicacion                  =       'Fracc. Real Solare Cp.76246 <br> El Marqués, Santiago de Querétaro. ';

            $img                        =       'real_solare_queretaro_2';

            $cantidad_img               =       6;


            // $nombre_propiedad = 'Real Solare - Querétaro';
            // $precio_prop = '769,000';
            // $texto_titulo_verde = __('landing.xan_d_p_t_v_2');
            // $texto_titulo_gris = __('landing.xan_d_p_t_g_2');
            // $metros = '50';
            // $camas = '2';
            // $banios = '1';
            // $equipamento = array(
            //     'closets' => 1,
            //     'cocina_integral' => 1,
            //     'protecciones' => 1,
            //     'cancel' => 0,
            //     'cocineta_sin_estufa' => 0,
            //     'piso' => 1
            // );
            // $titulo_prop = __('landing.xan_d_p_t_n_2');
            // $desc_porp = __('landing.xan_d_p_d_g_2');
            // $mapa_frame = "<iframe  class='h-40 mx-auto shadow-xl w-60 2xl:h-52 2xl:w-96 rounded-xl' src='https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3734.951283314052!2d-100.2877922!3d20.590047!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d342cbe7bfcc8b%3A0xe6779fe0ca58ef2b!2sPriv.%20Despina%2C%20Real%20Solare%2C%2076246%20Qro.!5e0!3m2!1ses-419!2smx!4v1656524848390!5m2!1ses-419!2smx'  style='border:0;' allowfullscreen='' loading='lazy' referrerpolicy='no-referrer-when-downgrade'></iframe>" ;
            // $mapa = '';
            // $img = 'real_solare_queretaro_2';
            // $vendida = 1;

        break;


        // case "real-segovia-puebla-4":

        //     $nombre_propiedad = 'Real Segovia - Puebla';
        //     $precio_prop = '991,000';
        //     $texto_titulo_verde = __('landing.xan_d_p_t_v_4');
        //     $texto_titulo_gris = __('landing.xan_d_p_t_g_4');
        //     $metros = '92';
        //     $camas = '3';
        //     $banios = '1 1/2';
        //     $equipamento = array(
        //         'closets' => 0,
        //         'cocina_integral' => 0,
        //         'protecciones' => 0,
        //         'cancel' => 0,
        //         'cocineta_sin_estufa' => 1,
        //         'piso' => 1
        //     );
        //     $titulo_prop = __('landing.xan_d_p_t_n_4');
        //     $desc_porp = __('landing.xan_d_p_d_g_4');
        //     $mapa_frame = "<iframe  class='h-40 mx-auto shadow-xl w-60 2xl:h-52 2xl:w-96 rounded-xl' src='https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d942.0802217410555!2d-98.3861201!3d19.181182!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cfcde7f5db286f%3A0x235339753d402237!2sReal%20Segovia!5e0!3m2!1ses-419!2smx!4v1656528246046!5m2!1ses-419!2smx'  style='border:0;' allowfullscreen='' loading='lazy' referrerpolicy='no-referrer-when-downgrade'></iframe>" ;
        //     $mapa = '';
        //     $img = 'real_segovia_puebla_4';
        //     $vendida = 1;

        // break;

        // case "real-granada-tecamac-5":

        //     $nombre_propiedad = 'Real Granada - Tecámac';
        //     $precio_prop = '689,000';
        //     $texto_titulo_verde = __('landing.xan_d_p_t_v_5');
        //     $texto_titulo_gris = __('landing.xan_d_p_t_g_5');
        //     $metros = '45';
        //     $camas = '2';
        //     $banios = '1';
        //     $equipamento = array(
        //         'closets' => 1,
        //         'cocina_integral' => 1,
        //         'protecciones' => 1,
        //         'cancel' => 1,
        //         'cocineta_sin_estufa' => 0,
        //         'piso' => 1
        //     );
        //     $titulo_prop = __('landing.xan_d_p_t_n_5');
        //     $desc_porp = __('landing.xan_d_p_d_g_5');
        //     $mapa_frame = "<iframe  class='h-40 mx-auto shadow-xl w-60 2xl:h-52 2xl:w-96 rounded-xl' src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3755.328499036349!2d-98.9593531!3d19.7411931!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d19324e4d9db3d%3A0xefcc899992bf5c8b!2sPriv.%20Belerda%2C%2055743%20Santa%20Mar%C3%ADa%20Ajoloapan%2C%20M%C3%A9x.!5e0!3m2!1ses-419!2smx!4v1656528519150!5m2!1ses-419!2smx'  style='border:0;' allowfullscreen='' loading='lazy' referrerpolicy='no-referrer-when-downgrade'></iframe>";
        //     $mapa = '';
        //     $img = 'real_ granada_tecamac_5';
        //     $vendida = 1;

        // break;

        // case "real-del-sol-tecamac-6":

        //     $nombre_propiedad = 'Real del Sol - Tecámac';
        //     $precio_prop = '1,595,000';
        //     $texto_titulo_verde = __('landing.xan_d_p_t_v_6');
        //     $texto_titulo_gris = __('landing.xan_d_p_t_g_6');
        //     $metros = '95';
        //     $camas = '3';
        //     $banios = '1 1/2';
        //     $equipamento = array(
        //         'closets' => 0,
        //         'cocina_integral' => 0,
        //         'protecciones' => 1,
        //         'cancel' => 1,
        //         'cocineta_sin_estufa' => 0,
        //         'piso' => 1
        //     );
        //     $titulo_prop = __('landing.xan_d_p_t_n_6');
        //     $desc_porp = __('landing.xan_d_p_d_g_6');
        //     $mapa_frame = "<iframe  class='h-40 mx-auto shadow-xl w-60 2xl:h-52 2xl:w-96 rounded-xl' src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3757.119816755542!2d-99.0193594!3d19.664893!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1f3f778865d77%3A0x4382813865ca4cb9!2sREAL%20DEL%20SOL!5e0!3m2!1ses-419!2smx!4v1656536349794!5m2!1ses-419!2smx'  style='border:0;' allowfullscreen='' loading='lazy' referrerpolicy='no-referrer-when-downgrade'></iframe>" ;
        //     $mapa = '';
        //     $img = 'real_del_sol_tecamac_6';
        //     $vendida = 0;

        // break;

        // case "real-del-cid-tecamac-7":

        //     $nombre_propiedad = 'Real del Cid - Tecámac';
        //     $precio_prop = '1,200,000';
        //     $texto_titulo_verde = __('landing.xan_d_p_t_v_7');
        //     $texto_titulo_gris = __('landing.xan_d_p_t_g_7');
        //     $metros = '77';
        //     $camas = '3';
        //     $banios = '1 1/2';
        //     $equipamento = array(
        //         'closets' => 1,
        //         'cocina_integral' => 1,
        //         'protecciones' => 1,
        //         'cancel' => 1,
        //         'cocineta_sin_estufa' => 0,
        //         'piso' => 1
        //     );
        //     $titulo_prop = __('landing.xan_d_p_t_n_7');
        //     $desc_porp = __('landing.xan_d_p_d_g_7');
        //     $mapa_frame = "<iframe  class='h-40 mx-auto shadow-xl w-60 2xl:h-52 2xl:w-96 rounded-xl' src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3757.5559076929057!2d-99.0230701!3d19.646274899999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1f3fb7d5ebe27%3A0xed931f1ac430834!2sReal%20del%20cid!5e0!3m2!1ses-419!2smx!4v1656536594203!5m2!1ses-419!2smx'  style='border:0;' allowfullscreen='' loading='lazy' referrerpolicy='no-referrer-when-downgrade'></iframe>" ;
        //     $mapa = '';
        //     $img = 'real_del_cid_tecamac_7';
        //     $vendida = 0;

        // break;

        case "real-solare-queretaro-8":

            $texto_titulo               =       'Casa equipada';

            $subtitulo                  =       'de 2 niveles';

            $ubicacion_propiedad        =       'Real Solare - Querétaro';

            $precio                     =       '1021000';

            $equipamento                = array(

                'metros'             =>     '78 m<sup>2</sup>',

                'cuartos'            =>     '2',

                'banios'             =>     '2 <small>1/2</small>',

                'estacionamiento'    =>     '2',
            );

            $descripcion                =       ' Preciosa casa de 2 niveles, ¡lista para estrenarse!   <br><br> Cuenta con cocina integral, clósets en recámaras, cancel de baño y protecciones. Está ubicada en Real Solare, un desarrollo con acceso controlado, parques infantiles, dog park, multicanchas, gym al aire libre, ciclopista, escuelas y comercio. Muy cerca de parques industriales de la zona El Marqués y a 15 min del centro de Querétaro.';

            $mapa                       =       'https://g.page/RealSolareQueretaro?share';

            $embed                       =       '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14939.829171849196!2d-100.284806!3d20.5898016!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4d7c41f6f1854fda!2sFraccionamiento%20Real%20Solare!5e0!3m2!1ses-419!2smx!4v1666217027567!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';

            $ubicacion                  =       'Av. Primavera <br> Fracc. Real Solare <br> El Marqués, Santiago de Querétaro.';

            $img                        =       'real_solare_queretaro_8';

            $cantidad_img               =       6;

            // $nombre_propiedad = 'Real Solare - Querétaro';
            // $precio_prop = '1,015,000';
            // $texto_titulo_verde = __('landing.xan_d_p_t_v_8');
            // $texto_titulo_gris = __('landing.xan_d_p_t_g_8');
            // $metros = '83';
            // $camas = '2';
            // $banios = '2 1/2';
            // $equipamento = array(
            //     'closets' => 1,
            //     'cocina_integral' => 1,
            //     'protecciones' => 1,
            //     'cancel' => 1,
            //     'cocineta_sin_estufa' => 0,
            //     'piso' => 1
            // );
            // $titulo_prop = __('landing.xan_d_p_t_n_8');
            // $desc_porp = __('landing.xan_d_p_d_g_8');
            // $mapa_frame = "<iframe class='h-40 mx-auto shadow-xl w-60 2xl:h-52 2xl:w-96 rounded-xl' src='https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d39889.15589822933!2d-100.34449270179942!3d20.57221752773438!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d342cc112abe65%3A0xcb0cd5063e7b5675!2sReal%20Solare%2C%2076246%20Qro.!5e0!3m2!1ses-419!2smx!4v1656536786829!5m2!1ses-419!2smx'  style='border:0;' allowfullscreen='' loading='lazy' referrerpolicy='no-referrer-when-downgrade'></iframe>" ;
            // $mapa = '';
            // $img = 'real_solare_queretaro_8';
            // $vendida = 0;
            break;

            case "real-segovia-puebla-14":

            $texto_titulo               =       'Departamento '; //frace a mostrar

            $subtitulo                  =       'en Planta Baja';

            $ubicacion_propiedad        =       'Real Segovia - Puebla';

            $precio                     =       '570000';

            $equipamento                = array(

                'metros'             =>     '54 m<sup>2</sup>',

                'cuartos'            =>     '2',

                'banios'             =>     '1',

                'estacionamiento'    =>     '1',
            );

            $descripcion                =       'Cuenta con 2 recámaras y ampliación en parte trasera.  <br><br>Está ubicada en Real Segovia, un desarrollo con acceso controlado, parques infantiles, parque canino, multicanchas, gimnasio al aire libre y comercio.
A solo 15 min se encuentra Cholula, ademas de estar rodeado de Parques Industriales.';

            $mapa                       =       'https://g.page/RealSegoviaPuebla?share';

            $embed                       =       '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3768.316789581044!2d-98.3846075850972!3d19.18136108702761!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cfcde7f5db286f%3A0x235339753d402237!2sReal%20Segovia!5e0!3m2!1ses-419!2smx!4v1667418746464!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';



            $ubicacion                  =       'Fracc. Real Segovia <br> Huejotzingo, Puebla.';

            $img                        =       'real-segovia-puebla-14';

            $cantidad_img               =       7;

        break;

        case "real-solare-queretaro-21":

            $texto_titulo               =       'Casa '; //frace a mostrar

            $subtitulo                  =       'de 2 niveles';

            $ubicacion_propiedad        =       'Real Solare - Querétaro';

            $precio                     =       '1320000';

            $equipamento                = array(

                'metros'             =>     '104 m<sup>2</sup>',

                'cuartos'            =>     '3',

                'banios'             =>     '2 <small>1/2</small>',

                'estacionamiento'    =>     '2',
            );

            $descripcion                =       'Cuenta con cocina integral, clósets en las 3 recámaras, cancel en baños y protecciones. <br><br>
Está ubicada en Real Solare, un desarrollo con acceso controlado, parques infantiles, dog park, multicanchas, gym al aire libre, ciclopista, escuelas y comercio.
Muy cerca de parques industriales de la zona El Marqués y a 15 min del centro de Querétaro.';

            $mapa                       =       'https://goo.gl/maps/DftWa64fS2engpLT6';

            $embed                       =       '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d21129.059027169744!2d-100.28829726437188!3d20.582942888942448!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d342cc112abe65%3A0xcb0cd5063e7b5675!2sReal%20Solare%2C%2076246%20Qro.!5e0!3m2!1ses-419!2smx!4v1667425747353!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';



            $ubicacion                  =       'Fracc. Real Solare <br> El Marqués, Querétaro.';

            $img                        =       'real-solare-queretaro-21';

            $cantidad_img               =       9;

        break;

        case "real-solare-queretaro-21":

        $texto_titulo               =       'Casa '; //frace a mostrar

        $subtitulo                  =       'de 2 niveles';

        $ubicacion_propiedad        =       'Real Solare - Querétaro';

        $precio                     =       '1320000';

        $equipamento                = array(

            'metros'             =>     '104 m<sup>2</sup>',

            'cuartos'            =>     '3',

            'banios'             =>     '2 <small>1/2</small>',

            'estacionamiento'    =>     '2',
        );

        $descripcion                =       'Cuenta con cocina integral, clósets en las 3 recámaras, cancel en baños y protecciones. <br><br>
        Está ubicada en Real Solare, un desarrollo con acceso controlado, parques infantiles, dog park, multicanchas, gym al aire libre, ciclopista, escuelas y comercio.
        Muy cerca de parques industriales de la zona El Marqués y a 15 min del centro de Querétaro.';

        $mapa                       =       'https://goo.gl/maps/DftWa64fS2engpLT6';

        $embed                       =       '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d21129.059027169744!2d-100.28829726437188!3d20.582942888942448!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d342cc112abe65%3A0xcb0cd5063e7b5675!2sReal%20Solare%2C%2076246%20Qro.!5e0!3m2!1ses-419!2smx!4v1667425747353!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';



        $ubicacion                  =       'Fracc. Real Solare <br> El Marqués, Querétaro.';

        $img                        =       'real-solare-queretaro-21';

        $cantidad_img               =       9;

        break;

        case "real-verona-edo-mex-22":

        $texto_titulo               =       'Departamento '; //frace a mostrar

        $subtitulo                  =       'equipado en 2do nivel';

        $ubicacion_propiedad        =       'Real Verona - Edo. Méx';

        $precio                     =       '650000';

        $equipamento                = array(

            'metros'             =>     '46 m<sup>2</sup>',

            'cuartos'            =>     '2',

            'banios'             =>     '1',

            'estacionamiento'    =>     '1',
        );

        $descripcion                =       'Depto que cuenta con 2 recámaras y 1 lugares de estacionamiento. <br><br>
Está ubicada en Real Verona, un desarrollo con acceso controlado, parques infantiles, parque canino, multicanchas, gimnasio al aire libre, comercio y escuelas desde jardin de niños hasta secundaria.
Muy cerca de avenidas principales y plazas comerciales.';

        $mapa                       =       'https://goo.gl/maps/1biX4ZJqqGt7VjFv5';

        $embed                       =       '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10627.604260174094!2d-99.02649863352019!3d19.651843335092167!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1f2135a3e68f7%3A0x2e64832cbc973d2c!2sFraccionamiento%20Real%20Verona%2C%20Ojo%20de%20Agua%2C%20M%C3%A9x.!5e0!3m2!1ses-419!2smx!4v1667426392498!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';


        $ubicacion                  =       'Fracc. Real Verona <br> Tecámac, Edo. Méx.';

        $img                        =       'real-verona-edo-mex-22';

        $cantidad_img               =       5;

        break;

        case "real-castilla-hidalgo-23":

        $texto_titulo               =       'Casa '; //frace a mostrar

        $subtitulo                  =       'de 2 niveles';

        $ubicacion_propiedad        =       'Real Castilla - Hidalgo';

        $precio                     =       '1300000';

        $equipamento                = array(

            'metros'             =>     '156 m<sup>2</sup>',

            'cuartos'            =>     '3',

            'banios'             =>     '1 <small>1/2</small>',

            'estacionamiento'    =>     '2',
        );

        $descripcion                =       'Casa que cuenta con 3 recámaras y 2 lugares de estacionamiento. <br><br>
Está ubicada en Real Castilla, un desarrollo con acceso controlado, parques infantiles, parque canino, multicanchas, gimnasio al aire libre, comercio. A 15min del Pueblo Mágico Tepotzotlán.';

        $mapa                       =       'https://g.page/RealCastillaTula?share';

        $embed                       =       '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15003.303089558738!2d-99.2364512!3d19.9317455!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d228f64fb7bfed%3A0x9d85323db9e53de!2sOficinas%20Real%20Castilla!5e0!3m2!1ses-419!2smx!4v1667426843095!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';

        $ubicacion                  =       'Fracc. Real Castilla <br> Tula, Hidalgo.';

        $img                        =       'real-castilla-hidalgo-23';

        $cantidad_img               =       8;

        break;

        case "real-castilla-hidalgo-23":

        $texto_titulo               =       'Casa '; //frace a mostrar

        $subtitulo                  =       'de 2 niveles';

        $ubicacion_propiedad        =       'Real Castilla - Hidalgo';

        $precio                     =       '1300000';

        $equipamento                = array(

            'metros'             =>     '156 m<sup>2</sup>',

            'cuartos'            =>     '3',

            'banios'             =>     '1 <small>1/2</small>',

            'estacionamiento'    =>     '2',
        );

        $descripcion                =       'Casa que cuenta con 3 recámaras y 2 lugares de estacionamiento. <br><br>
        Está ubicada en Real Castilla, un desarrollo con acceso controlado, parques infantiles, parque canino, multicanchas, gimnasio al aire libre, comercio. A 15min del Pueblo Mágico Tepotzotlán.';

        $mapa                       =       'https://g.page/RealCastillaTula?share';

        $embed                       =       '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15003.303089558738!2d-99.2364512!3d19.9317455!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d228f64fb7bfed%3A0x9d85323db9e53de!2sOficinas%20Real%20Castilla!5e0!3m2!1ses-419!2smx!4v1667426843095!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';

        $ubicacion                  =       'Quinea <br> Fracc. Real Castilla <br> Tula, Hidalgo.';

        $img                        =       'real-castilla-hidalgo-23';

        $cantidad_img               =       8;

        break;

        case "real-valencia-quintana-roo-24":

        $texto_titulo               =       'Departamento'; //frace a mostrar

        $subtitulo                  =       'equipado en 2do nivel';

        $ubicacion_propiedad        =       'Real Valencia - Quintana Roo';

        $precio                     =       '605000';

        $equipamento                = array(

            'metros'             =>     '49 m<sup>2</sup>',

            'cuartos'            =>     '1',

            'banios'             =>     '1',

            'estacionamiento'    =>     '1',
        );

        $descripcion                =       'Departamento en 3er piso que cuenta con 1 recámara, estancia y comedor.  <br><br> Está ubicado en Real Valencia, un desarrollo con acceso controlado, casa club, dog park, multicancha y más. A 25 min de la zona hotelera y la playa.';

        $mapa                       =       'https://g.page/RealValenciaCancun?share';

        $embed                       =       '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7442.185609125889!2d-86.9337164!3d21.1487047!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f4dd5c725dc8839%3A0xb9b3e711acc70bed!2sReal%20Valencia!5e0!3m2!1ses-419!2smx!4v1667427525449!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';


        $ubicacion                  =       'Fracc. Real Valencia <br> Cancún, Quintana Roo.';

        $img                        =       'real-valencia-quintana-roo-24';

        $cantidad_img               =       8;

        break;


        case "real-solare-queretaro-25":

        $texto_titulo               =       'Casa '; //frace a mostrar

        $subtitulo                  =       'Dúplex Planta Alta';

        $ubicacion_propiedad        =       'Real Solare - Querétaro';

        $precio                     =       '815000';

        $equipamento                = array(

            'metros'             =>     '45 m<sup>2</sup>',

            'cuartos'            =>     '2',

            'banios'             =>     '1',

            'estacionamiento'    =>     '1',
        );

        $descripcion                =       'Casa en planta alta que cuenta con cocina integral, 2 recámaras y área de lavado. <br><br>
Está ubicada en Real Solare, un desarrollo con acceso controlado, parques infantiles, dog park, multicanchas, gym al aire libre, ciclopista, escuelas y comercio.
Muy cerca de parques industriales de la zona El Marqués y a 15 min del centro de Querétaro.';

        $mapa                       =       'https://goo.gl/maps/D64ELL59FWjp4bh37';

        $embed                       =       '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d59760.59182421701!2d-100.2899472!3d20.586547!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d342cc112abe65%3A0xcb0cd5063e7b5675!2sReal%20Solare%2C%2076246%20Qro.!5e0!3m2!1ses-419!2smx!4v1667428580438!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';


        $ubicacion                  =       'Fracc. Real Solare <br> El Marqués, Querétaro.';

        $img                        =       'real-solare-queretaro-25';

        $cantidad_img               =       6;

        break;



        case "real-verona-edo-mex-26":

        $texto_titulo               =       'Casa '; //frace a mostrar

        $subtitulo                  =       'de 2 niveles';

        $ubicacion_propiedad        =       'Real Verona - Edo. Méx';

        $precio                     =       '1250000';

        $equipamento                = array(

            'metros'             =>     '51 m<sup>2</sup>',

            'cuartos'            =>     '3',

            'banios'             =>     '1 <small>1/2</small>',

            'estacionamiento'    =>     '2',
        );

        $descripcion                =       'Casa que cuenta con 3 recámaras y 2 lugares de estacionamiento. <br><br>
Está ubicada en Real Verona, un desarrollo con acceso controlado, parques infantiles, parque canino, multicanchas, gimnasio al aire libre, comercio y escuelas desde jardin de niños hasta secundaria.
Muy cerca de avenidas principales y plazas comerciales.';

        $mapa                       =       'https://goo.gl/maps/YZhEdbCaJMiwkZix7';

        $embed                       =       '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7514.85652875955!2d-99.02596534999999!3d19.651726150000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1f2135a3e68f7%3A0x2e64832cbc973d2c!2sFraccionamiento%20Real%20Verona%2C%20Ojo%20de%20Agua%2C%20M%C3%A9x.!5e0!3m2!1ses-419!2smx!4v1667429173424!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>';


        $ubicacion                  =       'Fracc. Real Verona <br> Tecámac, Edo. Méx.';

        $img                        =       'real-verona-edo-mex-26';

        $cantidad_img               =       9;

        break;

    }

@endphp

@extends('layouts.base')

@section('title')
    Xante.mx
@endsection

@section('css')

    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />

    <style>

        .slick-list, .slick-track{
            height: 100%!important;
        }

    </style>

@endsection

@section('contenido')

    <div class="relative w-full  min-h-screen ">

        @include('header')

        <div class="w-full lg:w-11/12  mx-auto pt-24 pb-8">

            <div class="w-11/12 lg:w-full mx-auto text-center lg:text-left">

                <h2 class="text-5xl font-montserrat-bold">

                    {{ $texto_titulo  }}

                </h2>

                <h3 class="text-3xl font-montserrat-regular text-gris-texto pt-4">

                    {{ $subtitulo }}

                </h3>

            </div>

            <div class="w-11/12 lg:w-full mx-auto flex lg:flex-row flex-col gap-4 mt-8">

                <div class="w-full lg:w-1/2 bg-cover bg-center" style="background-image: url('/assets/images/jpg/{{$img}}/1.png'); ">

                    <img src="/assets/images/jpg/test_cuadrados.jpg" alt="" class="w-full bg_oscuro invisible">

                </div>

                <div class="w-full lg:w-1/2 grid grid-cols-2 gap-4">

                    @for ($i = 1; $i < 5; $i++)

                        <div class="bg-center bg-cover flex justify-center items-center bg_oscuro relative" style="background-image: url('/assets/images/jpg/{{$img}}/{{$i}}.png')" >

                            <img src="/assets/images/jpg/test_cuadrados.jpg" alt="" class="w-full invisible">

                            @if ($i == 4)

                                <a onclick="abrirModal()"  class="uppercase absolute text-white cursor-pointer text-sm border-2 py-2 px-2 lg:px-8 font-roboto-bold rounded-md hover:bg-white hover:text-black transition duration-500 ease-out">

                                    {{ __('landing.xan_ver_mas') }}

                                </a>

                            @endif

                        </div>

                    @endfor

                </div>

            </div>

            <div class="flex flex-col lg:flex-row mt-4 w-11/12 lg:w-full mx-auto">

                <div class="w-full lg:w-1/2">

                    <h2 class="font-montserrat-regular text-2xl my-6">

                        {{ $ubicacion_propiedad }}

                    </h2>

                    <div class="w-full bg-morado-xante h-2"></div>

                    <div class="flex mt-12 gap-4 ">

                        @if ( $equipamento['metros'] != null )

                            <div class="flex justify-center items-center flex-col gap-1">

                                <div class="w-16 h-16 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                    <img src="/assets/images/png/iconos/metros_icon.png" alt="" class="h-8 w-8 mt-4">

                                </div>

                                <p class="text-base font-montserrat-regular">

                                    @php

                                        echo $equipamento['metros'];

                                    @endphp


                                </p>

                            </div>

                        @endif

                        @if ( $equipamento['cuartos'] != null )

                            <div class="flex justify-center items-center flex-col gap-1">

                                <div class="w-16 h-16 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                    <img src="/assets/images/png/iconos/habitaciones_icon.png" alt="" class="h-8 w-8 mt-4">

                                </div>

                                <p class="text-base font-montserrat-regular">

                                    {{ $equipamento['cuartos'] }}

                                </p>

                            </div>

                        @endif

                        @if ( $equipamento['banios'] != null )

                            <div class="flex justify-center items-center flex-col gap-1">

                                <div class="w-16 h-16 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                    <img src="/assets/images/png/iconos/banios_icon.png" alt="" class="h-8 w-8 mt-4">

                                </div>

                                <p class="text-base font-montserrat-regular">

                                    @php

                                        echo $equipamento['banios'];

                                    @endphp


                                </p>

                            </div>

                        @endif


                        @if ( $equipamento['estacionamiento'] != null )

                            <div class="flex justify-center items-center flex-col gap-1">

                                <div class="w-16 h-16 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                    <img src="/assets/images/png/iconos/estacionamiento_icon.png" alt="" class="h-8 w-8 mt-4">

                                </div>

                                <p class="text-base font-montserrat-regular">

                                    @php

                                        echo $equipamento['estacionamiento'];

                                    @endphp


                                </p>

                            </div>

                        @endif

                    </div>

                    <div class="mt-10 text-center lg:text-left">

                        <h2 class="text-5xl text-morado font-montserrat-bold">

                            {{ __('landing.xan_descripcion') }}

                        </h2>

                        <p class="text-xl mt-4">

                            {!! $descripcion !!}

                        </p>

                    </div>

                </div>

                <div class="w-full lg:w-1/2 flex flex-col mt-4 justify-start items-center">

                    <h2 class="font-montserrat-bold text-5xl text-morado-xante">

                        {{ __('landing.xan_precio') }}

                    </h2>

                    <h2 class="font-montserrat-bold mt-4 text-5xl">

                        ${{ number_format($precio,2) }}*

                    </h2>

                    <a target="_blank" href="https://wa.link/om0mki">

                        <div class="flex flex-col lg:flex-row mt-8 items-center gap-2 justify-center">

                            <p class="bg-verde-landing border-2 border-white font-montserrat-semibold text-xl lg:text-3xl text-white shadow-md rounded-md px-8 py-2">

                                {{__('landing.xan_sol_info')}}

                            </p>

                            <div class="relative flex justify-center items-center  ">

                                <img src="/assets/images/png/iconos/whats_fondo.png" alt="" class="h-24 w-24 ">

                                <img src="/assets/images/png/iconos/whats_logo.png" alt="" class="absolute h-12 w-12 mr-2 mb-1 ">

                            </div>

                        </div>

                    </a>

                    <form class="bg-white shadow-2xl rounded-3xl flex-col flex mt-8 p-8 w-full lg:w-2/3 2xl:w-1/2 gap-8" method="post" action="{{url('sendEmail')}}">
                        @csrf
                        <div class="w-full flex flex-col gap-2">

                            <input oninput="valida_nombre(); validar()" type="text" id="txt_nombre" name="txt_nombre" placeholder="Nombre y Apellido" class="rounded-full bg-gray-300 border-2 border-rose-500">

                            <small id="error_nombre" class="text-sm text-red-400 pl-4 font-roboto-light"></small>

                        </div>

                        <div class="w-full flex flex-col gap-2">

                            <input oninput="valida_telefono(); validar()" type="number" id="txt_telefono" name="txt_telefono" placeholder="Teléfono de contacto" class="rounded-full bg-gray-300 border-2 border-rose-500">

                            <small id="error_telefono" class="text-sm text-red-400 pl-4 font-roboto-light"></small>

                        </div>

                        <div class="w-full flex flex-col gap-2">

                            <input oninput="valida_correo(); validar()" type="email" id="txt_correo" name="txt_correo" placeholder="Correo electrónico" class="rounded-full bg-gray-300 border-2 border-rose-500">

                            <small id="error_correo" class="text-sm text-red-400 pl-4 font-roboto-light"></small>

                        </div>

                        <input type="text" class="hidden" name="txt_propiedad" id="txt_propiedad"  value="{{$id_prop}}">

                        <button disabled class="text-white btn_enviar bg-verde-desc font-roboto-normal text-xl py-1 w-52 rounded-xl mt-4 mx-auto shadow-lg border-2 border-white"> {{__('landing.xan_send')}} </button>

                    </form>

                </div>

            </div>

            <div class="lg:-mt-12 mt-8">

                <h2 class="text-5xl text-morado font-montserrat-bold text-center lg:text-left">

                    {{ __('landing.xan_mapa') }}

                </h2>

                <div class="flex mt-8 justify-center items-center flex-col lg:flex-row">

                    <div class="w-10/12 lg:w-1/2 p-2 lg:shadow-2xl shadow-xl h-64 lg:h-card rounded-xl -mb-8 lg:-mb-0 z-10">

                        {!!$embed!!}

                    </div>

                    <div class="w-full lg:w-1/2 flex flex-col justify-end">

                        <div class="bg-morado-xante w-full h-56 text-white flex flex-col justify-center items-center pl-0">

                            <h3 class="text-4xl font-montserrat-bold">

                                Showroom

                            </h3>

                            <p class="font-montserrat-regular text-center mt-4 text-xl">

                                {!! $ubicacion !!}

                            </p>

                        </div>

                        <div class="flex flex-col w-full lg:justify-start justify-center text-center lg:text-left items-center mt-4 pl-0">

                            <h3 class="text-2xl font-montserrat-bold">

                                {{ __('landing.xan_horario') }}

                            </h3>

                            <p class="font-montserrat-regular text-center">

                                {{ __('landing.xan_horario_lun') }}  <br>
                                {{ __('landing.xan_horario_dom') }}


                            </p>

                        </div>

                    </div>

                </div>

            </div>

            <div class="flex flex-col items-center justify-center text-center pb-7 mt-14">

                <span class="px-4 text-xs text-black opacity-70 lg:px-0 lg:text-sm font-montserrat-regular ">

                    {!! __('landing.xan_discraimer') !!}

                </span>

            </div>

            {{-- <div class="fixed top-0 bottom-0 left-0 right-0 z-50 hidden w-full min-h-screen scale-0 bg-black bg-opacity-50 modal-full"> --}}
            <div class="fixed top-0 bottom-0 left-0 right-0 w-full min-h-screen scale-0 invisible index bg-black bg-opacity-50 modal-full">

                <div class="relative flex items-center justify-center w-full h-full">

                    <div class="w-10/12 lg:w-1/3 relative">

                        <div class="absolute flex flex-col items-center justify-center -top-12 -right-12 ">

                            <div class="py-4">

                                <span class="text-white cursor-pointer" onclick="cerrarModal()">

                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-10 h-10" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                    </svg>

                                </span>

                            </div>

                        </div>

                        <div class="slider_desc">

                            @for ($i = 1; $i <= $cantidad_img; $i++)

                                    {{-- <div class="bg-center bg-cover flex justify-center items-center bg_oscuro relative" style="background-image: url('/assets/images/jpg/{{$img}}/{{$i}}.png')" > --}}
                                <div class="">
                                    <div class="w-full flex justify-center items-center h-85vh">
                                        <img src="/assets/images/jpg/{{$img}}/{{$i}}.png" alt="" class="w-full relative my-auto inset-0">
                                    </div>
                                </div>

                                    {{-- </div> --}}

                            @endfor

                        </div>

                        <div class="absolute w-full dots_slider_desc bottom-8"></div>

                        <div class="absolute flex justify-between w-full px-8 slider_propiedades_desk_arrows" style="top: 50%"></div>

                     </div>

                </div>

            </div>

        </div>

    </div>

@include('footer')

@endsection

@section('js-usable')

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

    <script type="text/javascript">

        $('.slider_desc').slick({

            slidesToShow: 1,
            dots: true,
            appendDots: $('.dots_slider_desc'),
            prevArrow: '<div class="text-white cursor-pointer a-left control-c prev slick-prev left-10"><svg xmlns="http://www.w3.org/2000/svg" class="w-16" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2"><path stroke-linecap="round" stroke-linejoin="round" d="M15 19l-7-7 7-7" /></svg></div>',
            nextArrow: '<div class="text-white cursor-pointer a-right control-c next slick-next"><svg xmlns="http://www.w3.org/2000/svg" class="w-16" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2"><path stroke-linecap="round" stroke-linejoin="round" d="M9 5l7 7-7 7" /> </svg></div>',
            appendArrows: $('.slider_propiedades_desk_arrows'),
            fade: true,
            cssEase: 'linear',
            adaptiveHeight: true

        });

        function abrirModal(){
            $('.modal-full').toggleClass('fade-in');
            $('.modal-full').toggleClass('invisible');
            $('.modal-full').removeClass('index');
            $('.modal-full').addClass('z-50');
        }

        function cerrarModal(){
            $('.modal-full').toggleClass('fade-in');
            $('.modal-full').toggleClass('invisible');
        }

        $(document).ready(function() {
            $('.acc-container .acc:nth-child(1) .acc-head').addClass('active');
            $('.acc-container .acc:nth-child(1) .acc-content').slideDown();
            $('.acc-head').on('click', function() {
                if ($(this).hasClass('active')) {
                    $(this).siblings('.acc-content').slideUp();
                    $(this).removeClass('active');
                } else {
                    $('.acc-content').slideUp();
                    $('.acc-head').removeClass('active');
                    $(this).siblings('.acc-content').slideToggle();
                    $(this).toggleClass('active');
                }
            });
        });

        function abrir_menu() {
            $('.menu').toggleClass('opened');
            $('#cont_menu').toggleClass(' menu_visible');
        }

        var correo = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var letras = /^[a-zA-Z áÁéÉíÍóÓúÚñÑüÜàè]+$/;
        var telefono = /^[0-9\- ]+$/;

        var nombre = 1;
        var email = 1;
        var telefono = 1;


        function valida_nombre() {

            nombre_val = $('#txt_nombre').val()

            if (nombre_val.length == 0 || !nombre_val.trim()) {
                document.getElementById('error_nombre').innerHTML = "El campo esta vacío.";
                nombre = 1;
            } else if (nombre_val.length < 3) {
                document.getElementById('error_nombre').innerHTML = "El campo debe tener más de 3 caracteres.";
                nombre = 1;
            } else if (!letras.test(nombre_val)) {
                document.getElementById('error_nombre').innerHTML = "Caracteres no validos";
                nombre = 1;
            } else {
                document.getElementById('error_nombre').innerHTML = "";
                nombre = 0;
            }
            return nombre;

        }

        function valida_telefono() {

            tel_val = $('#txt_telefono').val();

            if (tel_val.length == 0 || !tel_val.trim()) {
                document.getElementById('error_telefono').innerHTML = "El campo esta vacío.";
                telefono = 1;
            } else if (tel_val.length < 3) {
                document.getElementById('error_telefono').innerHTML = "El campo debe tener más de 3 caracteres.";
                telefono = 1;
            }else {
                document.getElementById('error_telefono').innerHTML = "";
                telefono = 0;
            }
            return telefono;

        }

        function valida_correo() {

            correo_val = $('#txt_correo').val();

            if (correo_val.length == 0 || !correo_val.trim()) {
                document.getElementById('error_correo').innerHTML = "El campo esta vacío.";
                email = 1;
            } else if (correo_val.length < 3) {
                document.getElementById('error_correo').innerHTML = "El campo debe tener más de 3 caracteres.";
                email = 1;
            } else if (!correo.test(correo_val)) {
                document.getElementById('error_correo').innerHTML = "El campo debe ser un correo.";
                email = 1;
            } else {
                document.getElementById('error_correo').innerHTML = "";
                email = 0;
            }
            return email;

        }

        function  validar(){

            if(nombre == 0 && telefono == 0 && email == 0 ) {

                console.log('si val');

                $('.btn_enviar').prop('disabled', false);

            }else{

                console.log('No val');

                $('.btn_enviar').prop('disabled', true);


            }



        }


    </script>
@endsection
