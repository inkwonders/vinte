@php

    switch ($id_prop) {


        case "real-toledo-pachuca-52":

            $nombre_propiedad = 'Real Toledo - Pachuca';
            $precio_prop = '990,000';
            $texto_titulo_verde = __('landing.xan_d_p_t_v_3');
            $texto_titulo_gris = __('landing.xan_d_p_t_g_3');
            $metros = '69';
            $camas = '2';
            $banios = '1 1/2';
            $equipamento = array(
                'closets' => 1,
                'cocina_integral' => 1,
                'protecciones' => 0,
                'cancel' => 0,
                'cocineta_sin_estufa' => 0,
                'piso' => 1
            );
            $titulo_prop = __('landing.xan_d_p_t_n_3');
            $desc_porp = __('landing.xan_d_p_d_g_3');
            $mapa_frame = "<iframe  class='h-40 mx-auto shadow-xl w-60 2xl:h-52 2xl:w-96 rounded-xl' src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d937.1265302382151!2d-98.78325199999999!3d20.0292326!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1a17a85360827%3A0xb02baa5ed7179d30!2sAlbarreal%2C%20Real%20de%20Toledo%2C%2042119%20Pachuca%20de%20Soto%2C%20Hgo.!5e0!3m2!1ses-419!2smx!4v1656527505062!5m2!1ses-419!2smx'  style='border:0;' allowfullscreen='' loading='lazy' referrerpolicy='no-referrer-when-downgrade'></iframe>";
            $mapa = '';
            $img = 'real_toledo_pachuca_3';
            $vendida = 0;

        break;

        // case "real-segovia-puebla-1":

        //     $nombre_propiedad = 'Real Segovia - Puebla';
        //     $precio_prop = '599,000';
        //     $texto_titulo_verde = __('landing.xan_d_p_t_v_1');
        //     $texto_titulo_gris = __('landing.xan_d_p_t_g_1');
        //     $metros = '54';
        //     $camas = '2';
        //     $banios = '1';
        //     $equipamento = array(
        //         'closets' => 1,
        //         'cocina_integral' => 1,
        //         'protecciones' => 1,
        //         'cancel' => 1,
        //         'cocineta_sin_estufa' => 0,
        //         'piso' => 1
        //     );
        //     $titulo_prop = __('landing.xan_d_p_t_n_1');
        //     $desc_porp = __('landing.xan_d_p_d_g_1');
        //     $mapa_frame = "<iframe  class='h-40 mx-auto shadow-xl w-60 2xl:h-52 2xl:w-96 rounded-xl' src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3768.3154760054817!2d-98.38233930000001!3d19.1814185!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cfcde7f5db286f%3A0x235339753d402237!2sReal%20Segovia!5e0!3m2!1ses-419!2smx!4v1656517741931!5m2!1ses-419!2smx'  style='border:0;' allowfullscreen='' loading='lazy' referrerpolicy='no-referrer-when-downgrade'></iframe>" ;
        //     $mapa = 'https://goo.gl/maps/MGAYoAAT8nzFfcMi6';
        //     $img = 'real_segovia_puebla_1';
        //     $vendida = 1;

        // break;

        // case "real-solare-queretaro-2":

        //     $nombre_propiedad = 'Real Solare - Querétaro';
        //     $precio_prop = '769,000';
        //     $texto_titulo_verde = __('landing.xan_d_p_t_v_2');
        //     $texto_titulo_gris = __('landing.xan_d_p_t_g_2');
        //     $metros = '50';
        //     $camas = '2';
        //     $banios = '1';
        //     $equipamento = array(
        //         'closets' => 1,
        //         'cocina_integral' => 1,
        //         'protecciones' => 1,
        //         'cancel' => 0,
        //         'cocineta_sin_estufa' => 0,
        //         'piso' => 1
        //     );
        //     $titulo_prop = __('landing.xan_d_p_t_n_2');
        //     $desc_porp = __('landing.xan_d_p_d_g_2');
        //     $mapa_frame = "<iframe  class='h-40 mx-auto shadow-xl w-60 2xl:h-52 2xl:w-96 rounded-xl' src='https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3734.951283314052!2d-100.2877922!3d20.590047!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d342cbe7bfcc8b%3A0xe6779fe0ca58ef2b!2sPriv.%20Despina%2C%20Real%20Solare%2C%2076246%20Qro.!5e0!3m2!1ses-419!2smx!4v1656524848390!5m2!1ses-419!2smx'  style='border:0;' allowfullscreen='' loading='lazy' referrerpolicy='no-referrer-when-downgrade'></iframe>" ;
        //     $mapa = '';
        //     $img = 'real_solare_queretaro_2';
        //     $vendida = 1;

        // break;


        // case "real-segovia-puebla-4":

        //     $nombre_propiedad = 'Real Segovia - Puebla';
        //     $precio_prop = '991,000';
        //     $texto_titulo_verde = __('landing.xan_d_p_t_v_4');
        //     $texto_titulo_gris = __('landing.xan_d_p_t_g_4');
        //     $metros = '92';
        //     $camas = '3';
        //     $banios = '1 1/2';
        //     $equipamento = array(
        //         'closets' => 0,
        //         'cocina_integral' => 0,
        //         'protecciones' => 0,
        //         'cancel' => 0,
        //         'cocineta_sin_estufa' => 1,
        //         'piso' => 1
        //     );
        //     $titulo_prop = __('landing.xan_d_p_t_n_4');
        //     $desc_porp = __('landing.xan_d_p_d_g_4');
        //     $mapa_frame = "<iframe  class='h-40 mx-auto shadow-xl w-60 2xl:h-52 2xl:w-96 rounded-xl' src='https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d942.0802217410555!2d-98.3861201!3d19.181182!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cfcde7f5db286f%3A0x235339753d402237!2sReal%20Segovia!5e0!3m2!1ses-419!2smx!4v1656528246046!5m2!1ses-419!2smx'  style='border:0;' allowfullscreen='' loading='lazy' referrerpolicy='no-referrer-when-downgrade'></iframe>" ;
        //     $mapa = '';
        //     $img = 'real_segovia_puebla_4';
        //     $vendida = 1;

        // break;

        // case "real-granada-tecamac-5":

        //     $nombre_propiedad = 'Real Granada - Tecámac';
        //     $precio_prop = '689,000';
        //     $texto_titulo_verde = __('landing.xan_d_p_t_v_5');
        //     $texto_titulo_gris = __('landing.xan_d_p_t_g_5');
        //     $metros = '45';
        //     $camas = '2';
        //     $banios = '1';
        //     $equipamento = array(
        //         'closets' => 1,
        //         'cocina_integral' => 1,
        //         'protecciones' => 1,
        //         'cancel' => 1,
        //         'cocineta_sin_estufa' => 0,
        //         'piso' => 1
        //     );
        //     $titulo_prop = __('landing.xan_d_p_t_n_5');
        //     $desc_porp = __('landing.xan_d_p_d_g_5');
        //     $mapa_frame = "<iframe  class='h-40 mx-auto shadow-xl w-60 2xl:h-52 2xl:w-96 rounded-xl' src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3755.328499036349!2d-98.9593531!3d19.7411931!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d19324e4d9db3d%3A0xefcc899992bf5c8b!2sPriv.%20Belerda%2C%2055743%20Santa%20Mar%C3%ADa%20Ajoloapan%2C%20M%C3%A9x.!5e0!3m2!1ses-419!2smx!4v1656528519150!5m2!1ses-419!2smx'  style='border:0;' allowfullscreen='' loading='lazy' referrerpolicy='no-referrer-when-downgrade'></iframe>";
        //     $mapa = '';
        //     $img = 'real_ granada_tecamac_5';
        //     $vendida = 1;

        // break;

        // case "real-del-sol-tecamac-6":

        //     $nombre_propiedad = 'Real del Sol - Tecámac';
        //     $precio_prop = '1,595,000';
        //     $texto_titulo_verde = __('landing.xan_d_p_t_v_6');
        //     $texto_titulo_gris = __('landing.xan_d_p_t_g_6');
        //     $metros = '95';
        //     $camas = '3';
        //     $banios = '1 1/2';
        //     $equipamento = array(
        //         'closets' => 0,
        //         'cocina_integral' => 0,
        //         'protecciones' => 1,
        //         'cancel' => 1,
        //         'cocineta_sin_estufa' => 0,
        //         'piso' => 1
        //     );
        //     $titulo_prop = __('landing.xan_d_p_t_n_6');
        //     $desc_porp = __('landing.xan_d_p_d_g_6');
        //     $mapa_frame = "<iframe  class='h-40 mx-auto shadow-xl w-60 2xl:h-52 2xl:w-96 rounded-xl' src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3757.119816755542!2d-99.0193594!3d19.664893!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1f3f778865d77%3A0x4382813865ca4cb9!2sREAL%20DEL%20SOL!5e0!3m2!1ses-419!2smx!4v1656536349794!5m2!1ses-419!2smx'  style='border:0;' allowfullscreen='' loading='lazy' referrerpolicy='no-referrer-when-downgrade'></iframe>" ;
        //     $mapa = '';
        //     $img = 'real_del_sol_tecamac_6';
        //     $vendida = 0;

        // break;

        // case "real-del-cid-tecamac-7":

        //     $nombre_propiedad = 'Real del Cid - Tecámac';
        //     $precio_prop = '1,200,000';
        //     $texto_titulo_verde = __('landing.xan_d_p_t_v_7');
        //     $texto_titulo_gris = __('landing.xan_d_p_t_g_7');
        //     $metros = '77';
        //     $camas = '3';
        //     $banios = '1 1/2';
        //     $equipamento = array(
        //         'closets' => 1,
        //         'cocina_integral' => 1,
        //         'protecciones' => 1,
        //         'cancel' => 1,
        //         'cocineta_sin_estufa' => 0,
        //         'piso' => 1
        //     );
        //     $titulo_prop = __('landing.xan_d_p_t_n_7');
        //     $desc_porp = __('landing.xan_d_p_d_g_7');
        //     $mapa_frame = "<iframe  class='h-40 mx-auto shadow-xl w-60 2xl:h-52 2xl:w-96 rounded-xl' src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3757.5559076929057!2d-99.0230701!3d19.646274899999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1f3fb7d5ebe27%3A0xed931f1ac430834!2sReal%20del%20cid!5e0!3m2!1ses-419!2smx!4v1656536594203!5m2!1ses-419!2smx'  style='border:0;' allowfullscreen='' loading='lazy' referrerpolicy='no-referrer-when-downgrade'></iframe>" ;
        //     $mapa = '';
        //     $img = 'real_del_cid_tecamac_7';
        //     $vendida = 0;

        // break;

        // case "real-solare-queretaro-8":

        //     $nombre_propiedad = 'Real Solare - Querétaro';
        //     $precio_prop = '1,015,000';
        //     $texto_titulo_verde = __('landing.xan_d_p_t_v_8');
        //     $texto_titulo_gris = __('landing.xan_d_p_t_g_8');
        //     $metros = '83';
        //     $camas = '2';
        //     $banios = '2 1/2';
        //     $equipamento = array(
        //         'closets' => 1,
        //         'cocina_integral' => 1,
        //         'protecciones' => 1,
        //         'cancel' => 1,
        //         'cocineta_sin_estufa' => 0,
        //         'piso' => 1
        //     );
        //     $titulo_prop = __('landing.xan_d_p_t_n_8');
        //     $desc_porp = __('landing.xan_d_p_d_g_8');
        //     $mapa_frame = "<iframe class='h-40 mx-auto shadow-xl w-60 2xl:h-52 2xl:w-96 rounded-xl' src='https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d39889.15589822933!2d-100.34449270179942!3d20.57221752773438!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d342cc112abe65%3A0xcb0cd5063e7b5675!2sReal%20Solare%2C%2076246%20Qro.!5e0!3m2!1ses-419!2smx!4v1656536786829!5m2!1ses-419!2smx'  style='border:0;' allowfullscreen='' loading='lazy' referrerpolicy='no-referrer-when-downgrade'></iframe>" ;
        //     $mapa = '';
        //     $img = 'real_solare_queretaro_8';
        //     $vendida = 0;

        // break;

    }

@endphp

@extends('layouts.base')

@section('title')
    Xante.mx
@endsection

@section('css')

    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />

    <style>

        .slick-list, .slick-track{
            height: 100%!important;
        }

    </style>

@endsection

@section('contenido')

    <div class="relative w-full min-h-screen ">

        @include('header')

        <div class="flex flex-col-reverse h-full bg-bottom bg-cover lg:bg-top lg:flex-row ">

            <div class="w-full bg-right bg-no-repeat bg-contain border-b-8 border-white lg:pt-16 border-b-16 lg:border-b-0 lg:border-r-8 lg:min-h-screen lg:w-3/5 lg:bg-fondo-desc-prop">

                <div class="flex flex-col items-center w-full lg:items-start lg:px-10 2xl:px-16 lg:pt-12">

                    <h2 class="hidden text-5xl 2xl:text-7xl lg:block text-verde-desc font-montserrat-bold">

                        {!! $texto_titulo_verde !!}

                    </h2>

                    <h3 class="hidden text-2xl 2xl:text-3xl lg:block text-gris-texto-desc font-montserrat-regular">

                        {!! $texto_titulo_gris !!}

                    </h3>

                    <h3 class="mt-8 text-2xl lg:text-3xl 2xl:text-4xl text-fondo-xante_morado font-montserrat-bold">
                        {!! $nombre_propiedad  !!} <br class="lg:hidden"> <span  class="text-amarillo-fin"> $ {!! $precio_prop !!}* </span>
                    </h3>

                </div>

                <div class="flex flex-col w-full my-8 lg:flex-row">

                    <div class="flex items-center justify-around w-11/12 py-6 mx-auto text-center text-white lg:w-3/7 bg-morado-xante">

                        <div class="flex flex-col items-center justify-center text-center">

                            <div class="items-center justify-center h-12">

                                <img src="/assets/images/png/icon_superficie.png" class="relative w-10 py-1">

                            </div>

                            <span class="flex flex-col items-center justify-center h-14">

                                {{ $metros }} m2 <br>

                                {{ __('landing.xan_d_iconos_1') }}

                            </span>

                        </div>

                        <div class="flex flex-col items-center justify-center text-center">

                            <div class="items-center justify-center h-12">

                                <img src="/assets/images/png/icon_cuartos.png" class="relative w-12 py-2">

                            </div>

                            <span class="flex flex-col items-center justify-center h-14">

                                {{ $camas }}

                                {{ __('landing.xan_d_iconos_2') }}

                            </span>

                        </div>

                        <div class="flex flex-col items-center justify-center text-center">

                            <div class="items-center justify-center h-12">

                                <img src="/assets/images/png/icon_banos.png" class="relative w-8 py-1">

                            </div>

                            <span class="flex flex-col items-center justify-center h-14">

                                {{ $banios }}

                                {{ __('landing.xan_d_iconos_3') }}

                            </span>

                        </div>

                    </div>

                    <div class="grid items-center justify-start w-11/12 grid-cols-3 gap-6 px-4 py-6 mx-auto text-center lg:flex text-morado-xante lg:w-4/7">

                        @if ($equipamento['closets'] != 0)

                            <div class="flex flex-col items-center justify-center text-center">


                                <div class="items-center justify-center h-20">

                                    <img src="/assets/images/png/iconos/closets.png" class="relative w-20 h-20">

                                </div>

                                <span class="flex flex-col items-center justify-center h-14">

                                    {!! __('landing.xan_d_iconos_4') !!}

                                </span>

                            </div>

                        @endif

                        @if ($equipamento['cocina_integral'] != 0)

                            <div class="flex flex-col items-center justify-center text-center">

                                <div class="items-center justify-center h-20">

                                    <img src="/assets/images/png/iconos/cocina.png" class="relative w-20 h-20">

                                </div>

                                <span class="flex flex-col items-center justify-center h-14">

                                    {!! __('landing.xan_d_iconos_5') !!}

                                </span>

                            </div>

                        @endif

                        @if ($equipamento['protecciones'] != 0)

                            <div class="flex flex-col items-center justify-center text-center">

                                <div class="items-center justify-center h-20">

                                    <img src="/assets/images/png/iconos/protecciones.png" class="relative w-20 h-20">

                                </div>

                                <span class="flex flex-col items-center justify-center h-14">

                                    {!! __('landing.xan_d_iconos_6') !!}

                                </span>

                            </div>

                        @endif

                        @if ($equipamento['cancel'] != 0)

                            <div class="flex flex-col items-center justify-center text-center">

                                <div class="items-center justify-center h-20">

                                    <img src="/assets/images/png/iconos/cancel.png" class="relative w-20 h-20">

                                </div>

                                <span class="flex flex-col items-center justify-center h-14">

                                    {!! __('landing.xan_d_iconos_7') !!}

                                </span>

                            </div>

                        @endif

                        @if ($equipamento['cocineta_sin_estufa'] != 0)

                            <div class="flex flex-col items-center justify-center text-center">

                                <div class="items-center justify-center h-20">

                                    <img src="/assets/images/png/iconos/cocineta.png" class="relative w-20 h-20">

                                </div>

                                <span class="flex flex-col items-center justify-center h-14">

                                    {!! __('landing.xan_d_iconos_8') !!}

                                </span>

                            </div>

                        @endif

                        @if ($equipamento['piso'] != 0)

                            <div class="flex flex-col items-center justify-center text-center">

                                <div class="items-center justify-center h-20">

                                    <img src="/assets/images/png/iconos/pisos.png" class="relative w-20 h-20">

                                </div>

                                <span class="flex flex-col items-center justify-center h-14">

                                    {!! __('landing.xan_d_iconos_9') !!}

                                </span>

                            </div>

                        @endif

                    </div>

                </div>

                <div class="flex flex-col w-full px-10 lg:px-10 2xl:px-16 lg:flex-row">

                    <div class="w-full text-center lg:w-1/2 lg:text-left">

                        <h4 class="text-sm lg:text-base 2xl:text-lg font-montserrat-regular text-naranja_desc">

                            {!! $titulo_prop !!}

                        </h4>

                        <p class="mt-6 text-sm text-justify text-black lg;text-base 2xl:text-lg font-montserrat-regular">

                            {!! $desc_porp  !!}

                        </p>

                    </div>

                    <div class="flex items-center justify-center w-full mt-6 lg:mt-0 lg:w-1/2 ">

                        {!! $mapa_frame !!}

                    </div>

                </div>

                 @if ($vendida == 0)

                    <div class="flex justify-center w-10/12 mx-auto mt-16">

                        <a target="_blank" href="https://wa.me/+525570464021" title="Contáctanos" class="items-center py-2 text-sm font-bold text-white transition duration-700 ease-in-out border-2 border-transparent border-white rounded-md shadow-lg cursor-pointer lg:text-2xl px-14 lg:px-28 bg-verde-landing font-montserrat-bold hover:bg-white hover:border-verde-landing hover:text-verde-landing">

                            {{ __('landing.xan_d_estrena') }}

                        </a>

                    </div>

                @endif

                <div class="flex justify-center my-4">

                    <small class="px-4 text-xs text-center text-gris-texto-desc lg:px-0 lg:text-left">

                        {{ __('landing.xan_d_estrena_d') }}

                    </small>


                </div>

            </div>

            <div class="relative min-h-screen lg:w-2/5 ">

                <div class="w-full py-8 mt-12 text-center lg:hidden">

                    <h2 class="text-xl text-verde-desc font-montserrat-bold">

                        {!! $texto_titulo_verde !!}

                    </h2>

                    <h3 class="text-base text-gris-texto-desc font-montserrat-regular">

                        {!! $texto_titulo_gris !!}

                    </h3>

                </div>

                <div class="h-full slider_desc">


                    @for ($i=1; $i < 5; $i ++)

                        <div onclick="abrirModal({{ $i }})" class="w-full h-full cursor-pointer">

                            <div class="w-full h-full min-h-screen bg-center bg-cover" style="background-image: url('/assets/images/jpg/{{ $img }}/{{ $i }}.jpg')"></div>

                        </div>

                    @endfor

                </div>

                <div class="absolute w-full dots_slider_desc bottom-8"></div>

                <div class="absolute flex justify-between w-full px-8 slider_propiedades_desk_arrows" style="top: 50%"></div>

            </div>

        </div>

    </div>

    <div class="fixed top-0 bottom-0 left-0 right-0 z-50 hidden w-full min-h-screen scale-0 bg-black bg-opacity-50 modal-full">

        <div class="relative flex items-center justify-center w-full h-full">

            <div class="absolute flex flex-col items-center justify-center ">

                <div class="py-4">

                    <span class="text-white cursor-pointer" onclick="cerrarModal()">

                        <svg xmlns="http://www.w3.org/2000/svg" class="w-10 h-10" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>
                    </span>

                </div>

                <img id="img_full" src="" class="w-3/4 lg:w-1/2" alt="">

            </div>

        </div>

    </div>

    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-227859785-1');

    </script>

@include('footer')


@endsection

@section('js-usable')

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

    <script type="text/javascript">

        $('.slider_desc').slick({

            slidesToShow: 1,
            dots: true,
            appendDots: $('.dots_slider_desc'),
            prevArrow: '<div class="text-white cursor-pointer a-left control-c prev slick-prev left-10"><svg xmlns="http://www.w3.org/2000/svg" class="w-16" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2"><path stroke-linecap="round" stroke-linejoin="round" d="M15 19l-7-7 7-7" /></svg></div>',
            nextArrow: '<div class="text-white cursor-pointer a-right control-c next slick-next"><svg xmlns="http://www.w3.org/2000/svg" class="w-16" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2"><path stroke-linecap="round" stroke-linejoin="round" d="M9 5l7 7-7 7" /> </svg></div>',
            appendArrows: $('.slider_propiedades_desk_arrows'),
            fade: true,
            cssEase: 'linear',
            adaptiveHeight: true

        });

        function abrirModal(img_num){
            $('.modal-full').toggleClass('fade-in');
            $('.modal-full').toggleClass('hidden');
            $("#img_full").attr("src","/assets/images/jpg/{{ $img }}/"+img_num+".jpg");
        }

        function cerrarModal(){
            $('.modal-full').toggleClass('fade-in');
            $('.modal-full').toggleClass('hidden');
        }

        $(document).ready(function() {
            $('.acc-container .acc:nth-child(1) .acc-head').addClass('active');
            $('.acc-container .acc:nth-child(1) .acc-content').slideDown();
            $('.acc-head').on('click', function() {
                if ($(this).hasClass('active')) {
                    $(this).siblings('.acc-content').slideUp();
                    $(this).removeClass('active');
                } else {
                    $('.acc-content').slideUp();
                    $('.acc-head').removeClass('active');
                    $(this).siblings('.acc-content').slideToggle();
                    $(this).toggleClass('active');
                }
            });
        });

        function abrir_menu() {
            $('.menu').toggleClass('opened');
            $('#cont_menu').toggleClass(' menu_visible');
        }

    </script>
@endsection
