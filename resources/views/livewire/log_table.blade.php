{{-- NOMBRE --}}
<x-livewire-tables::table.cell>
    {{ $row->created_at }}
</x-livewire-tables::table.cell>

{{-- PRESENTACIÓN --}}
<x-livewire-tables::table.cell>
    {{ $row->name }}
</x-livewire-tables::table.cell>

{{-- AÑADA --}}
<x-livewire-tables::table.cell>
    {{ $row->email }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>

    @php

    $data = json_decode($row->description);

    @endphp

    @if (!empty($data->message))


        {{ $data->message }}

    @else

        Error al cargar este mensaje

    @endif



</x-livewire-tables::table.cell>

