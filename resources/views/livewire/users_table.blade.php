{{--  ID  --}}


<x-livewire-tables::table.cell>
    {{ $row->id }}
</x-livewire-tables::table.cell>


{{--  NOMBRE  --}}

<x-livewire-tables::table.cell>
    {{ $row->name }}
</x-livewire-tables::table.cell>


{{-- correo --}}
<x-livewire-tables::table.cell>
    {{ $row->email }}
</x-livewire-tables::table.cell>


{{-- tipo --}}
<x-livewire-tables::table.cell>


@if (count($row->roles)>0)

    {{ $row->roles[0]->name }}

    @else
      {{ "-" }}
    @endif
</x-livewire-tables::table.cell>


{{--  FECHA REGISTRO  --}}

<x-livewire-tables::table.cell>

    @if ($row->created_at != null)
        <span class="titilliumwebregular text-grayTable">{{ $row->created_at->format('d / m / Y') }}</span>
    @else
        <span class="titilliumwebregular text-grayTable">{{ __('traduction.no_date') }}</span>
    @endif
</x-livewire-tables::table.cell>




{{-- acciones --}}

<x-livewire-tables::table.cell>


            @if ($users_count>1)

            <div class="flex justify-center items-center gap-8">

                <a  wire:click="deleteConfirm({{ $row }})">

                    <img src="/assets/images/svg/delete.svg" alt="" class="w-6 h-6 mx-auto cursor-pointer">

                </a>

                <a wire:click="modalEdit({{ $row }})">

                    <img src="/assets/images/svg/edit.svg" alt="" class="w-6 h-6 mx-auto cursor-pointer">

                </a>

            </div>

            @else
            <div class="flex justify-center items-center gap-8"> </div>
            @endif





</x-livewire-tables::table.cell>












