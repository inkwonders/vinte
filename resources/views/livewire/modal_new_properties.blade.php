<div class="flex w-full py-10">
    {{--  <div class="w-1/5">
        <a href="/">
            <img src="/assets/images/svg/vente_tucasa_dos.png" alt="" class="w-44">
        </a>
    </div>  --}}
    <div class="flex items-center justify-end w-full">
        @hasrole("Admin||EditorCompra")
        <button class="px-8 py-2 mx-2 text-white border-4 border-white rounded-lg shadow-lg cursor-pointer bg-red-500" wire:click="nuevaPropiedad()">
            {{ 'Nueva propiedad' }}
        </button>
        @endhasrole
        <div class="relative flex">
            <svg xmlns="http://www.w3.org/2000/svg" class="absolute w-4 h-4 top-4 left-6" fill="none" viewBox="0 0 24 24"
                stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
            </svg>

            <input wire:model="filters.search" type="text"
            class="h-12 pl-10 mx-4 border-t-0 border-l-0 border-r-0 titilliumwebsemibold focus:outline-none focus:ring-0"
            placeholder="{{ __('traduction.search') }}">

        </div>
        {{-- <input type="date" class="h-12 mx-4 border-t-0 border-l-0 border-r-0 titilliumwebsemibold focus:outline-none"
            onchange="emit_fechas()" wire:model.defer="inicio" id="inicio" required placeholder="Fecha Inicio">
        <input type="date" class="h-12 mx-4 border-t-0 border-l-0 border-r-0 titilliumwebsemibold focus:outline-none"
            onchange="emit_fechas()" wire:model.defer="fin" id="fin" required placeholder="Fecha Termino"> --}}

        <a href="{{ route('logout') }}"
            class="px-8 py-2 mx-4 text-white border-4 border-white rounded-lg shadow-lg cursor-pointer hover:bg-white "
            style="background-color: #60338a">{{ __('traduction.logout') }}
        </a>
        @if (App::getLocale() == 'en')
            <a href="/locale/es"
                class="px-8 py-2 mx-4 text-white border-4 border-white rounded-lg shadow-lg cursor-pointer bg-green-500">
                {{ 'Español' }}
            </a>
        @else
            <a href="/locale/en"
                class="px-8 py-2 mx-2 text-white border-4 border-white rounded-lg shadow-lg cursor-pointer bg-green-500">
                {{ 'English' }}
            </a>
        @endif

    </div>

    @if ($mostrar_info)
        <div class="fixed w-full h-full bg-black left-0 top-0 bg-opacity-50 flex justify-center items-center z-50">
            <div class="w-11/12 h-auto p-5 flex justify-center gap-3">
                <div class="w-8/12 h-auto p-5 bg-white rounded-md">
                    <div class="">
                        <h2 class="text-3xl text-morado-xante font-titillium-semibold">
                            {{ __('traduction.info_prop') . ' - ' . $propiedad->xanteid }}</h2>
                    </div>

                    <div class="flex w-full px-8 py-6 mt-4 border-t-2 border-b-2 " style="color: #b8d057">

                        <div class="flex flex-col w-full gap-3 max-h-96 pr-4 overflow-y-auto scroll_custom overflow-x-hidden">

                            <div class="w-full flex gap-8">

                                <div class="flex flex-col w-1/2 gap-2">
                                    <p class="text-gray-600 titilliumwebregular">
                                        Dirección:<br>
                                        <b class=" titilliumwebsemibold" style="color: #60338a">

                                            {{ $propiedad->neighborhood->type }}, {{ $propiedad->neighborhood->neighborhood }}, CP: {{ $propiedad->neighborhood->zip_code }}, {{ $propiedad->neighborhood->municipality->description }}, {{ $propiedad->neighborhood->municipality->state->description }}

                                        </b>
                                    </p>
                                    <p class="text-gray-600 titilliumwebregular">
                                        Tipo de propiedad:<br>
                                        <b class=" titilliumwebsemibold"
                                            style="color: #60338a">{{ $propiedad->property_type->name }}
                                        </b>
                                    </p>
                                    <p class="text-gray-600 titilliumwebregular">
                                        Xante ID:<br>
                                        <b class=" titilliumwebsemibold"
                                            style="color: #60338a">{{ $propiedad->xanteid }}
                                        </b>
                                    </p>
                                    <p class="text-gray-600 titilliumwebregular">
                                        Título:<br>
                                        <b class=" titilliumwebsemibold"
                                            style="color: #60338a">{{ $propiedad->title }}
                                        </b>
                                    </p>
                                    <p class="text-gray-600 titilliumwebregular">
                                        Subtítulo:
                                        <br> <b class=" titilliumwebsemibold" style="color: #60338a">
                                            {{ $propiedad->title2 }}
                                        </b>
                                    </p>
                                    <p class="text-gray-600 titilliumwebregular">
                                        Descripción:
                                        <br> <b class=" titilliumwebsemibold"
                                            style="color: #60338a">{{ $propiedad->description }}
                                        </b>
                                    </p>

                                </div>
                                <div class="flex flex-col w-1/2 gap-2">

                                    <p class="text-gray-600 titilliumwebregular">
                                        No. Exterior:<br>
                                        <b class=" titilliumwebsemibold" style="color: #60338a">
                                            {{ $propiedad->no_ext }}
                                        </b>
                                    </p>

                                    <p class="text-gray-600 titilliumwebregular">
                                        Calle:<br>
                                        <b class=" titilliumwebsemibold"
                                            style="color: #60338a">{{ $propiedad->street }}
                                        </b>
                                    </p>
                                    <p class="text-gray-600 titilliumwebregular">
                                        No. Interior:
                                        <br><b class=" titilliumwebsemibold" style="color: #60338a">
                                            {{ $propiedad->no_int }}
                                        </b>
                                    </p>
                                    <p class="text-gray-600 titilliumwebregular">
                                        No. de baños:
                                        <br><b class=" titilliumwebsemibold" style="color: #60338a">
                                            {{ $propiedad->bathrooms }}
                                        </b>
                                    </p>

                                    <p class="text-gray-600 titilliumwebregular">
                                        Medios baños: <br>
                                        <b class=" titilliumwebsemibold" style="color: #60338a">
                                            {{ $propiedad->half_bathrooms }}
                                        </b>
                                    </p>
                                    <p class="text-gray-600 titilliumwebregular">
                                        Dormitorios:
                                        <br>
                                        <b class=" titilliumwebsemibold" style="color: #60338a">
                                                {{ $propiedad->bedrooms }}

                                        </b>
                                    </p>
                                    <p class="text-gray-600 titilliumwebregular">
                                        Estacionamientos:<br>
                                        <b class=" titilliumwebsemibold" style="color: #60338a">
                                        {{ $propiedad->parking_slots  }}

                                        </b>
                                    </p>
                                    <p class="text-gray-600 titilliumwebregular">
                                        Estacionamientos:<br>
                                        <b class=" titilliumwebsemibold" style="color: #60338a">
                                        {{ $propiedad->parking_slots  }}

                                        </b>
                                    </p>
                                </div>

                            </div>

                            <div class="w-full flex gap-8">

                                <div class="flex flex-col w-1/2 gap-2">
                                    <p class="text-gray-600 titilliumwebregular">
                                        Metros cuadrados del terreno:<br>
                                        <b class=" titilliumwebsemibold" style="color: #60338a">
                                            {{ $propiedad->m2 }}
                                        </b>
                                    </p>
                                    <p class="text-gray-600 titilliumwebregular">
                                        Metros cuadrados de construcción:<br>
                                        <b class=" titilliumwebsemibold" style="color: #60338a">
                                            {{ $propiedad->m2_construction }}
                                        </b>
                                    </p>
                                    <p class="text-gray-600 titilliumwebregular">
                                        Metros cuadrados de jardín: <br>
                                        <b class=" titilliumwebsemibold" style="color: #60338a">
                                            {{ $propiedad->m2_garden }}
                                        </b>
                                    </p>
                                    <p class="text-gray-600 titilliumwebregular">
                                        No. de pisos: <br>
                                        <b class=" titilliumwebsemibold" style="color: #60338a">
                                            {{ $propiedad->floors }}
                                        </b>
                                    </p>
                                    {{-- <p class="text-gray-600 titilliumwebregular">
                                        Latitud: <br>
                                        <b class=" titilliumwebsemibold" style="color: #60338a">
                                            {{ $propiedad->lat }}
                                        </b>
                                    </p>
                                    <p class="text-gray-600 titilliumwebregular">
                                        Longitud: <br>
                                        <b class=" titilliumwebsemibold" style="color: #60338a">
                                            {{ $propiedad->lon }}
                                        </b>
                                    </p> --}}
                                    {{-- <p class="text-gray-600 titilliumwebregular">
                                        Recámaras: <br>
                                        <b class=" titilliumwebsemibold" style="color: #60338a">
                                            {{ $propiedad->number_rooms }}
                                        </b>
                                    </p> --}}
                                    <p class="text-gray-600 titilliumwebregular">
                                        Precio: <br>
                                        <b class="money titilliumwebsemibold" style="color: #60338a">
                                            {{ '$ ' . number_format($propiedad->price, 0, '.', ',') }}

                                        </b>
                                    </p>
                                    <p class="text-gray-600 titilliumwebregular">

                                        Ciudad: <br>
                                        <b class="money titilliumwebsemibold" style="color: #60338a">
                                            {{ $propiedad->city }}

                                        </b>
                                    </p>
                                </div>
                                <div class="flex flex-col w-1/2 gap-2">
                                    <p class="text-gray-600 titilliumwebregular">
                                        Código del mapa (iframe):<br>
                                        <b class=" titilliumwebsemibold" style="color: #60338a">
                                            {!! $propiedad->location_description !!}
                                        </b>
                                    </p>
                                    <br><br>
                                    <p class="text-gray-600 titilliumwebregular">
                                        Url del mapa: <br>
                                        <b class=" titilliumwebsemibold" style="color: #60338a">
                                            {{ $propiedad->map_url }}
                                        </b>
                                    </p>
                                    <p class="text-gray-600 titilliumwebregular">
                                        {{-- 0.- inactivo, 1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada --}}
                                        Estatus: <br>
                                        <b class=" titilliumwebsemibold" style="color: #60338a">
                                            @if ($propiedad->status == '0')
                                            <span class="font-titillium-semibold text-red-600">Inactivo</span>
                                            @elseif ($propiedad->status == '1')
                                                <span class="font-titillium-semibold text-green-500"> Disponible </span>
                                            @elseif ($propiedad->status == '2')
                                                <span class="font-titillium-semibold text-purple-300"> Apartada </span>
                                            @elseif ($propiedad->status == '3')
                                                <span class="font-titillium-semibold text-red-600"> Vendida </span>
                                            @elseif ($propiedad->status == '4')
                                                <span class="font-titillium-semibold text-yellow-400"> Escriturada </span>
                                            @elseif ($propiedad->status == '5')
                                                <span class="font-titillium-semibold text-red-600"> Entregada </span>
                                            @endif
                                        </b>
                                    </p>

                                    <p class="text-gray-600 titilliumwebregular">
                                        {{-- 0.- inactivo, 1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada --}}
                                        Destacado (Aparece en los primeros lugares): <br>
                                        <b class=" titilliumwebsemibold" style="color: #60338a">
                                            @if ($propiedad->outstanding == '0')
                                            <span class="font-titillium-semibold text-red-600">No</span>
                                            @elseif ($propiedad->outstanding == '1')
                                                <span class="font-titillium-semibold text-green-500"> Si </span>

                                            @endif
                                        </b>
                                    </p>

                                    <p class="text-gray-600 titilliumwebregular">
                                        Horario de atención: <br>
                                        <b class=" titilliumwebsemibold" style="color: #60338a">
                                            {{ $propiedad->atention_hours }}
                                        </b>
                                    </p>


                                    <p class="text-gray-600 titilliumwebregular">
                                        Fecha de creación: <br>
                                        <b class=" titilliumwebsemibold" style="color: #60338a">
                                            {{ $propiedad->created_at }}
                                        </b>
                                    </p>
                                    <p class="text-gray-600 titilliumwebregular">
                                        Descargar imágenes: <br>
                                    @if ($propiedad->images->count() > 0)
                                    <a href="/api/new_properties/{{ $propiedad->id }}/descargar" title="Descargar">
                                        <img src="/assets/images/svg/descargar_recursos_icon.svg" alt="" class="w-6 h-6 mx-auto">
                                    </a>
                                    @else
                                        <p class="text-center titilliumwebregular">
                                            {{ __('traduction.no_images') }}
                                        </p>
                                        {{-- <img src="/assets/images/svg/descargar_recursos_icon.svg" alt="" class="w-6 h-6 mx-auto"> --}}
                                    @endif
                                </p>
                                </div>


                            </div>
                        </div>

                    </div>

                    <div class="flex justify-center w-full">
                        @hasrole("Admin||EditorCompra")
                        {{-- <button class="px-8 py-2 mx-4 mt-6 text-xl text-white bg-green-500 border-4 border-white rounded-lg shadow-lg cursor-pointer hover:bg-white hover:border-green-500 hover:text-green-500 " wire:click="editarPropiedad({{ $propiedad->id }}); $set('mostrar_info',false)"> --}}
                            <button class="px-8 py-2 mx-4 mt-6 text-xl text-white bg-green-500 border-4 border-white rounded-lg shadow-lg cursor-pointer hover:bg-white hover:border-green-500 hover:text-green-500 " wire:click="closeVisualizarPropiedad">
                            Cerrar
                        </button>
                        @endhasrole
                    </div>

                </div>

                {{-- <div class="w-5 h-5 p-5 rounded-md flex items-center justify-center font-titillium-bold text-white bg-green-500 border-4 border-white cursor-pointer hover:bg-red-600" wire:click="$set('mostrar_info',false)">X</div> --}}
                <div class="w-5 h-5 p-5 rounded-md flex items-center justify-center font-titillium-bold text-white bg-green-500 border-4 border-white cursor-pointer hover:bg-red-600" wire:click="closeVisualizarPropiedad">X</div>
            </div>
        </div>
    @endif

    @if ($editar_info)
        <div class="fixed w-full h-full bg-black left-0 top-0 bg-opacity-50 flex justify-center items-center z-50">
            <div class="w-11/12 h-auto p-5 flex justify-center gap-3">
                <div class="w-8/12 h-auto p-5 bg-white rounded-md">
                    <div class="">
                        <h2 class="text-3xl text-morado-xante font-titillium-semibold">
                            {{'Editar Información de la propiedad - ' . $propiedad->id }}</h2>
                    </div>

                    <div class="flex w-full px-8 py-6 mt-4 border-t-2 border-b-2" style="color: #b8d057">

                        <div class="flex flex-col w-full gap-3 max-h-96 pr-4 overflow-y-auto scroll_custom overflow-x-hidden">

                            <div class="w-full flex gap-8">

                                <div class="flex flex-col w-1/2 gap-2">
                                    <p class="text-gray-600 titilliumwebregular">
                                        Colonia:<br>
                                        <b class=" titilliumwebsemibold" style="color: #60338a" >
                                            {{ $propiedad->neighborhood->type }}, {{ $propiedad->neighborhood->neighborhood }}, CP: {{ $propiedad->neighborhood->zip_code }}, {{ $propiedad->neighborhood->municipality->description }}, {{ $propiedad->neighborhood->municipality->state->description }}

                                        </b>

                                    </p>

                                    <p class="text-gray-600 titilliumwebregular">
                                        Cambiar Colonia:<br>
                                        <input type="text" name="" id="" wire:model.defer="neigbhbourhood_busca" wire:keydown.debounce.150ms='buscar' class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" placeholder="Escribe para buscar la colonia">
                                    </p>

                                    @if ($busqueda)
                                        <p class="text-gray-600 titilliumwebregular">

                                            Selecciona una colonia:<br>

                                            <select class="" id="neigbhbourhood_id" wire:model.defer="neigbhbourhood_id" >
                                                <option value="" selected>Selecciona</option>
                                                @foreach ($busqueda as $busqueda)
                                                <option value="{{ $busqueda->id }}" >{{ $busqueda->neighborhood }}, {{ $busqueda->zip_code }}, {{ $busqueda->municipality->description }}, {{ $busqueda->municipality->state->description }}</option>
                                                @endforeach

                                            </select>

                                        </p>
                                        <small class="text-rojo-default">{{ $errors->first('neigbhbourhood_id') }}</small>
                                    @endif

                                    <p class="text-gray-600 titilliumwebregular">
                                        Tipo de propiedad:<br>


                                        <select class="block appearance-none w-full  text-gray-700 py-3 px-4 pr-8 rounded leading-tight" id="property_type_id" name="property_type_id" wire:model.defer="property_type_id" >
                                            <option value=""  >Selecciona</option>
                                            <option value="1"  @if($propiedad->property_type_id == 1) selected @else @endif>Casa</option>
                                            <option value="2" @if($propiedad->property_type_id == 2) selected @else @endif>Departamento</option>

                                        </select>

                                    </p>

                                    <small class="text-rojo-default">{{ $errors->first('property_type_id') }}</small>

                                    <p class="text-gray-600 titilliumwebregular">
                                        Xante ID:<br>
                                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="xanteid" name="xanteid" type="text" wire:model.defer="xanteid" >

                                    </p>

                                    <small class="text-rojo-default">{{ $errors->first('xanteid') }}</small>
                                    <p class="text-gray-600 titilliumwebregular">
                                        Título:<br>
                                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="title" type="text" wire:model.defer="title" >

                                    </p>

                                    <small class="text-rojo-default">{{ $errors->first('title') }}</small>

                                    <p class="text-gray-600 titilliumwebregular">
                                        Subtítulo:
                                        <br>
                                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="title2" type="text" wire:model.defer="title2" >

                                    </p>

                                    <small class="text-rojo-default">{{ $errors->first('title2') }}</small>

                                    <p class="text-gray-600 titilliumwebregular">
                                        Descripción:
                                        <br>
                                        <div wire:ignore >
                                        <textarea class="shadow h-24 appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="editorEdita" name="editorEdita" wire:model.defer="description"  type="text" style="resize: none;"></textarea>
                                        </div>
                                    </p>
                                    <small class="text-rojo-default">{{ $errors->first('description') }}</small>
                                </div>

                                <div class="flex flex-col w-1/2 gap-2">
                                    <p class="text-gray-600 titilliumwebregular">
                                        No. Exterior:<br>

                                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="no_ext" type="text" wire:model.defer="no_ext" >
                                    </p>
                                    <small class="text-rojo-default">{{ $errors->first('no_ext') }}</small>
                                    <p class="text-gray-600 titilliumwebregular">
                                        Calle:<br>

                                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="street" type="text" wire:model.defer="street" >
                                    </p>
                                    <small class="text-rojo-default">{{ $errors->first('street') }}</small>
                                    <p class="text-gray-600 titilliumwebregular">
                                        No. Interior:
                                        <br>
                                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="no_int" type="text" wire:model.defer="no_int" >

                                    </p>
                                    <small class="text-rojo-default">{{ $errors->first('no_int') }}</small>
                                    <p class="text-gray-600 titilliumwebregular">
                                        No. de baños:

                                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="bathrooms" type="text" wire:model.defer="bathrooms" >
                                    </p>
                                    <small class="text-rojo-default">{{ $errors->first('bathrooms') }}</small>
                                    <p class="text-gray-600 titilliumwebregular">
                                        Medios baños: <br>

                                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="half_bathrooms" type="text" wire:model.defer="half_bathrooms" >
                                    </p>
                                    <small class="text-rojo-default">{{ $errors->first('half_bathrooms') }}</small>
                                    <p class="text-gray-600 titilliumwebregular">
                                        Dormitorios:
                                        <br>

                                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="bedrooms" type="text" wire:model.defer="bedrooms" >
                                    </p>
                                    <small class="text-rojo-default">{{ $errors->first('bedrooms') }}</small>
                                    <p class="text-gray-600 titilliumwebregular">
                                        Estacionamientos:<br>

                                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="parking_slots" type="text" wire:model.defer="parking_slots" >
                                    </p>
                                    <small class="text-rojo-default">{{ $errors->first('parking_slots') }}</small>
                                </div>

                            </div>

                            <div class="w-full flex gap-8">

                                <div class="flex flex-col w-1/2 gap-2">
                                    <p class="text-gray-600 titilliumwebregular">
                                        Metros cuadrados del terreno:<br>

                                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="m2" type="text" wire:model.defer="m2" >
                                    </p>
                                    <small class="text-rojo-default">{{ $errors->first('m2') }}</small>
                                    <p class="text-gray-600 titilliumwebregular">
                                        Metros cuadrados de construcción:<br>

                                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="m2_construction" type="text" wire:model.defer="m2_construction" >
                                    </p>
                                    <small class="text-rojo-default">{{ $errors->first('m2_construction') }}</small>
                                    <p class="text-gray-600 titilliumwebregular">
                                        Metros cuadrados de jardín: <br>

                                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="m2_garden" type="text" wire:model.defer="m2_garden" >
                                    </p>
                                    <small class="text-rojo-default">{{ $errors->first('m2_garden') }}</small>
                                    <p class="text-gray-600 titilliumwebregular">
                                        No. de pisos: <br>

                                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="floors" type="text" wire:model.defer="floors" >
                                    </p>
                                    <small class="text-rojo-default">{{ $errors->first('floors') }}</small>
                                    {{-- <p class="text-gray-600 titilliumwebregular">
                                        Latitud: <br>

                                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="lat" type="text" wire:model.defer="lat" >
                                    </p>
                                    <small class="text-rojo-default">{{ $errors->first('lat') }}</small>
                                    <p class="text-gray-600 titilliumwebregular">
                                        Longitud: <br>

                                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="lon" type="text" wire:model.defer="lon" >
                                    </p>
                                    <small class="text-rojo-default">{{ $errors->first('lon') }}</small> --}}
                                    {{-- <p class="text-gray-600 titilliumwebregular">
                                        Recámaras: <br>

                                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="number_rooms" type="text" wire:model.defer="number_rooms" >
                                    </p>
                                    <small class="text-rojo-default">{{ $errors->first('number_rooms') }}</small> --}}
                                    <p class="text-gray-600 titilliumwebregular">
                                        Precio: <br>

                                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="price" type="text" wire:model.defer="price" >
                                    </p>
                                    <small class="text-rojo-default">{{ $errors->first('price') }}</small>

                                    <p class="text-gray-600 titilliumwebregular">
                                        Ciudad: <br>

                                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="city" type="text" wire:model.defer="city" >
                                    </p>
                                    <small class="text-rojo-default">{{ $errors->first('city') }}</small>
                                </div>

                                <div class="flex flex-col w-1/2 gap-2">
                                    <p class="text-gray-600 titilliumwebregular">
                                        Código del mapa (iframe):<br>
                                        <b class=" titilliumwebsemibold" style="color: #60338a">

                                        </b>
                                        <br>
                                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="location_description" type="text" wire:model.defer="location_description" >
                                    </p>
                                    <small class="text-rojo-default">{{ $errors->first('location_description') }}</small>
                                    {!! $propiedad->location_description !!}
                                    <br>
                                    <p class="text-gray-600 titilliumwebregular">
                                        Url del mapa: <br>

                                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="map_url" type="text" wire:model.defer="map_url" >
                                    </p>
                                    <small class="text-rojo-default">{{ $errors->first('map_url') }}</small>
                                     {{-- 0.- inactivo, 1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada --}}
                                     {{-- @hasrole("Admin") --}}
                                    <p class="text-gray-600 titilliumwebregular">
                                        Estatus: <br>

                                        <select class="block appearance-none w-full text-gray-700 py-3 px-4 pr-8 rounded leading-tight" id="status" wire:model.defer="status" >
                                            <option value="0" @if($propiedad->status == 0) selected @else @endif>Inactivo</option>
                                            <option value="1" @if($propiedad->status == 1) selected @else @endif>Disponible</option>
                                            <option value="2" @if($propiedad->status == 2) selected @else @endif>Apartada</option>
                                            <option value="3" @if($propiedad->status == 3) selected @else @endif>Vendida</option>
                                            <option value="4" @if($propiedad->status == 4) selected @else @endif>Escriturada</option>
                                            <option value="5" @if($propiedad->status == 5) selected @else @endif>Entregada</option>
                                        </select>
                                    </p>
                                    <small class="text-rojo-default">{{ $errors->first('status') }}</small>

                                    <p class="text-gray-600 titilliumwebregular">
                                        Destacado (Aparece en los primeros lugares): <br>

                                        <select class="block appearance-none w-full text-gray-700 py-3 px-4 pr-8 rounded leading-tight" id="outstanding" wire:model.defer="outstanding" >
                                            <option value="0" @if($propiedad->outstanding == 0) selected @else @endif>No</option>
                                            <option value="1" @if($propiedad->outstanding == 1) selected @else @endif>Si</option>

                                        </select>
                                    </p>
                                    <small class="text-rojo-default">{{ $errors->first('outstanding') }}</small>

                                    {{-- @endhasrole --}}

                                    <p class="text-gray-600 titilliumwebregular">
                                        Horario de atención: <br>

                                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="add_attention" type="text" wire:model.defer="add_attention" >
                                    </p>
                                    <small class="text-rojo-default">{{ $errors->first('add_attention') }}</small>

                                    <p class="text-gray-600 titilliumwebregular">
                                        Fecha de creación: <br>
                                        <b class=" titilliumwebsemibold" style="color: #60338a">
                                            {{ $propiedad->created_at }}
                                        </b>

                                    </p>


                                </div>

                            </div>

                        </div>

                    </div>

                    {{-- <div class="flex justify-center w-full">
                        @for ($i = 1; $i <= count($propiedad->images); $i++)

                            <div class="bg-center bg-cover flex justify-center items-center bg_oscuro relative"  >
                                @foreach ($propiedad->images as $imagen)
                                <div>
                                    <img src="{{ storage_path('app/'.$imagen->url) }}{{ $imagen->nombre }}" alt=""
                                        class="w-full">
                                </div>
                                @endforeach

                            </div>

                        @endfor
                    </div> --}}

                    <div class="flex justify-center w-full">
                        <button class="px-8 py-2 mx-4 mt-6 text-xl text-white bg-green-500 border-4 border-white rounded-lg shadow-lg cursor-pointer hover:bg-white hover:border-green-500 hover:text-green-500" id="btn_edita" name="btn_edita" wire:click="edit" wire:loading.class='disabled' wire:loading.attr='disabled'>
                            Guardar
                        </button>
                    </div>

                </div>
                <div class="w-5 h-5 p-5 rounded-md flex items-center justify-center font-titillium-bold text-white bg-green-500 border-4 border-white cursor-pointer hover:bg-red-600"
                    wire:click="closeEditarPropiedad">X</div>
            </div>
        </div>
    @endif

   @if ($nueva_propiedad)

    <div class="fixed w-full h-full bg-black left-0 top-0 bg-opacity-50 flex justify-center items-center z-50">

        <div class="w-11/12 h-auto p-5 flex justify-center gap-3">

            <div class="w-8/12 h-auto p-5 bg-white rounded-md">

                <div class="">
                    <h2 class="text-3xl text-morado-xante font-titillium-semibold">
                        {{'Dar de alta nueva propiedad'}}</h2>
                </div>

                <div class="flex w-full px-8 py-6 mt-4 border-t-2 border-b-2 " style="color: #b8d057">

                    <div class="flex flex-col w-full gap-3 max-h-96 pr-4 overflow-y-auto scroll_custom overflow-x-hidden">

                        <div class="w-full flex gap-8">

                            <div class="flex flex-col w-1/2 gap-2">
                                <p class="text-gray-600 titilliumwebregular">
                                    Busca una colonia:<br>
                                    <input type="text" name="" id="" wire:model.defer="neigbhbourhood_busca" wire:keydown.debounce.150ms='buscar' class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" placeholder="Escribe para buscar la colonia">

                                </p>
                                <small class="text-rojo-default">{{ $errors->first('neigbhbourhood_id') }}</small>
                                @if ($busqueda)
                                <p class="text-gray-600 titilliumwebregular">

                                    Selecciona una colonia:<br>

                                    <select class="block appearance-none w-full  text-gray-700 py-3 px-4 pr-8 rounded leading-tight" id="neigbhbourhood_id" wire:model="neigbhbourhood_id" >
                                        <option value="" selected>Selecciona</option>
                                        @foreach ($busqueda as $busqueda)
                                        <option value="{{ $busqueda->id }}" >{{ $busqueda->neighborhood }}, {{ $busqueda->zip_code }}, {{ $busqueda->municipality->description }}, {{ $busqueda->municipality->state->description }}</option>
                                        @endforeach

                                    </select>

                                    </p>

                                @endif
                                <p class="text-gray-600 titilliumwebregular">
                                    Tipo de propiedad:<br>
                                    <select class="block appearance-none w-full  text-gray-700 py-3 px-4 pr-8 rounded leading-tight" id="property_type_id" wire:model.defer="property_type_id" >
                                        <option value="" selected>Selecciona</option>
                                        <option value="1" >Casa</option>
                                        <option value="2" >Departamento</option>
                                    </select>

                                </p>
                                <small class="text-rojo-default">{{ $errors->first('property_type_id') }}</small>
                                <p class="text-gray-600 titilliumwebregular">
                                    Xante ID:<br>
                                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="xanteid" name="xanteid" type="text" wire:model.defer="xanteid" >


                                </p>
                                <small class="text-rojo-default">{{ $errors->first('xanteid') }}</small>
                                <p class="text-gray-600 titilliumwebregular">
                                    Título:<br>
                                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="title" type="text" wire:model.defer="title" >

                                </p>
                                <small class="text-rojo-default">{{ $errors->first('title') }}</small>
                                <p class="text-gray-600 titilliumwebregular">
                                    Subtítulo:
                                    <br>
                                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="title2" type="text" wire:model.defer="title2" >

                                </p>
                                <small class="text-rojo-default">{{ $errors->first('title2') }}</small>
                                <div wire:ignore >
                                <p class="text-gray-600 titilliumwebregular">
                                    Descripción:
                                    <br>
                                    <textarea style="resize: none" class="shadow h-28 appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="editor" id="editor" wire:model.defer="description"  type="text" ></textarea>
                                </p>
                                </div>
                                <small class="text-rojo-default">{{ $errors->first('description') }}</small>
                            </div>

                            <div class="flex flex-col w-1/2 gap-2">

                                <p class="text-gray-600 titilliumwebregular">
                                    No. Exterior:<br>

                                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="no_ext" type="text" wire:model.defer="no_ext" >
                                </p>
                                <small class="text-rojo-default">{{ $errors->first('no_ext') }}</small>
                                <p class="text-gray-600 titilliumwebregular">
                                    Calle:<br>

                                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="street" type="text" wire:model.defer="street" >
                                </p>
                                <small class="text-rojo-default">{{ $errors->first('street') }}</small>
                                <p class="text-gray-600 titilliumwebregular">
                                    No. Interior:
                                    <br>
                                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="no_int" type="text" wire:model.defer="no_int" >

                                </p>
                                <small class="text-rojo-default">{{ $errors->first('no_int') }}</small>
                                <p class="text-gray-600 titilliumwebregular">
                                    No. de baños:

                                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="bathrooms" type="text" wire:model.defer="bathrooms" >
                                </p>
                                <small class="text-rojo-default">{{ $errors->first('bathrooms') }}</small>
                                <p class="text-gray-600 titilliumwebregular">
                                    Medios baños: <br>

                                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="half_bathrooms" type="text" wire:model.defer="half_bathrooms" >
                                </p>
                                <small class="text-rojo-default">{{ $errors->first('half_bathrooms') }}</small>
                                <p class="text-gray-600 titilliumwebregular">
                                    Dormitorios:
                                    <br>

                                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="bedrooms" type="text" wire:model.defer="bedrooms" >
                                </p>
                                <small class="text-rojo-default">{{ $errors->first('bedrooms') }}</small>
                                <p class="text-gray-600 titilliumwebregular">
                                    Estacionamientos:<br>

                                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="parking_slots" type="text" wire:model.defer="parking_slots" >
                                </p>
                                <small class="text-rojo-default">{{ $errors->first('parking_slots') }}</small>
                            </div>

                        </div>

                        <div class="w-full flex gap-8">

                            <div class="flex flex-col w-1/2 gap-2">
                                <p class="text-gray-600 titilliumwebregular">
                                    Metros cuadrados del terreno:<br>

                                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="m2" type="text" wire:model.defer="m2" >
                                </p>
                                <small class="text-rojo-default">{{ $errors->first('m2') }}</small>
                                <p class="text-gray-600 titilliumwebregular">
                                    Metros cuadrados de construcción:<br>

                                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="m2_construction" type="text" wire:model.defer="m2_construction" >
                                </p>
                                <small class="text-rojo-default">{{ $errors->first('m2_construction') }}</small>
                                <p class="text-gray-600 titilliumwebregular">
                                    Metros cuadrados de jardín: <br>

                                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="m2_garden" type="text" wire:model.defer="m2_garden" >
                                </p>
                                <small class="text-rojo-default">{{ $errors->first('m2_garden') }}</small>
                                <p class="text-gray-600 titilliumwebregular">
                                    No. de pisos: <br>

                                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="floors" type="text" wire:model.defer="floors" >
                                </p>
                                <small class="text-rojo-default">{{ $errors->first('floors') }}</small>
                                {{-- <p class="text-gray-600 titilliumwebregular">
                                    Latitud: <br>

                                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="lat" type="text" wire:model.defer="lat" >
                                </p>
                                <small class="text-rojo-default">{{ $errors->first('lat') }}</small>
                                <p class="text-gray-600 titilliumwebregular">
                                    Longitud: <br>

                                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="lon" type="text" wire:model.defer="lon" >
                                </p>
                                <small class="text-rojo-default">{{ $errors->first('lon') }}</small> --}}

                            </div>
                            <div class="flex flex-col w-1/2 gap-2">
                                {{-- <p class="text-gray-600 titilliumwebregular">
                                    Recámaras: <br>

                                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="number_rooms" type="text" wire:model.defer="number_rooms" >
                                </p>
                                <small class="text-rojo-default">{{ $errors->first('number_rooms') }}</small> --}}
                                <p class="text-gray-600 titilliumwebregular">
                                    Precio: <br>

                                    <input class="money shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="price" type="text" wire:model.defer="price" >
                                </p>
                                <small class="text-rojo-default">{{ $errors->first('price') }}</small>

                                <p class="text-gray-600 titilliumwebregular">
                                    Ciudad: <br>

                                    <input class="money shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="city" type="text" wire:model.defer="city" >
                                </p>
                                <small class="text-rojo-default">{{ $errors->first('city') }}</small>


                                <p class="text-gray-600 titilliumwebregular">
                                    Código del mapa (iframe):<br>
                                    <b class=" titilliumwebsemibold" style="color: #60338a">
                                        {{-- {!! $propiedad->location_description !!} --}}
                                    </b>
                                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="location_description" type="text" wire:model.defer="location_description" >
                                </p>
                                <small class="text-rojo-default">{{ $errors->first('location_description') }}</small>
                                <p class="text-gray-600 titilliumwebregular">
                                    Url del mapa: <br>

                                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="map_url" type="text" wire:model.defer="map_url" >
                                </p>
                                <small class="text-rojo-default">{{ $errors->first('map_url') }}</small>
                                {{-- @hasrole("Admin") --}}
                                    {{-- <p class="text-gray-600 titilliumwebregular">
                                        Estatus: <br>

                                        <select class="block appearance-none w-full  text-gray-700 py-3 px-4 pr-8 rounded leading-tight " id="status" wire:model.defer="status" >
                                            <option value="0" >Inactivo</option>
                                            <option value="1" >Disponible</option>
                                            <option value="2" >Apartada</option>
                                            <option value="3" >Vendida</option>
                                            <option value="4" >Escriturada</option>
                                            <option value="5" >Entregada</option>
                                        </select>
                                    </p>
                                    <small class="text-rojo-default">{{ $errors->first('status') }}</small> --}}
                                {{-- @endhasrole --}}

                                {{-- <p class="text-gray-600 titilliumwebregular">
                                    Horario de atención: <br>

                                    <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="add_attention" type="text" wire:model.defer="add_attention" >
                                </p>
                                <small class="text-rojo-default">{{ $errors->first('add_attention') }}</small> --}}



                            </div>

                        </div>

                    </div>

                </div>

                <div class="flex justify-center w-full">
                    <button
                        class="px-8 py-2 mx-4 mt-6 text-xl text-white bg-green-500 border-4 border-white rounded-lg shadow-lg cursor-pointer hover:bg-white hover:border-green-500 hover:text-green-500" id="btn_guarda" name="btn_guarda" wire:click="save" wire:loading.class='disabled' wire:loading.attr='disabled'>
                        Guardar
                    </button>
                </div>

            </div>

            <div class="w-5 h-5 p-5 rounded-md flex items-center justify-center font-titillium-bold text-white bg-green-500 border-4 border-white cursor-pointer hover:bg-red-600" wire:click="closeNuevaPropiedad">X</div>

        </div>

    </div>

   @endif

    @if ($modal_img_upload)
        <div class="fixed w-full h-full bg-black left-0 top-0 bg-opacity-50 flex justify-center items-center z-50">

            <div class="w-11/12 h-auto p-5 flex justify-center gap-3">

                <div class="w-8/12 h-auto p-5 bg-white rounded-md">

                    <div class="">
                        <h2 class="text-3xl text-morado-xante font-titillium-semibold">
                            {{'Subir imágenes de la propiedad - '.$propiedad_img->id}}</h2>
                    </div>

                    <div class="flex w-full px-8 py-6 mt-4 border-t-2 border-b-2 " style="color: #b8d057">

                        <div class="flex flex-col w-full gap-3 max-h-96 pr-4 overflow-y-auto scroll_custom overflow-x-hidden">

                            <div class="w-full flex gap-8">



                            </div>

                            <div class="w-full flex gap-8">


                                <div class="flex flex-col w-full gap-2">

                                    <small class="text-rojo-default">{{ $errors->first('status') }}</small>
                                    <p class="text-gray-600 titilliumwebregular text-2xl">
                                        *Recuerda seleccionar las fotos en el orden que deseas que aparezcan en la página siendo la primera la de fachada, se requiere un mínimo de 6 imágenes.
                                    </p>

                                    <div class="flex flex-col justify-center mt-4 items-center border-2 border-gray-600 text-gray-600 rounded-md  w-1/2 mx-auto p-8">

                                            @if($imagen_propiedad)
                                                <label for="imagen_propiedad" class="text-center flex flex-col justify-center items-center cursor-pointer text-indigo-700">

                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-16 h-16">
                                                        <path stroke-linecap="round" stroke-linejoin="round" d="M9 12.75L11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 01-1.043 3.296 3.745 3.745 0 01-3.296 1.043A3.745 3.745 0 0112 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 01-3.296-1.043 3.745 3.745 0 01-1.043-3.296A3.745 3.745 0 013 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 011.043-3.296 3.746 3.746 0 013.296-1.043A3.746 3.746 0 0112 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 013.296 1.043 3.746 3.746 0 011.043 3.296A3.745 3.745 0 0121 12z" />
                                                    </svg>

                                                    <p class="mt-4">
                                                        @foreach ($imagen_propiedad as $imagen)
                                                            {{ $imagen->getClientOriginalName().', ' }}
                                                        @endforeach
                                                    </p>
                                                </label>
                                            @else
                                                <label for="imagen_propiedad" class="text-center flex flex-col justify-center items-center cursor-pointer">

                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-16 h-16">
                                                        <path stroke-linecap="round" stroke-linejoin="round" d="M3 16.5v2.25A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75V16.5M16.5 12L12 16.5m0 0L7.5 12m4.5 4.5V3" />
                                                    </svg>

                                                    <p class="mt-4"> Elegir Archivos </p>
                                                </label>
                                            @endif

                                        <input hidden type="file" name="imagen_propiedad" id="imagen_propiedad" multiple wire:model.defer="imagen_propiedad" accept="image/jpeg, image/jpg, image/svg, image/png"/>

                                        <small class="text-rojo-default">{{ $errors->first('imagen_propiedad') }}</small>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="flex justify-center w-full">
                        @hasrole("Admin||EditorCompra")
                        <button
                            class="px-8 py-2 mx-4 mt-6 text-xl text-white bg-green-500 border-4 border-white rounded-lg shadow-lg cursor-pointer hover:bg-white hover:border-green-500 hover:text-green-500" id="btn_guarda_img" name="btn_guarda_img" wire:click="save_img" wire:loading.class='disabled' wire:loading.attr='disabled'>
                            Guardar
                        </button>
                        @endhasrole
                    </div>

                </div>

                <div class="w-5 h-5 p-5 rounded-md flex items-center justify-center font-titillium-bold text-white bg-green-500 border-4 border-white cursor-pointer hover:bg-red-600" wire:click="$set('modal_img_upload',false)">X</div>

            </div>

        </div>
    @endif

    @if($modal_see_img)
        <div class="fixed w-full h-full bg-black left-0 top-0 bg-opacity-50 flex justify-center items-center z-50">
            <div class="w-11/12 h-auto p-5 flex justify-center gap-3">

                <div class="w-8/12 h-auto p-5 bg-white rounded-md">

                    <div class="">
                        <h2 class="text-3xl text-morado-xante font-titillium-semibold">
                            {{'Imágenes de la propiedad - '.$propiedad_img->id}}
                        </h2>
                    </div>

                    <div class="flex w-full px-8 py-6 mt-4 border-t-2 border-b-2 " style="color: #b8d057">

                        <div class="flex flex-col w-full gap-3 max-h-96 pr-4 overflow-y-auto scroll_custom overflow-x-hidden">
                            <p class="text-2xl">Recuerda que debe haber minimo 6 imágenes por propiedad</p>
                            <div class="w-full flex gap-8"></div>

                            <div class="w-full flex gap-8">

                                <div class="flex flex-col w-full gap-2">

                                    <div class="grid grid-cols-5 gap-3 justify-center mt-4 items-center border-2 border-gray-600 text-gray-600 rounded-md  w-8/10 mx-auto p-4">

                                        {{-- <p>Foto de fachada -></p> --}}
                                        @foreach ($propiedad_img->images->sortBy('order') as $imagen)
                                            <div class="w-full relative">
                                                {{-- {{ dd($imagen->first()) }} --}}
                                                {{-- @if($imagen->first())<p>Foto de fachada</p>@endif --}}

                                                 @if ($loop->first)

                                                    <div class="absolute -top-5 left-14 text-morado">

                                                        <p>

                                                            Fachada

                                                        </p>

                                                    </div>

                                                 @endif
                                                 @hasrole("Admin||EditorCompra")
                                                <div class="w-10 h-10 p-2 rounded-md flex items-center justify-center font-titillium-bold text-white bg-green-500 border-2 border-white cursor-pointer hover:bg-red-600 absolute top-2 right-2" wire:click="delete_img({{$imagen}})">

                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-8 h-8">
                                                        <path stroke-linecap="round" stroke-linejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0" />
                                                    </svg>
                                                </div>
                                                @endhasrole

                                                <img src="{{ asset($imagen->url) }}" class="w-40 border-2 border-gray-500 rounded-md h-40" />
                                                <span>Orden </span>
                                                {{-- {{ $imagen->order }} --}}
                                                {{-- <select class="form-select block  appearance-none w-full text-gray-700 py-3 px-4 pr-8 rounded leading-tight" id="order"  wire:change="cambioOrden($event.target.value,{{$imagen->id}})" >
                                                @foreach ($propiedad_img->images as $select_order)
                                                <option value="{{ $select_order->order }}" @if($select_order->order == $imagen->order ) selected @else @endif>{{ $select_order->order  }}</option>
                                                @endforeach
                                                </select> --}}
                                                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="order" type="number" wire:change="cambioOrden($event.target.value,{{$imagen->id}})" placeholder="Ingresa el número de orden" value="{{ $imagen->order }}">

                                                {{-- <small class="text-rojo-default">{{ $errors->first('order') }}</small> --}}
                                            </div>
                                        @endforeach
                                        @hasrole("Admin||EditorCompra")
                                        <div class="w-40 h-40 relative border-2 border-gray-500 rounded-md flex items-center justify-center" title="Agregar imagen">
                                            <label for="imagen_propiedad_edit">
                                                @if($imagen_propiedad)
                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-20 h-20 cursor-pointer" title="">
                                                        <path stroke-linecap="round" stroke-linejoin="round" d="M9 12.75L11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 01-1.043 3.296 3.745 3.745 0 01-3.296 1.043A3.745 3.745 0 0112 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 01-3.296-1.043 3.745 3.745 0 01-1.043-3.296A3.745 3.745 0 013 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 011.043-3.296 3.746 3.746 0 013.296-1.043A3.746 3.746 0 0112 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 013.296 1.043 3.746 3.746 0 011.043 3.296A3.745 3.745 0 0121 12z" />
                                                    </svg>
                                                @else
                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-20 h-20 cursor-pointer" title="Agregar Imagen">
                                                        <path stroke-linecap="round" stroke-linejoin="round" d="M19.5 14.25v-2.625a3.375 3.375 0 00-3.375-3.375h-1.5A1.125 1.125 0 0113.5 7.125v-1.5a3.375 3.375 0 00-3.375-3.375H8.25m3.75 9v6m3-3H9m1.5-12H5.625c-.621 0-1.125.504-1.125 1.125v17.25c0 .621.504 1.125 1.125 1.125h12.75c.621 0 1.125-.504 1.125-1.125V11.25a9 9 0 00-9-9z" />
                                                    </svg>
                                                @endif
                                            </label>
                                            <input hidden type="file" name="imagen_propiedad_edit" id="imagen_propiedad_edit" multiple wire:model.defer="imagen_propiedad" accept="image/jpeg, image/jpg, image/svg, image/png"/>
                                        </div>
                                        @endhasrole
                                        <div class="w-40 h-40 border-2 border-gray-500 rounded-md flex items-center justify-center">
                                            <a href="/api/new_properties/{{ $propiedad_img->id }}/descargar" title="Descargar imágenes"><img src="/assets/images/svg/descargar_recursos_icon.svg" alt="" class="w-20 h-20 cursor-pointer"></a>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                    @if($imagen_propiedad)
                        <span class="">
                            {{ 'Imagenes nuevas: ' }}
                            @foreach ($imagen_propiedad as $imagen)
                                {{ $imagen->getClientOriginalName().', ' }}
                            @endforeach
                        </span>
                    @endif
                    <div class="flex justify-center w-full">
                        @hasrole("Admin||EditorCompra")
                        <button
                            @if($imagen_propiedad)
                                class="px-8 py-2 mx-4 mt-6 text-xl text-white bg-green-500 border-4 border-white rounded-lg shadow-lg cursor-pointer hover:bg-white hover:border-green-500 hover:text-green-500"
                            @else
                                class="px-8 py-2 mx-4 mt-6 text-xl text-white bg-green-500 border-4 border-white rounded-lg shadow-lg cursor-pointer hover:bg-white hover:border-green-500 hover:text-green-500 disabled"
                            @endif
                            id="btn_guarda_img"
                            name="btn_guarda_img"
                            wire:click="save_img"
                            wire:loading.class='disabled'
                            wire:loading.attr='disabled'>
                            Guardar
                        </button>
                        @endhasrole
                    </div>

                </div>

                <div class="w-5 h-5 p-5 rounded-md flex items-center justify-center font-titillium-bold text-white bg-green-500 border-4 border-white cursor-pointer hover:bg-red-600" wire:click="$set('modal_see_img',false)">X</div>

            </div>
        </div>
    @endif


    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .then(function(editor){
                editor.model.document.on('change:data', () =>{
                    @this.set('description', editor.getData());
                })

            })
            .catch( error => {
                console.error( error );
            } );
    </script>

    <script>
        ClassicEditor
            .create( document.querySelector( '#editorEdita' ) )
            .then(function(editorEdita){
                editorEdita.model.document.on('change:data', () =>{
                    @this.set('description', editorEdita.getData());
                })

            })
            .catch( error => {
                console.error( error );
            } );
    </script>
    {{-- <script>
         Livewire.on('initializeCkEditor', function () {
            ClassicEditor.create(document.getElementById('editor')).then(editor => { thisEditor = editor });
        });
    </script>
    <script>
        Livewire.on('initializeCkEditor', function () {
           ClassicEditor.create(document.getElementById('editorEdita')).then(editor => { thisEditor = editor });
       });
   </script> --}}

</div>



