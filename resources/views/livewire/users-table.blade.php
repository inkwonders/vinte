{{--  ID  --}}

<x-livewire-tables::table.cell>
    {{ $row->id }}
</x-livewire-tables::table.cell>

{{--  FECHA REGISTRO  --}}

<x-livewire-tables::table.cell>
    @if ($row->created_at != null)
        <span class="titilliumwebregular text-grayTable">{{ $row->created_at->format('d / m / Y') }}</span>
    @else
        <span class="titilliumwebregular text-grayTable">{{ __('traduction.no_date') }}</span>
    @endif
</x-livewire-tables::table.cell>

{{--  NOMBRE  --}}

<x-livewire-tables::table.cell>
    {{ $row->nombre_propietario }}
</x-livewire-tables::table.cell>

{{--  TELEFONO  --}}

<x-livewire-tables::table.cell>
    @php
        $numero_tel = sprintf('%s-%s-%s', substr($row->telefono, 0, 3), substr($row->telefono, 3, 3), substr($row->telefono, 6));
    @endphp
    <span class="titilliumwebregular">
        {{ $numero_tel }}
    </span>
</x-livewire-tables::table.cell>

{{--  NOMBRE DESARROLLO  --}}

<x-livewire-tables::table.cell>
    <span class="titilliumwebregular">
        {{ $row->nombre_desarrollo }}
    </span>
</x-livewire-tables::table.cell>

{{--  CASA O DEPARTAMENTO  --}}

<x-livewire-tables::table.cell>
    <span class="titilliumwebregular">
        @if ($row->tipo_propiedad == 'Casa')
            {{ __('traduction.house') }}
        @else
            {{ __('traduction.department') }}
        @endif
    </span>
</x-livewire-tables::table.cell>

{{--  ESTADO  --}}

<x-livewire-tables::table.cell>
    <span class="titilliumwebregular">
        {{ $row->estado }}
    </span>
</x-livewire-tables::table.cell>

{{--  CODIGO POSTAL  --}}

<x-livewire-tables::table.cell>
    <span class="titilliumwebregular">
        {{ $row->cp_colonia }}
    </span>
</x-livewire-tables::table.cell>

{{--  PRECIO  --}}

<x-livewire-tables::table.cell>
    <span class="titilliumwebregular">
        {{ '$ ' . number_format($row->precio, 2, '.', ',') }}
    </span>
</x-livewire-tables::table.cell>

{{--  ESTATUS  --}}

<x-livewire-tables::table.cell>
    <div class="w-full flex items-center justify-between gap-2">
        @if ($row->estatus == 'Activo')
            <span class="font-titillium-semibold text-green-500"> {{ __('traduction.active') }} </span>
        @else
            <span class="font-titillium-semibold text-red-600"> {{ __('traduction.discarded') }} </span>
        @endif
        <svg xmlns="http://www.w3.org/2000/svg" class="h-7 w-7 cursor-pointer" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" wire:click="cambioEstatus({{$row->id}})" title="">
            <title>{{ __('traduction.change') }}</title>
            <path stroke-linecap="round" stroke-linejoin="round" d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15" />
        </svg>
    </div>
</x-livewire-tables::table.cell>

{{-- VER MAS --}}

<x-livewire-tables::table.cell>
    <img src="/assets/images/svg/ver_mas_icon.svg" alt="" class="w-6 h-6 mx-auto cursor-pointer"
        wire:click="mostrarPropiedad({{ $row->id }})">
</x-livewire-tables::table.cell>

{{-- DESCARGAR FOTOS --}}

<x-livewire-tables::table.cell>
    @if ($row->images->count() > 0)
        <a href="/api/propiedades/{{ $row->id }}/descargar" title="Descargar">
            <img src="/assets/images/svg/descargar_recursos_icon.svg" alt="" class="w-6 h-6 mx-auto">
        </a>
    @else
        <p class="text-center titilliumwebregular">
            {{ __('traduction.no_images') }}
        </p>
        {{-- <img src="/assets/images/svg/descargar_recursos_icon.svg" alt="" class="w-6 h-6 mx-auto"> --}}
    @endif
</x-livewire-tables::table.cell>
