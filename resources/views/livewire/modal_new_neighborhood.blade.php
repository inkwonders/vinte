<div class="flex w-full py-10">
    {{--  <div class="w-1/3">
        <a href="/">
            <img src="/assets/images/svg/vente_tucasa_dos.png" alt="" class="w-2/6">
        </a>
    </div>  --}}
    <div class="flex items-center justify-end w-full">
        <button class="px-8 py-2 mx-2 text-white border-4 border-white rounded-lg shadow-lg cursor-pointer bg-red-500" wire:click="new_user()">
            {{__('traduction.new_neighborhood')}}
        </button>
        <div class="relative flex">
            <svg xmlns="http://www.w3.org/2000/svg" class="absolute w-4 h-4 top-4 left-6" fill="none" viewBox="0 0 24 24"
                stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
            </svg>
            <input wire:model="filters.search" type="text"
                class="h-12 pl-10 mx-4 border-t-0 border-l-0 border-r-0 titilliumwebsemibold focus:outline-none focus:ring-0"
                placeholder="{{ __('traduction.search') }}">
        </div>
        {{-- <input type="date" class="h-12 mx-4 border-t-0 border-l-0 border-r-0 titilliumwebsemibold focus:outline-none"
            onchange="emit_fechas()" wire:model.defer.defer="inicio" id="inicio" required placeholder="Fecha Inicio">
        <input type="date" class="h-12 mx-4 border-t-0 border-l-0 border-r-0 titilliumwebsemibold focus:outline-none"
            onchange="emit_fechas()" wire:model.defer.defer="fin" id="fin" required placeholder="Fecha Termino"> --}}

        <a href="{{ route('logout') }}"
            class="px-8 py-2 mx-4 text-white border-4 border-white rounded-lg shadow-lg cursor-pointer hover:bg-white "
            style="background-color: #60338a">{{ __('traduction.logout') }}
        </a>
        @if (App::getLocale() == 'en')
            <a href="/locale/es"
                class="px-8 py-2 mx-4 text-white border-4 border-white rounded-lg shadow-lg cursor-pointer bg-green-500">
                {{ 'Español' }}
            </a>
        @else
            <a href="/locale/en"
                class="px-8 py-2 mx-2 text-white border-4 border-white rounded-lg shadow-lg cursor-pointer bg-green-500">
                {{ 'English' }}
            </a>
        @endif

    </div>

   @if ($nuevo_usuario)


    <div class="fixed w-full h-full bg-black left-0 top-0 bg-opacity-50 flex justify-center items-center z-50">
        <div class="w-1/2 h-auto p-5 flex justify-center gap-3">
            <div class="w-full h-auto p-5 bg-white rounded-md">
                <div class="">
                    <h2 class="text-3xl text-morado-xante font-titillium-semibold">
                        {{ __('traduction.new_neighborhood') }}</h2>
                </div>
                <div class="flex w-full px-8 py-6 mt-4 border-t-2 border-b-2 " style="color: #b8d057">
                    <div class="flex w-full gap-3">

                        <div class="flex flex-col w-full gap-2">

                            <p class="text-gray-600 titilliumwebregular">
                                {{ __('traduction.neighborhood') }}:<br>
                                <input type="text" name="" id="" wire:model.defer="new_neighborhood" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" placeholder=" {{ __('traduction.neighborhood') }}">

                            </p>
                            <small class="text-rojo-default">
                                @error('new_neighborhood')
                                    <span class="text-rojo-default text-sm">{{ $message }}</span>
                                @enderror
                            </small>


                            <p class="text-gray-600 titilliumwebregular">
                                {{ __('traduction.municipality') }}<br>
                                <select class="block appearance-none w-full text-gray-700 py-3 px-4 pr-8 rounded leading-tight" id="" wire:model.defer="new_municipality" >
                                    <option value="" selected>Selecciona</option>

                                    @foreach ($municipalities as $municipality )

                                    <option value="{{ $municipality->id }}" >{{ $municipality->state->description." / ".$municipality->description }}</option>

                                    @endforeach

                                  </select>

                            </p>

                            <small class="text-rojo-default">
                                @error('new_municipality')
                                    <span class="text-rojo-default text-sm">{{ $message }}</span>
                                @enderror
                            </small>

                            <p class="text-gray-600 titilliumwebregular">
                                {{ __('traduction.cp') }}:<br>
                                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" type="text" name="cp" wire:model.defer="new_cp" >

                            </p>

                            <small class="text-rojo-default">
                                @error('new_cp')
                                <span class="text-rojo-default text-sm">{{ $message }}</span>
                            @enderror
                            </small>

                            <p class="text-gray-600 titilliumwebregular">
                                {{ __('traduction.neighborhood_type') }}:<br>
                                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" type="text" name="cp" wire:model.defer="new_type" >

                            </p>

                            <small class="text-rojo-default">
                                @error('new_type')
                                <span class="text-rojo-default text-sm">{{ $message }}</span>
                            @enderror
                            </small>

                        </div>

                    </div>

                </div>
                <div class="flex justify-center w-full">
                    <button
                        class="px-8 py-2 mx-4 mt-6 text-xl text-white bg-green-500 border-4 border-white rounded-lg shadow-lg cursor-pointer hover:bg-white hover:border-green-500 hover:text-green-500" id="btn_guarda" name="btn_guarda" wire:click="save" wire:loading.class='disabled' wire:loading.attr='disabled'>
                        Guardar
                </button>
                </div>
            </div>
            <div class="w-5 h-5 p-5 rounded-md flex items-center justify-center font-titillium-bold text-white bg-green-500 border-4 border-white cursor-pointer hover:bg-red-600"
                wire:click="$set('nuevo_usuario',false)">X</div>
        </div>
    </div>
   @endif


   @if ($modal_edit)

   <div class="fixed w-full h-full bg-black left-0 top-0 bg-opacity-50 flex justify-center items-center z-50">
    <div class="w-1/2 h-auto p-5 flex justify-center gap-3">
        <div class="w-full h-auto p-5 bg-white rounded-md">
            <div class="">
                <h2 class="text-3xl text-morado-xante font-titillium-semibold">
                    {{ __('traduction.new_neighborhood') }}</h2>
            </div>
            <div class="flex w-full px-8 py-6 mt-4 border-t-2 border-b-2 " style="color: #b8d057">
                <div class="flex w-full gap-3">

                    <div class="flex flex-col w-full gap-2">

                        <p class="text-gray-600 titilliumwebregular">
                            {{ __('traduction.neighborhood') }}:<br>
                            <input type="text" name="" id="" wire:model.defer="edit_neighborhood" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" placeholder=" {{ __('traduction.neighborhood') }}">

                        </p>
                        <small class="text-rojo-default">
                            @error('edit_neighborhood')
                                <span class="text-rojo-default text-sm">{{ $message }}</span>
                            @enderror
                        </small>


                        <p class="text-gray-600 titilliumwebregular">
                            {{ __('traduction.municipality') }}<br>
                            <select class="block appearance-none w-full text-gray-700 py-3 px-4 pr-8 rounded leading-tight" id="" wire:model.defer="edit_municipality" >
                                <option value="" selected>Selecciona</option>

                                @foreach ($municipalities as $municipality )

                                <option value="{{ $municipality->id }}" >{{ $municipality->state->description." / ".$municipality->description }}</option>

                                @endforeach

                              </select>

                        </p>

                        <small class="text-rojo-default">
                            @error('edit_municipality')
                                <span class="text-rojo-default text-sm">{{ $message }}</span>
                            @enderror
                        </small>

                        <p class="text-gray-600 titilliumwebregular">
                            {{ __('traduction.cp') }}:<br>
                            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" type="text" name="cp" wire:model.defer="edit_cp" >

                        </p>

                        <small class="text-rojo-default">
                            @error('edit_cp')
                            <span class="text-rojo-default text-sm">{{ $message }}</span>
                        @enderror
                        </small>

                        <p class="text-gray-600 titilliumwebregular">
                            {{ __('traduction.neighborhood_type') }}:<br>
                            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" type="text" name="cp" wire:model.defer="edit_type" >

                        </p>

                        <small class="text-rojo-default">
                            @error('edit_type')
                            <span class="text-rojo-default text-sm">{{ $message }}</span>
                        @enderror
                        </small>

                    </div>

                </div>

            </div>
            <div class="flex justify-center w-full">
                <button
                    class="px-8 py-2 mx-4 mt-6 text-xl text-white bg-green-500 border-4 border-white rounded-lg shadow-lg cursor-pointer hover:bg-white hover:border-green-500 hover:text-green-500" id="btn_guarda" name="btn_guarda" wire:click="edit" wire:loading.class='disabled' wire:loading.attr='disabled'>
                    Guardar
            </button>
            </div>
        </div>
        <div class="w-5 h-5 p-5 rounded-md flex items-center justify-center font-titillium-bold text-white bg-green-500 border-4 border-white cursor-pointer hover:bg-red-600"
            wire:click="$set('modal_edit',false)">X</div>
    </div>
</div>

    @endif



</div>
