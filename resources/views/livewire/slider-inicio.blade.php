@foreach ($casas as $casa)

    <div class="lg:mx-4">

        <div class="flex w-full mx-auto justify-center">

            <a  class="relative text-black bg-white rounded-md w-full shadow-lg mx-8">

                <div class="relative w-full h-full bg-white border rounded-md shadow-xl p-2">

                    <div class="relative w-full">

                        @if (!empty($casa->images[0]))

                            <div style="background-image: url(/{{$casa->images[0]->url}})"  class="w-full h-72 bg-center bg-no-repeat bg-cover" ></div>

                        @else

                            <div class="w-full h-72 bg-center bg-no-repeat bg-cover" ></div>

                        @endif

                    </div>

                    <div class="relative flex items-center justify-between px-4 pt-2 font-montserrat-bold">

                        <div class="flex justify-between w-full text-sm items-center border-b-4 pb-2 border-black">

                            <p class="font-montserrat-regular">

                                {{--  {{ $casas[$i]['tipo'] }}  --}}

                                <br>

                                {{--  {{ $casas[$i]['titulo'] }}  --}}

                            </p>

                            <p class="text-base font-montserrat-bold">

                                ${{ number_format($casa->price,0) }}*

                            </p>


                        </div>

                    </div>

                    <div class="relative justify-between w-full px-2 gap-4 py-4 flex font-montserrat-bold">

                        <div class="flex gap-4 text-sm w-8/12 font-montserrat-regular">

                            <div class="flex justify-center items-center flex-col gap-1 w-16">

                                <div class="w-9 h-9 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                    <img src="/assets/images/png/iconos/metros_icon.png" alt="" class="h-4 w-4 mt-2">

                                </div>

                                <p class="text-xs">

                                    {{ $casa->m2_construction }}

                                </p>

                            </div>

                            <div class="flex justify-center items-center flex-col gap-1 w-16">

                                <div class="w-9 h-9 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                    <img src="/assets/images/png/iconos/habitaciones_icon.png" alt="" class="h-4 w-4 mt-2">

                                </div>

                                <p class="text-xs">

                                    {{ $casa->bedrooms }}

                                </p>

                            </div>

                            <div class="flex justify-center items-center flex-col gap-1 w-16">

                                <div class="w-9 h-9 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                    <img src="/assets/images/png/iconos/banios_icon.png" alt="" class="h-4 w-4 mt-2">

                                </div>

                                <p class="text-xs">

                                    {{ $casa->bathrooms }}

                                </p>

                            </div>

                            <div class="flex justify-center items-center flex-col gap-1 w-16">

                                <div class="w-9 h-9 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                    <img src="/assets/images/png/iconos/estacionamiento_icon.png" alt="" class="h-4 w-4 mt-2">

                                </div>

                                <p class="text-xs">

                                    {{ $casa->parking_slots }}

                                </p>

                            </div>

                        </div>

                        <div class="flex justify-center items-center font-montserrat-regular gap-2 ml-4 w-4/12 text-xs ">

                            <img src="/assets/images/png/iconos/mapa_icon.png" alt="" class="h-6">
                            {{ $casa->city }}
                            {{-- {{ $casa->neighborhood->municipality->description }} --}}
                            {{-- , {{ $casa->neighborhood->municipality->state->description }} --}}

                        </div>

                    </div>

                </div>

            </a>

        </div>

    </div>

@endforeach
