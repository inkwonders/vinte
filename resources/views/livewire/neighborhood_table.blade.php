{{--  ID  --}}


<x-livewire-tables::table.cell>
    {{ $row->id }}
</x-livewire-tables::table.cell>


{{--  municipality  --}}

<x-livewire-tables::table.cell>
    {{ $row->description }}
</x-livewire-tables::table.cell>


{{-- neighborhood --}}
<x-livewire-tables::table.cell>
    {{ $row->neighborhood }}
</x-livewire-tables::table.cell>


{{-- tipo --}}


{{--  FECHA REGISTRO  --}}

<x-livewire-tables::table.cell>


        <span class="titilliumwebregular text-grayTable">{{ $row->neighborhood_type }}</span>

</x-livewire-tables::table.cell>




{{-- acciones --}}

<x-livewire-tables::table.cell>




            <div class="flex justify-center items-center gap-8">

                <a  wire:click="deleteConfirm({{ $row }})">

                    <img src="/assets/images/svg/delete.svg" alt="" class="w-6 h-6 mx-auto cursor-pointer">

                </a>

                <a wire:click="modalEdit({{ $row }})">

                    <img src="/assets/images/svg/edit.svg" alt="" class="w-6 h-6 mx-auto cursor-pointer">

                </a>

            </div>

</x-livewire-tables::table.cell>












