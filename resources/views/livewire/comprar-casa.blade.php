<div>

    <div class="flex flex-col pb-20 pt-28">

        <div class="relative z-30 flex items-center justify-center w-full h-8 text-sm text-center text-white">

            <a target="_blank" href="https://wa.link/om0mki" class="fixed w-16 h-16 bottom-2 right-2 ">

                <img src="/assets/images/png/whats.png" class="absolute h-full">

            </a>

        </div>

        {{--  textos de compra tu casa verde y blanco  --}}

        <div class="relative flex flex-col items-center justify-center gap-6 text-center">

            <h2 class="text-6xl text-verde-landing font-montserrat-bold">
                {{ __('landing.buy_h') }}
            </h2>

            <p class="text-sm text-white lg:text-morado-xante  lg:text-xl font-montserrat-regular" style="text-shadow: 2px 0px 5px rgba(150, 150, 150, 0.76);">

                {!! __('landing.buy_d_h') !!}

            </p>

        </div>

        {{--  terminan textos de compra tu casa verde y blanco  --}}

        {{--  textos de conoce props blanco recuadro morado  --}}

        <div class="mt-16 w-11/12 2xl:w-10/12 mx-auto flex justify-center items-center bg-purpura-xante py-8 ">

            <h1 class="text-white text-4xl 2xl:text-5xl font-montserrat-bold text-center">

                {{ __('landing.xan_nuestras_prop') }}

            </h1>

        </div>

        {{--  terminan textos de conoce props blanco recuadro morado  --}}

        {{--  cuadro blanco propiedades  --}}

        <div class="lg:bg-white py-16 w-full lg:w-11/12 2xl:w-10/12 mx-auto shadow-lg  lg:block">

            <div class="justify-around  w-full 2xl:w-11/12 2xl:gap-16 px-2 lg:px-8 gap-10 mx-auto grid grid-cols-1 lg:grid-cols-3">

                @foreach ($casas as $casa)

                    @if ($casa->status == 10)

                        <a class="relative text-black bg-white rounded-md shadow-lg col-span-1">

                            <div class="p-2 text-black bg-white rounded-md">

                                <div class="relative w-full h-full bg-white ">

                                    <div class="relative w-full">

                                        <div class="w-full h-96 bg-top bg-no-repeat bg-cover" style="background-image: url(/assets/images/png/prox.png)">
                                        </div>

                                    </div>

                                    <div class="relative flex items-center pt-4 justify-center  text-xl text-center font-montserrat-bold text-naranja-landing">

                                        <h3>

                                            {!! __('landing.xan_t_prox') !!}

                                        </h3>

                                    </div>

                                </div>

                            </div>

                        </a>

                    @else

                        <a href="/propiedad/{{$casa->slug}} " class="relative text-black bg-white rounded-md w-full  shadow-lg">

                            <div class="relative w-full h-full bg-white border rounded-md shadow-xl p-2">

                                @if ($casa->status  == 2)

                                    <div style="background-image: url(/assets/images/png/vendida_icon.png)" class="absolute left-0 z-40 flex items-center justify-center w-full py-4 text-2xl text-center text-white bg-center bg-no-repeat bg-contain -top-4 font-montserrat-bold">

                                        {!! __('landing.sell_apar') !!}

                                    </div>

                                @endif

                                @if ($casa->status  == 3)

                                    <div style="background-image: url(/assets/images/png/vendida_icon.png)" class="absolute left-0 z-40 flex items-center justify-center w-full py-4 text-2xl text-center text-white bg-center bg-no-repeat bg-contain -top-4 font-montserrat-bold">

                                        {!! __('landing.sell_c') !!}

                                    </div>

                                @endif

                                @if ($casa->status  == 4)

                                    <div style="background-image: url(/assets/images/png/vendida_icon.png)" class="absolute left-0 z-40 flex items-center justify-center w-full py-4 text-2xl text-center text-white bg-center bg-no-repeat bg-contain -top-4 font-montserrat-bold">

                                        {!! __('landing.sell_escr') !!}

                                    </div>

                                @endif

                                @if ($casa->status  == 5)

                                    <div style="background-image: url(/assets/images/png/vendida_icon.png)" class="absolute left-0 z-40 flex items-center justify-center w-full py-4 text-2xl text-center text-white bg-center bg-no-repeat bg-contain -top-4 font-montserrat-bold">

                                        {!! __('landing.sell_entr') !!}

                                    </div>

                                @endif

                                {{-- img de fachada --}}

                                <div class="relative w-full">

                                    @if (!empty($casa->images[0]))

                                        <div style="background-image: url(/{{$casa->images[0]->url}})"  class="w-full h-72 bg-center bg-no-repeat bg-cover" ></div>

                                    @else

                                        <div class="w-full h-72 bg-center bg-no-repeat bg-cover" ></div>

                                    @endif

                                </div>

                                {{-- termina img de fachada --}}

                                <div class="relative flex items-center justify-between px-4 pt-2 font-montserrat-bold">

                                    <div class="flex justify-between w-full text-sm items-center border-b-4 pb-2 border-black">

                                        <p class="font-montserrat-regular">

                                            {{--  {{ $casa['tipo'] }}  --}}

                                            <br>

                                            {{--  {{ $casa['titulo'] }}  --}}

                                        </p>


                                        <p class="text-base font-montserrat-bold">${{ number_format($casa->price,0) }}*</p>

                                    </div>

                                </div>

                                <div class="relative justify-between w-full px-2 gap-4 py-4 flex font-montserrat-bold">

                                    <div class="flex gap-4 text-sm w-8/12 font-montserrat-regular">

                                        <div class="flex justify-center items-center flex-col gap-1">

                                            <div class="w-11 h-11 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                                <img src="/assets/images/png/iconos/metros_icon.png" alt="" class="h-5 w-5 mt-2">

                                            </div>

                                            <p class="text-xs">

                                                {{ $casa->m2_construction }} m<sup>2</sup>

                                            </p>

                                        </div>

                                        <div class="flex justify-center items-center flex-col gap-1">

                                            <div class="w-11 h-11 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                                <img src="/assets/images/png/iconos/habitaciones_icon.png" alt="" class="h-5 w-5 mt-2">

                                            </div>

                                            <p class="text-xs">

                                                {{ $casa->bedrooms }}

                                            </p>

                                        </div>

                                        <div class="flex justify-center items-center flex-col gap-1">

                                            <div class="w-11 h-11 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                                <img src="/assets/images/png/iconos/banios_icon.png" alt="" class="h-5 w-5 mt-2">

                                            </div>

                                            <p class="text-xs">

                                                {{ $casa->bathrooms }}

                                            </p>

                                        </div>

                                        <div class="flex justify-center items-center flex-col gap-1">

                                            <div class="w-11 h-11 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                                <img src="/assets/images/png/iconos/estacionamiento_icon.png" alt="" class="h-5 w-5 mt-2">

                                            </div>

                                            <p class="text-xs">

                                                {{ $casa->parking_slots }}

                                            </p>

                                        </div>

                                    </div>

                                    <div class="flex justify-center items-center font-montserrat-regular gap-2 w-4/12 text-sm 2xl:text-base">

                                        <img src="/assets/images/png/iconos/mapa_icon.png" alt="" class="h-6">
                                        {{ $casa->city }}
                                        {{-- {{ $casa->neighborhood->municipality->description }} --}}
                                        {{-- , {{ $casa->neighborhood->municipality->state->description }} --}}

                                    </div>


                                </div>

                            </div>

                        </a>

                    @endif

                @endforeach

            </div>

        </div>

        {{-- termina cuadro blanco propiedades  --}}

        {{--  paginador  --}}

        {{ $casas->links('custom-pagination-links-view') }}

        {{--  termina paginador  --}}

        {{-- versión mobile --}}

        {{--  <div class="justify-around w-11/12 mx-auto mt-16 lg:hidden ">  --}}

            {{-- slider_propiedades --}}

            {{--  <div class="w-full flex flex-col gap-4">

                @foreach ($casasMovile as $casa)

                    <a href="/propiedad/" class="relative text-black bg-white rounded-md w-full mb-4">

                        <div class="relative w-full h-full bg-white border rounded-md shadow-xl p-2">

                            @if ($casa->status  == 2)

                                <div style="background-image: url(/assets/images/png/vendida_icon.png)" class="absolute left-0 z-40 flex items-center justify-center w-full py-4 text-2xl text-center text-white bg-center bg-no-repeat bg-contain -top-4 font-montserrat-bold">

                                    {!! __('landing.sell_apar') !!}

                                </div>

                            @endif

                            @if ($casa->status  == 3)

                                <div style="background-image: url(/assets/images/png/vendida_icon.png)" class="absolute left-0 z-40 flex items-center justify-center w-full py-4 text-2xl text-center text-white bg-center bg-no-repeat bg-contain -top-4 font-montserrat-bold">

                                    {!! __('landing.sell_c') !!}

                                </div>

                            @endif

                            @if ($casa->status  == 4)

                                <div style="background-image: url(/assets/images/png/vendida_icon.png)" class="absolute left-0 z-40 flex items-center justify-center w-full py-4 text-2xl text-center text-white bg-center bg-no-repeat bg-contain -top-4 font-montserrat-bold">

                                    {!! __('landing.sell_escr') !!}

                                </div>

                            @endif

                            @if ($casa->status  == 5)

                                <div style="background-image: url(/assets/images/png/vendida_icon.png)" class="absolute left-0 z-40 flex items-center justify-center w-full py-4 text-2xl text-center text-white bg-center bg-no-repeat bg-contain -top-4 font-montserrat-bold">

                                    {!! __('landing.sell_entr') !!}

                                </div>

                            @endif

                            <div class="relative w-full">

                                <div class="w-full h-72 bg-center bg-no-repeat bg-cover" style="background-image: url(/assets/images/png/{{$casa['img_fachada']}})">
                                </div>

                            </div>

                            <div class="relative flex items-center justify-between px-4 pt-2 font-montserrat-bold">

                                <div class="flex justify-between w-full text-sm items-center border-b-4 pb-2 border-black">

                                    <p class="font-roboto-normal">

                                        {{ $casa['tipo'] }}


                                        <br>


                                        {{ $casa['titulo'] }}


                                    </p>

                                    <p class="text-base font-montserrat-bold">${{ number_format($casa->price,2) }}*</p>

                                </div>

                            </div>

                            <div class="relative justify-between w-full px-2 gap-4 py-4 flex font-montserrat-bold">

                                <div class="flex gap-2 text-sm w-8/12 font-roboto-normal">

                                    <div class="flex justify-center items-center flex-col gap-1">

                                        <div class="w-11 h-11 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                            <img src="/assets/images/png/iconos/metros_icon.png" alt="" class="h-5 w-5 mt-2">

                                        </div>

                                        <p class="text-xs">

                                            {{ $casa->m2_construction }}

                                        </p>

                                    </div>

                                    <div class="flex justify-center items-center flex-col gap-1">

                                        <div class="w-11 h-11 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                            <img src="/assets/images/png/iconos/habitaciones_icon.png" alt="" class="h-5 w-5 mt-2">

                                        </div>

                                        <p class="text-xs">

                                            {{ $casa->floors }}

                                        </p>

                                    </div>

                                    <div class="flex justify-center items-center flex-col gap-1">

                                        <div class="w-11 h-11 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                            <img src="/assets/images/png/iconos/banios_icon.png" alt="" class="h-5 w-5 mt-2">

                                        </div>

                                        <p class="text-xs">

                                           {{ $casa->bathrooms }}

                                        </p>

                                    </div>

                                    <div class="flex justify-center items-center flex-col gap-1">

                                        <div class="w-11 h-11 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                            <img src="/assets/images/png/iconos/estacionamiento_icon.png" alt="" class="h-5 w-5 mt-2">

                                        </div>

                                        <p class="text-xs">

                                            {{ $casa->parking_slots }}

                                        </p>

                                    </div>

                                </div>

                                <div class="flex justify-center items-center font-roboto-normal gap-2 w-4/12 text-sm 2xl:text-base">

                                    <img src="/assets/images/png/iconos/mapa_icon.png" alt="" class="h-6">

                                    {{ $casa->neighborhood->municipality->description }}

                                </div>


                            </div>

                        </div>

                    </a>

                @endforeach

            </div>  --}}

        {{--  </div>  --}}

        {{-- termina versión mobile --}}

    </div>

    {{--  texto abajo  --}}

    <div class="flex flex-col items-center justify-center text-center pb-14">

        <span class="px-4 text-xs text-black opacity-70 lg:px-0 lg:text-sm font-montserrat-regular ">

            {!! __('landing.xan_discraimer') !!}

        </span>

        <a ta                                          rget="_blank" href="https://wa.me/+525570464021" title="Contáctanos" class="items-center px-24 py-2 mt-8 text-xl font-bold tracking-widest text-white transition duration-700 ease-in-out border-4 border-transparent border-white rounded-md shadow-lg cursor-pointer 2xl:mt-6 md:px-32 2xl:py-3 md:text-xl 2xl:text-base bg-verde-landing titilliumwebbold hover:bg-white hover:border-verde-landing hover:text-verde-landing">

            {!! __('landing.contact') !!}

        </a>

    </div>

    {{--  texto abajo  --}}

</div>
