<div class="flex w-full py-10">
    <div class="w-1/3"></div>
    <div class="flex items-center justify-end w-2/3">
        <div class="relative flex">
            <svg xmlns="http://www.w3.org/2000/svg" class="absolute w-4 h-4 top-4 left-6" fill="none" viewBox="0 0 24 24"
                stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
            </svg>
            <input wire:model="filters.search" type="text"
                class="h-12 pl-10 mx-4 border-t-0 border-l-0 border-r-0 titilliumwebsemibold focus:outline-none focus:ring-0"
                placeholder="{{ __('traduction.search') }}">
        </div>
        <input type="date" class="h-12 mx-4 border-t-0 border-l-0 border-r-0 titilliumwebsemibold focus:outline-none"
            onchange="emit_fechas()" wire:model="inicio" id="inicio" required placeholder="Fecha Inicio">
        <input type="date" class="h-12 mx-4 border-t-0 border-l-0 border-r-0 titilliumwebsemibold focus:outline-none"
            onchange="emit_fechas()" wire:model="fin" id="fin" required placeholder="Fecha Termino">

        <a href="{{ route('logout') }}"
            class="px-8 py-2 mx-4 text-white border-4 border-white rounded-lg shadow-lg cursor-pointer hover:bg-white "
            style="background-color: #60338a">{{ __('traduction.logout') }}
        </a>
        @if (App::getLocale() == 'en')
            <a href="/locale/es"
                class="px-8 py-2 mx-4 text-white border-4 border-white rounded-lg shadow-lg cursor-pointer bg-green-500">
                {{ 'Español' }}
            </a>
        @else
            <a href="/locale/en"
                class="px-8 py-2 mx-2 text-white border-4 border-white rounded-lg shadow-lg cursor-pointer bg-green-500">
                {{ 'English' }}
            </a>
        @endif
    </div>
    @if ($mostrar_info)
        <div class="fixed w-full h-full bg-black left-0 top-0 bg-opacity-50 flex justify-center items-center">
            <div class="w-11/12 h-auto p-5 flex justify-center gap-3">
                <div class="w-11/12 h-auto p-5 bg-white rounded-md">
                    <div class="">
                        <h2 class="text-3xl text-morado-xante font-titillium-semibold">
                            {{ __('traduction.info_prop') . ' - ' . $propiedad->id }}</h2>
                    </div>
                    <div class="flex w-full px-8 py-6 mt-4 border-t-2 border-b-2 " style="color: #b8d057">
                        <div class="flex w-1/2 gap-3">
                            <div class="flex flex-col w-1/3 gap-2">
                                <p class="text-gray-600 titilliumwebregular">
                                    {{ __('traduction.dev_name') }}:<br>
                                    <b class=" titilliumwebsemibold" style="color: #60338a">
                                        @if ($propiedad->desarrollo != null)
                                            {{ $propiedad->desarrollo->nombre }}
                                        @else
                                            {{ $propiedad->otro_desarrollo }}
                                        @endif
                                    </b>
                                </p>
                                <p class="text-gray-600 titilliumwebregular">
                                    {{ __('traduction.type_prop') }}:<br>
                                    <b class=" titilliumwebsemibold"
                                        style="color: #60338a">{{ $propiedad->propiedad_tipo->nombre }}</b>
                                </p>
                                <p class="text-gray-600 titilliumwebregular">
                                    {{ __('traduction.street') }}:<br>
                                    <b class=" titilliumwebsemibold"
                                        style="color: #60338a">{{ $propiedad->street }}</b>
                                </p>
                                <p class="text-gray-600 titilliumwebregular">
                                    {{ __('traduction.num_ext') }}:<br>
                                    <b class=" titilliumwebsemibold"
                                        style="color: #60338a">{{ $propiedad->num_ext }}</b>
                                </p>
                                <p class="text-gray-600 titilliumwebregular">
                                    Dirección Google:
                                    <br> <b class=" titilliumwebsemibold" style="color: #60338a">
                                        {{ $propiedad->google_direction }}
                                    </b>
                                </p>
                                <p class="text-gray-600 titilliumwebregular">{{ __('traduction.area') }}:
                                    <br> <b class=" titilliumwebsemibold"
                                        style="color: #60338a">{{ $propiedad->m2 . ' m' }}<sup>2</sup></b>
                                </p>
                                @if ($propiedad->propiedad_tipo->nombre == 'Casa')
                                    <p class="text-gray-600 titilliumwebregular">{{ __('traduction.levels') }}:
                                        <br> <b class=" titilliumwebsemibold"
                                            style="color: #60338a">{{ $propiedad->house_levels }}</b>
                                    </p>
                                @else
                                    <p class="text-gray-600 titilliumwebregular">{{ __('traduction.floor') }}:
                                        <br> <b class=" titilliumwebsemibold"
                                            style="color: #60338a">{{ $propiedad->level_department }}</b>
                                    </p>
                                @endif

                                <p class="text-gray-600 titilliumwebregular">
                                    {{ __('traduction.rooms') }}:<br>
                                    <b class=" titilliumwebsemibold"
                                        style="color: #60338a">{{ $propiedad->bedroom }}</b>
                                </p>
                            </div>
                            <div class="flex flex-col w-1/3 gap-2">
                                <p class="text-gray-600 titilliumwebregular">
                                    {{ __('traduction.baths') }}:<br>
                                    <b class=" titilliumwebsemibold" style="color: #60338a">
                                        {{ $propiedad->bathroom }}
                                    </b>
                                </p>
                                <p class="text-gray-600 titilliumwebregular">
                                    {{ __('traduction.med_baths') }}:
                                    <br><b class=" titilliumwebsemibold" style="color: #60338a">
                                        {{ $propiedad->half_bathrooms }}
                                    </b>
                                </p>
                                <p class="text-gray-600 titilliumwebregular">
                                    {{ __('traduction.parkins') }}:
                                    <br><b class=" titilliumwebsemibold" style="color: #60338a">
                                        {{ $propiedad->parking_slots }}
                                    </b>
                                </p>
                                <p class="text-gray-600 titilliumwebregular">
                                    {{ __('traduction.years') }}:<br>
                                    <b class=" titilliumwebsemibold" style="color: #60338a">
                                        {{ $propiedad->years }}
                                    </b>
                                </p>
                                <p class="text-gray-600 titilliumwebregular">
                                    {{ __('traduction.price') }}: <br>
                                    <b class=" titilliumwebsemibold" style="color: #60338a">
                                        {{ '$ ' . number_format($propiedad->offer_money, 2) }}
                                    </b>
                                </p>
                                <p class="text-gray-600 titilliumwebregular">
                                    {{ __('traduction.mortgage') }}:
                                    <br>
                                    <b class=" titilliumwebsemibold" style="color: #60338a">
                                        {{-- @if ($propiedad->mortgage == 'NO') --}}
                                            {{ $propiedad->mortgage }}
                                        {{-- @else
                                            {{ __('traduction.yes') }}
                                        @endif --}}

                                    </b>
                                </p>
                                {{-- <p class="text-gray-600 titilliumwebregular">
                                    {{ __('traduction.mortgage_type') }}:<br>
                                    <b class=" titilliumwebsemibold" style="color: #60338a">
                                        @if ($propiedad->hipoteca_tipo == null)
                                            Ninguna
                                        @else
                                            {{ $propiedad->hipoteca_tipo }}
                                        @endif
                                    </b>
                                </p> --}}
                                <p class="text-gray-600 titilliumwebregular">
                                    {{ __('traduction.reason_sale') }}:
                                    <br><b class=" titilliumwebsemibold" style="color: #60338a">
                                        {{ $propiedad->sale_reasons }}
                                        {{-- @switch($propiedad->sale_reasons)
                                            @case('Cambiar de casa o departamento')
                                                {{ __('traduction.mov_1') }}
                                            @break

                                            @case('Busco liquidez')
                                                {{ __('traduction.mov_2') }}
                                            @break

                                            @case('Remate bancario')
                                                {{ __('traduction.mov_3') }}
                                            @break

                                            @case('Cambiar de ciudad')
                                                {{ __('traduction.mov_4') }}
                                            @break

                                            @case('Otro')
                                                {{ __('traduction.mov_5') }}
                                            @break

                                            @default
                                        @endswitch --}}
                                    </b>
                                </p>
                            </div>
                            <div class="flex flex-col w-1/3 gap-2">
                                <p class="text-gray-600 titilliumwebregular">
                                    Whatsapp:<br>
                                    <b class=" titilliumwebsemibold" style="color: #60338a">
                                        {{ $propiedad->whatsapp }}
                                    </b>
                                </p>
                                <p class="text-gray-600 titilliumwebregular">
                                    {{ __('traduction.email_table') }}:<br>
                                    <b class=" titilliumwebsemibold" style="color: #60338a">
                                        {{ $propiedad->email }}
                                    </b>
                                </p>
                                <p class="text-gray-600 titilliumwebregular">
                                    {{ __('traduction.name') }}: <br>
                                    <b class=" titilliumwebsemibold" style="color: #60338a">
                                        {{ $propiedad->name }}
                                    </b>
                                </p>

                                <p class="text-gray-600 titilliumwebregular">
                                    Comentarios: <br>
                                    <b class=" titilliumwebsemibold" style="color: #60338a">
                                        {{ $propiedad->comments }}
                                    </b>
                                </p>

                                <p class="text-gray-600 titilliumwebregular">
                                    Prefiere contacto por: <br>
                                    <b class=" titilliumwebsemibold" style="color: #60338a">
                                        {{ $propiedad->preference }}
                                    </b>
                                </p>



                                </p>
                            </div>
                        </div>
                        {{-- <div class="grid w-1/2 grid-cols-4 gap-2">
                            @if (count($propiedad->images) > 0)
                                @foreach ($propiedad->images as $imagen)
                                    <div>
                                        <img src="/{{ $imagen->url }}{{ $imagen->nombre }}" alt=""
                                            class="w-full">
                                    </div>
                                @endforeach
                            @endif
                        </div> --}}
                    </div>
                    <div class="flex justify-center w-full">
                        <a href="/api/propiedades/{{ $propiedad->id }}/exportar"
                            class="px-8 py-2 mx-4 mt-6 text-xl text-white bg-green-500 border-4 border-white rounded-lg shadow-lg cursor-pointer hover:bg-white hover:border-green-500 hover:text-green-500 ">
                            {{ __('traduction.download_excel') }}
                        </a>
                    </div>
                </div>
                <div class="w-5 h-5 p-5 rounded-md flex items-center justify-center font-titillium-bold text-white bg-green-500 border-4 border-white cursor-pointer hover:bg-red-600"
                    wire:click="$set('mostrar_info',false)">X</div>
            </div>
        </div>
    @endif
</div>
