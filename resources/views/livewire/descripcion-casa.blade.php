<div>

    <div class="w-full lg:w-11/12  mx-auto pt-24 pb-8">

        <div class="w-11/12 lg:w-full mx-auto text-center lg:text-left">

             {{-- {{ dd() }} --}}

            <h2 class="text-5xl font-montserrat-bold">

                {{ $propiedad_datos->title }}

            </h2>

            <h3 class="text-3xl font-montserrat-regular text-gris-texto pt-2">

                {{ $propiedad_datos->title2 }}

            </h3>

        </div>

        <div class="w-11/12 lg:w-full mx-auto flex lg:flex-row flex-col gap-4 mt-8">

            <div class="w-full lg:w-1/2 bg-cover bg-center" style="background-image: url('/{{$propiedad_datos->images[1]->url}}'); ">

                <img src="/assets/images/jpg/test_cuadrados.jpg" alt="" class="w-full bg_oscuro invisible">

            </div>

            <div class="w-full lg:w-1/2 grid grid-cols-2 gap-4">

                @for ($i = 2; $i < 6; $i++)

                    <div class="bg-center bg-cover flex justify-center items-center bg_oscuro relative" style="background-image: url('/{{$propiedad_datos->images[$i]->url}}');">

                        <img src="/assets/images/jpg/test_cuadrados.jpg" alt="" class="w-full invisible">

                        @if ($i == 5)

                            <a onclick="abrirModal()"  class="uppercase absolute text-white cursor-pointer text-sm border-2 py-2 px-2 lg:px-8 font-roboto-bold rounded-md hover:bg-white hover:text-black transition duration-500 ease-out">

                                {{ __('landing.xan_ver_mas') }}

                            </a>

                        @endif

                    </div>

                @endfor

            </div>

        </div>

        <div class="flex flex-col lg:flex-row mt-4 w-11/12 lg:w-full mx-auto">

            <div class="w-full lg:w-1/2">

                <h2 class="font-montserrat-regular text-2xl my-6">

                    {{ $propiedad_datos->neighborhood->neighborhood }} - {{ $propiedad_datos->neighborhood->municipality->description; }}


                </h2>

                <div class="w-full bg-morado-xante h-2"></div>

                <div class="flex mt-12 gap-4 ">

                    @if ( $propiedad_datos->m2_construction != null )

                        <div class="flex justify-center items-center flex-col gap-1">

                            <div class="w-16 h-16 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                <img src="/assets/images/png/iconos/metros_icon.png" alt="" class="h-8 w-8 mt-4">

                            </div>

                            <p class="text-base font-montserrat-regular">

                                {{ $propiedad_datos->m2_construction }}m<sup>2</sup>

                            </p>

                        </div>

                    @endif

                    @if ( $propiedad_datos->bedrooms != null )

                        <div class="flex justify-center items-center flex-col gap-1">

                            <div class="w-16 h-16 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                <img src="/assets/images/png/iconos/habitaciones_icon.png" alt="" class="h-8 w-8 mt-4">

                            </div>

                            <p class="text-base font-montserrat-regular">

                                {{ $propiedad_datos->bedrooms }}

                            </p>

                        </div>

                    @endif

                    @if ( $propiedad_datos->bathrooms != null )

                        <div class="flex justify-center items-center flex-col gap-1">

                            <div class="w-16 h-16 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                <img src="/assets/images/png/iconos/banios_icon.png" alt="" class="h-8 w-8 mt-4">

                            </div>

                            <p class="text-base font-montserrat-regular">

                                {{$propiedad_datos->bathrooms}}

                            </p>

                        </div>

                    @endif

                    @if ( $propiedad_datos->parking_slots != null )

                        <div class="flex justify-center items-center flex-col gap-1">

                            <div class="w-16 h-16 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                <img src="/assets/images/png/iconos/estacionamiento_icon.png" alt="" class="h-8 w-8 mt-4">

                            </div>

                            <p class="text-base font-montserrat-regular">

                                {{$propiedad_datos->parking_slots}}

                            </p>

                        </div>

                    @endif

                </div>

                <div class="mt-10 text-center lg:text-left">

                    <h2 class="text-5xl text-morado font-montserrat-bold">

                        {{ __('landing.xan_descripcion') }}

                    </h2>

                    <p class="text-xl mt-4">

                        {!!$propiedad_datos->description!!}

                        {{--  {!! $descripcion !!}  --}}

                    </p>

                </div>

            </div>

            <div class="w-full lg:w-1/2 flex flex-col mt-4 justify-start items-center">

                <h2 class="font-montserrat-bold text-5xl text-morado-xante">

                    {{ __('landing.xan_precio') }}

                </h2>

                <h2 class="font-montserrat-bold mt-4 text-5xl">

                    ${{ number_format($propiedad_datos->price) }}*

                </h2>

                <a target="_blank" href="https://wa.link/om0mki">

                    <div class="flex flex-col lg:flex-row mt-8 items-center gap-2 justify-center">

                        <p class="bg-verde-landing border-2 border-white font-montserrat-semibold text-xl lg:text-3xl text-white shadow-md rounded-md px-8 py-2">

                            {{__('landing.xan_sol_info')}}

                        </p>

                        <div class="relative flex justify-center items-center  ">

                            <img src="/assets/images/png/iconos/whats_fondo.png" alt="" class="h-24 w-24 ">

                            <img src="/assets/images/png/iconos/whats_logo.png" alt="" class="absolute h-12 w-12 mr-2 mb-1 ">

                        </div>

                    </div>

                </a>

                {{--  method="post" action="{{url('sendEmail')}}"  --}}
                <form class="bg-white shadow-2xl rounded-3xl flex-col flex mt-8 p-8 w-full lg:w-2/3 2xl:w-1/2 gap-8" wire:submit.prevent="submit">
                    @csrf
                    <div class="w-full flex flex-col gap-2">

                        <input wire:model="nombre" type="text" id="txt_nombre" name="txt_nombre" placeholder="Nombre y Apellido" class="rounded-full bg-gray-300 border-2 border-rose-500">

                        @error('nombre')

                            <small id="error_nombre" class="text-sm text-red-400 pl-4 font-roboto-light"> {{ $message }} </small>

                        @enderror

                    </div>

                    <div class="w-full flex flex-col gap-2">

                        <input wire:model="telefono" type="number" id="txt_telefono" name="txt_telefono" placeholder="Teléfono de contacto" class="rounded-full bg-gray-300 border-2 border-rose-500">

                        @error('telefono')

                            <small id="error_telefono" class="text-sm text-red-400 pl-4 font-roboto-light">{{ $message }}</small>

                        @enderror

                    </div>

                    <div class="w-full flex flex-col gap-2">

                        <input wire:model="correo" type="email" id="txt_correo" name="txt_correo" placeholder="Correo electrónico" class="rounded-full bg-gray-300 border-2 border-rose-500">

                        @error('correo')

                            <small id="error_correo" class="text-sm text-red-400 pl-4 font-roboto-light">{{ $message }}</small>

                        @enderror


                    </div>

                    <input type="text" class="hidden" name="txt_propiedad" id="txt_propiedad"  value="{{$propiedad_datos->slug}}">

                    <div class="w-full flex flex-col gap-2 justify-center items-center">

                        <label class="text-xs text-gris-texto">

                            <input wire:model="terminos" type="checkbox" id="aviso" value="aceptado">  &nbsp; Acepto términos y condiciones. <a href="/assets/pdf/AVISO%20DE%20PRIVACIDAD%20PROMOTORA.pdf" target="_blank" class="text-azul-footer"> Consultar aviso de privacidad. </a>

                        </label>

                        <br>

                        @error('terminos')

                            <small id="error_checkbox" class="text-sm text-red-400 pl-4 font-roboto-light"> {{ $message }} </small>

                        @enderror

                    </div>

                    <button  class="text-white btn_enviar bg-verde-desc font-roboto-normal text-xl py-1 w-52 rounded-xl mt-4 mx-auto shadow-lg border-2 border-white"> {{__('landing.xan_send')}} </button>

                </form>

            </div>

        </div>

        <div class="lg:-mt-12 mt-8">

            <h2 class="text-5xl text-morado font-montserrat-bold text-center lg:text-left">

                {{ __('landing.xan_mapa') }}

            </h2>

            <div class="flex mt-8 justify-center items-center flex-col lg:flex-row">

                <div class="w-10/12 lg:w-1/2 p-2 lg:shadow-2xl shadow-xl h-64 lg:h-card rounded-xl -mb-8 lg:-mb-0 z-10">

                    {!!$propiedad_datos->location_description!!}

                </div>

                <div class="w-full lg:w-1/2 flex flex-col justify-end">

                    <div class="bg-morado-xante w-full h-56 text-white flex flex-col justify-center items-center pl-0">

                        <h3 class="text-4xl font-montserrat-bold">

                            Showroom

                        </h3>

                        <p class="font-montserrat-regular text-center mt-4 text-xl">

                            {{ $propiedad_datos->neighborhood->neighborhood }}, {{ $propiedad_datos->neighborhood->municipality->description; }}
                            <br>
                            {{ $propiedad_datos->street }}
                            {{--  {!! $ubicacion !!}  --}}

                        </p>

                    </div>

                    <div class="flex flex-col w-full lg:justify-start justify-center text-center lg:text-left items-center mt-4 pl-0">

                        <h3 class="text-2xl font-montserrat-bold">

                            {{ __('landing.xan_horario') }}

                        </h3>

                        <p class="font-montserrat-regular text-center">

                            {!! $propiedad_datos->atention_hours !!}

                        </p>

                    </div>

                </div>

            </div>

        </div>

        <div class="flex flex-col items-center justify-center text-center pb-7 mt-14">

            <span class="px-4 text-xs text-black opacity-70 lg:px-0 lg:text-sm font-montserrat-regular ">

                {!! __('landing.xan_discraimer') !!}

            </span>

        </div>

        {{-- <div class="fixed top-0 bottom-0 left-0 right-0 z-50 hidden w-full min-h-screen scale-0 bg-black bg-opacity-50 modal-full"> --}}
        <div class="fixed top-0 bottom-0 left-0 right-0 w-full min-h-screen scale-0 invisible index bg-black bg-opacity-50 modal-full">

            <div class="relative flex items-center justify-center w-full h-full">

                <div class="w-10/12 lg:w-1/3 relative">

                    <div class="absolute flex flex-col items-center justify-center -top-12 -right-12 ">

                        <div class="py-4">

                            <span class="text-white cursor-pointer" onclick="cerrarModal()">

                                <svg xmlns="http://www.w3.org/2000/svg" class="w-10 h-10" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                </svg>

                            </span>

                        </div>

                    </div>

                    <div class="slider_desc">

                        @for ($i = 1; $i < count($propiedad_datos->images); $i++)

                            <div class="bg-center bg-cover flex justify-center items-center bg_oscuro relative" style="background-image: url('/{{$propiedad_datos->images[$i]->url}}');">

                                <div class="">

                                    <div class="w-full flex justify-center items-center h-85vh">

                                        <img src="'/{{$propiedad_datos->images[$i]->url}}'" alt="" class="w-full relative my-auto inset-0">

                                    </div>

                                </div>

                            </div>

                        @endfor

                    </div>

                    <div class="absolute w-full dots_slider_desc bottom-8"></div>

                    <div class="absolute flex justify-between w-full px-8 slider_propiedades_desk_arrows" style="top: 50%"></div>

                 </div>

            </div>

        </div>

    </div>

</div>
