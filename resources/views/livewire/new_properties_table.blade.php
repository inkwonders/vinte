{{--  ID  --}}

<x-livewire-tables::table.cell>
    {{ $row->id }}
</x-livewire-tables::table.cell>

{{--  FECHA REGISTRO  --}}

<x-livewire-tables::table.cell>
    @if ($row->created_at != null)
        <span class="titilliumwebregular text-grayTable">{{ $row->created_at->format('d / m / Y') }}</span>
    @else
        <span class="titilliumwebregular text-grayTable">{{ __('traduction.no_date') }}</span>
    @endif
</x-livewire-tables::table.cell>

{{--  NOMBRE  --}}

<x-livewire-tables::table.cell>
    {{ $row->xanteid }}
</x-livewire-tables::table.cell>

{{--  TELEFONO  --}}

<x-livewire-tables::table.cell>
    {{ $row->type }}, {{ $row->neighbourhood }}, CP: {{ $row->zip_code }}, {{ $row->municipality }}, {{ $row->state }}
</x-livewire-tables::table.cell>

{{--  NOMBRE DESARROLLO  --}}

<x-livewire-tables::table.cell>
    <span class="titilliumwebregular">
        {{ $row->property_type }}
    </span>
</x-livewire-tables::table.cell>

{{--  CASA O DEPARTAMENTO  --}}

<x-livewire-tables::table.cell>
    <span class="titilliumwebregular">
        {{ $row->street }}
    </span>
</x-livewire-tables::table.cell>

{{--  ESTADO  --}}

<x-livewire-tables::table.cell>
    <span class="titilliumwebregular">
        {{ $row->no_ext }}
    </span>
</x-livewire-tables::table.cell>

{{--  CODIGO POSTAL  --}}

{{--  PRECIO  --}}



{{--  ESTATUS  --}}

<x-livewire-tables::table.cell>
    {{-- 0.- inactivo, 1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada --}}
    <div class="w-full flex items-center justify-between gap-2">
        @if ($row->status == '0')
            <span class="font-titillium-semibold text-red-600">Inactivo</span>
        @elseif ($row->status == '1')
            <span class="font-titillium-semibold text-green-500"> Disponible </span>
        @elseif ($row->status == '2')
            <span class="font-titillium-semibold text-purple-300"> Apartada </span>
        @elseif ($row->status == '3')
            <span class="font-titillium-semibold text-red-600"> Vendida </span>
        @elseif ($row->status == '4')
            <span class="font-titillium-semibold text-yellow-400"> Escriturada </span>
        @elseif ($row->status == '5')
            <span class="font-titillium-semibold text-red-600"> Entregada </span>
        @endif
        {{-- <svg xmlns="http://www.w3.org/2000/svg" class="h-7 w-7 cursor-pointer" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" wire:click="cambioEstatus({{$row->id}})" title="">
            <title>{{ __('traduction.change') }}</title>
            <path stroke-linecap="round" stroke-linejoin="round" d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15" />
        </svg> --}}
    </div>
</x-livewire-tables::table.cell>

{{-- VER MAS --}}

<x-livewire-tables::table.cell>
    <img src="/assets/images/svg/ver_mas_icon.svg" alt="" class="w-6 h-6 mx-auto cursor-pointer"
        wire:click="mostrarPropiedad({{ $row->id }})">
</x-livewire-tables::table.cell>

{{-- DESCARGAR FOTOS --}}

<x-livewire-tables::table.cell>
    @if ($row->images->count() > 0)
        {{--  <a href="/api/new_properties/{{ $row->id }}/descargar" title="Descargar">
            <img src="/assets/images/svg/descargar_recursos_icon.svg" alt="" class="w-6 h-6 mx-auto">
        </a>  --}}
        <button class="px-8 py-2 mx-4 text-md text-white bg-green-500 border-4 border-white rounded-lg shadow-lg cursor-pointer hover:bg-white hover:border-green-500 hover:text-green-500" wire:click="modalSeeImg({{ $row->id }})" >
            Ver imágenes
        </button>
    @else
    <button class="px-8 py-2 mx-4 text-md text-white bg-red-500 border-4 border-white rounded-lg shadow-lg cursor-pointer hover:bg-white hover:border-red-500 hover:text-red-500" id="btn_upload_nw_img" name="btn_upload_nw_img" wire:click="modalImg({{ $row->id }})" wire:loading.class='disabled' wire:loading.attr='disabled'>
        Subir imágenes
    </button>
        {{-- <p class="text-center titilliumwebregular">
            {{ __('traduction.no_images') }}
        </p> --}}
        {{-- <img src="/assets/images/svg/descargar_recursos_icon.svg" alt="" class="w-6 h-6 mx-auto"> --}}
    @endif
</x-livewire-tables::table.cell>
<x-livewire-tables::table.cell>
    @if ($row->count()>1)

    <div class="flex justify-center items-center gap-8">
        @hasrole("Admin")
        <a  wire:click="deleteConfirm({{ $row->id }})">

            <img src="/assets/images/svg/delete.svg" alt="" class="w-6 h-6 mx-auto cursor-pointer">

        </a>
        @endhasrole
        @hasrole("Admin||EditorCompra")
        <a wire:click="editarPropiedad({{ $row->id }})">

            <img src="/assets/images/svg/edit.svg" alt="" class="w-6 h-6 mx-auto cursor-pointer">

        </a>
        @endhasrole
    </div>

    @else
    <div class="flex justify-center items-center gap-8"> </div>
    @endif

</x-livewire-tables::table.cell>
