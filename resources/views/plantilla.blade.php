@extends('layouts.base')

@section('title')
    Xante.mx
@endsection

@section('css')

    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />

@endsection

@section('contenido')

    <div class="relative w-full min-h-screen ">

        @include('header')

        <div class="flex flex-col h-full bg-bottom bg-cover lg:bg-top lg:gap-2 lg:flex-row bg-fondo-back-movil lg:bg-fondo-desc">

            <div class="w-full pt-16 lg:h-screen lg:w-3/5 ">

                <div class="relative flex flex-col items-center justify-center gap-6 py-20 text-center lg:py-32">

                    <img src="/assets/images/png/logo_blanco.png" class="absolute h-20 lg:h-32 lg:left-20" alt="">

                </div>

                <div class="flex flex-col">

                    <div class="flex flex-col w-full gap-2 pt-8 pb-10 lg:pl-12 bg-morado-xante">

                        <div class="flex flex-col justify-between lg:flex-row">

                            <div class="px-4 lg:px-0">


                                <h1 class="text-3xl text-white font-montserrat-bold">Real Segovia - Puebla</h1>

                                <h2 class="text-3xl text-verde-boton font-montserrat-bold">$ 650,000</h2>

                            </div>

                            <div class="flex items-center justify-around w-2/3 gap-10 px-10 py-2 mt-8 text-xs text-center text-white lg:mt-0 lg:w-auto 2xl:text-base bg-verde-landing">

                                <div class="flex flex-col justify-center">

                                    <img src="/assets/images/png/icon_superficie.png" class="relative py-1 h-7">

                                    54m2

                                </div>

                                <div class="flex flex-col justify-center">

                                    <img src="/assets/images/png/icon_cuartos.png" class="relative py-1 h-7">

                                    2

                                </div>

                                <div class="flex flex-col justify-center">

                                    <img src="/assets/images/png/icon_banos.png" class="relative py-1 h-7">

                                    1

                                </div>

                            </div>

                        </div>

                        <span class="px-4 pt-4 text-sm text-white lg:pr-12 lg:px-0 font-montserrat-regular">

                            {!! __('landing.desc_prop_1') !!}

                        </span>

                    </div>

                    <div class="hidden pt-20 pl-12 pr-8 lg:flex">

                        <div class="w-1/4 pr-8">
                            <a title="Real de segovia" href="https://g.page/RealSegoviaPuebla?share">
                                <iframe class="w-full h-full rounded-2xl" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15073.261904021927!2d-98.3823393!3d19.1814185!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x235339753d402237!2sReal%20Segovia!5e0!3m2!1ses-419!2smx!4v1652715689258!5m2!1ses-419!2smx"  style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                            </a>

                        </div>

                        <div class="w-3/4 text-white pr-28">

                            <h3 class="font-montserrat-semibold">

                                Privada Carabia 124

                            </h3>

                            <p class="text-sm font-montserrat-regular">

                                {!! __('landing.desc_prop_abajo_1') !!}

                            </p>

                        </div>

                    </div>

                    <div class="justify-end hidden lg:flex">

                        <div class="flex justify-start w-3/4 pl-8">

                            <a class="items-center px-12 py-2 mt-2 text-xs font-semibold tracking-widest text-white uppercase transition duration-700 ease-in-out border border-transparent border-white rounded-md shadow-lg cursor-pointer 2xl:mt-6 2xl:py-3 md:text-sm 2xl:text-base bg-verde-landing titilliumwebbold hover:bg-white hover:border-verde-landing hover:text-verde-landing" target="_blank" href="https://wa.me/+525579318910" title="Contáctanos">

                                {!! __('landing.desc_contact') !!}


                            </a>

                        </div>

                    </div>

                </div>

            </div>

            <div class="relative w-full h-screen lg:w-2/5 ">

                <div class="slider_desc">


                    @for ($i=1; $i < 6; $i ++)

                        <div onclick="cerrarModal()" class="cursor-pointer">

                            <div class="w-full h-screen bg-center bg-cover" style="background-image: url('/assets/images/jpg/slider_real_segovia/{{ $i }}.jpg')"></div>

                        </div>

                    @endfor

                </div>

                <div class="absolute w-full dots_slider_desc bottom-8"></div>

                <div class="absolute flex justify-between w-full px-8 slider_propiedades_desk_arrows" style="top: 50%"></div>

            </div>

            <div class="px-12 lg:hidden">

                <div class="flex flex-col pt-10 lg:hidden">

                    <div class="w-full">

                        <h3 class="text-center text-white font-montserrat-semibold">

                            Privada Carabia 124

                        </h3>

                        <a  title="Real de segovia" href="https://g.page/RealSegoviaPuebla?share">
                            <iframe class="w-full h-full my-8 rounded-2xl" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15073.261904021927!2d-98.3823393!3d19.1814185!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x235339753d402237!2sReal%20Segovia!5e0!3m2!1ses-419!2smx!4v1652715689258!5m2!1ses-419!2smx"  style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                        </a>

                    </div>

                    <div class="w-full text-white ">

                        <p class="text-sm text-center font-montserrat-regular">

                            {!! __('landing.desc_prop_abajo_1') !!}

                        </p>

                    </div>

                </div>

                <div class="flex justify-center lg:hidden ">

                    <div class="flex justify-center w-full pt-14 pb-7">

                        <a class="items-center px-12 py-2 mt-2 text-xs font-semibold tracking-widest text-white uppercase transition duration-700 ease-in-out border border-transparent border-white rounded-md shadow-lg cursor-pointer 2xl:mt-6 2xl:py-3 md:text-sm 2xl:text-base bg-verde-landing titilliumwebbold hover:bg-white hover:border-verde-landing hover:text-verde-landing" target="_blank" href="https://wa.me/+525579318910" title="Contáctanos">

                            {!! __('landing.desc_contact') !!}


                        </a>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="fixed top-0 bottom-0 left-0 right-0 z-50 hidden w-full h-screen scale-0 bg-black bg-opacity-50 modal-full">

        <div class="relative flex items-center justify-center w-full h-full">

            <div class="absolute flex flex-col items-center justify-center ">

                <div class="py-4">

                    <span class="text-white cursor-pointer" onclick="cerrarModal()">

                        <svg xmlns="http://www.w3.org/2000/svg" class="w-10 h-10" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>
                    </span>

                </div>

                <img src="/assets/images/jpg/slider_real_segovia/1.jpg" class="w-3/4 lg:w-1/2" alt="">

            </div>

        </div>

    </div>

    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-227859785-1');

    </script>

@include('footer')


@endsection

@section('js-usable')

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

    <script type="text/javascript">

        $('.slider_desc').slick({

            slidesToShow: 1,
            dots: true,
            appendDots: $('.dots_slider_desc'),
            prevArrow: '<div class="text-white cursor-pointer a-left control-c prev slick-prev left-10"><svg xmlns="http://www.w3.org/2000/svg" class="w-16" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2"><path stroke-linecap="round" stroke-linejoin="round" d="M15 19l-7-7 7-7" /></svg></div>',
            nextArrow: '<div class="text-white cursor-pointer a-right control-c next slick-next"><svg xmlns="http://www.w3.org/2000/svg" class="w-16" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2"><path stroke-linecap="round" stroke-linejoin="round" d="M9 5l7 7-7 7" /> </svg></div>',
            appendArrows: $('.slider_propiedades_desk_arrows'),
            fade: true,
            cssEase: 'linear',

        });

        function cerrarModal(){
            $('.modal-full').toggleClass('fade-in');
            $('.modal-full').toggleClass('hidden');
        }

    </script>
@endsection
