<div class="flex justify-center items-center py-8">

    @if ($paginator->hasPages())

        <div class="flex justify-center items-center bg-white rounded-md">

            @if ( ! $paginator->onFirstPage())

                <a wire:click="previousPage" class="cursor-pointer">

                    <div class="flex bg-white justify-center items-center py-2">

                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-4">

                            <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 19.5L8.25 12l7.5-7.5" />

                        </svg>

                    </div>

                </a>

            @endif

            @foreach ($elements as $element)

                @if (is_array($element))

                    @foreach ($element as $page => $url)

                        @if ($page == $paginator->currentPage())

                            <div class="flex bg-white justify-center items-center w-14  py-2">

                                <span  class="rounded-full bg-black text-white w-10 flex justify-center items-center">{{ $page }}</span>

                            </div>

                        @elseif ($page === $paginator->currentPage() + 1 || $page === $paginator->currentPage() + 2 || $page === $paginator->currentPage() - 1 || $page === $paginator->currentPage() - 2)

                            <div class="flex bg-white justify-center items-center w-14  py-2">

                                <a  class="flex bg-white justify-center items-center w-14  py-2 cursor-pointer" wire:click="gotoPage({{$page}})">{{ $page }}</a>

                            </div>

                        @endif

                    @endforeach

                @endif

            @endforeach

            @if ($paginator->hasMorePages())


                    <a class="cursor-pointer" wire:click="nextPage" rel="next">

                        <div class="flex bg-white justify-center items-center py-2">

                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-4">

                                <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />

                            </svg>

                        </div>

                    </a>

            @endif


        </div>

    @endif

</div>
