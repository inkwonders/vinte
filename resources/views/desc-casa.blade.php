
@extends('layouts.base')

@section('title')
    Xante.mx
@endsection

@section('css')

    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />

    <style>

        .slick-list, .slick-track{
            height: 100%!important;
        }

    </style>

@endsection

@section('contenido')

    <div class="relative w-full  min-h-screen ">

        @include('header')

        @livewire('descripcion-casa', ['id_prop' => $id_prop] )

    </div>

@include('footer')

@endsection

@section('js-usable')

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

    <script type="text/javascript">

        $('.slider_desc').slick({

            slidesToShow: 1,
            dots: true,
            appendDots: $('.dots_slider_desc'),
            prevArrow: '<div class="text-white cursor-pointer a-left control-c prev slick-prev left-10"><svg xmlns="http://www.w3.org/2000/svg" class="w-16" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2"><path stroke-linecap="round" stroke-linejoin="round" d="M15 19l-7-7 7-7" /></svg></div>',
            nextArrow: '<div class="text-white cursor-pointer a-right control-c next slick-next"><svg xmlns="http://www.w3.org/2000/svg" class="w-16" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2"><path stroke-linecap="round" stroke-linejoin="round" d="M9 5l7 7-7 7" /> </svg></div>',
            appendArrows: $('.slider_propiedades_desk_arrows'),
            fade: true,
            cssEase: 'linear',
            adaptiveHeight: true

        });

        function abrirModal(){
            $('.modal-full').toggleClass('fade-in');
            $('.modal-full').toggleClass('invisible');
            $('.modal-full').removeClass('index');
            $('.modal-full').addClass('z-50');
        }

        function cerrarModal(){
            $('.modal-full').toggleClass('fade-in');
            $('.modal-full').toggleClass('invisible');
        }

        $(document).ready(function() {
            $('.acc-container .acc:nth-child(1) .acc-head').addClass('active');
            $('.acc-container .acc:nth-child(1) .acc-content').slideDown();
            $('.acc-head').on('click', function() {
                if ($(this).hasClass('active')) {
                    $(this).siblings('.acc-content').slideUp();
                    $(this).removeClass('active');
                } else {
                    $('.acc-content').slideUp();
                    $('.acc-head').removeClass('active');
                    $(this).siblings('.acc-content').slideToggle();
                    $(this).toggleClass('active');
                }
            });
        });

        function abrir_menu() {
            $('.menu').toggleClass('opened');
            $('#cont_menu').toggleClass(' menu_visible');
        }

        var correo = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var letras = /^[a-zA-Z áÁéÉíÍóÓúÚñÑüÜàè]+$/;
        var telefono = /^[0-9\- ]+$/;

        var nombre = 1;
        var email = 1;
        var telefono = 1;
        var check = 1;


        function valida_nombre() {

            nombre_val = $('#txt_nombre').val()

            if (nombre_val.length == 0 || !nombre_val.trim()) {
                document.getElementById('error_nombre').innerHTML = "El campo esta vacío.";
                nombre = 1;
            } else if (nombre_val.length < 3) {
                document.getElementById('error_nombre').innerHTML = "El campo debe tener más de 3 caracteres.";
                nombre = 1;
            } else if (!letras.test(nombre_val)) {
                document.getElementById('error_nombre').innerHTML = "Caracteres no validos";
                nombre = 1;
            } else {
                document.getElementById('error_nombre').innerHTML = "";
                nombre = 0;
            }
            return nombre;

        }

        function valida_telefono() {

            tel_val = $('#txt_telefono').val();

            if (tel_val.length == 0 || !tel_val.trim()) {
                document.getElementById('error_telefono').innerHTML = "El campo esta vacío.";
                telefono = 1;
            } else if (tel_val.length < 3) {
                document.getElementById('error_telefono').innerHTML = "El campo debe tener más de 3 caracteres.";
                telefono = 1;
            }else {
                document.getElementById('error_telefono').innerHTML = "";
                telefono = 0;
            }
            return telefono;

        }

        function valida_correo() {

            correo_val = $('#txt_correo').val();

            if (correo_val.length == 0 || !correo_val.trim()) {
                document.getElementById('error_correo').innerHTML = "El campo esta vacío.";
                email = 1;
            } else if (correo_val.length < 3) {
                document.getElementById('error_correo').innerHTML = "El campo debe tener más de 3 caracteres.";
                email = 1;
            } else if (!correo.test(correo_val)) {
                document.getElementById('error_correo').innerHTML = "El campo debe ser un correo.";
                email = 1;
            } else {
                document.getElementById('error_correo').innerHTML = "";
                email = 0;
            }
            return email;

        }

        function  validar(){

            if(nombre == 0 && telefono == 0 && email == 0 ) {

                $('.btn_enviar').prop('disabled', false);

            }else{

                $('.btn_enviar').prop('disabled', true);

            }

        }

    </script>

@endsection
