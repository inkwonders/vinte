
@extends('layouts.base')

@section('title')
    Xante.mx
@endsection

@section('css')

    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />

@endsection

@section('contenido')

    <div class="relative w-full min-h-screen bg-top bg-no-repeat bg-cover lg:bg-completo bg-fondo-back-movil lg:bg-fondo-back">

        @include('header')

        @livewire('comprar-casa')

    </div>

    @include('footer')

@endsection

@section('js-usable')

    <script>

        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-227859785-1');

    </script>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

    <script type="text/javascript">

        $('.slider_propiedades').slick({

            slidesToShow: 1,
            arrows:false,
            dots:false,
            centerMode: true,
            centerPadding: '10px',

        });

        $(document).ready(function() {
            $('.acc-container .acc:nth-child(1) .acc-head').addClass('active');
            $('.acc-container .acc:nth-child(1) .acc-content').slideDown();
            $('.acc-head').on('click', function() {
                if ($(this).hasClass('active')) {
                    $(this).siblings('.acc-content').slideUp();
                    $(this).removeClass('active');
                } else {
                    $('.acc-content').slideUp();
                    $('.acc-head').removeClass('active');
                    $(this).siblings('.acc-content').slideToggle();
                    $(this).toggleClass('active');
                }
            });
        });

        function abrir_menu() {
            $('.menu').toggleClass('opened');
            $('#cont_menu').toggleClass(' menu_visible');
        }

    </script>
@endsection
