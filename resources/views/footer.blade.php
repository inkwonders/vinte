
    <footer class="bg-black ">

        <div class="flex flex-col justify-center gap-4 py-10 mx-auto text-white lg:flex-row font-montserrat-regular">

            <div class="flex items-center justify-center w-full text-center lg:text-left lg:w-1/5">

                <img src="/assets/images/png/vende_tucasa_tres.png" class="w-28 lg:w-48" alt="">

            </div>

            <div class="flex flex-col w-full gap-4 text-center lg:text-left lg:w-1/5">

                <h2 class="text-verde-landing font-montserrat-bold">{{ __('landing.company') }}</h2>

                <a title="{{ __('landing.sell_h') }}" href="/vender">

                    <h3>{{ __('landing.sell_h') }}</h3>

                </a>

                <a title="{{ __('landing.buy_h') }}" href="/comprar/1">

                    <h3>{{ __('landing.buy_h') }}</h3>

                </a>

                <a title="{{ __('landing.buy_h') }}" target="_blank" href="https://www.xanteenlosmedios.com/?utm_source=web&utm_medium=linkweb&utm_campaign=xantemedios">

                    <h3>{{ __('landing.xan_medios') }}</h3>

                </a>

            </div>

            <div class="flex flex-col w-full gap-4 text-center lg:text-left lg:w-1/5">

                <h2 class="text-verde-landing font-montserrat-bold">Legal</h2>

                <a title="{{ __('landing.policy') }}" href="/assets/pdf/AvisodePrivacidadXante.pdf" target="_blank">

                    <h3>{{ __('landing.policy') }}</h3>

                </a>

                <h2 class="text-verde-landing font-montserrat-bold">Sugerencias o quejas</h2>

                {{-- <a href="mailto:"></a> --}}
                <a title="{{ __('landing.sugerencias_msg') }}" href="mailto:sugerencias@xante.mx">

                    <h3>{{ __('landing.sugerencias_msg') }}</h3>

                </a>

            </div>

            <div class="flex flex-col w-full gap-4 text-center lg:text-left lg:w-1/5">

                <h2 class="text-verde-landing font-montserrat-bold">{{ __('landing.help')}}</h2>

                <a title="{{ __('landing.faq') }}" href="/faqs">

                    <h3>{{ __('landing.faq') }}</h3>

                </a>

            </div>

            <div class="flex flex-col items-center w-full gap-4 text-center lg:gap-0 lg:items-start lg:text-left lg:w-1/5">

                <h2 class="text-verde-landing font-montserrat-bold lg:mb-4">{{ __('landing.contact') }}</h2>

                {{-- <a href="tel:5554321234"  class="flex items-center gap-4">

                    <img src="/assets/images/png/telefono_icon.png" class="w-4 h-4" alt="">

                    <h3>55 5432 1234</h3>

                </a> --}}

                <span class="lg:mb-1">
                    <b>
                        Vende tu casa
                    </b>
                </span>

                <a target="_blank" href="https://wa.link/r8ni13" class="flex items-center gap-4 lg:mb-3">

                    <img src="/assets/images/png/icon_whats.png" class="w-4 h-4" alt="">

                    <h3>55 7931 8910</h3>
                </a>

                <span class="lg:mb-1">
                    <b>
                        Compra tu casa
                    </b>
                </span>

                <a target="_blank" href="https://wa.link/qqkjna" class="flex items-center gap-4 lg:mb-3">

                    <img src="/assets/images/png/icon_whats.png" class="w-4 h-4" alt="">

                    <h3>55 7046 4021</h3>
                </a>

                <a class="flex items-center gap-4 lg:my-4" href="mailto:contacto@xante.mx">

                    <img src="/assets/images/png/icon_mail.png" class="w-5 h-4" alt="">

                    <h3>contacto@xante.mx</h3>

                </a>

                <a class="flex items-center gap-4 lg:my-4" href="mailto:ventas@xante.mx">

                    <img src="/assets/images/png/icon_mail.png" class="w-5 h-4" alt="">

                    <h3>ventas@xante.mx</h3>

                </a>

                {{-- <a class="flex gap-4 px-4 md:px-0 lg:px-0 xl:px-0 2xl:px-0 lg:my-4 " href="mailto:ventas@xante.mx">

                    <img src="/assets/images/png/icon_ubi.png" class="w-6 h-5" alt="">

                    <h3>Downtown Santa Fe, Av. Santa Fe 428, Torre I, Piso 12, oficina 1201A, alcaldía Cuajimalpa, CP 05300, CDMX.</h3>

                </a> --}}

                <div class="flex items-center gap-4 lg:mt-3">

                    <a target="_blank" href="https://www.facebook.com/Xante_Mx-105814098903767">

                        <img src="/assets/images/svg/facebook_icon.svg" class="w-4 h-4" alt="">

                    </a>

                    <a target="_blank" href="https://www.instagram.com/xante_mx/">

                        <img src="/assets/images/svg/instagram_icon.png" class="w-4 h-4" alt="">

                    </a>

                    <a target="_blank" href="https://twitter.com/xante_mx?s=11&t=bM5X9cZUhul1G4ivCUSf_w">

                        <img src="/assets/images/svg/twitter_icon.svg" class="w-4 h-4" alt="">

                    </a>

                    <a target="_blank" href="https://www.youtube.com/@Xante_mx">

                        <img src="/assets/images/svg/icono-youtube.svg" class="w-4 h-4" alt="">

                    </a>
                    <a target="_blank" href="https://www.tiktok.com/@xantemx">

                        <img src="/assets/images/svg/icono-tiktok.svg" class="w-4 h-4" alt="">

                    </a>

                </div>

            </div>

        </div>

    </footer>
