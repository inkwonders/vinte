<x-guest-layout>
    <x-jet-authentication-card class="flex">
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <form method="POST" action="{{ route('login') }}">
            <x-jet-validation-errors class="mb-4" />

            @if (session('status'))
                <div class="mb-4 text-sm font-medium text-green-600">
                    {{ session('status') }}
                </div>
            @endif

            @csrf
            <div class="mb-8 text-3xl  titilliumwebbold" style="color: #b8d057">{{ __('traduction.login') }}</div>
            <div>
                <x-jet-label for="email" class="titilliumwebsemibold" value="{{ __('traduction.email') }}" />
                <x-jet-input id="email" class="block w-full mt-1" type="email" name="email" :value="old('email')" required
                    autofocus />
            </div>

            <div class="mt-4">
                <x-jet-label for="password" class="titilliumwebsemibold" value="{{ __('traduction.password') }}" />
                <x-jet-input id="password" class="block w-full mt-1" type="password" name="password" required
                    autocomplete="current-password" />
            </div>

            {{-- <div class="block mt-4">
                <label for="remember_me" class="flex items-center">
                    <x-jet-checkbox id="remember_me" name="remember" />
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>
            </div> --}}

            <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="text-base text-white titilliumwebsemibolditalic hover:text-purple-900"
                        href="{{ route('password.request') }}">
                        {{ __('traduction.forgot_pass') }}
                    </a>
                @endif
            </div>
            <div class="">
                <x-jet-button class="shadow-lg rounded-xl titilliumwebbold">
                    {{ __('traduction.enter') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
    @if (App::getLocale() == 'en')
        <a href="/locale/es"
            class="absolute right-5 bottom-5 px-8 py-2 mx-4 text-white border-4 border-white rounded-lg shadow-lg cursor-pointer bg-green-500">
            {{ 'Español' }}
        </a>
    @else
        <a href="/locale/en"
            class="absolute right-5 bottom-5 px-8 py-2 mx-2 text-white border-4 border-white rounded-lg shadow-lg cursor-pointer bg-green-500">
            {{ 'English' }}
        </a>
    @endif
</x-guest-layout>
