<button {{ $attributes->merge(['type' => 'submit', 'class' => 'inline-flex items-center py-3 px-5 bg-transparent  border-transparent rounded-md font-semibold text-base text-white uppercase tracking-widest border-4 border-white disabled:opacity-25 transition shadow-lg']) }}>
    {{ $slot }}
</button>
