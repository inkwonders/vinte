<div class="relative flex flex-row items-center min-h-screen pt-6 overflow-hidden sm:justify-center sm:pt-0 " style="background-color: #60338a">
    <div class="absolute z-0 block w-3/4 bg-white rounded-full h-120 -left-2/4"></div>
    <div class="relative block w-4/12 h-full p-10 text-left">
        {{ $logo }}
    </div>

    <div class="w-8/12 text-center">
        <div class="inline-block w-10/12 px-6 py-4 mt-6 overflow-hidden text-left sm:max-w-2xl sm:rounded-lg">
            {{ $slot }}
        </div>
    </div>
</div>
