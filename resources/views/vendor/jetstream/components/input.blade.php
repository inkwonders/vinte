@props(['disabled' => false])

<input {{ $disabled ? 'disabled' : '' }} {{ $attributes->merge(['class' => 'w-full border-0 bg-transparent border-morado border-b-2 text-white focus:border-0 focus:border-b-2 focus:outline-none text-Roboto-bold text-xl p-2 focus:ring-0 ']) }} >
