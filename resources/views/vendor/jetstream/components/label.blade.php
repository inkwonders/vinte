@props(['value'])

<label {{ $attributes->merge(['class' => 'block font-medium text-xl text-white ']) }}>
    {{ $value ?? $slot }}
</label>
