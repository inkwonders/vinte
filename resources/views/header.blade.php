<div class="fixed top-0 z-50 flex w-full h-12 text-xl lg:h-16 md:justify-between bg-morado-fuerte">

    <div class="items-center hidden pr-4 lg:flex lg:w-1/3">

        <a href="/">

            <img src="/assets/images/svg/home_icon.svg" alt="home" class="ml-4 md:ml-12 w-7 h-7 lg:h-10 lg:w-10">

        </a>

        <div class="flex gap-10 ml-4 text-white font-montserrat-bold">

            <a href="/">

                <img src="/assets/images/png/xante_header.png" alt="home" class="h-5">

            </a>

            {{-- <a href="/vender">

                <span>

                    {{ strtoupper(__('landing.sell')) }}

                </span>

            </a>

            <a href="/comprar">

                <span>

                    {{ strtoupper(__('landing.buy')) }}

                </span>

            </a> --}}

        </div>

    </div>

    <div class="items-center justify-start hidden gap-0 text-white lg:flex lg:gap-8 lg:justify-end lg:pr-4 lg:w-2/3 ">

        {{-- <a href="tel:5512345678" class="flex items-center gap-2 text-xs lg:text-xl lg:gap-4 font-montserrat-bold">

            <img src="/assets/images/png/telefono_icon.png" alt="vinte" class="h-4 ">

            55 1234 5678

        </a> --}}

        <a href="https://www.vinte.com.mx" target="_blank">

            <img src="/assets/images/byvinte.png" alt="vinte" class="hidden w-auto lg:block h-7 lg:h-12">

        </a>

        <div class="flex items-center justify-center w-auto py-5 text-center">
            @if (App::getLocale() == 'en')
                <a title="language spanish" href="/locale/es"
                    class="flex items-center gap-2 text-white transition duration-700 rounded-lg cursor-pointer w-14 titilliumwebbold">

                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round"
                            d="M21 12a9 9 0 01-9 9m9-9a9 9 0 00-9-9m9 9H3m9 9a9 9 0 01-9-9m9 9c1.657 0 3-4.03 3-9s-1.343-9-3-9m0 18c-1.657 0-3-4.03-3-9s1.343-9 3-9m-9 9a9 9 0 019-9" />
                    </svg>

                    {{ 'Es' }}
                    {{-- <img class="w-full" src="/assets/images/mexico.png" alt="español mexico"> --}}
                </a>
            @else
                <a title="idioma ingles" href="/locale/en"
                    class="flex items-center gap-2 text-white transition duration-700 rounded-lg cursor-pointer w-14 titilliumwebbold ">

                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round"
                            d="M21 12a9 9 0 01-9 9m9-9a9 9 0 00-9-9m9 9H3m9 9a9 9 0 01-9-9m9 9c1.657 0 3-4.03 3-9s-1.343-9-3-9m0 18c-1.657 0-3-4.03-3-9s1.343-9 3-9m-9 9a9 9 0 019-9" />
                    </svg>

                    {{ 'En' }}
                    {{-- <img class="w-full" src="/assets/images/usa.png" alt="ingles"> --}}
                </a>
            @endif
        </div>

        {{-- <a target="_blank" href="https://www.facebook.com/XanteMx">

            <img src="/assets/images/svg/logo-f.svg" alt="vinte" class="w-auto h-5 lg:h-7 ">

        </a>

        <a target="_blank" href="https://www.instagram.com/xante_mx/">

            <img src="/assets/images/svg/instagram_icon.png" alt="vinte" class="w-auto h-5 lg:h-7 ">

        </a> --}}

        <div class="relative flex items-center justify-center m-0">

            <button class="w-12 h-1w-12 menu" onclick="abrir_menu()" aria-label="Main Menu">
                <svg class="w-full h-full" viewBox="0 0 100 100">
                    <path class="line line1"
                        d="M 20,29.000046 H 80.000231 C 80.000231,29.000046 94.498839,28.817352 94.532987,66.711331 94.543142,77.980673 90.966081,81.670246 85.259173,81.668997 79.552261,81.667751 75.000211,74.999942 75.000211,74.999942 L 25.000021,25.000058" />
                    <path class="line line2" d="M 20,50 H 80" />
                    <path class="line line3"
                        d="M 20,70.999954 H 80.000231 C 80.000231,70.999954 94.498839,71.182648 94.532987,33.288669 94.543142,22.019327 90.966081,18.329754 85.259173,18.331003 79.552261,18.332249 75.000211,25.000058 75.000211,25.000058 L 25.000021,74.999942" />
                </svg>
            </button>

        </div>

    </div>

    <div class="z-50 flex items-center justify-around w-full gap-2 text-white lg:hidden">

        <div class="flex items-center justify-center w-1/3">

            <img src="/assets/images/svg/home_icon.svg" alt="home" class="ml-4 md:ml-12 w-7 h-7">

            <div class="flex gap-10 ml-4 text-white font-montserrat-bold">

                <img src="/assets/images/png/xante_header.png" alt="home" class="h-4">

            </div>

        </div>

        <div class="flex items-center w-2/3 justify-evenly">

            {{-- <span class="flex items-center gap-2 text-xs lg:gap-4 font-montserrat-bold">

                <img src="/assets/images/png/telefono_icon.png" alt="vinte" class="h-4">

                55 1234 5678

            </span> --}}

            <a href="https://www.vinte.com.mx" target="_blank">

                <img src="/assets/images/byvinte.png" alt="vinte" class="hidden w-16">

            </a>

            <div class="flex items-center justify-center w-auto text-center">
                @if (App::getLocale() == 'en')
                    <a title="language spanish" href="/locale/es"
                        class="flex items-center justify-center text-white transition duration-700 rounded-lg cursor-pointer w-14 titilliumwebbold">
                        {{-- {{ 'Español' }} --}}
                        {{-- <img class="w-full" src="/assets/images/mexico.png" alt="español mexico"> --}}

                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round"
                            d="M21 12a9 9 0 01-9 9m9-9a9 9 0 00-9-9m9 9H3m9 9a9 9 0 01-9-9m9 9c1.657 0 3-4.03 3-9s-1.343-9-3-9m0 18c-1.657 0-3-4.03-3-9s1.343-9 3-9m-9 9a9 9 0 019-9" />
                         </svg>

                        {{ 'Es' }}

                    </a>
                @else
                    <a title="idioma ingles" href="/locale/en"
                        class="flex items-center justify-center text-white transition duration-700 rounded-lg cursor-pointer w-14 titilliumwebbold hover:bg-white">
                        {{-- {{ 'English' }} --}}
                        {{-- <img class="w-full" src="/assets/images/england.png" alt="ingles"> --}}

                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round"
                            d="M21 12a9 9 0 01-9 9m9-9a9 9 0 00-9-9m9 9H3m9 9a9 9 0 01-9-9m9 9c1.657 0 3-4.03 3-9s-1.343-9-3-9m0 18c-1.657 0-3-4.03-3-9s1.343-9 3-9m-9 9a9 9 0 019-9" />
                        </svg>

                        {{ 'En' }}

                    </a>
                @endif
            </div>

            {{-- <img src="/assets/images/svg/logo-f.svg" alt="vinte" class="w-auto h-5 ">

            <img src="/assets/images/svg/instagram_icon.png" alt="vinte" class="w-auto h-5 "> --}}

            <div class="relative flex items-center justify-center m-0">

                <button class="w-10 h-10 menu" onclick="abrir_menu()" aria-label="Main Menu">
                    <svg class="w-full h-full" viewBox="0 0 100 100">
                        <path class="line line1"
                            d="M 20,29.000046 H 80.000231 C 80.000231,29.000046 94.498839,28.817352 94.532987,66.711331 94.543142,77.980673 90.966081,81.670246 85.259173,81.668997 79.552261,81.667751 75.000211,74.999942 75.000211,74.999942 L 25.000021,25.000058" />
                        <path class="line line2" d="M 20,50 H 80" />
                        <path class="line line3"
                            d="M 20,70.999954 H 80.000231 C 80.000231,70.999954 94.498839,71.182648 94.532987,33.288669 94.543142,22.019327 90.966081,18.329754 85.259173,18.331003 79.552261,18.332249 75.000211,25.000058 75.000211,25.000058 L 25.000021,74.999942" />
                    </svg>
                </button>

            </div>

        </div>




    </div>

</div>

<div id="cont_menu" class="fixed right-0 z-40 w-full bg-white lg:w-1/5">

    <div
        class="flex flex-col items-end w-full h-full gap-4 pt-20 pr-6 text-xl text-right text-white font-montserrat-regular">

        {{-- <p class="text-verde-boton font-montserrat-semibold">

            {{ __('landing.company') }}

        </p> --}}

        <a onclick="abrir_menu()" title="Vender" href="/vender">

            {{ __('landing.sell_h') }}

        </a>

        <a onclick="abrir_menu()" title="Comprar" href="/comprar/1">

            {{ __('landing.buy_h') }}

        </a>

        {{-- <p class="text-verde-boton font-montserrat-semibold">

            Legal

        </p> --}}

        {{-- <a href="/assets/pdf/AvisodePrivacidadXante.pdf" target="_blank">

            {{ __('landing.policy') }}

        </p> --}}
        {{-- <br> --}}

        {{-- <p class="text-verde-boton font-montserrat-semibold">

            {{ __('landing.help')}}

        </p> --}}

        <a onclick="abrir_menu()" title="faqs" href="/faqs">

            {{ __('landing.faq') }}

        </a>

        <a onclick="abrir_menu()" title="contacto" target="_blank" href="https://wa.me/+525579318910">

            {{ __('landing.contact') }}

        </a>

        {{-- <p class="text-verde-boton font-montserrat-semibold">

            {{ __('landing.contact') }}

        </p>


        <a onclick="abrir_menu()" href="tel:5554321234" class="flex items-center gap-4">

            <img src="/assets/images/png/telefono_icon.png" class="w-4 h-4" alt="">

            <h3>55 5432 1234</h3>

        </a>

        <a onclick="abrir_menu()" target="_blank" href="https://wa.me/+525579318910" class="flex items-center gap-4">

            <img src="/assets/images/png/icon_whats.png" class="w-4 h-4" alt="">

            <h3>557 93 18 910</h3>
        </a>

        <a onclick="abrir_menu()" class="flex items-center gap-4" href="mailto:contacto@xante.mx">

            <img src="/assets/images/png/icon_mail.png" class="w-5 h-4" alt="">

            <h3>contacto@xante.mx</h3>

        </a> --}}

        {{-- <div class="flex items-center gap-4">

            <a onclick="abrir_menu()" target="_blank" href="https://www.facebook.com/XanteMx">

                <img src="/assets/images/svg/facebook_icon.svg" class="w-4 h-4" alt="">

            </a>

            <a onclick="abrir_menu()" target="_blank" href="https://www.instagram.com/xante_mx/">

                <img src="/assets/images/svg/instagram_icon.png" class="w-4 h-4" alt="">

            </a>

        </div> --}}

        {{-- <div class="flex items-center justify-center w-full py-5 text-center">
            @if (App::getLocale() == 'en')
                <a href="/locale/es"
                    class="px-8 py-2 mx-4 text-white transition duration-700 border-4 border-white rounded-lg shadow-lg cursor-pointer bg-verde-landing titilliumwebbold hover:bg-white hover:border-verde-landing hover:text-verde-landing">
                    {{ 'Español' }}
                </a>
            @else
                <a href="/locale/en"
                    class="px-8 py-2 mx-2 text-white transition duration-700 border-4 border-white rounded-lg shadow-lg cursor-pointer bg-verde-landing titilliumwebbold hover:bg-white hover:border-verde-landing hover:text-verde-landing">
                    {{ 'English' }}
                </a>
            @endif
        </div> --}}
    </div>

</div>

