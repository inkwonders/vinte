<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#603cba">
    <meta name="theme-color" content="#ffffff">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" />
    @livewireStyles

    @yield('css')

    <!-- Global site tag (gtag.js) - Google Analytics -->

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-227859785-1"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script src="https://cdn.ckeditor.com/ckeditor5/36.0.1/classic/ckeditor.js"></script>

    <!-- Start of HubSpot Embed Code -->

    <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/22204705.js"></script>

    <!-- End of HubSpot Embed Code -->


</head>

<body>



    <div id="app" class="w-full min-h-screen">
        @yield('contenido')
    </div>

    <script src="/js/app.js"></script>
    @livewireScripts

    <script>

        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-227859785-1');

    </script>
       <script>

        Livewire.on('successAlert', (mensaje, route) =>{
            Swal.fire(mensaje)
            .then(function(){
                window.location = "/"+route;
            })
        })


        Livewire.on('errorAlert', (mensaje) =>{
            Swal.fire(mensaje)
        })

        Livewire.on('notifAlert', (mensaje) => {
            Swal.fire(mensaje)
        })

    </script>
    {{-- <script>
        ClassicEditor
            .create( document.querySelector( '#description' ) )
            .then(editor => {
                editor.model.document.on('change:data', () => {
                @this.set('description', editor.getData());
                    })
                })
            .catch( error => {
                console.error( error );
            } );
    </script> --}}


    @yield('js-usable')

</body>

</html>
