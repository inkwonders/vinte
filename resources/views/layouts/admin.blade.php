<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#603cba">
    <meta name="theme-color" content="#ffffff">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" />
    @livewireStyles

    @yield('css')

    <!-- Global site tag (gtag.js) - Google Analytics -->

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-227859785-1"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/36.0.1/classic/ckeditor.js"></script>


</head>

<body>



    <div id="app" class="flex justify-between w-full min-h-screen gap-5">

        <div class="flex flex-col items-center w-2/12 h-screen gap-2" style="background-color: #60338a">
            <a href="/" class="w-1/2 mt-5 mb-10">
                <img src="{{ asset('assets/images/png/vende_tucasa_tres.png') }}" alt="" class="w-full">
            </a>
            <div class="flex flex-col w-full gap-6 text-2xl text-white titilliumwebbold">
                    @hasrole("Admin||EditorCompra||Viewer||EditorVende")
                    <a href="{{ route('admin.show_properties') }}"
                        class="flex items-center justify-between w-full gap-2 px-5 text-left "><span>{{ __('traduction.new_property') }}</span></a>
                    @endhasrole

                    @hasrole("Admin||EditorVende||Viewer")
                    <a href="{{ route('admin.index') }}"
                        class="flex items-center justify-between w-full gap-2 px-5 text-left "><span>{{ __('traduction.properties') }}</span></a>
                    @endhasrole
                    @hasrole("Admin||EditorCompra||Viewer")
                    {{-- <a href="{{ route('admin.old') }}"
                        class="flex items-center justify-between w-full gap-2 px-5 text-left "><span>{{ __('traduction.new_property_old') }}</span></a> --}}
                    @endhasrole
                    @hasrole("Admin")
                    <a href="{{ route('admin.show_users') }}"
                        class="flex items-center justify-between w-full gap-2 px-5 text-left "><span>{{ __('traduction.users') }}</span></a>
                    @endhasrole
                    @hasrole("Admin")
                    <a href="{{ route('admin.show_neighborhoods') }}"
                        class="flex items-center justify-between w-full gap-2 px-5 text-left "><span>{{ __('traduction.neighborhoods') }}</span></a>
                    @endhasrole
                    @hasrole("Admin")
                    <a href="{{ route('admin.show_log') }}"
                        class="flex items-center justify-between w-full gap-2 px-5 text-left "><span>Log</span></a>
                    @endhasrole

            </div>
        </div>

        @yield('contenido')

    </div>

    <script src="/js/app.js"></script>
    @livewireScripts

    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-227859785-1');
    </script>
    <script>
        Livewire.on('successAlert', (mensaje, route) => {
            Swal.fire(mensaje)
                .then(function() {
                    window.location = "/" + route;
                })
        })


        Livewire.on('errorAlert', (mensaje) => {
            Swal.fire(mensaje)
        })

        Livewire.on('notifAlert', (mensaje) => {
            Swal.fire(mensaje)
        })


        Livewire.on('confirmDeleteUser', function(user) {

            Swal.fire({

                title: '¿Eliminar? ' + user.name,

                icon: 'warning',

                showCancelButton: true,

                confirmButtonText: 'Eliminar',

                cancelButtonText: 'Cancelar',

                confirmButtonColor: '#FF0012',

                cancelButtonColor: '#3085d6',

            }).then((result) => {

                if (result.isConfirmed) {

                    Livewire.emit('deleteUser', user)

                }

            })

        });

        Livewire.on('confirmDeleteProperty', function(propiedad_id) {

        Swal.fire({

            title: '¿Eliminar? ' + propiedad_id.xanteid,

            icon: 'warning',

            showCancelButton: true,

            confirmButtonText: 'Eliminar',

            cancelButtonText: 'Cancelar',

            confirmButtonColor: '#FF0012',

            cancelButtonColor: '#3085d6',

        }).then((result) => {

            if (result.isConfirmed) {

                Livewire.emit('deleteProperty', propiedad_id)

            }

        })

        });


        Livewire.on('confirmNeighborhood', function(neighborhood) {

                    Swal.fire({

                        title: '¿Eliminar? ' + neighborhood.neighborhood,

                        icon: 'warning',

                        showCancelButton: true,

                        confirmButtonText: 'Eliminar',

                        cancelButtonText: 'Cancelar',

                        confirmButtonColor: '#FF0012',

                        cancelButtonColor: '#3085d6',

                    }).then((result) => {

                        if (result.isConfirmed) {

                            Livewire.emit('deleteNeighborhood', neighborhood)

                        }

                    })

                    });


        Livewire.on('confirmDelete', function(data, accion, title){
            Swal.fire({
                title: title,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Eliminar',
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#FF0012',
                cancelButtonColor: '#3085d6',
            }).then((result) => {
                if(result.isConfirmed){
                    Livewire.emit(accion, data);
                }
            })
        });

    </script>
        <script>
            $(document).ready(function () {
                $(".money").inputmask("9{+},99", {
                    positionCaretOnClick: "none",
                    radixPoint: ",",
                    numericInput: true,
                    placeholder: "0",
                });
            });
        </script>



    @yield('js-usable')

</body>

</html>
