@extends('layouts.admin')

@section('title')
    Admin | Xante.com
@endsection


@section('contenido')
    <div class="w-10/12 mx-auto max-h-screen overflow-y-auto px-8 py-8">
        @livewire('new-properties-table')
        <div class="flex items-center justify-center">
            {{-- <a href="api/propiedades/exportarTabla" --}}
            {{-- <a onclick="exportar()"
                class="px-8 py-2 mx-4 text-xl text-white bg-green-500 border-4 border-white rounded-lg shadow-lg cursor-pointer hover:bg-white hover:border-green-500 hover:text-green-500 ">
                {{__('traduction.download_excel')}}
            </a> --}}
        </div>
    </div>
@endsection

@section('js-usable')

    <script type="text/javascript">
        let fecha_inicio = '',
            fecha_fin = '';

        function emit_fechas() {
            if (fecha_inicio != '') {
                Livewire.emit('inicio', fecha_inicio)
            }
            if (fecha_fin != '') {
                Livewire.emit('fin', fecha_fin)
            }
        }

        function exportar() {
            inicio = document.getElementById("inicio").value
            fin = document.getElementById("fin").value
            var url = "{{ route('exportar_tabla', ['inicio' => 'variable_uno', 'fin' => 'variable_dos']) }}";
            url = url.replace('amp;','')
            url = url.replace('variable_uno', inicio)
            url = url.replace('variable_dos', fin)
            location.href=url;
        }
    </script>


@endsection
