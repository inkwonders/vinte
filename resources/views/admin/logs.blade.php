@extends('layouts.admin')

@section('title')
    Admin | Xante.com
@endsection


@section('contenido')
    <div class="w-10/12 mx-auto px-8">
        @livewire('log-table')
        <div class="flex items-center justify-center">
        </div>
    </div>
@endsection

@section('js-usable')



@endsection
