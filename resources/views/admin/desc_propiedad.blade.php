@extends('layouts.base')

@section('title')
    Admin | Xante.mx
@endsection
@section('contenido')
    <div class="w-11/12 py-8 mx-auto">

        <div class="flex w-full">


            <div class="w-1/3">
                <a href="/">
                    <img src="/assets/images/svg/vente_tucasa_dos.png" alt="" class="w-2/6">
                </a>
            </div>
            <div class="flex items-center justify-end w-2/3">
                <a href="/propiedades"
                    class="px-8 py-2 mx-4 text-white border-4 border-white rounded-lg shadow-lg hover:bg-white"
                    style="background-color: #60338a">
                    Regresar
                </a>
                <a href="{{ route('logout') }}"
                    class="px-8 py-2 mx-4 text-white border-4 border-white rounded-lg shadow-lg cursor-pointer hover:bg-white"
                    style="background-color: #60338a">Cerrar Sesión
                </a>
                {{ App::getLocale() }}
                @if (App::getLocale() == 'en')
                    <a href="/locale/es"
                        class="px-8 py-2 mx-4 text-white border-4 border-white rounded-lg shadow-lg cursor-pointer bg-green-500">
                        {{ 'Español' }}
                    </a>
                @else
                    <a href="/locale/en"
                        class="px-8 py-2 mx-2 text-white border-4 border-white rounded-lg shadow-lg cursor-pointer bg-green-500">
                        {{ 'English' }}
                    </a>
                @endif
            </div>
        </div>

        <div class="flex w-full mt-8 ">
            <div class="w-1/2 ">
                <span class="text-gray-600 titilliumwebregular">Fecha de registro: </span> <span
                    class=" titilliumwebsemibold" style="color: #60338a"> {{ $propiedad['fecha_registro'] }} </span>
            </div>
            <div class="w-1/2">
                <h3 class="text-2xl titilliumwebbold" style="color: #b8d057">{{ $propiedad['propiedad_tipo']['tipo'] }}
                    {{ $propiedad['desarrollo'] }} </h3>
            </div>
        </div>
        <div class="flex w-full px-8 py-6 mt-4 border-t-2 border-b-2 " style="color: #b8d057">
            <div class="flex w-1/2">
                <div class="flex flex-col w-1/3 gap-2">
                    {{-- <p class="text-gray-600 titilliumwebregular">
                        Condición:<br>
                        <b class=" titilliumwebsemibold" style="color: #60338a">Buena</b>
                    </p> --}}
                    <p class="text-gray-600 titilliumwebregular">
                        Nombre del desarrollo:<br>
                        <b class=" titilliumwebsemibold" style="color: #60338a">{{ $propiedad['desarrollo'] }}</b>
                    </p>
                    <p class="text-gray-600 titilliumwebregular">
                        Tipo de inmueble:<br>
                        <b class=" titilliumwebsemibold"
                            style="color: #60338a">{{ $propiedad['propiedad_tipo']['tipo'] }}</b>
                    </p>
                    <p class="text-gray-600 titilliumwebregular">
                        Calle o privada:<br>
                        <b class=" titilliumwebsemibold" style="color: #60338a">{{ $propiedad['calle'] }}</b>
                    </p>
                    <p class="text-gray-600 titilliumwebregular">
                        Número exterior:<br>
                        <b class=" titilliumwebsemibold" style="color: #60338a">{{ $propiedad['num_exterior'] }}</b>
                    </p>
                    <p class="text-gray-600 titilliumwebregular">
                        Código Postal:
                        <br> <b class=" titilliumwebsemibold" style="color: #60338a"> {{ $propiedad['codigo_postal'] }}
                        </b>
                    </p>
                    <p class="text-gray-600 titilliumwebregular">Medidas:
                        <br> <b class=" titilliumwebsemibold" style="color: #60338a">{{ $propiedad['superficie'] }}</b>
                    </p>
                    @if ($propiedad['propiedad_tipo']['tipo'] == 'Casa')
                        <p class="text-gray-600 titilliumwebregular">Niveles:
                            <br> <b class=" titilliumwebsemibold"
                                style="color: #60338a">{{ $propiedad['propiedad_tipo']['num_niveles'] }}</b>
                        </p>
                    @endif
                    @if ($propiedad['propiedad_tipo']['tipo'] != 'Casa')
                        <p class="text-gray-600 titilliumwebregular">Piso:
                            <br> <b class=" titilliumwebsemibold"
                                style="color: #60338a">{{ $propiedad['propiedad_tipo']['piso_departamento'] }}</b>
                        </p>
                    @endif

                    <p class="text-gray-600 titilliumwebregular">
                        Número de recámaras:<br>
                        <b class=" titilliumwebsemibold" style="color: #60338a">{{ $propiedad['recamaras'] }}</b>
                    </p>
                </div>
                <div class="flex flex-col w-1/3 gap-2">

                    <p class="text-gray-600 titilliumwebregular">
                        Número de baños:<br>
                        <b class=" titilliumwebsemibold" style="color: #60338a">
                            {{ $propiedad['banios'] }}
                        </b>
                    </p>
                    <p class="text-gray-600 titilliumwebregular">
                        Número de medios baños:
                        <br><b class=" titilliumwebsemibold" style="color: #60338a">
                            {{ $propiedad['med_banios'] }}
                        </b>
                    </p>
                    <p class="text-gray-600 titilliumwebregular">
                        Número de estacionamientos:
                        <br><b class=" titilliumwebsemibold" style="color: #60338a">
                            {{ $propiedad['estacionamientos'] }}
                        </b>
                    </p>
                    <p class="text-gray-600 titilliumwebregular">
                        Años de antigüedad:<br>
                        <b class=" titilliumwebsemibold" style="color: #60338a">
                            {{ $propiedad['antiguedad'] }}
                        </b>
                    </p>
                    <p class="text-gray-600 titilliumwebregular">
                        Precio estimado: <br>
                        <b class=" titilliumwebsemibold" style="color: #60338a">
                            {{ $propiedad['precio'] }}
                        </b>
                    </p>
                    <p class="text-gray-600 titilliumwebregular">
                        Hipotecada:
                        <br><b class=" titilliumwebsemibold" style="color: #60338a">
                            {{ $propiedad['hipoteca'] }}
                        </b>
                    </p>
                    <p class="text-gray-600 titilliumwebregular">
                        Tipo de hipoteca:<br>
                        <b class=" titilliumwebsemibold" style="color: #60338a">
                            @if ($propiedad['hipoteca_tipo'] == null)
                                Ninguna
                            @endif
                            {{ $propiedad['hipoteca_tipo'] }}
                        </b>
                    </p>
                    <p class="text-gray-600 titilliumwebregular">
                        Motivo de la venta:
                        <br><b class=" titilliumwebsemibold" style="color: #60338a">
                            {{ $propiedad['motivo_venta'] }}
                        </b>
                    </p>
                </div>
                <div class="flex flex-col w-1/3 gap-2">
                    <p class="text-gray-600 titilliumwebregular">
                        Teléfono celular:<br>
                        <b class=" titilliumwebsemibold" style="color: #60338a">
                            {{ $propiedad['telefono'] }}
                        </b>
                    </p>
                    <p class="text-gray-600 titilliumwebregular">
                        Correo electrónico:<br>
                        <b class=" titilliumwebsemibold" style="color: #60338a">
                            {{ $propiedad['correo'] }}
                        </b>
                    </p>
                    <p class="text-gray-600 titilliumwebregular">
                        Nombre: <br>
                        <b class=" titilliumwebsemibold" style="color: #60338a">
                            {{ $propiedad['propietario'] }}
                        </b>
                    </p>
                    @if ($propiedad['propiedad_tipo']['tipo'] == 'Casa')
                        <p class="text-gray-600 titilliumwebregular">¿La propiedad cuenta con Terraza o Jardin?:
                            <br> <b class=" titilliumwebsemibold"
                                style="color: #60338a">{{ $propiedad['propiedad_tipo']['terraza_jardin'] }}</b>
                        </p>
                    @endif
                    <p class="text-gray-600 titilliumwebregular">Equipada:
                        <br> <b class=" titilliumwebsemibold" style="color: #60338a">{{ $propiedad['equipado'] }}</b>
                    </p>
                    <p class="text-gray-600 titilliumwebregular">
                        Tipo de equipamiento:
                        <br> <b class=" titilliumwebsemibold" style="color: #60338a">
                            @if ($propiedad['equipado'] != 'SI')
                                NA
                            @endif
                            @foreach ($propiedad['equipos'] as $propiedad_item)
                                {{ $propiedad_item['nombre'] }}
                                <br>
                            @endforeach

                        </b>
                    </p>
                </div>
            </div>
            <div class="grid w-1/2 grid-cols-3 gap-2">
                @foreach ($propiedad['imagenes'] as $item)
                    <div class="w-40">
                        <img src="/{{ $item['url'] }}{{ $item['nombre'] }}" alt="" class="w-full h-64">
                    </div>
                @endforeach
            </div>
        </div>
        <div class="flex justify-center w-full">
            <a href="/api/propiedades/{{ $propiedad['id'] }}/exportar"
                class="px-8 py-2 mx-4 mt-6 text-xl text-white bg-green-500 border-4 border-white rounded-lg shadow-lg cursor-pointer hover:bg-white hover:border-green-500 hover:text-green-500 ">
                Descargar Excel
            </a>
        </div>
    @endsection

    {{-- este es un comentario de prueba --}}
