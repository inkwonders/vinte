@php

    $casas =

        array(

            '0' => array(
                'id_xan'            =>      "XA00012",
                'titulo'            =>      "Real Navarra",
                'ubicacion'         =>      "Pachuca",
                'tipo_propiedad'    =>      "Casa",
                'medidas'           =>      "134 m<sup>2</sup>",
                'cuartos'           =>      "3",
                'banios'            =>      "2 <small>1/2</small>",
                'precio'            =>      "1,650,000",
                'vendido'           =>      1,
                'link'              =>      "real-navarra-pachuca-12",
                'tipo'              =>      "Casa en Venta",
                'estacionamiento'   =>      "2"
            ),

            '1' => array(
                'id_xan'            =>      "XA00015",
                'titulo'            =>      "Real Segovia",
                'ubicacion'         =>      "Puebla",
                'tipo_propiedad'    =>      "Dpto",
                'medidas'           =>      "54 m<sup>2</sup>",
                'cuartos'           =>      "3",
                'banios'            =>      "1",
                'precio'            =>      "607,000",
                'vendido'           =>      0,
                'link'              =>      "real-segovia-puebla-15",
                'tipo'              =>      "Departamento en Venta",
                'estacionamiento'   =>      "1"
            ),

            '2' => array(
                'id_xan'            =>      "XA00017",
                'titulo'            =>      "Real Bilbao",
                'ubicacion'         =>      "Playa del Carmen",
                'tipo_propiedad'    =>      "Dpto",
                'medidas'           =>      "62 m<sup>2</sup>",
                'cuartos'           =>      "3",
                'banios'            =>      "1",
                'precio'            =>      "1,202,000",
                'vendido'           =>      0,
                'link'              =>      "real-bilbao-playa-del-carmen-17",
                'tipo'              =>      "Departamento en Venta",
                'estacionamiento'   =>      "1"
            ),

            '3' => array(
                'id_xan'            =>      "XA00018",
                'titulo'            =>      "Real Ibiza Plus",
                'ubicacion'         =>      "Playa del Carmen",
                'tipo_propiedad'    =>      "Dpto",
                'medidas'           =>      "52 m<sup>2</sup>",
                'cuartos'           =>      "2",
                'banios'            =>      "1",
                'precio'            =>      "1,020,000",
                'vendido'           =>      0,
                'link'              =>      "real-bilbao-playa-del-carmen-18 ",
                'tipo'              =>      "Departamento en Venta",
                'estacionamiento'   =>      "1"
            ),

            '4' => array(
                'id_xan'            =>      "52",
                'titulo'            =>      "Real Toledo",
                'ubicacion'         =>      "Pachuca",
                'tipo_propiedad'    =>      "Casa",
                'medidas'           =>      "69 m<sup>2</sup>",
                'cuartos'           =>      "2",
                'banios'            =>      "1 <small>1/2</small>",
                'precio'            =>      "990,000",
                'vendido'           =>      1,
                'link'              =>      "real-toledo-pachuca-52",
                'tipo'              =>      "Casa en Venta",
                'estacionamiento'   =>      "1"
            ),

        )
    ;

@endphp

@extends('layouts.base')

@section('title')
    Xante.mx
@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <style>
        .focus-select::before {
    content: '';
    width: 0;
    height: 2px;
    background: black; /* Color de la línea del foco, ajusta según tus necesidades */
    position: absolute;
    bottom: 0;
    left: 50%;
}
    </style>
@endsection

@section('contenido')

    <div class="relative w-full min-h-screen fondoback">

        @include('header')

        <div
            class="relative w-full h-auto pt-12 pb-0 overflow-hidden bg-no-repeat lg:h-auto lg:pb-12">

            {{-- <div class="absolute right-0 justify-end hidden h-full lg:flex"> --}}
                <div class="absolute flex hidden w-full h-full lg:flex">
                {{-- <img class="w-full h-full" src="/assets/images/home2.png" alt="xante"> --}}
                <img class="w-full h-full" src="/assets/images/home_full.png" alt="xante">
            </div>

            {{-- <div class="absolute hidden w-full h-full lg:flex gradiente-home"></div> --}}

            <div
                class="relative flex items-center justify-center w-full pt-2 mt-1 lg:pl-12 lg:top-8 2xl:top-18 lg:my-0 lg:justify-start">
                <img src="/assets/images/svg/vente_tucasa_dos.png" alt="xante" class="w-32 lg:h-20 2xl:h-28 lg:w-auto">
            </div>

            <div class="relative flex flex-col items-center justify-center w-auto h-auto pt-8 lg:w-1/2 lg:pt-10 lg:items-start lg:left-4 md:pl-6">

                <p class="w-11/12 text-xl text-center lg:text-2xl lg:w-full text-morado-xante font-poppins-regular lg:text-left">{{ __('landing.message1') }} <br /> <span class="text-verde-landing font-poppins-bold"> {{ __('landing.message3') }}</span> <br /> {{ __('landing.message2') }}</p>

                <div class="relative flex flex-col items-center justify-center w-full pt-10 lg:pt-6 lg:items-center lg:justify-around lg:flex-row">

                    <div class="relative flex flex-col items-center justify-around p-6 bg-white rounded-md shadow-lg lg:items-start w-72 h-72">
                        <img class="h-7" src="/assets/images/svg/icono-moneyhouse.svg" alt="vende tu casa">
                        <p class="pt-6 font-poppins-bold text-morado-claro">{{ __('landing.send_yout_house') }}</p>
                        <p class="leading-tight text-center font-nunito-bold text-gris-texto lg:text-left">{{ __('landing.message_send1') }} <span class="text-purpura">{{ __('landing.message_send2') }}</span></p>
                        <div class="flex items-center justify-center w-full h-auto lg:justify-end">
                            <button id="btnVender" data-tooltip-target="tooltip-click" data-tooltip-trigger="click" class="relative flex justify-center h-auto pt-2 pb-2 text-sm text-white rounded-md shadow-md cursor-pointer lg:w-28 w-36 button-gradient font-poppins-bold hover:shadow-lg" onclick="vender()">{{ __('landing.vender_button') }}</button>
                            <div id="tooltip-click" role="tooltip" class="absolute z-10 invisible inline-block px-3 py-2 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 tooltip dark:bg-gray-700">
                                Tooltip content
                                <div class="tooltip-arrow" data-popper-arrow></div>
                            </div>
                        </div>
                    </div>

                    <div class="relative flex flex-col items-center justify-around p-6 mt-6 bg-white rounded-md shadow-lg lg:mt-0 lg:ml-6 w-72 h-72 lg:items-start">
                        <img class="pb-2 h-14" src="/assets/images/svg/icono-keyshouse.svg" alt="compra tu casa">
                        <p class="font-poppins-bold text-morado-claro">{{ __('landing.buy_house') }}</p>
                        <p class="leading-tight text-center font-nunito-bold text-gris-texto lg:text-left">{{ __('landing.buy_house_message') }} <span class="text-purpura">{{ __('landing.buy_house_message_2') }}</span></p>
                        <div class="flex items-center justify-center w-full h-auto lg:justify-end">
                            <div class="relative flex justify-center h-auto pt-2 pb-2 text-sm text-white rounded-md shadow-md cursor-pointer lg:w-28 w-36 button-gradient font-poppins-bold hover:shadow-lg" onclick="window.location='/comprar/1'">{{ __('landing.buy_button') }}</div>
                        </div>
                    </div>

                </div>

                {{-- <div id="seccion-vender" class="flex flex-col items-center justify-center w-11/12 h-auto pt-10 pb-20 lg:items-start lg:w-full lg:pb-0"> --}}
                    <div id="seccion-vender" class="flex flex-col items-center justify-center w-11/12 h-auto pt-10 lg:items-start lg:w-full lg:pb-0">
                    <p class="pb-2 font-poppins-semibold text-morado">{{ __('landing.send_house') }}</p>
                    <p class="pb-2 text-center font-poppins-regular lg:text-left">{{ __('landing.share_direction') }}</p>

                    <select id="lista-desarrollos" class="w-full bg-white rounded-xl font-poppins-regular" tabindex="0">
                        {{-- <option id="otro" value="999">{{ __('landing.other') }}</option> --}}
                        <option  value="" selected>Selecciona si es Comunidad Vinte u Otro</option>
                        {{-- <option id="otro" value="999">{{ __('landing.other') }}</option> --}}

                    </select>
                    <div class="flex items-center justify-center w-full lg:justify-end">
                        <div onclick="enviar()" class="flex items-center justify-center w-32 pt-4 pb-4 mt-4 text-sm bg-white rounded-md shadow-md cursor-pointer text-purpura font-poppins-bold hover:shadow-lg">{{ __('landing.send_button') }}</div>
                    </div>
                </div>

                {{-- bg-fondo-end bg-cover --}}
                <div class="w-full h-auto realtive lg:hidden">
                    {{-- <img class="w-full h-full" src="/assets/images/home3.png" alt="xante"> --}}
                    <img class="w-full h-full" src="/assets/images/home_full_mobile.png" alt="xante">
                </div>

            </div>

            {{-- cuadro uno --}}
            {{-- <div class="flex flex-col w-11/12 mx-auto 2xl:w-6/12 lg:w-5/12 lg:mr-12">

                <div class="w-full py-1 text-2xl text-center md:py-4 2xl:py-8 2xl:text-5xl xl:text-3xl">
                    <h1>{!! __('landing.sell_h1') !!}</h1>
                </div>

            </div> --}}

            {{-- <div class="flex flex-col w-11/12 mx-auto mt-4 md:w-2/3 lg:mt-2 xl:mt-4 2xl:mt-8 lg:mr-12 2xl:w-6/12 lg:w-5/12 ">

                <div class="flex items-center w-full p-3 bg-white border-black" style="border:solid 7px">

                    <div class="w-1/2 2xl:py-8">

                        @if (App::getLocale() == 'en')

                            <img src="/assets/images/svg/sell_house.png" alt="" class="mx-auto h-14 md:h-16 lg:h-20 2xl:h-36">


                        @else

                            <img src="/assets/images/svg/vende_casa_icon.png" alt="" class="mx-auto h-14 md:h-16 lg:h-20 2xl:h-36">

                        @endif



                    </div>

                    <div class="flex flex-col w-1/2 text-center 2xl:py-2 ">

                        <img src="/assets/images/svg/monney_icon.svg" alt="" class="h-10 mx-auto md:h-12 2xl:h-20 ">

                        <div class="flex justify-center w-full text-center">

                            <a href="/vender" title="Quiero vender" class="flex items-center justify-center w-auto px-2 py-2 mt-2 text-xs font-semibold tracking-widest text-white uppercase transition duration-700 ease-in-out border-4 border-transparent border-white shadow-lg rounded-2xl 2xl:mt-6 md:px-5 2xl:py-3 md:text-sm 2xl:text-base bg-verde-landing titilliumwebbold hover:bg-white hover:border-verde-landing hover:text-verde-landing">

                                {{ __('landing.w_sell') }}

                            </a>

                        </div>

                    </div>

                </div>

            </div> --}}

            {{-- cuadro dos --}}
            {{-- <div class="flex flex-col w-11/12 mx-auto mt-4 md:w-2/3 lg:mt-2 xl:mt-4 lg:mr-12 2xl:w-6/12 lg:w-5/12">
                <div class="flex items-center w-full p-3 bg-white border-black" style="border:solid 7px">
                    <div class="w-1/2 2xl:py-8">

                        @if (App::getLocale() == 'en')

                        <img src="/assets/images/svg/buy_house.png" alt="" class="mx-auto h-14 md:h-16 lg:h-20 2xl:h-36">

                        @else
                        <img src="/assets/images/svg/compra_casa_icon.png" alt="" class="mx-auto h-14 md:h-16 lg:h-20 2xl:h-36">
                        @endif

                    </div>
                    <div class="flex flex-col w-1/2 text-center 2xl:py-2 ">
                        <img src="/assets/images/svg/key_icon.svg" alt="" class="h-12 mx-auto md:h-16 2xl:h-24">
                        <div class="flex justify-center w-full text-center">
                            <a href="/comprar/1" title="Quiero comprar"
                                class="flex items-center justify-center w-auto px-2 py-2 mt-2 text-xs font-semibold tracking-widest text-white uppercase transition duration-700 ease-in-out border-4 border-transparent border-white shadow-lg rounded-2xl 2xl:mt-6 md:px-5 2xl:py-3 md:text-sm 2xl:text-base bg-verde-landing titilliumwebbold hover:bg-white hover:border-verde-landing hover:text-verde-landing">
                                {{ __('landing.w_buy') }}
                            </a>
                        </div>
                    </div>

                </div>
            </div> --}}

            {{-- <div
                class="flex w-full px-4 pt-4 mx-auto leading-3 text-center text-justify text-gris-texto text-xxs lg:text-xs font-montserrat-regular md:w-2/3 lg:mt-2 lg:mr-12 2xl:w-6/12 lg:w-5/12">
                {{ __('landing.time_') }}
            </div> --}}

        </div>

        <div class="relative z-30 flex items-center justify-center w-full h-8 text-sm text-center text-white"
            style="background-color: #60338a">

            <a target="_blank" href="https://wa.link/om0mki" class="fixed w-16 h-16 bottom-2 right-2 ">

                <img src="/assets/images/png/whats.png" class="absolute h-full">

            </a>

        </div>

        <div class="relative w-full">

            <div class="back_grad_1">

                <div class="w-full px-5 mx-auto text-center lg:w-4/5">

                    <h2 class="py-5 text-xl lg:py-10 lg:text-3xl 2xl:text-5xl font-montserrat-regular ">
                        {!! __('landing.h2_p') !!} </h2>

                </div>

                <div class="relative flex flex-col justify-around w-9/12 gap-8 mx-auto mt-10 lg:w-10/12 2xl:9/12 lg:flex-row">

                    <div class="vector_1"></div>

                    <div class="vector_2"></div>

                    <div class="flex flex-col items-center justify-start w-full py-12 mx-auto bg-white rounded-sm shadow-lg md:w-4/5 lg:w-2/5 2xl:w-1/3">

                        <div class="flex items-center justify-center w-full h-32 ">

                            <img src="/assets/images/png/casa_venta.png" class="w-24 " alt="Vende tu casa">

                        </div>

                        <div class="flex items-center justify-center w-full pt-4">

                            <h3 class="text-2xl lg:text-3xl font-montserrat-bold">
                                @if (App::getLocale() == 'es'){{ '¡' }}@endif{{ __('landing.sell_h'). '!' }}
                            </h3>

                        </div>

                        <div class="flex items-start justify-center w-full h-48 mt-4 ">

                            <span class="px-2 text-lg text-center lg:text-xl lg:px-16 font-montserrat-regular">{!! __('landing.sell_h_p') !!}</span>

                        </div>


                        <a href="#seccion-vender" title="Quiero vender"
                            class="inline-flex items-center px-8 py-2 mt-2 text-base font-semibold tracking-widest text-white uppercase transition duration-700 ease-in-out border-4 border-transparent border-white rounded-md shadow-lg cursor-pointer 2xl:mt-6 md:px-16 2xl:py-3 md:text-sm 2xl:text-base bg-verde-landing titilliumwebbold hover:bg-white hover:border-verde-landing hover:text-verde-landing" onclick="vender()">
                            {{ __('landing.w_sell') }}
                        </a>

                    </div>

                    <div class="flex flex-col items-center justify-start w-full py-12 mx-auto bg-white rounded-sm shadow-lg md:w-4/5 lg:w-2/5 2xl:w-1/3">

                        <div class="flex items-center justify-center w-full h-32">

                            <img src="/assets/images/png/casa_renta.png" class="w-24 " alt="Vende tu casa">

                        </div>

                        <div class="flex items-center justify-center w-full pt-4">

                            <h3 class="text-2xl lg:text-3xl font-montserrat-bold">

                                @if (App::getLocale() == 'es'){{ '¡' }}@endif{{ __('landing.buy_h').'!' }}

                            </h3>

                        </div>

                        <div class="flex items-start justify-center w-full h-48 mt-4 lg:h-48">

                            <span class="px-2 text-lg text-center lg:text-xl lg:px-16 font-montserrat-regular">
                                {!! __('landing.buy_h_p') !!}
                            </span>


                        </div>


                        <a href="/comprar/1" title="Quiero comprar"
                            class="inline-flex items-center px-8 py-2 mt-2 text-base font-semibold tracking-widest text-white uppercase transition duration-700 ease-in-out border-4 border-transparent border-white rounded-md shadow-lg cursor-pointer 2xl:mt-6 md:px-16 2xl:py-3 md:text-sm 2xl:text-base bg-verde-landing titilliumwebbold hover:bg-white hover:border-verde-landing hover:text-verde-landing">
                            {{ __('landing.w_buy') }}

                        </a>

                    </div>

                </div>

            </div>

            <div class="w-full px-8 py-10 mx-auto lg:w-9/12">

                <span
                    class="block w-full text-justify text-md leading-2 lg:text-justify lg:w-full 2xl:w-full text-gris-texto-claro font-montserrat-regular ">

                    {{-- {{ __('landing.time_') }} <br /><br />

                    {{ __('landing.bills') }} --}}
                    {{ __('landing.time_1_nw') }} <br /><br />
                    {!! __('landing.buy_h_p_2') !!}



                </span>

            </div>

            <div class="relative flex-col items-center justify-center gap-4 py-8 text-center text-white lg:text-left lg:flex bg-fondo-xante_morado">
                <h2 class="text-3xl lg:text-5xl font-montserrat-bold"> {{ __('landing.buy_h') }} </h2>
    
                <p class="text-base text-center lg:text-2xl">
                    {!! __('landing.buy_p') !!}
                </p>
    
                <div class="relative flex items-center justify-center w-full">
    
                    <div class="flex justify-between w-full gap-16 px-4 mx-auto mt-6 2xl:w-9/12 slider_propiedades_desk">
    
                        @livewire('slider-inicio')
    
                    </div>
    
                    <div class="absolute flex justify-between w-11/12 slider_propiedades_desk_arrows"></div>
    
                </div>
    
                <div class="flex flex-col items-center justify-center">
    
                    <span class="px-4 py-4 text-xs text-justify lg:text-xl font-montserrat-regular ">{{ __('landing.apart') }}</span>
    
                    {{-- <a target="_blank" href="https://wa.me/+525579318910" title="Contáctanos" --}}
                    <a target="_blank" href="https://wa.me/+525570464021" title="Contáctanos"
                        class="items-center px-24 py-2 mt-2 text-xs font-semibold tracking-widest text-white uppercase transition duration-700 ease-in-out border-4 border-transparent border-white rounded-md shadow-lg cursor-pointer 2xl:mt-6 md:px-32 2xl:py-3 md:text-sm 2xl:text-base bg-verde-landing titilliumwebbold hover:bg-white hover:border-verde-landing hover:text-verde-landing">
                        {{ __('landing.contact') }}
                    </a>
    
                </div>
    
            </div>
                <div class="flex flex-col items-end w-full mt-16 lg:items-stretch lg:flex-row">

                <div class="w-full px-8 text-center lg:w-1/2 lg:pl-24 2xl:pl-48 lg:text-left">

                    <h2 class="py-8 text-3xl lg:text-4xl 2xl:text-6xl font-montserrat-bold text-morado-xante">
                        {{ __('landing.why_sell') }} </h2>

                    <div class="flex flex-col pt-4 lg:flex-row">

                        <div class="flex justify-center w-full p-4 lg:block lg:w-1/6">

                            <img src="/assets/images/png/icon_ofertas.png" class="w-16" alt="">

                        </div>

                        <div class="w-full lg:w-5/6">

                            <h3 class="py-1 text-xl font-montserrat-bold"> {{ __('landing.days_10') }} </h3>

                            <p class="text-xl font-montserrat-regular"> {{ __('landing.wr_p') }}</p>

                        </div>

                    </div>

                    <div class="flex flex-col pt-4 lg:flex-row">

                        <div class="flex justify-center w-full p-4 lg:block lg:w-1/6">

                            <img src="/assets/images/png/icon_reloj.png" class="w-16" alt="">

                        </div>

                        <div class="w-full lg:w-5/6 ">

                            <h3 class="py-1 text-xl font-montserrat-bold"> {{ __('landing.accept_offer') }} </h3>

                            <p class="text-xl font-montserrat-regular">{{ __('landing.accept_offer_p') }}</p>

                        </div>

                    </div>

                    <div class="flex flex-col pt-4 lg:flex-row">

                        <div class="flex justify-center w-full p-4 lg:block lg:w-1/6">

                            <img src="/assets/images/png/icon_conversacion.png" class="w-16" alt="">

                        </div>

                        <div class="w-full lg:w-5/6">

                            <h3 class="py-1 text-xl font-montserrat-bold"> {{ __('landing.attention') }} </h3>

                            <p class="text-xl font-montserrat-regular">{{ __('landing.attention_p') }} </p>

                        </div>

                    </div>

                </div>

                <div class="block w-full px-8 py-8 mx-auto lg:hidden lg:w-9/12">

                    <span
                        class="block w-full text-justify text-md leading-2 lg:text-justify text-gris-texto-claro font-montserrat-regular">

                        {{ __('landing.time_1_nw') }} <br /><br />
{{--
                        {{ __('landing.bills') }} --}}

                    </span>

                </div>

                <div class="w-11/12 min-h-screen px-8 pt-56 lg:w-1/2 back_vender "></div>

            </div>

            <div class="hidden w-full px-8 py-8 mx-auto lg:block lg:w-9/12">

                <span
                    class="block w-full text-justify text-md leading-2 lg:text-justify text-gris-texto-claro font-montserrat-regular">

                    {{ __('landing.time_1_nw') }} <br /><br />

                    {{-- {{ __('landing.bills') }} --}}

                </span>

            </div>

        </div>

        <div class="relative back_grad_3">

            <div class="hidden vector_6 lg:block"></div>

            <div class="w-full px-8 py-8 mx-auto lg:w-11/12 2xl:w-9/12 ">

                <div class="w-full text-center">

                    <h2 class="text-3xl lg:text-5xl font-montserrat-bold"> {{ __('landing.experience') }} </h2>

                    <h3 class="py-4 text-2xl lg:text-4xl font-montserrat-regular"> {{ __('landing.easy') }}</h3>

                </div>

                <div class="hidden w-full gap-4 mt-12 2xl:gap-6 lg:flex ">

                    @for ($i = 1; $i < 5; $i++)
                        <div
                            class="relative flex flex-col items-center justify-start w-1/4 bg-white rounded-sm shadow-lg pb-7">

                            <div
                                class="absolute top-0 left-0 flex items-center justify-center w-8 h-8 text-white bg-morado-xante">
                                {{ $i }}
                            </div>

                            <div class="relative flex justify-center w-full py-14">

                                <img src="/assets/images/png/icon_experiencia_{{ $i }}.png"
                                    class="h-16" alt="">

                            </div>

                            <div class="relative flex flex-col items-center justify-start w-full px-6 text-center h-3/4">

                                <div class="flex items-start justify-center w-full h-24">


                                    <h3 class="text-2xl font-montserrat-bold">

                                        @switch($i)
                                            @case(1)
                                                {{ __('landing.meet_h') }}
                                            @break

                                            @case(2)
                                                {{ __('landing.visit_h') }}
                                            @break

                                            @case(3)
                                                {{ __('landing.offer_h') }}
                                            @break

                                            @case(4)
                                                {{ __('landing.money_72') }}
                                            @break
                                        @endswitch


                                    </h3>

                                </div>


                                <span class="text-xlfont-montserrat-regular">


                                    @switch($i)
                                        @case(1)
                                            {!! __('landing.meet_h_p') !!}
                                        @break

                                        @case(2)
                                            {{ __('landing.visit_h_p') }}
                                        @break

                                        @case(3)
                                            {{ __('landing.offer_h_p') }}
                                        @break

                                        @case(4)
                                            {{ __('landing.money_72_p') }}
                                        @break
                                    @endswitch

                                </span>

                            </div>

                        </div>
                    @endfor

                </div>

                <div class="flex lg:hidden">

                    <div class="block w-full mt-8 bg-white shadow md:w-3/5 md:mx-auto lg:hidden slider_movil">

                        @for ($i = 1; $i < 5; $i++)
                            <div
                                class="relative flex flex-col items-center justify-center w-full h-full bg-white rounded-sm pb-7">

                                <div
                                    class="absolute top-0 left-0 flex items-center justify-center w-8 h-8 text-white bg-morado-xante">
                                    {{ $i }}
                                </div>

                                <div class="flex justify-center py-14">

                                    <img src="/assets/images/png/icon_experiencia_{{ $i }}.png"
                                        class="h-12" alt="">

                                </div>

                                <div class="px-8 text-center ">

                                    <h3 class="text-2xl pb-7 font-montserrat-bold">

                                        @switch($i)
                                            @case(1)
                                                {{ __('landing.meet_h') }}
                                            @break

                                            @case(2)
                                                {{ __('landing.visit_h') }}
                                            @break

                                            @case(3)
                                                {{ __('landing.offer_h') }}
                                            @break

                                            @case(4)
                                                {{ __('landing.money_72') }}
                                            @break
                                        @endswitch


                                    </h3>

                                    <span class="text-xl font-montserrat-regular">


                                        @switch($i)
                                            @case(1)
                                                {!! __('landing.meet_h_p') !!}
                                            @break

                                            @case(2)
                                                {{ __('landing.visit_h_p') }}
                                            @break

                                            @case(3)
                                                {{ __('landing.offer_h_p') }}
                                            @break

                                            @case(4)
                                                {{ __('landing.money_72_p') }}
                                            @break
                                        @endswitch

                                    </span>

                                </div>

                            </div>
                        @endfor

                    </div>

                </div>

                <div class="my-4 dots_movil lg:hidden"></div>

            </div>


            <div class="w-full px-4 py-8 mx-auto leading-2 md:w-3/5 md:mx-auto lg:px-2">

                <span
                    class="block w-full text-justify text-md leading-2 lg:text-md lg:text-justify text-gris-texto-claro font-montserrat-regular">

                    {{ __('landing.time_1_nw') }} <br /><br />

                    {{-- {{ __('landing.bills') }} --}}

                </span>

            </div>

        </div>


        <div class="w-full py-8 bg-morado-xante_claro">

            <div class="flex justify-center py-4 text-center">

                <h2 class="text-4xl text-verde-landing font-montserrat-bold">{!! __('landing.partners') !!}</h2>

            </div>

            <div class="flex slick_container">

                {{-- @for ($i = 1; $i < 6; $i++)

                    <div class="w-1/3 py-12">

                        <div class="flex items-center justify-center w-full">

                            <img src="/assets/images/png/slider_{{ $i }}.png" class="w-3/4 lg:w-1/4 lg:h-auto" alt="logos">

                        </div>

                    </div>

                @endfor --}}

                <div class="w-1/3 py-12 ">

                    <div class="flex flex-col items-center justify-end w-full h-40 gap-4 ">

                        <a class="flex flex-col items-center justify-center"
                            href="https://homie.mx/new?campaign=GGL_OB_ENE_01_2021_BRAND&gclid=CjwKCAjwj42UBhAAEiwACIhADh7v6zO7c1gHrEnkTTRkEOhPTe9AjhWUQgTDRECazaEHLelR_TxnlBoC5dsQAvD_BwE"
                            target="_blank">

                            <img src="/assets/images/png/slider_1.png" class="w-56" alt="logos">

                        </a>

                        <span
                            class="text-2xl text-center font-montserrat-semibold text-gris-texto-footer">{{ __('landing.partnes_uno') }}</span>

                    </div>

                </div>

                <div class="w-1/3 py-12">

                    <div class="flex flex-col items-center justify-end w-full h-40 gap-4 ">

                        <a class="flex flex-col items-center justify-center"
                            href="https://casabravo.com.mx/desarrollos-vinte-renta-para-comprar?utm_source=vinte&utm_medium=landing&utm_campaign=real_alcala&utm_id=alc-02"
                            target="_blank">

                            <img src="/assets/images/png/slider_2.png" class="w-80 lg:w-96" alt="logos">

                        </a>

                        <span
                            class="text-2xl text-center font-montserrat-semibold text-gris-texto-footer">{{ __('landing.partnes_dos') }}</span>

                    </div>

                </div>

                <div class="w-1/3 py-12">

                    <div class="flex flex-col items-center justify-end w-full h-40 gap-4 ">

                        <a class="flex flex-col items-center justify-center"
                            href="https://yave.mx/adquisicion-de-vivienda?utm_source=Google&utm_medium=Paid&utm_campaign=2020-Search_Adquisici%C3%B3n_Trafico&utm_content=113788415922&gclid=CjwKCAjwj42UBhAAEiwACIhADsF7vzOIMpXYz2rNGqYvKIBmD0k7cI6Hg916AgLTgOT_j3MKb8NPQRoCjtQQAvD_BwE"
                            target="_blank">

                            <img src="/assets/images/png/slider_3.png" class="w-64 pt-7" alt="logos">

                        </a>

                        <span
                            class="text-2xl text-center font-montserrat-semibold text-gris-texto-footer">{{ __('landing.partnes_tres') }}</span>

                    </div>

                </div>

                <div class="w-1/3 py-12">

                    <div class="flex flex-col items-center justify-end w-full h-40 gap-4 ">

                        <a class="flex flex-col items-center justify-center" href="https://emobel.com.mx/" target="_blank">

                            <img src="/assets/images/png/slider_4.png" class="w-56 pt-12" alt="logos">

                        </a>

                        <span
                            class="px-4 text-2xl text-center font-montserrat-semibold text-gris-texto-footer">{{ __('landing.partnes_cuatro') }}</span>


                    </div>

                </div>

                <div class="w-1/3 py-12">

                    <div class="flex flex-col items-center justify-end w-full h-40 gap-4 ">

                        <a class="flex flex-col items-center justify-end w-full h-40 gap-4" href="https://www.vinte.com.mx/"
                            target="_blank">

                            <img src="/assets/images/png/slider_5.png" class="w-64 pt-10" alt="logos">

                        </a>

                        {{-- <span
                            class="px-4 text-2xl text-center font-montserrat-semibold text-gris-texto-footer">{{ __('landing.partnes_cinco') }}
                        </span> --}}

                    </div>

                </div>

            </div>

            <div class="relative flex items-center justify-center py-4 dots_slider"></div>

        </div>

        <div class="flex flex-col-reverse lg:flex-row ">

            <div class="relative flex items-end justify-center w-full lg:w-5/12 ">

                <img src="/assets/images/png/bg_asesor.png" class="absolute bottom-0 z-0 w-full" alt="">

                <img src="/assets/images/png/asesor.png" class="relative z-10 w-3/4 h-auto lg:w-auto lg:h-3/4 " alt="">

            </div>

            <div
                class="flex flex-col justify-center w-full gap-8 px-8 pt-16 pb-20 pl-4 text-center bg-white lg:pt-20 lg:text-left lg:justify-end lg:w-7/12 ">

                <h3 class="text-2xl lg:text-5xl text-morado-xante font-montserrat-bold">{{ __('landing.adviser') }}</h3>

                <p class="text-lg lg:text-3xl text-gris-texto-claro">{{ __('landing.adviser_p1') }}</p>

                <p class="text-lg lg:text-3xl text-gris-texto-claro"> {{ __('landing.adviser_p2') }} </p>

                <div class="flex justify-center w-full">

                    <a target="_blank" href="https://wa.me/+525579318910" title="Contáctanos"
                        class="inline-flex items-center px-6 py-2 mt-2 text-base font-semibold tracking-widest text-white uppercase transition duration-700 ease-in-out border-4 border-transparent border-white rounded-md shadow-lg cursor-pointer 2xl:mt-6 md:px-24 2xl:py-3 md:text-lg bg-verde-landing titilliumwebbold hover:bg-white hover:border-verde-landing hover:text-verde-landing">
                        {{ __('landing.contact') }}
                    </a>

                </div>

            </div>

        </div>

        <div class="flex flex-col w-full pb-16 lg:py-28 bg-fondo-xante_gris">

            <div class="relative flex items-center">

                <div class=" vector_3"></div>

                <div class="z-10 flex flex-col gap-4 px-4 py-8 text-white lg:px-14 bg-verde-landing">

                    <h3 class="text-3xl text-center lg:text-left lg:text-4xl 2xl:text-6xl font-montserrat-bold">

                        {{ __('landing.client') }}

                    </h3>

                    <span class="text-lg text-center lg:text-left lg:text-2xl">

                        {!! __('landing.client_p') !!}

                    </span>

                </div>

            </div>

            <div class="relative flex w-5/6 mx-auto">

                <div class="vector_4"></div>

                <div class="w-full slider_resenias">

                    <div>
                        <div class="flex flex-col w-full gap-8 pt-10 pb-4 lg:flex-row lg:pb-12 lg:pt-24 lg:w-full ">
                            @for ($i = 1; $i < 3; $i++)
                                <div class="relative z-10 flex w-11/12 py-20 mx-auto bg-white shadow-md lg:py-16 md:w-3/4 lg:px-20">
                                    <div class="absolute top-0 left-0 w-6 h-20 bg-morado-xante"></div>
                                    <div class="w-full px-10 text-xs text-justify lg:pl-0 lg:text-base font-montserrat-regular">
                                        @switch($i)
                                            @case(1)
                                                {!! __('landing.opinion_9') !!}
                                            @break
                                            @case(2)
                                                {!! __('landing.opinion_10') !!}
                                            @break
                                        @endswitch
                                    </div>

                                    <div class="absolute flex flex-col items-center justify-center w-full text-sm lg:w-4/5 lg:justify-between lg:flex-row bottom-5">
                                        <div>
                                            <p><b>
                                                    @if ($i == 1)
                                                        Mario Jiménez Montoya
                                                    @else
                                                    Lenya Paoola Agraz Rodríguez
                                                    @endif
                                                </b></p>
                                        </div>
                                        <div class="flex items-center gap-2">
                                            @if ($i == 2)
                                                @for ($a = 0 ; $a < 4; $a++)

                                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 lg:h-7 lg:w-7 text-amarillo-estrella"
                                                        viewBox="0 0 20 20" fill="currentColor">
                                                        <path
                                                            d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                                                    </svg>

                                                @endfor
                                            @else
                                                @for ($a = 0 ; $a < 5; $a++)

                                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 lg:h-7 lg:w-7 text-amarillo-estrella"
                                                        viewBox="0 0 20 20" fill="currentColor">
                                                        <path
                                                            d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                                                    </svg>

                                                @endfor
                                            @endif

                                            {{-- <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4 lg:w-6 lg:h-6" fill="none"
                                                viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                    d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z" />
                                            </svg> --}}

                                        </div>
                                    </div>
                                </div>
                            @endfor
                        </div>
                    </div>


                    <div>
                        <div class="flex flex-col w-full gap-8 pt-10 pb-4 lg:flex-row lg:pb-12 lg:pt-24 lg:w-full ">
                            @for ($i = 1; $i < 3; $i++)
                                <div class="relative z-10 flex w-11/12 py-20 mx-auto bg-white shadow-md lg:py-16 md:w-3/4 lg:px-20">
                                    <div class="absolute top-0 left-0 w-6 h-20 bg-morado-xante"></div>
                                    <div class="w-full px-10 text-xs text-justify lg:pl-0 lg:text-base font-montserrat-regular">
                                        @switch($i)
                                            @case(1)
                                                {!! __('landing.opinion_11') !!}
                                            @break
                                            @case(2)
                                                {!! __('landing.opinion_12') !!}
                                            @break
                                        @endswitch
                                    </div>

                                    <div class="absolute flex flex-col items-center justify-center w-full text-sm lg:w-4/5 lg:justify-between lg:flex-row bottom-5">
                                        <div>
                                            <p><b>
                                                    @if ($i == 1)
                                                    Bárbara Rangel Fonseca
                                                    @else
                                                    Pedro Alfonso Bojaca Santiago
                                                    @endif
                                                </b></p>
                                        </div>
                                        <div class="flex items-center gap-2">
                                            @if ($i == 2)
                                                @for ($a = 0 ; $a < 4; $a++)

                                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 lg:h-7 lg:w-7 text-amarillo-estrella"
                                                        viewBox="0 0 20 20" fill="currentColor">
                                                        <path
                                                            d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                                                    </svg>

                                                @endfor
                                            @else
                                                @for ($a = 0 ; $a < 5; $a++)

                                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 lg:h-7 lg:w-7 text-amarillo-estrella"
                                                        viewBox="0 0 20 20" fill="currentColor">
                                                        <path
                                                            d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                                                    </svg>

                                                @endfor
                                            @endif

                                            {{-- <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4 lg:w-6 lg:h-6" fill="none"
                                                viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                    d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z" />
                                            </svg> --}}

                                        </div>
                                    </div>
                                </div>
                            @endfor
                        </div>
                    </div>

                    <div>
                        <div class="flex flex-col w-full gap-8 pt-10 pb-4 lg:flex-row lg:pb-12 lg:pt-24 lg:w-full ">
                            @for ($i = 1; $i < 3; $i++)
                                <div class="relative z-10 flex w-11/12 py-20 mx-auto bg-white shadow-md lg:py-16 md:w-3/4 lg:px-20">
                                    <div class="absolute top-0 left-0 w-6 h-20 bg-morado-xante"></div>
                                    <div class="w-full px-10 text-xs text-justify lg:pl-0 lg:text-base font-montserrat-regular">
                                        @switch($i)
                                            @case(1)
                                                {!! __('landing.opinion_13') !!}
                                            @break
                                            @case(2)
                                                {!! __('landing.opinion_7') !!}
                                            @break
                                        @endswitch
                                    </div>

                                    <div class="absolute flex flex-col items-center justify-center w-full text-sm lg:w-4/5 lg:justify-between lg:flex-row bottom-5">
                                        <div>
                                            <p><b>
                                                    @if ($i == 1)
                                                    Ma. Dolores Rivera Bustamante
                                                    @else
                                                    Armando Suárez
                                                    @endif
                                                </b></p>
                                        </div>
                                        <div class="flex items-center gap-2">
                                            {{-- @if ($i == 2) --}}
                                                @for ($a = 0 ; $a < 5; $a++)

                                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 lg:h-7 lg:w-7 text-amarillo-estrella"
                                                        viewBox="0 0 20 20" fill="currentColor">
                                                        <path
                                                            d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                                                    </svg>

                                                @endfor
                                            {{-- @else
                                                @for ($a = 0 ; $a < 5; $a++)

                                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 lg:h-7 lg:w-7 text-amarillo-estrella"
                                                        viewBox="0 0 20 20" fill="currentColor">
                                                        <path
                                                            d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                                                    </svg>

                                                @endfor
                                            @endif --}}

                                            {{-- <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4 lg:w-6 lg:h-6" fill="none"
                                                viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                    d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z" />
                                            </svg> --}}

                                        </div>
                                    </div>
                                </div>
                            @endfor
                        </div>
                    </div>

                    {{-- anteoriores --}}
                    <div>
                        <div class="flex flex-col w-full gap-8 pt-10 pb-4 lg:flex-row lg:pb-12 lg:pt-24 lg:w-full ">
                            @for ($i = 1; $i < 3; $i++)
                                <div class="relative z-10 flex w-11/12 py-20 mx-auto bg-white shadow-md lg:py-16 md:w-3/4 lg:px-20">
                                    <div class="absolute top-0 left-0 w-6 h-20 bg-morado-xante"></div>
                                    <div class="w-full px-10 text-xs text-justify lg:pl-0 lg:text-base font-montserrat-regular">
                                        @switch($i)
                                            @case(1)
                                                {!! __('landing.opinion_2') !!}
                                            @break
                                            @case(2)
                                                {!! __('landing.opinion_3') !!}
                                            @break
                                        @endswitch
                                    </div>

                                    <div class="absolute flex flex-col items-center justify-center w-full text-sm lg:w-4/5 lg:justify-between lg:flex-row bottom-5">
                                        <div>
                                            <p><b>
                                                    @if ($i == 1)
                                                    Omar Nahoul
                                                    @else
                                                    Noé Espinosa
                                                    @endif
                                                </b></p>
                                        </div>
                                        <div class="flex items-center gap-2">
                                            @for ($a = 0 ; $a < 5; $a++)
                                                <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 lg:h-7 lg:w-7 text-amarillo-estrella"
                                                    viewBox="0 0 20 20" fill="currentColor">
                                                    <path
                                                        d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                                                </svg>
                                            @endfor
                                            {{-- <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4 lg:w-6 lg:h-6" fill="none"
                                                viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                    d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z" />
                                            </svg> --}}
                                        </div>
                                    </div>
                                </div>
                            @endfor
                        </div>
                    </div>

                    <div>

                        <div class="flex flex-col w-full gap-8 pt-10 pb-4 lg:flex-row lg:pb-12 lg:pt-24 lg:w-full ">

                            @for ($i = 1; $i < 3; $i++)

                                <div class="relative z-10 flex w-11/12 py-20 mx-auto bg-white shadow-md lg:py-16 md:w-3/4 lg:px-20">

                                    <div class="absolute top-0 left-0 w-6 h-20 bg-morado-xante"></div>

                                    <div class="w-full px-10 text-xs text-justify lg:pl-0 lg:text-base font-montserrat-regular">

                                        @switch($i)

                                            @case(1)

                                                {!! __('landing.opinion_1') !!}

                                            @break

                                            @case(2)

                                                {!! __('landing.opinion_8') !!}

                                            @break

                                        @endswitch

                                    </div>

                                    <div class="absolute flex flex-col items-center justify-center w-full text-sm lg:w-4/5 lg:justify-between lg:flex-row bottom-5">

                                        <div>

                                            <p><b>

                                                    @if ($i == 1)

                                                    Alejandra Delgado

                                                    @else

                                                    Armando Suárez
                                                    @endif

                                                </b></p>


                                        </div>

                                        <div class="flex items-center gap-2">
                                            @if ($i == 2)
                                                @for ($a = 0 ; $a < 4; $a++)

                                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 lg:h-7 lg:w-7 text-amarillo-estrella"
                                                        viewBox="0 0 20 20" fill="currentColor">
                                                        <path
                                                            d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                                                    </svg>

                                                @endfor
                                            @else
                                                @for ($a = 0 ; $a < 5; $a++)

                                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 lg:h-7 lg:w-7 text-amarillo-estrella"
                                                        viewBox="0 0 20 20" fill="currentColor">
                                                        <path
                                                            d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                                                    </svg>

                                                @endfor
                                            @endif

                                            {{-- <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4 lg:w-6 lg:h-6" fill="none"
                                                viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                    d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z" />
                                            </svg> --}}

                                        </div>

                                    </div>

                                </div>

                            @endfor

                        </div>

                    </div>

                    <div>

                        <div class="flex flex-col w-full gap-8 pt-10 pb-4 lg:flex-row lg:pb-12 lg:pt-24 lg:w-full ">

                            @for ($i = 1; $i < 3; $i++)

                                <div class="relative z-10 flex w-11/12 py-20 mx-auto bg-white shadow-md lg:py-16 md:w-3/4 lg:px-20">

                                    <div class="absolute top-0 left-0 w-6 h-20 bg-morado-xante"></div>

                                    <div class="w-full px-10 text-xs text-justify lg:pl-0 lg:text-base font-montserrat-regular">

                                        @switch($i)

                                            @case(1)

                                                {!! __('landing.opinion_5') !!}

                                            @break

                                            @case(2)

                                                {!! __('landing.opinion_6') !!}

                                            @break

                                        @endswitch

                                    </div>

                                    <div class="absolute flex flex-col items-center justify-center w-full text-sm lg:w-4/5 lg:justify-between lg:flex-row bottom-5">

                                        <div>

                                            <p><b>

                                                    @if ($i == 1)
                                                    Dafne Cruz

                                                    @else
                                                    Diana Granados
                                                    @endif

                                                </b></p>


                                        </div>

                                        <div class="flex items-center gap-2">
                                            {{-- @if ($i == 2)
                                                @for ($a = 0 ; $a < 4; $a++)

                                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 lg:h-7 lg:w-7 text-amarillo-estrella"
                                                        viewBox="0 0 20 20" fill="currentColor">
                                                        <path
                                                            d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                                                    </svg>

                                                @endfor
                                            @else --}}
                                                @for ($a = 0 ; $a < 5; $a++)

                                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 lg:h-7 lg:w-7 text-amarillo-estrella"
                                                        viewBox="0 0 20 20" fill="currentColor">
                                                        <path
                                                            d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                                                    </svg>

                                                @endfor
                                            {{-- @endif --}}

                                            {{-- <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4 lg:w-6 lg:h-6" fill="none"
                                                viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                    d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z" />
                                            </svg> --}}

                                        </div>

                                    </div>

                                </div>

                            @endfor

                        </div>

                    </div>

                    <div>

                        <div class="flex flex-col w-full gap-8 pt-10 pb-4 lg:flex-row lg:pb-12 lg:pt-24 lg:w-full ">

                            @for ($i = 1; $i < 3; $i++)

                                <div class="relative z-10 flex w-11/12 py-20 mx-auto bg-white shadow-md lg:py-16 md:w-3/4 lg:px-20">

                                    <div class="absolute top-0 left-0 w-6 h-20 bg-morado-xante"></div>

                                    <div class="w-full px-10 text-xs text-justify lg:pl-0 lg:text-base font-montserrat-regular">

                                        @switch($i)

                                            @case(1)

                                                {!! __('landing.opinion_4') !!}

                                            @break

                                            {{-- @case(2)

                                                {!! __('landing.') !!}

                                            @break --}}

                                        @endswitch

                                    </div>

                                    <div class="absolute flex flex-col items-center justify-center w-full text-sm lg:w-4/5 lg:justify-between lg:flex-row bottom-5">

                                        <div>

                                            <p><b>

                                                    @if ($i == 1)

                                                    Bernardo Tovar

                                                    @else



                                                    @endif

                                                </b></p>


                                        </div>

                                        <div class="flex items-center gap-2">

                                            @for ($a = 0 ; $a < 5; $a++)
                                            @if ($i == 1)
                                                <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 lg:h-7 lg:w-7 text-amarillo-estrella"
                                                    viewBox="0 0 20 20" fill="currentColor">
                                                    <path
                                                        d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                                                </svg>
                                                @else



                                                @endif
                                            @endfor

                                            {{-- <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4 lg:w-6 lg:h-6" fill="none"
                                                viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                                <path stroke-linecap="round" stroke-linejoin="round"
                                                    d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z" />
                                            </svg> --}}

                                        </div>

                                    </div>

                                </div>

                            @endfor

                        </div>

                    </div>


                </div>

            </div>

        </div>

        <div class="relative pb-6 bg-fondo-xante_palabras">

            <div class="py-10 lg:pl-32">

                <h2 class="text-xl text-center lg:text-left lg:text-4xl font-montserrat-bold text-morado-xante">
                    {!! __('landing.faq_p') !!}</h2>

            </div>

            <div class="vector_5"></div>

            <div class="relative z-10 flex flex-col w-10/12 gap-4 px-4 py-6 mx-auto bg-white lg:px-16 acc-container">

                <div class="px-4 acc bg-fondo-xante_cuadros">

                    <h4
                        class="flex items-center justify-between py-4 text-base cursor-pointer lg:text-2xl font-montserrat-regular lg:font-montserrat-bold acc-head">

                        {{ __('landing.faq_q1') }}

                        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 img_voltear" viewBox="0 0 20 20"
                            fill="currentColor">
                            <path fill-rule="evenodd"
                                d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                clip-rule="evenodd" />
                        </svg>

                    </h4>

                    <div class="pl-6 text-sm lg:text-xl font-montserrat-regular acc-content">
                        {!! __('landing.faq_q1_a') !!}
                    </div>

                </div>

                <div class="px-4 acc bg-fondo-xante_cuadros">

                    <h4
                        class="flex items-center justify-between py-4 text-base cursor-pointer lg:text-2xl font-montserrat-regular lg:font-montserrat-bold acc-head">

                        {{ __('landing.faq_q2') }}

                        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 img_voltear" viewBox="0 0 20 20"
                            fill="currentColor">
                            <path fill-rule="evenodd"
                                d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                clip-rule="evenodd" />
                        </svg>

                    </h4>

                    <div class="hidden pl-6 text-sm lg:text-xl font-montserrat-regular acc-content">
                        {!! __('landing.faq_q2_a') !!}
                    </div>

                </div>

                <div class="px-4 acc bg-fondo-xante_cuadros">

                    <h4
                        class="flex items-center justify-between py-4 text-base cursor-pointer lg:text-2xl font-montserrat-regular lg:font-montserrat-bold acc-head">

                        {{ __('landing.faq_q3') }}

                        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 img_voltear " viewBox="0 0 20 20"
                            fill="currentColor">
                            <path fill-rule="evenodd"
                                d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                clip-rule="evenodd" />
                        </svg>

                    </h4>

                    <div class="hidden pl-6 text-sm lg:text-xl font-montserrat-regular acc-content">
                        {!! __('landing.faq_q3_a') !!}
                    </div>

                </div>

            </div>

            <div class="w-full px-4 mx-auto mt-4 leading-2 md:w-4/5 md:mx-auto lg:px-2">

                <span
                    class="block w-full text-justify text-md leading-2 lg:text-md lg:text-justify text-gris-texto-claro font-montserrat-regular">

                    {{ __('landing.time_1_nw') }} <br />

                    {{-- {{ __('landing.bills') }} --}}

                </span>

            </div>

            <div class="w-11/12 py-4 mx-auto text-sm text-right lg:text-xl font-montserrat-bold ">

                <a class="transition duration-200 ease-in text-verde-landing hover:text-morado-xante" href="/faqs"
                    title="Preguntas frecuentes">

                    {{ __('landing.see_all_q') }}

                </a>

            </div>

        </div>

    </div>

    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-227859785-1');
    </script>

    @include('footer')
@endsection

@section('js-usable')

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

    <script type="text/javascript">
        $('.slick_container').slick({
            slidesToShow: 2,
            dots: true,
            arrows: false,
            appendDots: $('.dots_slider'),
            responsive: [{
                breakpoint: 920,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }, ]
        });

        $('.slider_movil').slick({
            slidesToShow: 1,
            arrows: false,
            dots: true,
            appendDots: $('.dots_movil'),

        });

        $('.slider_propiedades').slick({

            slidesToShow: 1,
            arrows: false,
            // prevArrow: '<div class="a-left control-c prev slick-prev"><svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2"><path stroke-linecap="round" stroke-linejoin="round" d="M15 19l-7-7 7-7" /></svg></div>',
            // nextArrow: '<div class="a-right control-c next slick-next"><svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2"><path stroke-linecap="round" stroke-linejoin="round" d="M9 5l7 7-7 7" /> </svg></div>',
            // appendArrows: $('.arrows'),
            centerMode: true,
            centerPadding: '40px',
        });

        $('.slider_propiedades_desk').slick({
            slidesToShow: 3,
            prevArrow: '<div class="cursor-pointer a-left control-c prev slick-prev left-10"><svg xmlns="http://www.w3.org/2000/svg" class="w-16 h-2w-16" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2"><path stroke-linecap="round" stroke-linejoin="round" d="M15 19l-7-7 7-7" /></svg></div>',
            nextArrow: '<div class="cursor-pointer a-right control-c next slick-next"><svg xmlns="http://www.w3.org/2000/svg" class="w-16 h-2w-16" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2"><path stroke-linecap="round" stroke-linejoin="round" d="M9 5l7 7-7 7" /> </svg></div>',
            appendArrows: $('.slider_propiedades_desk_arrows'),
            responsive: [
                {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                }
                },
            ]
        });

        $('.slider_resenias').slick({
            slidesToShow: 1,
            arrows: false,
            dots:true,
            fade: true,
        });

        $(document).ready(function() {
            $('.acc-container .acc:nth-child(1) .acc-head').addClass('active');
            $('.acc-container .acc:nth-child(1) .acc-content').slideDown();
            $('.acc-head').on('click', function() {
                if ($(this).hasClass('active')) {
                    $(this).siblings('.acc-content').slideUp();
                    $(this).removeClass('active');
                } else {
                    $('.acc-content').slideUp();
                    $('.acc-head').removeClass('active');
                    $(this).siblings('.acc-content').slideToggle();
                    $(this).toggleClass('active');
                }
            });

            fetch('/api/desarrollos-all', {
                method: 'GET'
            }).then((res) => {
                return res.json()
            }).then((res) => {

                console.log(res)

                if(res.success) {

                    // let otro = document.getElementById('otro').innerHTML

                    let select = document.getElementById("lista-desarrollos")
                    // let resultado = `
                    // <option  value="" disabled required selected>Selecciona si es Comunidad Vinte u Otro</option>
                    // <option value="999">${otro}</option>
                    // `
                    let resultado = `<option  value="" disabled required selected>Selecciona si es Comunidad Vinte u Otro</option>`

                    res.data.map(function(item){
                        resultado += `<option value="${item.id}"> ${item.nombre} </option>`
                    })

                    select.innerHTML = resultado

                }

            })
            .catch(error => {
                console.error(error)
            })
            .finally(() => {
                console.log('peticion realizada con exito');
            });

        });

        function abrir_menu() {
            $('.menu').toggleClass('opened');
            $('#cont_menu').toggleClass(' menu_visible');
        }

        function enviar(){
            // window.location="/form?option="+document.getElementById('lista-desarrollos').value;

            localStorage.setItem('respuesta1', document.getElementById('lista-desarrollos').value)

            if(localStorage.getItem('respuesta1') != undefined || localStorage.getItem('respuesta1') != null) {
                if(document.getElementById('lista-desarrollos').value != ''){
                    if(document.getElementById('lista-desarrollos').value == '999'){
                        localStorage.setItem('noSlide',1)
                        window.location="/1"
                    }else{
                        localStorage.setItem('noSlide',2)
                        window.location="/2"
                    }
            }

            }
        }

        function vender(){
            document.getElementById("lista-desarrollos").focus();
            // let select = document.getElementById('lista-desarrollos');
            // select.focus();
            // select.classList.add('focus-select');

            // setTimeout(function() {
            //     select.classList.remove('focus-select');
            // }, 100);


            }

            $('#btnVender').click(function(){
            $('#lista-desarrollos').show();
            $('#lista-desarrollos')[0].size=10;/* Choose a size that's perfect */
            });
            // Ocultar select cuando el usuario selecciona una opción
            $('#lista-desarrollos').on("change", function() {
                // $(this).hide();
                this.size = 1;  // Restaurar tamaño
            });

            // Ocultar select si el usuario hace clic fuera del select
            $(document).on("click", function(event) {
                if (!$(event.target).closest("#lista-desarrollos, #btnVender").length) {
                    // $('#lista-desarrollos').hide();
                    $('#lista-desarrollos')[0].size = 1;  // Restaurar tamaño
                }
            });

    </script>
@endsection
