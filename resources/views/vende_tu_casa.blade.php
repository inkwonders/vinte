@extends('layouts.base')

@section('title')
Xante.mx
@endsection

@section('contenido')
    <div class="relative flex flex-col items-center justify-center w-full min-h-screen bg-center bg-no-repeat bg-cover lg:h-screen md:pb-0 index_fondo">
        {{-- <div class="fixed top-0 z-50 flex justify-between w-full h-12 bg-black">
            <div class="fixed top-0 z-50 flex justify-between w-full h-12" style="background-color: #60338a" >
            <div class="flex items-center w-1/2">
                <a title="Home" href="{{ route("index.nuevo") }}">
                    <img src="/assets/images/svg/home_icon.svg" alt="" class="ml-6 md:ml-12 w-7 h-7">
                </a>
            </div>
            <div class="flex items-center justify-end w-1/2 pr-3 lg:pr-7">
                <a href="https://www.vinte.com.mx" target="_blank"><img src="/assets/images/logo_vinte_blanco.png" alt="vinte" class="w-auto h-10 ml-6 md:ml-12 lg:h-9"></a>
                {{-- <a title="Facebook" href="https://www.facebook.com/VinteMx/" target="_blank">
                    <img src="/assets/images/svg/facebook_icon.svg" alt="" class="ml-4 md:ml-8 w-7 h-7">
                </a>
                <a title="Linkedin" href="https://www.instagram.com/vintecomunidad/?hl=es-la" target="_blank">
                    <img src="/assets/images/svg/instagram_icon.png" alt="" class="ml-4 md:ml-8 w-7 h-7">
                </a>
            </div>
        </div> --}}

        @include('header')

        {{-- <div class="relative flex flex-col items-center justify-center w-full h-full py-16 pb-28 lg:py-0 lg:pb-0 lg:flex-row"> --}}
        <div class="relative flex flex-col items-center justify-center w-full h-full pt-10 pb-28 lg:py-0 lg:pb-0 lg:flex-row">
            <div class="relative flex-col items-center justify-start hidden w-2/5 h-full pt-12 lg:flex">
                <img src="/assets/images/svg/vente_tucasa_dos.png" alt="Xante.mx" class="absolute w-36 left-6 top-16 xl:w-40 2xl:w-64">
                <img src="/assets/images/version2/jpg/pareja.png" alt="Xante.mx" class="absolute bottom-0 left-0 w-8/12 2xl:w-10/12">
            </div>
            <div class="relative flex flex-col items-center justify-center w-full h-full 2xl:pb-24 lg:w-3/5">
                <div class="flex flex-col items-center w-full justify-evenly">
                    <img src="/assets/images/svg/vende_casa_icon.png" alt="" class="hidden w-36 xl:w-40 2xl:mt-14 2xl:w-64 lg:flex">
                    <img src="/assets/images/svg/vente_tucasa_dos.png" alt="Xante.mx" class="relative w-1/3 mt-4 md:w-1/5 lg:hidden xl:w-6/12">
                    <img src="/assets/images/svg/vende_casa_icon.png" alt="" class="w-1/4 mt-4 md:w-2/12 lg:hidden">
                </div>
                <h2 class="my-6 text-base text-black lg:my-2 lg:text-xl font-montserrat-bold">{{ __('landing.follow') }}</h2>
                {{-- <div class="grid w-full h-2/5 lg:h-auto md:flex md:flex-col"> --}}
                <div class="grid w-full h-auto lg:h-auto md:flex md:flex-col">
                    <div class="relative flex w-11/12 gap-4 mx-auto shadow-md md:mt-2 md:w-1/2 2xl:w-8/12">
                        <div class="flex flex-col items-center justify-between w-1/2 bg-white border-4 border-black">
                            <div class="relative flex items-center justify-center w-full h-3/4">
                                <img src="/assets/images/version2/svg/casa.svg" alt="Xante.mx" class="w-10/12 p-4 py-2 md:w-1/2 2xl:w-10/12 2xl:p-8">
                            </div>
                            <div class="flex w-full h-12 2xl:h-14">
                                <div class="flex items-center justify-center w-12 h-full 2xl:w-14 bg-azul-footer_index" >
                                    <p class="text-base text-white lg:text-2xl titilliumwebbold">1</p>
                                </div>
                                <div class="flex items-center w-3/4 ml-2 md:ml-4">
                                    <p class="text-sm leading-none text-black 2xl:text-xl font-montserrat-bold line">
                                        {{__('landing.upload')}}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="flex flex-col items-center justify-end w-1/2 h-full bg-white border-4 border-black shadow-md center">
                            <div class="relative flex items-center justify-center w-full h-3/4">
                                <img src="/assets/images/version2/svg/dinero.svg" alt="Xante.mx" class="w-10/12 p-4 py-2 md:w-1/2 2xl:w-10/12 2xl:p-8">
                            </div>
                            <div class="flex w-full h-12 2xl:h-14">
                                <div class="flex items-center justify-center w-12 h-full 2xl:w-14 bg-azul-footer_index" >
                                    <p class="text-base text-white lg:text-2xl titilliumwebbold">2</p>
                                </div>
                                <div class="flex items-center w-3/4 ml-2 md:ml-4">
                                    <p class="text-sm leading-none text-black 2xl:text-xl font-montserrat-bold">
                                        {{__('landing.offer')}}
                                    </p>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="relative flex items-center justify-center w-1/2 bg-white border-4 border-black realtive h-60">
                            <div class="relative flex items-center justify-center w-full h-auto">
                                <img src="/assets/images/version2/svg/casa.png" alt="tu casa" class="relative w-2/3 h-auto">
                            </div>
                            <div></div>
                        </div>
                        <div class="relative flex flex-col items-center justify-between w-1/2 bg-white border-4 border-black realtive h-60">
                            <div class="relative flex items-center justify-center w-full h-auto">
                                <img src="/assets/images/version2/svg/dinero.svg" alt="tu depa" class="relative w-2/3 h-auto">
                            </div>
                            <div class="absolute bottom-0 left-0 flex items-center justify-center">
                                <div class="flex items-center justify-center w-12 h-12 lg:h-full " style="background-color: #60338a">
                                    <p class="text-2xl text-white titilliumwebbold">2</p>
                                </div>
                                <div class="flex items-center w-3/4 ml-2 md:ml-4">
                                    <p class="text-xl leading-none text-black font-montserrat-bold">
                                        Te haremos una oferta
                                    </p>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                    <div class="flex flex-col items-center justify-center my-2 mt-4 text-center md:mt-2 2xl:mt-8 lg:pt-0">
                        <h2 class="text-base leading-4 text-center text-black lg:text-xl md:text-xl titilliumwebsemibold">
                            <b>
                                {!! __('landing.free_fast') !!}
                            </b>
                        </h2>
                        {{-- <a href="/form" class="px-8 py-2 mx-4 mt-6 text-xl text-white border rounded-lg shadow-lg cursor-pointer border-amarillo-fin bg-amarillo-fin hover:bg-white hover:text-amarillo-fin"> --}}
                        <a href="/form" class="inline-flex items-center px-8 py-1 mt-6 text-base tracking-widest text-white transition border-4 border-transparent border-white shadow-lg font-montserrat-bold rounded-2xl 2xl:mt-6 md:mt-1 2xl:py-3 2xl:text-xl bg-verde-landing hover:bg-white hover:border-verde-landing hover:text-verde-landing">
                            {{ __('landing.begin') }}
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <footer class="absolute bottom-0 w-full">
            <div class="flex w-full gap-2 font-montserrat-bold" style="background-color: #60338a">
                <div class="flex flex-col items-center justify-center w-1/3 py-2 text-center text-xxs md:text-xs">
                    <p class="text-center text-black">
                        {{__('landing.help')}}
                    </p>
                    <a href="/faqs" class="text-center text-white">
                        {{__('landing.faq')}}
                    </a>
                </div>
                <div class="flex flex-col items-center justify-center w-1/3 py-2 text-center text-xxs md:text-xs">
                    <p class="text-center text-black">
                        {{__('landing.contact')}}
                    </p>
                    <a href="mailto:contacto@xante.mx" class="text-center text-white">
                        contacto@xante.mx
                    </a>
                </div>
                <div class="flex flex-col items-center justify-center w-1/3 py-2 text-center text-xxs md:text-xs">
                    <p class="text-center text-black">
                        Legal
                    </p>
                    <a class="text-center text-white" href="/assets/pdf/AvisodePrivacidadXante.pdf" target="_blank">
                        {{__('landing.policy')}}
                    </a>
                </div>
            </div>
            <div class="flex justify-center w-full p-2 text-xs text-center font-montserrat-regular md:bg-transparent"  style="background-color: 60338a">
                <span class="text-black md:text-gray-500">
                    <a class="text-black" title="MOMENTUM"> {{__('landing.dev')}}</a>
                </span>
            </div>
        </footer>
    </div>

    <script>

        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-227859785-1');

    </script>
@endsection

@section('js-usable')
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script type="text/javascript">

        function abrir_menu() {
            $('.menu').toggleClass('opened');
            $('#cont_menu').toggleClass(' menu_visible');
        }
    </script>
@endsection

