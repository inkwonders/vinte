@php

// ANTES DE MODIFICAR ESTO FIJATE EN EL ID DE LA IMAGEN O INDEX
// Acomodo de las propiedades segun documento 1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada
//puto el que lo lea ☺

    $casas =

        array(

            // Departamento en Venta
            '1' => array(
                'id_xan'            =>      "XA00018",
                'titulo'            =>      "Real Ibiza Plus",
                'ubicacion'         =>      "Playa del Carmen",
                'tipo_propiedad'    =>      "Dpto",
                'medidas'           =>      "52 m<sup>2</sup>",
                'cuartos'           =>      "2",
                'banios'            =>      "1",
                'precio'            =>      "1,020,000",
                'vendido'           =>      0, //1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada
                'link'              =>      "real-ibiza-playa-del-carmen-18",
                'tipo'              =>      "Departamento en Venta",
                'estacionamiento'   =>      "1",
                'img_fachada'       =>      "casa_4.png"
            ),

            //apartada
            '2' => array(
                'id_xan'            =>      "57",
                'titulo'            =>      "Real Solare",
                'tipo_propiedad'    =>      "Casa",
                'medidas'           =>      "78 m<sup>2</sup>",
                'cuartos'           =>      "2",
                'banios'            =>      "2 <small>1/2</small>",
                'precio'            =>      "1,021,000",
                'vendido'           =>      3, //1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada
                'link'              =>      "real-solare-queretaro-8",
                'ubicacion'         =>      "Querétaro",
                'tipo'              =>      "Casa en Venta",
                'estacionamiento'   =>      "2",
                'img_fachada'       =>      "casa_8.png"
            ),

            //vendida
            '3' => array(
                'id_xan'            =>      "XA00012",
                'titulo'            =>      "Real Navarra",
                'ubicacion'         =>      "Pachuca",
                'tipo_propiedad'    =>      "Casa",
                'medidas'           =>      "134 m<sup>2</sup>",
                'cuartos'           =>      "3",
                'banios'            =>      "2 <small>1/2</small>",
                'precio'            =>      "1,650,000",
                'vendido'           =>      3, //1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada
                'link'              =>      "real-navarra-pachuca-12",
                'tipo'              =>      "Casa en Venta",
                'estacionamiento'   =>      "2",
                'img_fachada'       =>      "casa_1.png"
            ),

            //vendida
            '4' => array(
                'id_xan'            =>      "XA005",
                'titulo'            =>      "Real Toledo",
                'ubicacion'         =>      "Pachuca",
                'tipo_propiedad'    =>      "Casa",
                'medidas'           =>      "69 m<sup>2</sup>",
                'cuartos'           =>      "2",
                'banios'            =>      "1 <small>1/2</small>",
                'precio'            =>      "990,000",
                'vendido'           =>      3, //1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada
                'link'              =>      "real-toledo-pachuca-52",
                'tipo'              =>      "Casa en Venta",
                'estacionamiento'   =>      "1",
                'img_fachada'       =>      "casa_5.png"
            ),


            '5' => array(
                'id_xan'            =>      "inv_vinte",
                'titulo'            =>      "Real del Sol",
                'ubicacion'         =>      "Tecámac",
                'tipo_propiedad'    =>      "Casa",
                'medidas'           =>      "95 m<sup>2</sup>",
                'cuartos'           =>      "3",
                'banios'            =>      "1 <small>1/2</small>",
                'precio'            =>      "1,595,000",
                'vendido'           =>      3, //1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada
                'link'              =>      "real-del-sol-inv",
                'tipo'              =>      "Casa en Venta",
                'estacionamiento'   =>      "2",
                'img_fachada'       =>      "casa_6.png"
            ),


            //vendida
            '6' => array(
                'id_xan'            =>      "XA00013",
                'titulo'            =>      "Real Verona",
                'ubicacion'         =>      "Tecámac",
                'tipo_propiedad'    =>      "Casa",
                'medidas'           =>      "77 m<sup>2</sup>",
                'cuartos'           =>      "3",
                'banios'            =>      "1 <small>1/2</small>",
                'precio'            =>      "1,115,000",
                'vendido'           =>      3, //1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada
                'link'              =>      "real-verona-tecamac-13",
                'tipo'              =>      "Casa en Venta",
                'estacionamiento'   =>      "1",
                'img_fachada'       =>      "casa_9.png"
            ),
             //escriturada
             '7' => array(
                'id_xan'            =>      "XA006",
                'titulo'            =>      "Real del Cid",
                'ubicacion'         =>      "Tecámac",
                'tipo_propiedad'    =>      "Casa",
                'medidas'           =>      "77 m<sup>2</sup>",
                'cuartos'           =>      "3",
                'banios'            =>      "1 <small>1/2</small>",
                'precio'            =>      "1,200,000",
                'vendido'           =>      3, //1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada
                'link'              =>      "real-del-cid-96",
                'tipo'              =>      "Casa en Venta",
                'estacionamiento'   =>      "2",
                'img_fachada'       =>      "casa_7.png"
            ),

    );

@endphp

@extends('layouts.base')

@section('title')
    Xante.mx
@endsection

@section('css')


    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />

@endsection

@section('contenido')

    <div class="relative w-full min-h-screen bg-top bg-no-repeat bg-cover lg:bg-completo bg-fondo-back-movil lg:bg-fondo-back">

        @include('header')

            <div class="flex flex-col pb-20 pt-28">
                <div class="relative z-30 flex items-center justify-center w-full h-8 text-sm text-center text-white">

                <a target="_blank" href="https://wa.link/om0mki" class="fixed w-16 h-16 bottom-2 right-2 ">

                    <img src="/assets/images/png/whats.png" class="absolute h-full">

                </a>

            </div>
            <div class="relative flex flex-col items-center justify-center gap-6 text-center">
                {{--
                <img src="/assets/images/png/logo_blanco.png" class="absolute hidden h-20 left-20 lg:block" alt=""> --}}

                <h2 class="text-4xl text-verde-landing font-montserrat-bold">
                    {{ __('landing.buy_h') }}
                </h2>

                <p class="text-sm text-white  lg:text-xl font-montserrat-regular" style="text-shadow: 2px 0px 5px rgba(150, 150, 150, 0.76);">

                    {!! __('landing.buy_d_h') !!}

                </p>

            </div>

            <div class="mt-16 w-11/12 2xl:w-10/12 mx-auto lg:flex justify-center items-center bg-purpura-xante py-8 hidden ">

                <h1 class="text-white text-4xl 2xl:text-5xl font-montserrat-bold">

                    {{ __('landing.xan_nuestras_prop') }}

                </h1>

            </div>

            <div class="bg-white py-16 w-11/12 2xl:w-10/12 mx-auto shadow-lg hidden lg:block">


            <div class="justify-around hidden w-full 2xl:w-11/12 2xl:gap-16 px-8 gap-10 mx-auto lg:grid lg:grid-cols-3">

                 @foreach ($casas as $casa)

                    <a href="/propiedad/{{ $casa['link'] }}" class="relative text-black bg-white rounded-md w-full 2xl:w-card shadow-lg">

                        <div class="relative w-full h-full bg-white border rounded-md shadow-xl p-2">


                            {{-- {{ $casa['id_xan'] }} --}}
                            {{-- //1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada --}}
                            {{-- 'sell_apar'     =>  'APARTADA',
                            'sell_escr'     =>  'ESCRITURADA',
                            'sell_entr'     =>  'ENTREGADO',
                            'sell_disp'     =>  'DISPONIBLE', --}}
                            @if ($casa['vendido'] == 3)

                                <div style="background-image: url(/assets/images/png/vendida_icon.png)" class="absolute left-0 z-40 flex items-center justify-center w-full py-4 text-2xl text-center text-white bg-center bg-no-repeat bg-contain -top-4 font-montserrat-bold">

                                    {!! __('landing.sell_c') !!}

                                </div>

                            @endif
                            @if ($casa['vendido'] == 2)

                                <div style="background-image: url(/assets/images/png/vendida_icon.png)" class="absolute left-0 z-40 flex items-center justify-center w-full py-4 text-2xl text-center text-white bg-center bg-no-repeat bg-contain -top-4 font-montserrat-bold">

                                    {!! __('landing.sell_apar') !!}

                                </div>

                            @endif
                            @if ($casa['vendido'] == 4)

                                <div style="background-image: url(/assets/images/png/vendida_icon.png)" class="absolute left-0 z-40 flex items-center justify-center w-full py-4 text-2xl text-center text-white bg-center bg-no-repeat bg-contain -top-4 font-montserrat-bold">

                                    {!! __('landing.sell_escr') !!}

                                </div>

                            @endif
                            @if ($casa['vendido'] == 5)

                                <div style="background-image: url(/assets/images/png/vendida_icon.png)" class="absolute left-0 z-40 flex items-center justify-center w-full py-4 text-2xl text-center text-white bg-center bg-no-repeat bg-contain -top-4 font-montserrat-bold">

                                    {!! __('landing.sell_entr') !!}

                                </div>

                            @endif


                            <div class="relative w-full">

                                {{--
                                <span class="absolute w-1/2 text-2xl text-white font-montserrat-semibold top-16 left-4">

                                    {{ $casa['titulo'] }}

                                </span>
                                <div class="absolute flex justify-around w-3/4 gap-2 p-2 text-center text-white bottom-4 bg-verde-boton opacity-90">

                                    <span>

                                        <img src="/assets/images/png/icon_superficie.png" class="relative w-8 h-8 py-1" alt="">

                                        {{ $casa['medidas'] }}

                                    </span>

                                    <span>

                                        <img src="/assets/images/png/icon_cuartos.png" class="relative w-8 h-8 py-1" alt="">

                                        {{ $casa['cuartos'] }}

                                    </span>

                                    <span>

                                        <img src="/assets/images/png/icon_banos.png" class="relative w-8 h-8 py-1" alt="">

                                        {{ $casa['banios'] }}

                                    </span>

                                </div> --}}

                                <div class="w-full h-72 bg-center bg-no-repeat bg-cover" style="background-image: url(/assets/images/png/{{$casa['img_fachada']}})">
                                </div>

                            </div>

                            <div class="relative flex items-center justify-between px-4 pt-2 font-montserrat-bold">

                                <div class="flex justify-between w-full text-sm items-center border-b-4 pb-2 border-black">

                                    <p class="font-montserrat-regular">

                                        {{-- Casa en Venta --}}

                                        {{ $casa['tipo'] }}

                                        <br>


                                        {{ $casa['titulo'] }}

                                    </p>


                                    <p class="text-base font-montserrat-bold">${{ $casa['precio'] }}*</p>

                                </div>

                                {{--  <div class="flex justify-end text-xs ">

                                    <span class="font-bold text-right uppercase text-morado-xante">

                                        {!! $casa['disponibilidad'] !!}

                                    </span>

                                </div> --}}

                            </div>



                            <div class="relative justify-between w-full px-2 gap-4 py-4 flex font-montserrat-bold">

                                {{-- <span> {!! $casa['descripcion'] !!} </span> --}}

                                <div class="flex gap-4 text-sm w-8/12 font-montserrat-regular">

                                    <div class="flex justify-center items-center flex-col gap-1">

                                        <div class="w-11 h-11 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                            <img src="/assets/images/png/iconos/metros_icon.png" alt="" class="h-5 w-5 mt-2">

                                        </div>

                                        <p class="text-xs">

                                            @php
                                                echo $casa['medidas'];
                                            @endphp
                                        </p>

                                    </div>

                                    <div class="flex justify-center items-center flex-col gap-1">

                                        <div class="w-11 h-11 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                            <img src="/assets/images/png/iconos/habitaciones_icon.png" alt="" class="h-5 w-5 mt-2">

                                        </div>

                                        <p class="text-xs">

                                            {{ $casa['cuartos'] }}

                                        </p>

                                    </div>

                                    <div class="flex justify-center items-center flex-col gap-1">

                                        <div class="w-11 h-11 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                            <img src="/assets/images/png/iconos/banios_icon.png" alt="" class="h-5 w-5 mt-2">

                                        </div>

                                        <p class="text-xs">

                                            @php

                                                echo $casa['banios'];

                                            @endphp


                                        </p>

                                    </div>

                                    <div class="flex justify-center items-center flex-col gap-1">

                                        <div class="w-11 h-11 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                            <img src="/assets/images/png/iconos/estacionamiento_icon.png" alt="" class="h-5 w-5 mt-2">

                                        </div>

                                        <p class="text-xs">

                                            {{--  --}}

                                            {{ $casa['estacionamiento'] }}

                                        </p>

                                    </div>

                                </div>

                                <div class="flex justify-center items-center font-montserrat-regular gap-2 w-4/12 text-sm 2xl:text-base">

                                    <img src="/assets/images/png/iconos/mapa_icon.png" alt="" class="h-6">

                                    {{ $casa['ubicacion'] }}

                                </div>


                            </div>

                        </div>

                    </a>

                 @endforeach

                <a class="relative text-black bg-white rounded-md shadow-lg col-span-2">

                    <div class="p-2 text-black bg-white rounded-md">

                        <div class="relative w-full h-full bg-white ">

                            <div class="relative w-full">

                                <div class="w-full h-96 bg-top bg-no-repeat bg-cover" style="background-image: url(/assets/images/png/prox.png)">
                                </div>

                            </div>

                            <div class="relative flex items-center pt-4 justify-center  text-2xl text-center font-montserrat-bold text-naranja-landing">

                                <h3>

                                    {!! __('landing.xan_t_prox') !!}

                                </h3>

                            </div>

                        </div>

                    </div>

                </a>

            </div>

            </div>

            <div class="justify-around w-11/12 mx-auto mt-16 lg:hidden ">

                {{-- slider_propiedades --}}

                <div class="w-full flex flex-col gap-4">

                    @foreach ($casas as $casa)

                        <a href="/propiedad/{{ $casa['link'] }}" class="relative text-black bg-white rounded-md w-full mb-4">

                            <div class="relative w-full h-full bg-white border rounded-md shadow-xl p-2">

                                @if ($casa['vendido'] == 3)

                                <div style="background-image: url(/assets/images/png/vendida_icon.png)" class="absolute left-0 z-40 flex items-center justify-center w-full py-4 text-2xl text-center text-white bg-center bg-no-repeat bg-contain -top-4 font-montserrat-bold">

                                    {!! __('landing.sell_c') !!}

                                </div>

                            @endif
                            @if ($casa['vendido'] == 2)

                                <div style="background-image: url(/assets/images/png/vendida_icon.png)" class="absolute left-0 z-40 flex items-center justify-center w-full py-4 text-2xl text-center text-white bg-center bg-no-repeat bg-contain -top-4 font-montserrat-bold">

                                    {!! __('landing.sell_apar') !!}

                                </div>

                            @endif
                            @if ($casa['vendido'] == 4)

                                <div style="background-image: url(/assets/images/png/vendida_icon.png)" class="absolute left-0 z-40 flex items-center justify-center w-full py-4 text-2xl text-center text-white bg-center bg-no-repeat bg-contain -top-4 font-montserrat-bold">

                                    {!! __('landing.sell_escr') !!}

                                </div>

                            @endif
                            @if ($casa['vendido'] == 5)

                                <div style="background-image: url(/assets/images/png/vendida_icon.png)" class="absolute left-0 z-40 flex items-center justify-center w-full py-4 text-2xl text-center text-white bg-center bg-no-repeat bg-contain -top-4 font-montserrat-bold">

                                    {!! __('landing.sell_entr') !!}

                                </div>

                            @endif

                                <div class="relative w-full">

                                    <div class="w-full h-72 bg-center bg-no-repeat bg-cover" style="background-image: url(/assets/images/png/{{$casa['img_fachada']}})">
                                    </div>

                                </div>

                                <div class="relative flex items-center justify-between px-4 pt-2 font-montserrat-bold">

                                    <div class="flex justify-between w-full text-sm items-center border-b-4 pb-2 border-black">

                                        <p class="font-roboto-normal">

                                            {{ $casa['tipo'] }}


                                            <br>


                                            {{ $casa['titulo'] }}


                                        </p>


                                        <p class="text-base font-roboto-black">${{ $casa['precio'] }}*</p>

                                    </div>

                                </div>

                                <div class="relative justify-between w-full px-2 gap-4 py-4 flex font-montserrat-bold">

                                    <div class="flex gap-2 text-sm w-8/12 font-roboto-normal">

                                        <div class="flex justify-center items-center flex-col gap-1">

                                            <div class="w-11 h-11 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                                <img src="/assets/images/png/iconos/metros_icon.png" alt="" class="h-5 w-5 mt-2">

                                            </div>

                                            <p class="text-xs">

                                                {{-- {{ $casa['medidas'] }} --}}
                                                @php
                                                    echo $casa['medidas'];
                                                @endphp

                                            </p>

                                        </div>

                                        <div class="flex justify-center items-center flex-col gap-1">

                                            <div class="w-11 h-11 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                                <img src="/assets/images/png/iconos/habitaciones_icon.png" alt="" class="h-5 w-5 mt-2">

                                            </div>

                                            <p class="text-xs">

                                                {{ $casa['cuartos'] }}

                                            </p>

                                        </div>

                                        <div class="flex justify-center items-center flex-col gap-1">

                                            <div class="w-11 h-11 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                                <img src="/assets/images/png/iconos/banios_icon.png" alt="" class="h-5 w-5 mt-2">

                                            </div>

                                            <p class="text-xs">

                                                {{-- {{ $casa['banios'] }} --}}
                                                @php
                                                    echo $casa['banios'];
                                                @endphp

                                            </p>

                                        </div>

                                        <div class="flex justify-center items-center flex-col gap-1">

                                            <div class="w-11 h-11 flex justify-center items-center bg-cover bg-center" style="background-image: url('/assets/images/png/iconos/casa_icon.png')">

                                                <img src="/assets/images/png/iconos/estacionamiento_icon.png" alt="" class="h-5 w-5 mt-2">

                                            </div>

                                            <p class="text-xs">

                                                {{ $casa['estacionamiento'] }}

                                            </p>

                                        </div>

                                    </div>

                                    <div class="flex justify-center items-center font-roboto-normal gap-2 w-4/12 text-sm 2xl:text-base">

                                        <img src="/assets/images/png/iconos/mapa_icon.png" alt="" class="h-6">

                                        {{ $casa['ubicacion'] }}

                                    </div>


                                </div>

                            </div>

                        </a>

                    @endforeach


                    <a class="relative text-black bg-white rounded-md shadow-lg col-span-2">

                        <div class="p-2 text-black bg-white rounded-md">

                            <div class="relative w-full h-full bg-white ">

                                <div class="relative w-full">

                                    <div class="w-full h-96 bg-top bg-no-repeat bg-cover" style="background-image: url(/assets/images/png/prox.png)">
                                    </div>

                                </div>

                                <div class="relative flex items-center pt-4 justify-center  text-xl text-center font-montserrat-bold text-naranja-landing">

                                    <h3>

                                        {!! __('landing.xan_t_prox') !!}

                                    </h3>

                                </div>

                            </div>

                        </div>

                    </a>

                </div>

            </div>
              <div class="w-full flex justify-center items-center mt-20">
                    <div class="flex justify-center items-center bg-white rounded-md">
                        <a href="/comprar/1">
                            <div class="flex bg-white justify-center items-center py-2">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-4">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 19.5L8.25 12l7.5-7.5" />
                                </svg>
                            </div>
                        </a>
                        <a href="/comprar/1">
                            <div class="flex bg-white justify-center items-center w-14  py-2">
                                <div class="flex bg-white justify-center items-center w-8  py-2">
                                    1
                                </div>
                            </div>
                        </a>
                        <a href="/comprar/2">
                            <div class="rounded-full bg-black text-white w-10 flex justify-center items-center">
                                2
                            </div>
                        </a>
                        <a href="/comprar/2">
                            <div class="flex bg-white justify-center items-center py-2">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-4">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                                </svg>
                            </div>
                        </a>
                    </div>

                </div>
                <br> <br>
        </div>

        <div class="flex flex-col items-center justify-center text-center pb-14">

            <span class="px-4 text-xs text-black opacity-70 lg:px-0 lg:text-sm font-montserrat-regular ">

                {!! __('landing.xan_discraimer') !!}

            </span>

            <a ta                                          rget="_blank" href="https://wa.me/+525570464021" title="Contáctanos" class="items-center px-24 py-2 mt-8 text-xl font-bold tracking-widest text-white transition duration-700 ease-in-out border-4 border-transparent border-white rounded-md shadow-lg cursor-pointer 2xl:mt-6 md:px-32 2xl:py-3 md:text-xl 2xl:text-base bg-verde-landing titilliumwebbold hover:bg-white hover:border-verde-landing hover:text-verde-landing">

                {!! __('landing.contact') !!}

            </a>

        </div>

    </div>

    @include('footer')

@endsection

@section('js-usable')

    <script>

        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-227859785-1');

    </script>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

    <script type="text/javascript">

        $('.slider_propiedades').slick({

            slidesToShow: 1,
            arrows:false,
            dots:false,
            centerMode: true,
            centerPadding: '10px',

        });

        $(document).ready(function() {
            $('.acc-container .acc:nth-child(1) .acc-head').addClass('active');
            $('.acc-container .acc:nth-child(1) .acc-content').slideDown();
            $('.acc-head').on('click', function() {
                if ($(this).hasClass('active')) {
                    $(this).siblings('.acc-content').slideUp();
                    $(this).removeClass('active');
                } else {
                    $('.acc-content').slideUp();
                    $('.acc-head').removeClass('active');
                    $(this).siblings('.acc-content').slideToggle();
                    $(this).toggleClass('active');
                }
            });
        });

        function abrir_menu() {
            $('.menu').toggleClass('opened');
            $('#cont_menu').toggleClass(' menu_visible');
        }

    </script>
@endsection
