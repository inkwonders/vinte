

<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Xante.mx</title>

    <style>

        *{

            margin      :   0;

            padding     :   0;

        }

        #Correo{

            width                   :   750px;

            background-color        : #bbb;

            padding                 :   50px 0;

        }

        .contenedor{

            width               :    600px;

            background-color    :    #fff;

            padding             :    15px;

            margin: auto;

        }

        .contendor_logo{

            margin                  :    15px auto;

            width                   :    40%;

            padding                 :    25px 50px;

            background-color        :    #fff;

            text-align              :    center;

            display                 :    block;

        }

        .contendor_logo img{

            width       :   90%;

        }

        .contenedor_titulo{

            display                 :   block;

            width                   :   100%;

            text-align              :   center;

            margin                  :   40px 0;

        }

        .contenedor_titulo h1{

            color: #60338a;

            font-family: Arial, Helvetica, sans-serif;

            font-size: 1.4rem;

        }

        .contenedor_mensaje{

            width               :   90%;

            text-align          :   center;

            font-size           :   1rem;

            font-family         :   Verdana, Geneva, Tahoma, sans-serif;

            color               :    #6d6d6d;

            margin              :   40px auto;

        }

        .texto_azul{

            color               :    #60338a;

            margin              :     10px 0;

        }

        .texto_gris{

            color               :   #6d6d6d!important;

            text-decoration     :   none

        }

        .contenedor_btn{

            width               :   90%;

            text-align          :   center;

            font-family         :   Verdana, Geneva, Tahoma, sans-serif;

            margin              :   40px auto;

        }

        .contenedor_btn a{

            background-color: #006cbc;

            color: white;

            font: 1em ;

            padding: 15px 25px;

        }

        a{
            text-decoration: none
        }

        .discraimer{
            padding-top:16px;
            font-size: 12px
        }

    </style>

</head>

<body>

    <div id="Correo">

        <div class="contenedor">

            <div class="contendor_logo">

                <img src="https://xante.mx/assets/images/svg/vente_tucasa_dos.png" alt="">

            </div>

            <div class="contenedor_titulo">

                <h1>

                    {{ $accion['titulo'] }}

                </h1>

            </div>

            <div class="contenedor_mensaje">


                    <p class="texto_azul">

                        ID

                    </p>

                    <p class="texto_gris">

                        {{ $accion['propiedades']['id'] }}

                    </p>


                    <p class="texto_azul">

                        XanteID

                    </p>

                    <a class="texto_gris" >

                        {{ $accion['propiedades']['xanteid'] }}

                    </a>
                    {{-- <p class="texto_azul">

                        Título

                    </p>

                    <a class="texto_gris" >

                        {{ $accion['propiedades']['title'] }}

                    </a> --}}





                <p class="discraimer">

Muchas gracias

                </p>
            </div>

        </div>

    </div>

</body>

</html>
