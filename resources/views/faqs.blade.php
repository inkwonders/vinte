@extends('layouts.base')

@section('title')

    Faqs

@endsection


@php

$preguntas = [
    [

        'pregunta' => __('landing.faq_q1'),
        'respuesta' => __('landing.faq_q1_a'),

    ],
    [

        'pregunta' => __('landing.faq_q2'),
        'respuesta' => __('landing.faq_q2_a'),

    ],
    [

        'pregunta' => __('landing.faq_q3'),
        'respuesta' => __('landing.faq_q3_a'),

    ],
    [

        'pregunta' => __('landing.faq_q4'),
        'respuesta' => __('landing.faq_q4_a'),

    ],
    [

        'pregunta' => __('landing.faq_q5'),
        'respuesta' => __('landing.faq_q5_a'),

    ],
    [

        'pregunta' => __('landing.faq_q6'),
        'respuesta' => __('landing.faq_q6_a'),

    ],
    [

        'pregunta' => __('landing.faq_q7'),
        'respuesta' => __('landing.faq_q7_a'),

    ],
    [

        'pregunta' => __('landing.faq_q8'),
        'respuesta' => __('landing.faq_q8_a'),

    ]
];

@endphp




@section('contenido')

    <div class="relative w-full min-h-screen pt-12 fondoback">

        @include('header')

        <div class="relative pb-8 lg:pt-8 lg:pb-6 bg-fondo-xante_palabras">

            <div class="vector_7"></div>

            <div class="w-full py-10 lg:py-8 lg:pl-32">

                <h2 class="text-base text-center md:text-2xl lg:text-left lg:text-4xl font-montserrat-bold text-morado-xante">{!! __('landing.faq_p') !!}</h2>

            </div>

            <div class="relative z-10 flex flex-col w-10/12 gap-4 px-4 py-6 mx-auto mt-5 bg-white lg:mt-0 lg:px-16 acc-container">

                @foreach ($preguntas as $pregunta)

                <div class="px-4 acc bg-fondo-xante_cuadros">

                    <h4 class="flex items-center justify-between py-4 text-base cursor-pointer lg:text-2xl font-montserrat-bold acc-head">

                        {{ $pregunta['pregunta'] }}

                        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 img_voltear" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                        </svg>

                    </h4>

                    <div class="hidden pl-6 text-sm lg:text-xl font-montserrat-regular acc-content">

                        {!! $pregunta['respuesta'] !!}

                    </div>

                </div>

                @endforeach


            </div>

            <div class="w-full px-4 mx-auto mt-4 leading-3 md:w-4/5 md:mx-auto lg:px-2">

                <span
                    class="block w-full text-md leading-3 text-center lg:text-md lg:text-left text-gris-texto-claro font-montserrat-regular">

                    {{ __('landing.time_') }} <br />

                    {{-- {{ __('landing.bills') }} --}}

                </span>

            </div>

        </div>

        @include('footer')

    </div>


@endsection

    @section('js-usable')

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function() {
            $('.acc-container .acc:nth-child(1) .acc-head').addClass('active');
            $('.acc-container .acc:nth-child(1) .acc-content').slideDown();
            $('.acc-head').on('click', function() {
                if($(this).hasClass('active')) {
                $(this).siblings('.acc-content').slideUp();
                $(this).removeClass('active');
                }
                else {
                $('.acc-content').slideUp();
                $('.acc-head').removeClass('active');
                $(this).siblings('.acc-content').slideToggle();
                $(this).toggleClass('active');
                }
            });
        });

        function abrir_menu(){
            $('.menu').toggleClass('opened');
            $('#cont_menu').toggleClass(' menu_visible');
        }

    </script>

    @endsection
