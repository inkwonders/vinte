//require('lang.js');
import Alpine from 'alpinejs'; /** se importa alpine  */
import Vue from 'vue';
// import VueLang from '@eli5/vue-lang-js'
// import translations from './vue-translations.js';

window.Alpine = Alpine;
Alpine.start();

window.Vue = Vue;

// Vue.component('padre', require('./components/padre.vue').default);
// Vue.component('cabecera', require('./components/cabecera.vue').default);
// Vue.component('nota', require('./components/nota.vue').default);

Vue.component('inicio', require('./components/inicio.vue').default);
Vue.component('end', require('./components/end.vue').default);

// Vue.use(VueLang, {
//     messages: translations, // Provide locale file
//     locale: 'en', // Set locale
//     fallback: 'en' // Set fallback lacale
// });

const app = new Vue({
}).$mount('#app');
