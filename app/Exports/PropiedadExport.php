<?php

namespace App\Exports;

use App\Models\Propiedad;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class PropiedadExport implements FromCollection, WithHeadings, ShouldAutoSize,  WithColumnFormatting
{
    use Exportable;
    public $fecha_ini;
    public $fecha_fin;

    function __construct($fecha_ini, $fecha_fin)
    {
        $this->fecha_ini = $fecha_ini;
        $this->fecha_fin = $fecha_fin;
    }

    public function headings(): array
    {
        return [
            'ID',
            'Fecha de registro',
            'Propietario',
            'Telefono',
            'Correo',
            'Nombre del desarrollo',
            'Casa o Departamento',
            'Calle / Privada',
            'Numero Exterior',
            'Estado',
            'Colonia',
            'Código Postal',
            'Superficie Habitable',
            'Piso Departamento',
            'Niveles Casa',
            'Terraza y/o Jardín',
            'Recamaras',
            'Baños',
            'Medios Baños',
            'Estacionamientos',
            'Cuenta con equipo',
            'Equipo',
            'Antigüedad',
            'Hipoteca',
            'Tipo de hipoteca',
            'Precio',
            'Motivo Venta',
            'Comentario',
            'Estatus'
        ];
    }

    public function columnFormats(): array
    {

        return [
            'Y' =>   NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
        ];
    }


    public function collection()
    {

        // return Propiedad::where('id')
        return (Propiedad::whereDate('created_at', '>=', $this->fecha_ini)->whereDate('created_at', '<=', $this->fecha_fin)->get()->map(function ($propiedad) {

            if (!is_null($propiedad->desarrollo)) {
                $name = $propiedad->desarrollo->nombre;
            } else {
                $name = $propiedad->otro_desarrollo;
            }

            if ($propiedad->propiedad_tipo->nombre == 'Casa') {
                $piso_dep = 'N/A';
                $terraza = $propiedad->terraza_jardin;
                $niveles = $propiedad->num_niveles_casa;
            } else {
                $piso_dep = $propiedad->piso_departemento;
                $terraza = 'N/A';
                $niveles = 'N/A';
            }

            if ($propiedad->hipoteca == 'NO') {
                $hipoteca_tipo = 'N/A';
            } else {
                $hipoteca_tipo = $propiedad->hipoteca_tipo;
            }

            if ($propiedad->equipado == 'SI') {
                // dd($propiedad->equipos);
                if(empty($propiedad->equipos->nombre)){
                    $equipo_lista = "N/A";
                }

                foreach ($propiedad->equipos as $equipo) {
                    $equipo_lista = $equipo->nombre . ', ';
                }

            } else {
                $equipo_lista = 'N/A';
            }

            if($propiedad->num_med_bath == 0){
                $med_bath = '0';
            }else{
                $med_bath = $propiedad->num_med_bath;
            }

            if($propiedad->num_estacionamientos == 0){
                $park = '0';
            }else{
                $park = $propiedad->num_med_bath;
            }

            return [
                $propiedad->id,
                date_format($propiedad->created_at, "d / m / Y"),
                $propiedad->nombre_propietario,
                $propiedad->telefono,
                $propiedad->correo,
                $name,
                $propiedad->propiedad_tipo->nombre,
                $propiedad->direccion,
                $propiedad->num_exterior,
                $propiedad->estado->descripcion,
                $propiedad->colonia->nombre,
                $propiedad->colonia->codigo_postal,
                $propiedad->superficie_habitable,
                $piso_dep,
                $niveles,
                $terraza,
                $propiedad->num_recamaras,
                $propiedad->num_bath,
                $med_bath,
                $park,
                $propiedad->equipado,
                $equipo_lista,
                $propiedad->antiguedad.' años',
                $propiedad->hipoteca,
                $hipoteca_tipo,
                $propiedad->propiedad_precio,
                $propiedad->motivo,
                $propiedad->motivo_comp,
                $propiedad->estatus
            ];
        }));
    }
}
