<?php

namespace App\Exports;

use App\Models\Propiedad;
use App\Models\Questionnaire;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class PropiedadEspecificaExport implements FromCollection, WithHeadings, ShouldAutoSize,  WithColumnFormatting
{

    use Exportable;

    public $id = null;
    public $headers = null;



    function __construct($id)
    {
        $this->id = $id;
    }

    public function columnFormats(): array
    {

        return [
            'P' =>   NumberFormat::FORMAT_CURRENCY_USD_SIMPLE,
        ];
    }

    public function collection()
    {
        return Questionnaire::where('id', $this->id)->get()->map(function ($propiedad) {
            if (!is_null($propiedad->desarrollo)) {
                $name = $propiedad->desarrollo->nombre;
            } else {
                $name = $propiedad->google_direction;
            }

            // if ($propiedad->propiedad_tipo->nombre == 'Casa') {
            //     $piso_dep = 'N/A';

            // } else {
            //     $piso_dep = $propiedad->piso_departemento;
            //     $terraza = 'N/A';
            //     $niveles = 'N/A';
            // }

            if ($propiedad->mortgage == 'NO') {
                $hipoteca_tipo = 'N/A';
            } else {
                $hipoteca_tipo = $propiedad->mortgage;
            }



            if($propiedad->half_bathrooms == 0){
                $med_bath = '0';
            }else{
                $med_bath = $propiedad->half_bathrooms;
            }

            if($propiedad->parking_slots == 0){
                $park = '0';
            }else{
                $park = $propiedad->parking_slots;
            }

            if($propiedad->status == 1){
                $status = 'ACTIVO';
            }else{
                $status = 'INACTIVO';
            }

            return [
                $propiedad->id,
                date_format($propiedad->created_at, "d / m / Y"),

                $propiedad->propiedad_tipo->nombre,
                $propiedad->desarrollo->nombre,
                $propiedad->street,
                $propiedad->num_ext,
                $propiedad->num_int,
                $propiedad->google_direction,
                $propiedad->property_type,
                $propiedad->house_levels,
                $propiedad->level_department,
                $propiedad->bedroom,
                $propiedad->bathroom,
                $med_bath,
                $park,
                $propiedad->years.' años',
                $propiedad->m2,
                $propiedad->offer_money,
                $propiedad->mortgage,
                $propiedad->sale_reasons,
                $propiedad->name,
                $propiedad->whatsapp,
                $propiedad->email,
                $propiedad->comments,
                $status,
            ];
        });
    }
    public function headings(): array
    {
        $this->headers = [ 'Id',
        'Fecha de Creación',
        "Propiedad tipo",
        "Desarrollo",
        'Calle',
        'Num Ext',
        'Num Int',
        'Dirección google',
        'Tipo de propiedad',
        'Niveles de casa',
        'Nivel de depto',
        'Habitaciones',
        'Baño',
        'Medio baño',
        'Estacionamientos',
        'Años',
        'm2',
        'Oferta',
        'Hipoteca',
        'Razón de venta',
        'Nombre',
        'Whatsapp',
        'Email',
        'Comments',
        'Estatus',];

        return $this->headers;
    }
}
