<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Desarrollo;
use App\Models\Equipo;
use App\Models\Propiedad;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use App\Transformers\Serializer;
use App\Transformers\DesarrolloTransformer;
use App\Transformers\EquipoTransformer;
use Exception;
use Illuminate\Http\Request;

/**
 * @group Utilidades
 *
 * Listas de utilidades para el registro de las propiedades
 */
class DesarrolloAPIController extends Controller
{
    /**
     * Desarrollos
     *
     * Genera una lista de los desarrollos de Vinte para la venta de propiedades
     */
    public function getDesarrollos($estado_id){

        $fractal = new Manager();
        $listaDesarrollos = $fractal->setSerializer(new Serializer())
            ->createData(new Collection(Desarrollo::where('estado_id', $estado_id)->get(), new DesarrolloTransformer))->toArray();

        return response()->json(['success' => true, 'msg' => 'Colección: Desarrollos generada con exito', 'data' => $listaDesarrollos], 200);
    }

    public function getAllDesarrollos(){

        $fractal = new Manager();
        $listaDesarrollos = $fractal->setSerializer(new Serializer())
            ->createData(new Collection(Desarrollo::get()->sortBy('order'), new DesarrolloTransformer))->toArray();


        return response()->json(['success' => true, 'msg' => 'Colección: Desarrollos generada con exito', 'data' => $listaDesarrollos], 200);
    }

    /**
     * Equipos
     *
     * Genera una lista de equipo que puede tener la propiedad, para checkboxes
     */
    public function getEquipos(){
        $fractal = new Manager();
        $listaEquipos = $fractal->setSerializer(new Serializer())
            ->createData(new Collection(Equipo::all(), new EquipoTransformer))->toArray();

        return response()->json(['success' => true, 'msg' => 'Colección: Equipos generada con exito', 'data' => $listaEquipos], 200);
    }

    // api de google places
    public function getGooglePlaces(Request $request){

            $text = str_replace(' ','',$request->text);

            $key = 'AIzaSyBzQ3o9UXBuGJQdfv4wrg4oYfKzZO4q4ZE'; //zulumexico
            $ruta = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input='.$text.'&key='.$key;

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $ruta, // ruta
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_TIMEOUT => 100,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",

            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                return response()
                    ->json([
                        'success' => false,
                        'error' => $err,
                        'url' => $ruta
                    ], 400);
            } else {

                return response()
                    ->json([
                        'success' => true,
                        'data' => json_decode($response),
                        'url' => $ruta
                    ], 200);
            }


    }
}
