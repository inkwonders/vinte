<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Models\Property;
use App\Models\State;
use App\Models\Neighborhood;
use App\Models\Municipality;

class ApiController extends Controller
{


            public function getNeighborhood(Request $request){

                $request->validate([
                    'municipality_id'      => 'required|exists:municipalities,id',
                ]);

                try {

                    Log::debug('APIController@getNeighborhood', ['Obtenemos las colonias del cp' ,$request->all()]);

                    $active=0;
                    $municipality_id=$request->municipality_id;
                    $colonias=Neighborhood::where('municipality_id',$municipality_id)
                                           ->where('active',$active)
                                           ->orderBy('neighborhood','ASC')
                                           ->get()
                                           ->map(function($resultado) {
                                            return [
                                                'id'                  => $resultado->id,
                                                'municipality_id'     => $resultado->municipality_id,
                                                'neighborhood'        => $resultado->neighborhood,
                                                'zip_code'            => $resultado->zip_code,
                                                'type'                => $resultado->type,
                                                'slug'                => $resultado->slug,
                                                'active'              =>$resultado->active

                                            ];
                                        });


                        return response()
                        ->json([
                            'success' => true,
                            'msg' => "Se encontraron {$colonias->count()} colonias",
                            'data' => $colonias
                        ], 200);



                } catch (\Exception $e) {

                    Log::debug('APIController@getNeighborhood', ['Error al obtener colonias']);

                    $array = [
                        'success' => false,
                        'ERROR on the transaction Exception: ' => $e->getMessage(),
                    ];

                    Log::error('APIController@getNeighborhood ERROR on the transaction Exception: ', $array);

                     return response()->json($array);

                }


            }


            public function getStates(){

                try {

                    Log::debug('APIController@getStates', ['Obtenemos los estados']);

                    $states=State::all()
                                  ->orderBy('description','ASC')
                                  ->map(function($resultado) {
                                   return [
                                       'id'                  => $resultado->id,
                                       'description'         => $resultado->description,
                                       'abbreviation'        => $resultado->abbreviation,
                                   ];
                               });


                        return response()
                        ->json([
                            'success' => true,
                            'msg' => "Se encontraron {$states->count()} estados",
                            'data' => $states
                        ], 200);



                } catch (\Exception $e) {

                    Log::debug('APIController@getStates', ['Error al obtener estados']);

                    $array = [
                        'success' => false,
                        'ERROR on the transaction Exception: ' => $e->getMessage(),
                    ];

                    Log::error('APIController@getStates ERROR on the transaction Exception: ', $array);

                     return response()->json($array);

                }


            }

}
