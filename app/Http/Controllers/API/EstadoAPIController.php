<?php

namespace App\Http\Controllers\API;

use App\Models\Estado;
use App\Models\Municipio;
use League\Fractal\Manager;
use Illuminate\Http\Request;
use App\Transformers\Serializer;
use App\Http\Controllers\Controller;
use App\Models\Colonia;
use App\Transformers\EstadoTransformer;
use League\Fractal\Resource\Collection;
use App\Transformers\ColoniaTransformer;
use App\Transformers\MunicipioTransformer;

/**
 * @group Ubicaciones
 *
 * Obtiene listas de ubicaciones sea Estado, Municipio o Colonia
 *
 */
class EstadoAPIController extends Controller
{
    /**
     * Estados
     *
     * Genera la lista de estados de la republica mexicana, con formato
     */
    public function getEstados()
    {

        $fractal = new Manager();
        $estadoInformacion = $fractal->setSerializer(new Serializer())
            ->createData(new Collection(Estado::all()->where('habilitado','=',true), new EstadoTransformer))->toArray();

        return response()->json(['success' => true, 'msg' => 'Colección: Estados generada con exito', 'data' => $estadoInformacion], 200);

    }

    // listado de colonias
    public function getColoniasEstado(Request $request){
        $colonias = Colonia::where('codigo_postal',$request->cp)->get()->toArray();
        return response()->json(['success' => true, 'msg' => 'El listado de colonias del codigo postal '.$request->cp, 'data' => $colonias], 200);
    }

    /**
     * Municipios por Estado
     *
     * Genera una lista de municipios por estado
     *
     * @urlParam estado integer required ID del Estado a buscar Example: 22
     */
    public function getMunicipios(Estado $estado)
    {
        $fractal = new Manager();

        $municipiosEstado = $fractal->setSerializer(new Serializer())
            ->createData(new Collection($estado->municipios, new MunicipioTransformer))->toArray();

        return response()->json(['success' => true, 'msg' => 'Colección: Municipios generada con exito', 'data' => $municipiosEstado], 200);

    }

      /**
     * Colonias por Municipio
     *
     * Genera una lista de colonias por municipio
     *
     * @urlParam municipio integer required ID del Municipio a buscar Example: 1831
     */
    public function getColonias(Municipio $municipio)
    {
        $fractal = new Manager();

        $coloniasMunicipio = $fractal->setSerializer(new Serializer())
            ->createData(new Collection($municipio->colonias, new ColoniaTransformer))->toArray();

        return response()->json(['success' => true, 'msg' => 'Colección: Colonias generada con exito', 'data' => $coloniasMunicipio] , 200);

    }
}
