<?php

namespace App\Http\Controllers\API;

use App\Models\Propiedad;
use League\Fractal\Manager;
use App\Transformers\Serializer;
use League\Fractal\Resource\Item;
use App\Http\Controllers\Controller;
use App\Models\Colonia;
use App\Transformers\EquipoTransformer;
use League\Fractal\Resource\Collection;
use App\Transformers\PropiedadTransformer;
use Illuminate\Http\Request;

/**
 * @group Propiedades
 *
 * API endpoints para la obtención de info sobre las propiedades registradas
 */
class PropiedadAPIController extends Controller
{
    /**
     * Lista de propiedades
     *
     * Genera la lista de propiedades registradas con formato
     *
     * @response {
     *      "success": true,
     *      "msg": "Colección: Propiedades generada con éxito",
     *      "data": [
     *  {
     *      "id": 1,
     *      "fecha_registro": "31/12/1969",
     *      "propietario": "Carlos Daniel Juarez",
     *      "telefono": "442-567-8945",
     *      "correo": "correo@prueba.com",
     *      "desarrollo": "Real del Sol",
     *      "propiedad_tipo": {
     *          "tipo": "Casa",
     *          "num_niveles": 2,
     *          "terraza_jardin": "NO"
     *      },
     *      "calle": "Av zaragoza, esquina con la piedad",
     *      "num_exterior": "150",
     *      "colonia": "Centro",
     *      "codigo_postal": "76000",
     *      "estado": "Aguascalientes",
     *      "municipio": "Querétaro",
     *      "precio": "$ 2,500,000.00",
     *      "superficie": 200.5,
     *      "recamaras": 3,
     *      "baños": 2,
     *      "med_baños": 0,
     *      "estacionamientos": 2,
     *      "antiguedad": "5 Años",
     *      "hipoteca": "NO",
     *      "hipoteca_tipo": null,
     *      "motivo_venta": "Cambiar de casa o depto",
     *      "motivo_complemento": "Otra empresa",
     *      "equipado": "SI",
     *      "equipos": [
     *          {
     *              "id": 2,
     *              "nombre": "Cocina Integral"
     *          },
     *          {
     *              "id": 3,
     *              "nombre": "Closet"
     *          },
     *          {
     *              "id": 5,
     *              "nombre": "Sala"
     *          },
     *          {
     *              "id": 6,
     *              "nombre": "Centro recreativo"
     *          }
     *      ]
     *  },
     *  {
     *      "id": 2,
     *      "fecha_registro": "31/12/1969",
     *      "propietario": "Martin Suarez",
     *      "telefono": "441-452-7896",
     *      "correo": "correo@correo.com",
     *      "desarrollo": "Real del Sol",
     *      "propiedad_tipo": {
     *          "tipo": "Casa",
     *          "num_niveles": 1,
     *          "terraza_jardin": "SI"
     *      },
     *      "calle": "Av Juarez",
     *      "num_exterior": "220B",
     *      "colonia": "Colinas del Rio",
     *      "codigo_postal": "20010",
     *      "estado": "Aguascalientes",
     *      "municipio": "Aguascalientes",
     *      "precio": "$ 1,250,000.00",
     *      "superficie": 150,
     *      "recamaras": 2,
     *      "baños": 1,
     *      "med_baños": 0,
     *      "estacionamientos": 2,
     *      "antiguedad": "10 Años",
     *      "hipoteca": "SI",
     *      "hipoteca_tipo": "Infonavit",
     *      "motivo_venta": "Remate bancario",
     *      "motivo_complemento": null,
     *      "equipado": "NO",
     *      "equipos": [
     *          {
     *              "id": 1,
     *              "nombre": "Cocina"
     *          },
     *          {
     *              "id": 4,
     *              "nombre": "Cancel"
     *          },
     *          {
     *              "id": 5,
     *              "nombre": "Sala"
     *          },
     *          {
     *              "id": 6,
     *              "nombre": "Centro recreativo"
     *          }
     *      ]
     *  },
     * }
     */
    public function getPropiedades()
    {
        $fractal = new Manager();
        $listaPropiedades = $fractal->setSerializer(new Serializer())
            ->createData(new Collection(Propiedad::all(), new PropiedadTransformer))->toArray();

        return response()->json(['success' => true, 'msg' => 'Colección: Propiedades generada con éxito', 'data' => $listaPropiedades], 200);
    }

    /**
     * Info de propiedad
     *
     * Genera detalle de propiedad con formato
     *
     * @urlParam propiedad integer required El ID de la propiedad registrada previamente Example: 1
     *
     * @response{
     *  "success": true,
     *  "msg": "Detalle de propiedad generada con éxito",
     *  "data": {
     *      "id": 1,
     *      "fecha_registro": "31/12/1969",
     *      "propietario": "Carlos Daniel Juarez",
     *      "telefono": "442-567-8945",
     *      "correo": "correo@prueba.com",
     *       "desarrollo": "Real del Sol",
     *      "propiedad_tipo": {
     *          "tipo": "Casa",
     *          "num_niveles": 2,
     *          "terraza_jardin": "NO"
     *      },
     *      "calle": "Av zaragoza, esquina con la piedad",
     *      "num_exterior": "150",
     *      "colonia": "Centro",
     *      "codigo_postal": "76000",
     *      "estado": "Aguascalientes",
     *      "municipio": "Querétaro",
     *      "precio": "$ 2,500,000.00",
     *      "superficie": 200.5,
     *      "recamaras": 3,
     *      "baños": 2,
     *      "med_baños": 0,
     *      "estacionamientos": 2,
     *      "antiguedad": "5 Años",
     *      "hipoteca": "NO",
     *      "hipoteca_tipo": null,
     *      "motivo_venta": "Cambiar de casa o depto",
     *      "motivo_complemento": "Otra empresa",
     *      "equipado": "SI",
     *      "equipos": [
     *          {
     *              "id": 2,
     *              "nombre": "Cocina Integral"
     *          },
     *          {
     *              "id": 3,
     *              "nombre": "Closet"
     *          },
     *          {
     *              "id": 5,
     *              "nombre": "Sala"
     *          },
     *          {
     *              "id": 6,
     *              "nombre": "Centro recreativo"
     *          }
     *      ]
     *  }
     * }
     */
    public function getPropiedadInfo(Propiedad $propiedad)
    {
        $fractal = new Manager();
        $infoPropiedad = $fractal->setSerializer(new Serializer())
            ->createData(new Item($propiedad, new PropiedadTransformer))->toArray();
        // return response()->json(['success' => true, 'msg' => 'Detalle de propiedad generada con éxito', 'data' => $infoPropiedad], 200);

        return view('admin.desc_propiedad')->with('propiedad',$infoPropiedad);
    }

    /**
     * Equipo propiedad
     *
     * Devuelve una lista de los equipos asignados a la propiedad
     *
     * @urlParam propiedad integer required ID de la propiedad Example: 1
     *
     * @response{
     *      "success": true,
     *      "msg": "Colección: Equipos de propiedad generada con exito",
     *      "data": [
     *          {
     *              "id": 2,
     *              "nombre": "Cocina Integral"
     *          },
     *          {
     *              "id": 3,
     *              "nombre": "Closet"
     *          },
     *          {
     *              "id": 5,
     *              "nombre": "Sala"
     *          },
     *          {
     *              "id": 6,
     *              "nombre": "Centro recreativo"
     *          }
     *      ]
     *  }
     */

    public function getEquipoPropiedad(Propiedad $propiedad)
    {
        $fractal = new Manager();
        $listaEquipos = $fractal->setSerializer(new Serializer())
            ->createData(new Collection($propiedad->equipos, new EquipoTransformer))->toArray();

        return response()->json(['success' => true, 'msg' => 'Colección: Equipos de propiedad generada con exito', 'data' => $listaEquipos], 200);
    }

}
