<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Property;
use Illuminate\Http\Request;
use App\Transformers\PropertyTransformer;
use League\Fractal\Resource\Collection;
use App\Transformers\Serializer;
use League\Fractal\Manager;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Mail;

class PropertyAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $property=Property::whereNull('deleted_at')->get();

        $fractal = new Manager();


        $results = $fractal
          ->setSerializer(new Serializer())
          ->createData(new Collection($property, new PropertyTransformer))
          ->toArray();

          return $results;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {

            Log::debug('PropertyAPIController@store', ['Datos del la propiedad' ,$request->all()]);

            DB::beginTransaction();

            Log::debug('PropertyAPIController@store', ['Generamos el la propiedad']);

            $property=new Property();
            $property->neigbhbourhood_id  = $request->neigbhbourhood_id;
            $property->property_type_id  = $request->property_type_id;
            $property->xanteid  = $request->xanteid;
            $property->title  = $request->title;
            $property->title2  = $request->title2;
            $property->description  = $request->description;
            $property->street  = $request->street;
            $property->no_ext  = $request->no_ext;
            $property->no_int  = $request->no_int;
            $property->slug  = $request->slug;
            $property->bathrooms  = $request->bathrooms;
            $property->location_description  = $request->location_description;
            $property->half_bathrooms  = $request->half_bathrooms;
            $property->bedrooms  = $request->bedrooms;
            $property->parking_slots  = $request->parking_slots;
            $property->m2  = $request->m2;
            $property->m2_construction  = $request->m2_construction;
            $property->m2_garden  = $request->m2_garden;
            $property->floors  = $request->floors;
            $property->lat  = $request->lat;
            $property->lon  = $request->lon;
            // $property->number_rooms  = $request->number_rooms;
            $property->price  = $request->price;
            $property->map_url  = $request->map_url;
            $property->status = 1;
            $property->save();


            Log::debug('propertyAPIController@store', ['property generado con id '.$property->id]);

            DB::commit();


            return response()
            ->json([
                'success' => true,
                'id' => $property->id,
                'msg' => 'property generado con id '.$property->id,
            ], 200);

            //enviamos la notificacion via mail

        } catch (\Exception $e) {

            Log::debug('propertyAPIController@store', ['Error al generar property']);

            $array = [
                'success' => false,
                'ERROR on the transaction Exception: ' => $e->getMessage(),
            ];

            Log::error('propertyAPIController@store ERROR on the transaction Exception: ', $array);

             return response()->json($array);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $property=Property::where('id',$id)->get();

        $fractal = new Manager();


        $results = $fractal
          ->setSerializer(new Serializer())
          ->createData(new Collection($property, new PropertyTransformer))
          ->toArray();

          return $results;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
