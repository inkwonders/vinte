<?php

namespace App\Http\Controllers;


use App\Models\Propiedad;
use App\Models\Questionnaire;
use App\Models\PropiedadImagen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;
use Image;

use App\Models\PropiedadTipo;
use App\Exports\PropiedadExport;
use App\Models\Colonia;
use App\Models\Desarrollo;
use App\Models\Estado;
use App\Models\Municipio;
use Exception;
use Facade\Ignition\DumpRecorder\Dump;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;


class QuestionnaireController extends Controller
{
    //

    public function store(Request $request)
    {
        //dd($request->equipo);
        Log::info('QuestionnaireController@store', $request->all());

        //return response()->json(['success' => false, 'msg' => 'Error', 'data' => $request->all()], 400);


        $validarqueexista = Questionnaire::where('name',$request->text['name'])
        ->where('email',$request->text['email'])
        ->where('whatsapp',$request->text['whatsapp'])
        ->where('property_type',$request->text['property_type'])
        ->where('bedroom',$request->text['bedroom'])
        ->where('bathroom',$request->text['bathroom'])
        ->where('years',$request->text['years'])
        ->where('m2',$request->text['m2'])
        ->where('offer_money',$request->text['offer_money'])
        ->where('mortgage',$request->text['mortgage'])
        ->where('sale_reasons',$request->text['sale_reasons'])
        ->first();

    if($validarqueexista == null || $validarqueexista == ""){
            // dd("no existe en la base de datos");

            DB::beginTransaction();

            try {

                $questionnaire = new Questionnaire;

                // dd($request->text);


                if( $request->text['property_type'] == 1){
                    $house_levels = $request->text['house_levels'];
                    $level_department = 0;
                }
                else{
                    $level_department =  $request->text['house_levels'];
                    $house_levels = 0;
                }

                // if( $request->text['desarrollo_id'] == 999 ){

                //     $google_direction =  $request->text['google_direction'];
                // }
                // else{
                //     $google_direction = "N/A";
                // }

                // if ($request->text['desarrollo_id'] == 999 || $request->text['desarrollo_id'] == null ) {
                //     $nombre_des = $google_direction;
                // } else {
                //     $cons = Desarrollo::find($request->text['desarrollo_id']);
                //     $nombre_des = $cons->nombre;
                // }

                if ($request->text['google_direction'] != null) {
                    $desarollo_id = 999;
                    $google_direction =  $request->text['google_direction'];
                    $street = 'N/A';
                    $num_ext = 'N/A';

                    $nombre_des = 'Otro';
                } else {
                    $desarollo_id =$request->text['desarrollo_id'];
                    $google_direction = "N/A";
                    $street = $request->text['street'];
                    $num_ext = $request->text['num_ext'];

                    $cons = Desarrollo::find($request->text['desarrollo_id']);
                    $nombre_des = $cons->nombre;
                }


                if (isset($request->text['comments']))
                {
                    $comments = $request->text['comments'];

                }
                else
                {
                    $comments = "N/A";
                }
                // dd($desarollo_id);

                $questionnaire->desarrollo_id  = $desarollo_id ;
                $questionnaire->propiedad_tipo_id  = $request->text['property_type'];
                $questionnaire->street  = $street;
                $questionnaire->num_ext  = $num_ext;
                // $questionnaire->num_int  = $num_int;
                $questionnaire->google_direction  = $google_direction;
                $questionnaire->property_type  = $request->text['property_type'];
                $questionnaire->house_levels  = $house_levels;
                $questionnaire->level_department  = $level_department;
                $questionnaire->bedroom  = $request->text['bedroom'];
                $questionnaire->bathroom  = $request->text['bathroom'];
                $questionnaire->half_bathrooms  = $request->text['half_bathrooms'];
                $questionnaire->parking_slots  = $request->text['parking_slots'];
                $questionnaire->years  = $request->text['years'];
                $questionnaire->m2  = $request->text['m2'];
                $questionnaire->offer_money  = floatval($request->text['offer_money']);
                $questionnaire->mortgage  = $request->text['mortgage'];
                $questionnaire->sale_reasons  = $request->text['sale_reasons'];
                $questionnaire->name  = $request->text['name'];
                $questionnaire->whatsapp  = $request->text['whatsapp'];
                $questionnaire->email  = $request->text['email'];
                $questionnaire->comments  = $comments;
                $questionnaire->accept_terms = 1;
                $questionnaire->preference = $request->text['preferencia'];
                $questionnaire->status  = 1;


                // dd($request);

                //guardar propiedad en bd
                $questionnaire->save();




                //SINCRONIZACION CON HUBSPOT la cuenta activa es de benito@momentum.com, se selecciona la fase 1 de benito,id 22204705

                $hubspot = \SevenShores\Hubspot\Factory::createWithOAuth2Token('pat-na1-0a47eab6-274e-4f07-8bce-ab8d4d93652f');


                if($questionnaire->propiedad_tipo_id == 1){
                    $niveles = ', No. Niveles de Casa: '.$questionnaire->house_levels;
                }else{
                    $niveles = ', Piso del departamento: '.$questionnaire->level_department;
                }

                if($questionnaire->mortgage == 'SI'){
                    $hipoteca = ', Hipoteca: Si, Tipo de hipoteca: '.$questionnaire->mortgage;
                }else{
                    $hipoteca = ', Hipoteca: No';
                }


                if($questionnaire->property_type == 1){
                    $tipo_de_inmueble = 'Casa';
                }else{
                    $tipo_de_inmueble = 'Departamento';
                }

                $descripcion = 'Propietario: '.$questionnaire->name.', Desarrollo: '.$nombre_des.',Dirección: '.$google_direction.', Correo: '.$questionnaire->email.', Whatsapp: '.$questionnaire->whatsapp.', Calle: '.$questionnaire->street.', No. Exterior: '.$questionnaire->num_ext.', No. Recamaras: '.$questionnaire->bedroom.', No. Baños: '.$questionnaire->bathroom.', No. Estacionamientos: '.$questionnaire->parking_slots.$niveles.$hipoteca.', Antigüedad: '.$questionnaire->years.' años, Motivo: '.$questionnaire->sale_reasons.', Precio: $'.number_format($questionnaire->offer_money,2);

                $propiedades = array(
                    array(
                        'value' =>  $questionnaire->name,
                        'name'  =>  'dealname'
                    ),
                    array(
                        'value' =>  'appointmentscheduled',
                        'name'  =>  'dealstage'
                    ),
                    array(
                        'value' =>  'default',
                        'name'  =>  'pipeline'
                    ),
                    array(
                        'value' =>  '61564762',
                        'name'  =>  'hubspot_owner_id'
                    ),
                    array(
                        'value' =>  $descripcion,
                        'name'  =>  'description'
                    ),
                    array(
                        'value' =>  'newbusiness',
                        'name'  =>  'dealtype'
                    ),
                    array(
                        'value' =>  $questionnaire->sale_reasons,
                        'name'  =>  'hs_campaign'
                    ),
                    array(
                        'value' =>  $questionnaire->offer_money,
                        'name'  => 'amount'
                    ),
                    // array(
                    //     'value' =>  'Fase 1',
                    //     'name'  => 'fase'
                    // ),

                );


                $contactos = array(

                    array(
                        'property' => 'email',
                        'value' =>  $questionnaire->email,
                    ),
                    array(
                        'property' => 'firstname',
                        'value' =>  $questionnaire->name,
                    ),
                    array(
                        'property' => 'phone',
                        'value' =>  $questionnaire->whatsapp,
                    ),
                    array(
                        'value' =>  $questionnaire->id,
                        'property'  =>  'xante_id'
                    ),
                    array(
                        'property' => 'hubspot_owner_id',
                        'value' =>  '196035747',
                    ),
                    array(
                        'value' =>  'Fase 1',
                        'property'  => 'fase'
                    ),
                    array(
                        'value' =>  $nombre_des,
                        'property'  => 'nombre_del_desarrollo'
                    ),
                    array(
                        'value' =>  $questionnaire->years,
                        'property'  => 'antiguedad_en_anos'
                    ),
                    array(
                        'value' =>  $tipo_de_inmueble,
                        'property'  => 'tipo_de_inmueble_'
                    ),
                    array(
                        'value' => $questionnaire->offer_money,
                        'property'  => 'precio_estimado'
                    ),

                );
                $hubspot->contacts()->createOrUpdate($questionnaire->email, $contactos);


                DB::commit();

                // $to = 'karla.ramirez@vinte.com';

                $to = 'contacto@xante.mx';

                // Mail::send('emails.property_form', ['propiedad' => $questionnaire, 'estado' => $estadoCo, 'desarrollo' => $nombre_des], function ($message) use ($to) {
                    Mail::send('emails.property_form', ['propiedad' => $questionnaire,  'desarrollo' => $nombre_des], function ($message) use ($to) {
                    $message->from('ventas@xante.mx');
                    $message->to($to)
                        ->subject('Se agrego una nueva propiedad en Xante.mx')
                        // ->cc('contacto@xante.mx')
                        ->cc('karla.ramirez@vinte.com');
                        // ->cc('natalia.vilchis@vinte.com')
                        // ->bcc('eddy@inkwonders.com');
                });

                //ENVIO DE SMS
                // $destino = "0052" . $questionnaire->whatsapp;
                // $message = "Se registró una propiedad  en Xante.mx a nombre de: ".$questionnaire->name.", en ".$nombre_des.", ID: ".$questionnaire->id;

                // $url = "https://api.netelip.com/v1/sms/api.php";
                // $post = array(
                //     "token"       => "025a086021128a5558b432008a3f06411b844fd06cb300186e7fdb8511e1e8c0",
                //     "from"        => "XANTE",
                //     "destination" => "00525535020645", //Natalia
                //     //"destination" => "00524428493129", //Eddy
                //     // "destination" => "00525531293712", //Alain
                //     "message"     => $message
                // );

                // $post2 = array(
                //     "token"       => "025a086021128a5558b432008a3f06411b844fd06cb300186e7fdb8511e1e8c0",
                //     "from"        => "XANTE",
                //     "destination" => "00525591898173", //Karla
                //     // "destination" => "00524424347581", //oswa
                //     "message"     => $message
                // );

                // $post3 = array(
                //     "token"       => "025a086021128a5558b432008a3f06411b844fd06cb300186e7fdb8511e1e8c0",
                //     "from"        => "XANTE",
                //     "destination" => "00525579318910", //
                //     "message"     => $message
                // );

                // $peticion = curl_init($url);
                // curl_setopt($peticion, CURLOPT_POST, 1);
                // curl_setopt($peticion, CURLOPT_POSTFIELDS, $post);
                // curl_setopt($peticion, CURLOPT_TIMEOUT, 180);
                // curl_setopt($peticion, CURLOPT_RETURNTRANSFER, 1);

                // $peticion2 = curl_init($url);
                // curl_setopt($peticion2, CURLOPT_POST, 1);
                // curl_setopt($peticion2, CURLOPT_POSTFIELDS, $post2);
                // curl_setopt($peticion2, CURLOPT_TIMEOUT, 180);
                // curl_setopt($peticion2, CURLOPT_RETURNTRANSFER, 1);

                // $peticion3 = curl_init($url);
                // curl_setopt($peticion3, CURLOPT_POST, 1);
                // curl_setopt($peticion3, CURLOPT_POSTFIELDS, $post3);
                // curl_setopt($peticion3, CURLOPT_TIMEOUT, 180);
                // curl_setopt($peticion3, CURLOPT_RETURNTRANSFER, 1);

                // $response = curl_exec($peticion);

                // if ($response !== false) {

                //     $response_code = curl_getinfo($peticion, CURLINFO_HTTP_CODE);

                //     if ($response_code == 200) {

                //         curl_close($peticion);

                //         $response2 = curl_exec($peticion2);

                //         if($response2 !== false){

                //             $response_code_2 = curl_getinfo($peticion2, CURLINFO_HTTP_CODE);

                //             if ($response_code_2 == 200) {

                //                 curl_close($peticion2);

                //                 $response3 = curl_exec($peticion3);

                //                 if($response3 !== false){

                //                     $response_code_3 = curl_getinfo($peticion3, CURLINFO_HTTP_CODE);

                //                     if($response_code_3 == 200){

                //                         curl_close($peticion3);

                //                         return response()->json(['success' => true, 'mgs' => 'Propiedad registrada con éxito', 'codigo' => 'ok'], 200);

                //                     }else{
                //                         throw new Exception('Falló el envio del tercer SMS, no se guardó la propiedad');
                //                     }

                //                 }else{
                //                     throw new Exception('Falló el envio del tercer SMS, no se guardó la propiedad');
                //                 }

                //             }else{
                //                 throw new Exception('Falló el envio del segundo SMS, no se guardó la propiedad');
                //             }

                //         }else{
                //             throw new Exception('Falló el envio del segundo SMS, no se guardó la propiedad');
                //         }

                //     } else {

                //         throw new Exception('Falló el envio de SMS, no se guardó la propiedad');

                //     }

                // }else{

                //     throw new Exception('No se completó la solicitud de envio de SMS');

                // }

                // return response()->json(['success' => true, 'mgs' => 'Propiedad registrada con éxito', 'codigo' => 'ok', 'data' => $questionnaire, 'hubspot' => $hubspot], 200);

            } catch (Exception $e) {
                DB::rollBack();
                Log::error("Error al resgistrar la propiedad", ['message' => $e->getMessage()]);
                return response()->json(['success' => false, 'codigo' => 'error', 'msg' => 'Error al registrar la propiedad, '.$e->getMessage()], 400);
            }


    }
    else{
        // dd("existe en la base de datos");
        Log::error("El registro ya existe en la base de datos");
        return response()->json(['success' => false, 'codigo' => 'error', 'msg' => 'El registro ya existe en la base de datos, '], 400);
        // return redirect()->route('index.nuevo');
    }


    }

}
