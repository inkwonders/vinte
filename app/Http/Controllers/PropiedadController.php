<?php

namespace App\Http\Controllers;

use App\Models\Propiedad;
use App\Models\PropiedadImagen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;
use Image;

use App\Models\PropiedadTipo;
use App\Exports\PropiedadExport;
use App\Models\Colonia;
use App\Models\Desarrollo;
use App\Models\Estado;
use App\Models\Municipio;
use Exception;
use Facade\Ignition\DumpRecorder\Dump;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

/**
 * @group Propiedades
 */
class PropiedadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.index");
    }

    public function index_old()
    {
        return view("admin.index_old");
    }


    public function show_properties()
    {
        return view("admin.new_properties");
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Guardar Propiedad (store)
     *
     * Registra una nueva propiedad en la base de datos
     *
     * @response {
     *      "success" : true,
     *      "msg" : "Registro de propiedad exitoso"
     * }
     *
     * @response 400 scenario="Validaciones erroneas" {
     *      "success" : false,
     *      "msg" : "Error, fallaron las validaciones",
     *      "data" : {
     *          "El campo superficie habitable debe ser un número.",
     *          "El campo num niveles casa es obligatorio cuando el campo propiedad tipo id es 1.",
     *          "El campo propiedad precio debe ser un número."
     *      }
     * }
     *
     * @response 400 scenario="No se pudo registrar la propiedad"{
     *      "success" : false,
     *      "msg" : "Error al registrar la propiedad"
     * }
     *
     * @response 400 scenario="Formato de imagen invalido"{
     *      "success" : false,
     *      "msg" : "Verifique el formato de la imagen, deben ser imagenes png o jpg, error 1"
     * }
     *
     * @response 400 scenario="Error al subir archivos de imagen"{
     *      "success" : false,
     *      "msg" : "Ocurrio un error al subir el archivo o el contenido del archivo no es válido, error 2"
     * }
     *
     * @response 400 scenario="Extension de imagen invalido"{
     *      "success" : false,
     *      "msg" : "La extensión del archivo debe ser png, jpg o jpeg, error 3"
     * }
     *
     */
    public function store(Request $request)
    {
        //dd($request->equipo);
        Log::info('PropiedadController@store', $request->all());

        //return response()->json(['success' => false, 'msg' => 'Error', 'data' => $request->all()], 400);

        $mensajes = [
            'required'              => 'Se esperaba :attribute'
        ];

        $validator = Validator::make(
            $request->all(),
            [
                //ID del estado donde se ubica la propiedad a vender Example:22
                'estado_id'             => 'required|exists:estados,id|integer',
                //ID del desarrollo Vinte Example: 1
                // 'desarrollo_id'         => 'required|exists:desarrollos,id|integer',
                'desarrollo_id'         => 'required',
                // 'otro_desarrollo'       => 'required_if:desarrollo_id,9999',
                'desarrollo_otro'       => 'required',
                //Tipo de propiedad a vender (1 = Casa | 2 = Departamento) Example: 1
                'propiedad_tipo_id'     => 'required|exists:propiedad_tipos,id|integer',
                //Colonia id
                'colonia_id'            => 'nullable|integer',
                //Calle, dirección de la propiedad Example: Av. Corregidora
                'direccion'             => 'required|min:3',
                //Parte de la direccion de la propiedad Example: 100A
                'num_exterior'          => 'required',
                //Superficie del terreno de la propiedad, puede ser una estimación Example: 180.50
                'superficie_habitable'  => 'required|numeric',
                //Indica si la propiedad cuenta con equipo (SI/NO) Example: NO
                'equipado'              => 'required',
                //En caso de que sea un departamento indicar el piso del edificio en el que se encuentra la propiedad Example:2
                'piso_departamento'     => 'required_if:propiedad_tipo_id,2|numeric|nullable',
                //En caso de que sea una casa indicar cuantos pisos/niveles tiene Example: 3
                'num_niveles_casa'      => 'required_if:propiedad_tipo_id,1|numeric|nullable',
                //En caso de que sea una casa indicar si cuenta con terraza/jardin (SI/NO) Example: NO
                'terraza_jardin'        => 'required_if:propiedad_tipo_id,1|nullable',
                //Número de recamaras que tiene la propiedad Example: 3
                'num_recamaras'         => 'required|numeric',
                //Número de baños que tiene la propiedad Example:2
                'num_bath'              => 'required|numeric',
                //Número de medios baños que tiene la propiedad Example:0
                'num_med_bath'          => 'numeric',
                //Número de estacionamientos asignados de la propiedad Example: 1
                'num_estacionamientos'  => 'required|numeric',
                //Años de antigüedad que tiene la propiedad Example: 5
                'antiguedad'            => 'required',
                //Precio estimado que se espera por la venta de la propiedad Example: 1250000
                'propiedad_precio'      => 'required|numeric',
                //Indica si la propiedad cuenta con hipoteca
                'hipoteca'              => 'required',
                //En caso de que hipoteca sea 'SI', indica el tipo de hipoteca de la propiedad Example: Infonavit
                'hipoteca_tipo'         => 'required_if:hipoteca,"SI"',
                //Número de telefono del propietario Example: 4421567845
                'telefono'              => 'required|numeric|digits:10',
                //Correo Electronico del propietario Example: juan_marquez_78@gmail.com
                'correo'                => 'required|email',
                //Nombre del propietario Example: Juan Marquez Solis Herrera
                'nombre_propietario'    => 'required',
                //Motivo de venta de la propiedad Example: Cambio de casa o departamento
                'motivo'                => 'required',
                //Complemento del motivo en caso de que 'motivo' sea 'Cambio de casa o departamento' Example: Otro desarrollo Vinte
                'motivo_comp'           => 'required_if:motivo,"Cambiar de casa o departamento"',
                'cp_estado_existe'      => 'required|numeric',
                'nombre_colonia'        => 'required_if:cp_estado_existe,0',
                'codigo_postal'         => 'required|numeric'
            ],
            $mensajes
        );

        if ($validator->fails()) {
            //return $validator->errors()->all();
            return response()->json(['success' => false, 'msg' => 'Error, fallaron las validaciones', 'data' => $validator->errors()->all()], 400);
        }

        // dd($request);

        DB::beginTransaction();

        try {

            $propiedad = new Propiedad;

            //Caracteristicas generales
            $propiedad->estado_id             = $request->estado_id;

            // if (intval($request->otro_desarrollo) == 1) {
            //     $propiedad->desarrollo_id         = NULL;
            //     $propiedad->otro_desarrollo       = $request->desarrollo_id;
            // } else {
            //     $id_des = intval($request->desarrollo_id);
            //     $propiedad->desarrollo_id   =  $id_des;
            //     $propiedad->otro_desarrollo = NULL;
            // }

            if($request->desarrollo_id == intval(9999)){
                $propiedad->desarrollo_id         = NULL;
                $propiedad->otro_desarrollo       = $request->desarrollo_otro;
            }else{
                $id_des = intval($request->desarrollo_id);
                $propiedad->desarrollo_id   =  $id_des;
                $propiedad->otro_desarrollo = NULL;
            }

            if($request->cp_estado_existe == 0){

                $municipio = new Municipio;
                $municipio->descripcion = 'NO_MUNICIPIO';
                $municipio->lat = 0;
                $municipio->lon = 0;
                $municipio->estado_id = $request->estado_id;
                $municipio->save();

                $colonia = new Colonia;
                $colonia->nombre = $request->nombre_colonia;
                $colonia->codigo_postal = $request->codigo_postal;
                $colonia->tipo = 'Colonia';
                $colonia->municipio_id = $municipio->id;
                $colonia->save();

            }else{
                $colonia = Colonia::find($request->colonia_id);
            }

            $propiedad->propiedad_tipo_id     = $request->propiedad_tipo_id;
            $propiedad->colonia_id            = $colonia->id;
            $propiedad->direccion             = $request->direccion;
            $propiedad->num_exterior          = $request->num_exterior;
            $propiedad->superficie_habitable  = $request->superficie_habitable;
            $propiedad->equipado              = $request->equipado;
            $propiedad->num_recamaras         = $request->num_recamaras;
            $propiedad->num_bath              = $request->num_bath;
            $propiedad->num_med_bath          = $request->num_med_bath;
            $propiedad->num_estacionamientos  = $request->num_estacionamientos;
            $propiedad->antiguedad            = $request->antiguedad;
            $propiedad->propiedad_precio      = $request->propiedad_precio;
            $propiedad->hipoteca              = $request->hipoteca;
            $propiedad->telefono              = $request->telefono;
            $propiedad->correo                = $request->correo;
            $propiedad->nombre_propietario    = $request->nombre_propietario;
            $propiedad->motivo                = $request->motivo;
            $propiedad->estatus               = 'Activo';

            //Si es casa
            if ($request->propiedad_tipo_id == 1) {

                $propiedad->num_niveles_casa = $request->num_niveles_casa;
                $propiedad->terraza_jardin = $request->terraza_jardin;

                //Si es Departamento
            } else {

                $propiedad->piso_departamento = $request->piso_departamento;
            }

            //Si hay hipoteca
            if ($request->hipoteca == 'SI') {

                $propiedad->hipoteca_tipo = $request->hipoteca_tipo;
            }

            //Si motivo de venta es 'Cambiar de casa o depto'
            if ($request->motivo == 'Cambiar de casa o departamento') {

                $propiedad->motivo_comp = $request->motivo_comp;
            }


            //guardar propiedad en bd
            $propiedad->save();

            //  Log::debug("PropiedadController@store los datos de la propiedad son: ", ['propiedad' => $propiedad]);

            if ($request->equipado == 'SI') {
                //guardar equipos

                if (empty($request->equipo)) {
                    $request->equipado == 'NO';
                } else {
                    $array = explode(',', $request->equipo);
                    // dd($array);
                    // dd($propiedad);
                    foreach ($array as $key=>$id_elemento_seleccionado) {
                        // dd($id_elemento_seleccionado);
                        // if($key != 0){
                            $propiedad->equipos()->attach($id_elemento_seleccionado);
                        // }
                    }
                }
            }

            // Log::info('PropiedadController@store Equipo de la propiedad', $request->equipo);

            //Imagenes
            if (count($request->files) > 10) {
                return response()->json(['success' => false, 'msg' => 'Solo se pueden subir un máximo de 10 imagenes por propiedad, error 5'], 400);
            } else {
                if ($request->files) {
                    foreach ($request->files as $contenido => $imagen) {

                        if (!in_array($imagen->getMimeType(), ["image/png", "image/jpeg", "image/jpg"])) {
                            return response()->json(['success' => false, 'msg' => 'Verifique el formato de la imagen, deben ser imagenes png o jpg, error 1'], 400);
                        }

                        if (!$imagen->isValid()) {
                            return response()->json(['success' => false, 'msg' => 'Ocurrio un error al subir el archivo o el contenido del archivo no es válido, error 2'], 400);
                        }

                        if (!in_array($imagen->guessExtension(), ["png", "jpg", "jpeg"])) {
                            return response()->json(['success' => false, 'msg' => 'La extensión del archivo debe ser png, jpg o jpeg, error 3'], 400);
                        }

                        if (filesize($imagen) > 5242880) {
                            return response()->json(['success' => false, 'msg' => 'El tamaño de la imagen no puede superar los 5MB, error 4'], 400);
                        }

                        $nombre_imagen = $propiedad->id . '_' . date('d-m-Y') . '_' . $this->rand_str() . '.' . $imagen->guessExtension();

                        // $destinationPath = 'img/propiedad_imagenes/';
                        // $destinationPath = $imagen->storeAs('propiedad_imagenes', );

                        //EL 24 DE 01 DE 2023 SE CAMBIA EL STORE A DISK PARA EVITAR CAMBIAR PERSMISOS EN CADA PULL, EL NUEVO LUGAR DONDE SE GUARDARAN
                        // LAS IMAGENES ES EN C:\laragon\www\vinte-main\storage\propiedad_imagenes, LA CONFIGURACION DEL DISK ESTÁ EN FILESYSTEMS.PHP

                        // dd($imagen);
                        // $destinationPath = $imagen->storeAs('propiedad_imagenes', $nombre_imagen);
                        Storage::disk('propiedad_imagenes')->put($nombre_imagen, file_get_contents($imagen));
                        // $imagen->move($destinationPath, $nombre_imagen);

                        $image = new PropiedadImagen([
                            'url' => 'propiedad_imagenes/'.$nombre_imagen,
                            'nombre' => $nombre_imagen,
                        ]);

                        $propiedad->images()->save($image);
                    }
                }
            }

            //DB::commit();

            $estadoCo = Estado::find($propiedad->estado_id);

            if ($propiedad->desarrollo_id == 9999 || $propiedad->desarrollo_id == null) {
                $nombre_des = $propiedad->otro_desarrollo;
            } else {
                $cons = Desarrollo::find($propiedad->desarrollo_id);
                $nombre_des = $cons->nombre;
            }

            //SINCRONIZACION CON HUBSPOT
            // $hubspot = \SevenShores\Hubspot\Factory::create('5e061413-010d-47af-a12a-1a94deeb721b');//api key HUBSPOT : benito@momentumkt.com

            $hubspot = \SevenShores\Hubspot\Factory::createWithOAuth2Token('pat-na1-0a47eab6-274e-4f07-8bce-ab8d4d93652f');
             // pat-na1-429f062c-f3ab-49da-95a5-e57736e09173 xante vende tu casa, este api key se usa para vende tu casa, ahi cae el lead
            // pat-na1-0a47eab6-274e-4f07-8bce-ab8d4d93652f   --> xante compra tu casa , este api key se usa para compra tu casa, ahi cae el lead


            // pat-na1-0a47eab6-274e-4f07-8bce-ab8d4d93652f
            // pat-na1-a4969485-3da3-40e1-8393-d86881127130   --> ficha de cuenta de benito@momentumkt.com


            if($propiedad->propiedad_tipo_id == 1){
                $niveles = ', No. Niveles de Casa: '.$propiedad->num_niveles_casa.', Terraza/Jardín: '.$propiedad->terraza_jardin;
            }else{
                $niveles = ', Piso del departamento: '.$propiedad->departamento_piso;
            }

            if($propiedad->hipoteca == 'SI'){
                $hipoteca = ', Hipoteca: Si, Tipo de hipoteca: '.$propiedad->hipoteca_tipo;
            }else{
                $hipoteca = ', Hipoteca: No';
            }

            $descripcion = 'Propietario: '.$propiedad->nombre_propietario.', Correo: '.$propiedad->correo.', Teléfono: '.$propiedad->telefono.', Desarrollo: '.$nombre_des.', Calle: '.$propiedad->direccion.', No. Exterior: '.$propiedad->num_exterior.', Estado: '.$propiedad->estado->descripcion.', Tipo de propiedad: '.$propiedad->propiedad_tipo->nombre.', No. Recamaras: '.$propiedad->num_recamaras.', No. Baños: '.$propiedad->num_bath.', No. Estacionamientos: '.$propiedad->num_estacionamientos.$niveles.$hipoteca.', Equipado: '.$propiedad->equipado.', Antigüedad: '.$propiedad->antiguedad.' años, Motivo: '.$propiedad->motivo.', Precio: $'.number_format($propiedad->propiedad_precio,2);

            $propiedades = array(
                array(
                    'value' =>  $propiedad->nombre_propietario,
                    'name'  =>  'dealname'
                ),
                array(
                    'value' =>  'appointmentscheduled',
                    'name'  =>  'dealstage'
                ),
                array(
                    'value' =>  'default',
                    'name'  =>  'pipeline'
                ),
                array(
                    'value' =>  '61564762', //ID PROPIETARIO HUBSPOT : operaciones@momnetumkt.com  //se obtiene el hubspot id https://www.youtube.com/watch?v=MkTXZSg5Vtg
                    'name'  =>  'hubspot_owner_id'
                ),
                array(
                    'value' =>  $descripcion,
                    'name'  =>  'description'
                ),
                array(
                    'value' =>  'newbusiness',
                    'name'  =>  'dealtype'
                ),
                array(
                    'value' =>  $propiedad->motivo,
                    'name'  =>  'hs_campaign'
                ),
                array(
                    'value' =>  $propiedad->propiedad_precio,
                    'name'  => 'amount'
                ),

            );

            // $hubspot->deals()->create($propiedades);

            $contactos = array(

                array(
                    'property' => 'email',
                    'value' =>  $propiedad->correo,
                ),
                array(
                    'property' => 'firstname',
                    'value' =>  $propiedad->nombre_propietario,
                ),
                array(
                    'property' => 'phone',
                    'value' =>  $propiedad->telefono,
                ),
                array(
                    'value' =>  $propiedad->id,
                    'property'  =>  'xante_id'
                ),
                array(
                    'property' => 'hubspot_owner_id',
                    'value' =>  '196035747', //id de contacto (contacto@xante.mx)
                )
            );
            $hubspot->contacts()->createOrUpdate($propiedad->correo, $contactos);

            // 61564762 --> id de benito (benito@momentumkt.com)
            // 196035747 --> id contacto@xante.mx

            DB::commit();

            $to = 'karla.ramirez@vinte.com';
            // $to = 'alainttlm@gmail.com';

            Mail::send('emails.property_form', ['propiedad' => $propiedad, 'estado' => $estadoCo, 'desarrollo' => $nombre_des], function ($message) use ($to) {
                $message->from('contacto@xante.mx');
                $message->to($to)
                    ->subject('Se agrego una nueva propiedad en Xante.mx')
                    ->cc('natalia.vilchis@vinte.com')
                    ->bcc('eddy@inkwonders.com');
            });

            //ENVIO DE SMS
            $destino = "0052" . $propiedad->telefono;
            $message = "Se registró una propiedad  en Xante.mx a nombre de: ".$propiedad->nombre_propietario.", en ".$nombre_des.", ".$estadoCo->abreviacion.", ID: ".$propiedad->id;

            $url = "https://api.netelip.com/v1/sms/api.php";
            $post = array(
                "token"       => "025a086021128a5558b432008a3f06411b844fd06cb300186e7fdb8511e1e8c0",
                "from"        => "XANTE",
                "destination" => "00525535020645", //Natalia
                //"destination" => "00524428493129", //Eddy
                // "destination" => "00525531293712", //Alain
                "message"     => $message
            );

            $post2 = array(
                "token"       => "025a086021128a5558b432008a3f06411b844fd06cb300186e7fdb8511e1e8c0",
                "from"        => "XANTE",
                "destination" => "00525591898173", //Karla
                // "destination" => "00524424347581", //oswa
                "message"     => $message
            );

            $post3 = array(
                "token"       => "025a086021128a5558b432008a3f06411b844fd06cb300186e7fdb8511e1e8c0",
                "from"        => "XANTE",
                "destination" => "00525579318910", //
                "message"     => $message
            );

            $peticion = curl_init($url);
            curl_setopt($peticion, CURLOPT_POST, 1);
            curl_setopt($peticion, CURLOPT_POSTFIELDS, $post);
            curl_setopt($peticion, CURLOPT_TIMEOUT, 180);
            curl_setopt($peticion, CURLOPT_RETURNTRANSFER, 1);

            $peticion2 = curl_init($url);
            curl_setopt($peticion2, CURLOPT_POST, 1);
            curl_setopt($peticion2, CURLOPT_POSTFIELDS, $post2);
            curl_setopt($peticion2, CURLOPT_TIMEOUT, 180);
            curl_setopt($peticion2, CURLOPT_RETURNTRANSFER, 1);

            $peticion3 = curl_init($url);
            curl_setopt($peticion3, CURLOPT_POST, 1);
            curl_setopt($peticion3, CURLOPT_POSTFIELDS, $post3);
            curl_setopt($peticion3, CURLOPT_TIMEOUT, 180);
            curl_setopt($peticion3, CURLOPT_RETURNTRANSFER, 1);

            $response = curl_exec($peticion);

            if ($response !== false) {

                $response_code = curl_getinfo($peticion, CURLINFO_HTTP_CODE);

                if ($response_code == 200) {

                    curl_close($peticion);

                    $response2 = curl_exec($peticion2);

                    if($response2 !== false){

                        $response_code_2 = curl_getinfo($peticion2, CURLINFO_HTTP_CODE);

                        if ($response_code_2 == 200) {

                            curl_close($peticion2);

                            $response3 = curl_exec($peticion3);

                            if($response3 !== false){

                                $response_code_3 = curl_getinfo($peticion3, CURLINFO_HTTP_CODE);

                                if($response_code_3 == 200){

                                    curl_close($peticion3);

                                    return response()->json(['success' => true, 'mgs' => 'Propiedad registrada con éxito', 'codigo' => 'ok'], 200);

                                }else{
                                    throw new Exception('Falló el envio del tercer SMS, no se guardó la propiedad');
                                }

                            }else{
                                throw new Exception('Falló el envio del tercer SMS, no se guardó la propiedad');
                            }

                        }else{
                            throw new Exception('Falló el envio del segundo SMS, no se guardó la propiedad');
                        }

                    }else{
                        throw new Exception('Falló el envio del segundo SMS, no se guardó la propiedad');
                    }

                } else {

                    throw new Exception('Falló el envio de SMS, no se guardó la propiedad');

                }

            }else{

                throw new Exception('No se completó la solicitud de envio de SMS');

            }

            // return response()->json(['success' => true, 'mgs' => 'Propiedad registrada con éxito', 'codigo' => 'ok'], 200);

        } catch (Exception $e) {
            DB::rollBack();
            Log::error("Error al resgistrar la propiedad", ['message' => $e->getMessage()]);
            return response()->json(['success' => false, 'codigo' => 'error', 'msg' => 'Error al registrar la propiedad, '.$e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Propiedad  $propiedad
     * @return \Illuminate\Http\Response
     */
    public function show(Propiedad $propiedad)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Propiedad  $propiedad
     * @return \Illuminate\Http\Response
     */
    public function edit(Propiedad $propiedad)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Propiedad  $propiedad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Propiedad $propiedad)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Propiedad  $propiedad
     * @return \Illuminate\Http\Response
     */
    public function destroy(Propiedad $propiedad)
    {
        //
    }

    public function rand_str()
    {
        $length = 5;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    // verificar estado con cp
    public function verificarEstado(Request $request){
        $colonia = Colonia::where('codigo_postal',$request->cp)->first();
        $estado = $colonia->municipio->estado;

        if($estado->id == $request->estado ){
            return response()->json(['success' => true, 'msg' => 'El cp pertenece al estado '.$estado->descripcion, 'data' => 1], 200);
        }else{
            return response()->json(['success' => true, 'msg' => 'Error, El cp no pertenece al estado', 'data' => 0], 200);
        }

    }

    // SMS DE PRUEBAS
    public function prueba(){
        //ENVIO DE SMS
        $destino = "0052";
        $message = "Se registró una propiedad  en Xante.mx a nombre de: prueba sms";

        //dd($message);

        $url = "https://api.netelip.com/v1/sms/api.php";
        $post = array(
            "token"       => "025a086021128a5558b432008a3f06411b844fd06cb300186e7fdb8511e1e8c0",
            "from"        => "XANTE",
            "destination" => "00525531293712", //Alain
            "message"     => $message
        );

        $post2 = array(
            "token"       => "025a086021128a5558b432008a3f06411b844fd06cb300186e7fdb8511e1e8c0",
            "from"        => "XANTE",
            "destination" => "00524421899404", //Emma
            "message"     => $message
        );

        $post3 = array(
            "token"       => "025a086021128a5558b432008a3f06411b844fd06cb300186e7fdb8511e1e8c0",
            "from"        => "XANTE",
            "destination" => "00524424347581", //Oswaldo
            "message"     => $message
        );

        $peticion = curl_init($url);
        curl_setopt($peticion, CURLOPT_POST, 1);
        curl_setopt($peticion, CURLOPT_POSTFIELDS, $post);
        curl_setopt($peticion, CURLOPT_TIMEOUT, 180);
        curl_setopt($peticion, CURLOPT_RETURNTRANSFER, 1);

        $peticion2 = curl_init($url);
        curl_setopt($peticion2, CURLOPT_POST, 1);
        curl_setopt($peticion2, CURLOPT_POSTFIELDS, $post2);
        curl_setopt($peticion2, CURLOPT_TIMEOUT, 180);
        curl_setopt($peticion2, CURLOPT_RETURNTRANSFER, 1);

        $peticion3 = curl_init($url);
        curl_setopt($peticion3, CURLOPT_POST, 1);
        curl_setopt($peticion3, CURLOPT_POSTFIELDS, $post3);
        curl_setopt($peticion3, CURLOPT_TIMEOUT, 180);
        curl_setopt($peticion3, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($peticion);
        $response_code = curl_getinfo($peticion, CURLINFO_HTTP_CODE);

        $res = "";


            curl_close($peticion);
            $res.="se mando 1,  ";


        $response = curl_exec($peticion2);
        $response_code = curl_getinfo($peticion2, CURLINFO_HTTP_CODE);


            curl_close($peticion2);
            $res.="se mando 2,  ";


        $response = curl_exec($peticion3);
        $response_code = curl_getinfo($peticion3, CURLINFO_HTTP_CODE);


            curl_close($peticion3);
            $res.="se mando 3,  ";


        return $res;
    }

    public function sendEmail(Request $request){

        // YA NO ESTÁ AQUI LA FUNCION, SE MUDO AL LIVEWIRE C:\laragon\www\vinte-main\app\Http\Livewire\DescripcionCasa.php



    //     // $hubspot = \SevenShores\Hubspot\Factory::createWithOAuth2Token('pat-na1-0a47eab6-274e-4f07-8bce-ab8d4d93652f');
    //     //key de oauth 2 token uenta xante compra tu casa (vendemos casa) se genero ya que el mail pertenece a la otra cuenta
    //     $hubspot = \SevenShores\Hubspot\Factory::createWithOAuth2Token('pat-na1-429f062c-f3ab-49da-95a5-e57736e09173');

    //   Log::debug("PropiedadController@sendEmail conecion hubspot: ", ['conexion' => $hubspot]);


    //     $contactos = array(

    //         array(
    //             'property' => 'email',
    //             'value' =>  $request->txt_correo,
    //         ),
    //         array(
    //             'property' => 'firstname',
    //             'value' =>  $request->txt_nombre,
    //         ),
    //         array(
    //             'property' => 'phone',
    //             'value' =>  $request->txt_telefono,
    //         ),
    //         array(
    //             'property' => 'hubspot_owner_id',
    //             //  'value' =>  '196035747', //id de contacto (contacto@xante.mx)
    //              'value' =>  '251810094', //id de contacto (vende@xante.com)
    //         )
    //     );
    //     $hubspot->contacts()->createOrUpdate($request->txt_correo, $contactos);

    //     // $to ="venta@xante.com";
    //     $to ="vende@xante.com";
    //     Mail::send('emails.contact_property_form', ['propiedad' => $request->txt_propiedad, 'txt_nombre' => $request->txt_nombre, 'txt_telefono' => $request->txt_telefono, 'txt_correo' => $request->txt_correo], function ($message) use ($to) {
    //         $message->from('contacto@xante.mx');
    //         $message->to($to)
    //             ->subject('Solicitud de información de propiedad en Xante.mx')
    //             ->bcc('eddy@inkwonders.com');
    //     });



    //     return redirect('thankyou');
    }



}
