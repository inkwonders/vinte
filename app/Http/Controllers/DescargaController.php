<?php

namespace App\Http\Controllers;

use App\Exports\PropiedadCasaExport;
use App\Exports\PropiedadEspecificaExport;
use App\Exports\QuestionnaireExport;
use App\Models\Propiedad;
use App\Exports\PropiedadExport;
use App\Models\Property;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use File;
use Illuminate\Support\Facades\Storage;
use ZipArchive;

/**
 * @group Propiedades
 */
class DescargaController extends Controller
{
    /*
     *
     */
    public function descargarZip(Propiedad $propiedad)
    {

        if ($propiedad->images->count() != 0) {

            $zip = new ZipArchive;
            //revisar son manuel la funcion transform

            $nuevo_nombre = str_replace(' ', '_', $propiedad->id . '-' . $propiedad->direccion . '-' . $propiedad->num_exterior . '-' . $propiedad->colonia->codigo_postal);

            $file_name = $nuevo_nombre . '.zip';

            //usar disk, cambiar ruta
            if ($zip->open(storage_path('zip_archives/' . $file_name), ZipArchive::CREATE) === TRUE) {
                // dd($propiedad->images);
                foreach ($propiedad->images as $imagen) {

                    // dd($imagen->url);

                    if($imagen->url == 'img/propiedad_imagenes/'){
                    $zip->addFile(public_path('img/propiedad_imagenes/' . $imagen->nombre), $imagen->nombre);
                    }
                    else{
                        // $zip->addFile(public_path($imagen->url), $imagen->nombre);
                        $zip->addFile( storage_path('propiedad_imagenes/' . $imagen->nombre), $imagen->nombre);
                    }
                }

                $zip->close();
            }

            return response()->download(storage_path('zip_archives/' . $file_name));
        } else {
            return response()->json(['success' => false, 'msg' => 'La propiedad no tiene imagenes'], 404);
        }
    }

    public function descargarNewProperties(Property $propiedad)
    {

        if ($propiedad->images->count() != 0) {

            $zip = new ZipArchive;
            //revisar son manuel la funcion transform

            $nuevo_nombre = str_replace(' ', '_', $propiedad->id );

            $file_name = $nuevo_nombre . '.zip';



            //usar disk, cambiar ruta
            if ($zip->open(storage_path('zip_archives/' . $file_name), ZipArchive::CREATE) === TRUE) {
// dd($propiedad->images);
                foreach ($propiedad->images as $imagen) {
                    // dd(storage_path('properties_pics/' . $imagen->name));
                    $zip->addFile( storage_path('app/properties_pics/' . $imagen->name), $imagen->name);
                }

                $zip->close();
            }

            return response()->download(storage_path('zip_archives/' . $file_name));
        } else {
            return response()->json(['success' => false, 'msg' => 'La propiedad no tiene imagenes'], 404);
        }
    }

    // public function exportarPropiedad()
    public function exportarPropiedad(Request $request)
    {
        // dd($request);
        return (new QuestionnaireExport($request->inicio, $request->fin))->download('Propiedades Xante.xlsx');
        // return (new PropiedadExport())->download('Propiedades Vinte.xlsx');
    }
    public function exportarPropiedadOld(Request $request)
    {
        // dd($request);
        // return (new QuestionnaireExport($request->inicio, $request->fin))->download('Propiedades Xante.xlsx');
        return (new PropiedadExport($request->inicio, $request->fin))->download('Propiedades Xante.xlsx');
    }

    public function exportarPropiedadEspecifica(Propiedad $propiedad)
    {
        if ($propiedad->propiedad_tipo->nombre == 'Casa') {
            return (new PropiedadCasaExport($propiedad->id))->download('Propiedad Casa xante.xlsx');
        } else {
            return (new PropiedadEspecificaExport($propiedad->id))->download('Propiedad Departamento xante.xlsx');
        }
    }


}
