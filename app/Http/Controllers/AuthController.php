<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    //
     //CERRAMOS SESION
     public function logOut(Request $request)
     {
         Auth::logout();
         return redirect()->route('index.nuevo');
     }
}
