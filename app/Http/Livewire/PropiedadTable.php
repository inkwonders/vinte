<?php

namespace App\Http\Livewire;

use Exception;
use App\Models\Propiedad;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\DataTableComponent;

class PropiedadTable extends DataTableComponent
{
    public $table_headers = 'livewire.header-devoluciones';

    public bool $showSearch = false;
    public bool $showPerPage = false;
    public $inicio = '';
    public $fin = '';
    public $mostrar_info = false;

    protected $listeners = [
        'inicio',
        'fin'
    ];

    public function mount()
    {
        $date = date('Y-m-d');
        $this->inicio = date('Y-m-01');
        $this->fin = date('Y-m-d');

        $hubspot = \SevenShores\Hubspot\Factory::createWithOAuth2Token('pat-na1-a4969485-3da3-40e1-8393-d86881127130');
        // dd($hubspot->owners()->all());
    }

    public function inicio($inicio)
    {
        $this->inicio = $inicio;
    }

    public function fin($fin)
    {
        $this->fin = $fin;
    }

    //Creacion de las columnas de la tabla
    public function columns(): array
    {
        return [
            Column::make('ID', 'id')
                ->sortable(),
            Column::make(__('traduction.registration_date'), 'created_at')
                ->sortable(),
            Column::make(__('traduction.name'), 'nombre_propietario')
                ->sortable()
                ->searchable(function (Builder $query, $searchTerm) {
                    $query->orWhere('propiedades.nombre_propietario', 'LIKE', "%$searchTerm%");
                }),
            Column::make(__('traduction.phone'), 'telefono')
                ->sortable()
                ->searchable(function (Builder $query, $searchTerm) {
                    $query->orWhere('propiedades.telefono', 'LIKE', "%$searchTerm%");
                }),

            Column::make(__('traduction.dev_name'), 'nombre_desarrollo')
                ->sortable()
                ->searchable(function (Builder $query, $searchTerm) {
                    $query->orWhere('desarrollos.nombre', 'LIKE', "%$searchTerm%");
                }),

            Column::make(__('traduction.property_type'), 'tipo_propiedad')
                ->sortable()
                ->searchable(function (Builder $query, $searchTerm) {
                    $query->orWhere('propiedad_tipos.nombre', 'LIKE', "%$searchTerm%");
                }),


            Column::make(__('traduction.state'), 'estado_id')
                ->sortable()
                ->searchable(function (Builder $query, $searchTerm) {
                    $query->orWhere('estados.descripcion', 'LIKE', "%$searchTerm%");
                }),

            Column::make(__('traduction.zip_code'), 'cp_colonia')
                ->sortable()
                ->searchable(function (Builder $query, $searchTerm) {
                    $query->orWhere('colonias.codigo_postal', 'LIKE', "%$searchTerm%");
                }),

            Column::make(__('traduction.price'), 'propiedad_precio')
                ->sortable(),
            // ->searchable(function (Builder $query, $searchTerm) {
            //     $query->orWhere('propiedades.propiedad_precio', '>=', $searchTerm);
            // }),

            Column::make(__('traduction.status'), 'estatus')
                ->sortable(),

            Column::make(__('traduction.see')),
            Column::make(__('traduction.download_photos'))
        ];
    }


    //Query de los registros
    public function query(): Builder
    {
        $query = Propiedad::query()
            ->leftJoin('desarrollos', 'desarrollos.id', '=', 'propiedades.desarrollo_id')
            ->join('propiedad_tipos', 'propiedad_tipos.id', 'propiedades.propiedad_tipo_id')
            ->join('colonias', 'colonias.id', '=', 'propiedades.colonia_id')
            ->join('estados', 'estados.id', '=', 'propiedades.estado_id')
            ->whereBetween('propiedades.created_at', ["{$this->inicio} 00:00:00", "{$this->fin} 23:59:59"])
            ->select(
                'propiedades.telefono',
                'propiedades.id',
                'propiedades.direccion',
                'propiedades.nombre_propietario',
                'propiedades.created_at',
                'propiedades.otro_desarrollo',
                'propiedades.estatus',
                DB::raw('propiedades.propiedad_precio as precio'),
                DB::raw('propiedad_tipos.nombre as tipo_propiedad'),
                // DB::raw('desarrollos.nombre as nombre_desarrollo'),
                DB::raw('IF(desarrollos.nombre IS NULL, propiedades.otro_desarrollo, (SELECT nombre FROM desarrollos WHERE id = propiedades.desarrollo_id)) as nombre_desarrollo'),
                // DB::raw('IF(desarrollos.nombre IS NULL, propiedades.otro_desarrollo, desarrollos.nombre) as nombre_desarrollo'),
                DB::raw('colonias.nombre as nombre_colonia'),
                DB::raw('colonias.codigo_postal as cp_colonia'),
                DB::raw('estados.descripcion as estado'),
            );

        // dd($query);

        return $query;
    }


    //Vista donde se muestran los registros
    public function rowView(): string
    {
        return 'livewire.propiedad_table';
    }

    public function mostrarPropiedad($propiedad_id)
    {
        $this->mostrar_info = true;
        $this->propiedad = Propiedad::find($propiedad_id);
    }

    public function cambioEstatus($propiedad_id)
    {

        $propiedad = Propiedad::find($propiedad_id);


        DB::beginTransaction();

        try {
            if ($propiedad->estatus == 'Activo') {
                $propiedad->estatus = 'Descartado';
            } else {
                $propiedad->estatus = 'Activo';
            }

            $propiedad->save();

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            Log::error("Error al cambiar el estatus", ['message' => $e->getMessage()]);
        }
    }
}
