<?php

namespace App\Http\Livewire;

use Exception;
use App\Models\Questionnaire;
use App\Models\Propiedad;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\DataTableComponent;

class QuestionnaireTable extends DataTableComponent
{
    public $table_headers = 'livewire.header-devoluciones';

    public bool $showSearch = false;
    public bool $showPerPage = false;
    public $inicio = '';
    public $fin = '';
    public $mostrar_info = false;

    protected $listeners = [
        'inicio',
        'fin'
    ];

    public function mount()
    {
        $date = date('Y-m-d');
        $this->inicio = date('Y-m-01');
        $this->fin = date('Y-m-d');

        $hubspot = \SevenShores\Hubspot\Factory::createWithOAuth2Token('pat-na1-a4969485-3da3-40e1-8393-d86881127130');
        // dd($hubspot->owners()->all());
    }

    public function inicio($inicio)
    {
        $this->inicio = $inicio;
    }

    public function fin($fin)
    {
        $this->fin = $fin;
    }

    //Creacion de las columnas de la tabla
    public function columns(): array
    {
        return [
            Column::make('ID', 'id')
                ->sortable(),
            Column::make(__('traduction.registration_date'), 'created_at')
                ->sortable(),
            Column::make(__('traduction.name'), 'name')
                ->sortable()
                ->searchable(function (Builder $query, $searchTerm) {
                    $query->orWhere('questionnaire.name', 'LIKE', "%$searchTerm%");
                }),
            Column::make('WhatsApp', 'whatsapp')
                ->sortable()
                ->searchable(function (Builder $query, $searchTerm) {
                    $query->orWhere('questionnaire.whatsapp', 'LIKE', "%$searchTerm%");
                }),

            Column::make(__('traduction.dev_name'), 'nombre_desarrollo')
                ->sortable()
                ->searchable(function (Builder $query, $searchTerm) {
                    $query->orWhere('desarrollos.nombre', 'LIKE', "%$searchTerm%");
                }),

            Column::make(__('traduction.property_type'), 'tipo_propiedad')
                ->sortable()
                ->searchable(function (Builder $query, $searchTerm) {
                    $query->orWhere('propiedad_tipos.nombre', 'LIKE', "%$searchTerm%");
                }),


            Column::make('Dirección Google', 'google_direction')
                ->sortable()
                ->searchable(function (Builder $query, $searchTerm) {
                    $query->orWhere('questionnaire.google_direction', 'LIKE', "%$searchTerm%");
                }),

            Column::make('Oferta', 'offer_money')
                ->sortable(),
            // ->searchable(function (Builder $query, $searchTerm) {
            //     $query->orWhere('propiedades.propiedad_precio', '>=', $searchTerm);
            // }),

            Column::make(__('traduction.status'), 'status')
                ->sortable(),

            Column::make(__('traduction.see')),
            // Column::make(__('traduction.download_photos'))
        ];
    }


    //Query de los registros
    public function query(): Builder
    {
        $query = Questionnaire::query()
            ->leftJoin('desarrollos', 'desarrollos.id', '=', 'questionnaire.desarrollo_id')
            ->join('propiedad_tipos', 'propiedad_tipos.id', 'questionnaire.propiedad_tipo_id')
            // ->join('colonias', 'colonias.id', '=', 'questionnaire.colonia_id')
            // ->join('estados', 'estados.id', '=', 'questionnaire.estado_id')
            ->whereBetween('questionnaire.created_at', ["{$this->inicio} 00:00:00", "{$this->fin} 23:59:59"])
            ->select(
                'questionnaire.whatsapp',
                'questionnaire.id',
                'questionnaire.google_direction',
                'questionnaire.name',
                'questionnaire.created_at',
                // 'questionnaire.otro_desarrollo',
                'questionnaire.status',
                DB::raw('questionnaire.offer_money as offer_money'),
                DB::raw('propiedad_tipos.nombre as tipo_propiedad'),
                DB::raw('IF(desarrollos.nombre IS NULL, questionnaire.google_direction, (SELECT nombre FROM desarrollos WHERE id = questionnaire.desarrollo_id)) as nombre_desarrollo'),

                // DB::raw('colonias.nombre as nombre_colonia'),
                // DB::raw('colonias.codigo_postal as cp_colonia'),
                // DB::raw('estados.descripcion as estado'),
            );

        // dd($query);

        return $query;
    }


    //Vista donde se muestran los registros
    public function rowView(): string
    {
        return 'livewire.questionnaire_table';
    }

    public function mostrarPropiedad($propiedad_id)
    {
        $this->mostrar_info = true;
        $this->propiedad = Questionnaire::find($propiedad_id);
    }

    public function cambioEstatus($propiedad_id)
    {

        $propiedad = Questionnaire::find($propiedad_id);


        DB::beginTransaction();

        try {
            if ($propiedad->status == '1') {
                $propiedad->status = '0';
            } else {
                $propiedad->status = '1';
            }

            $propiedad->save();

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            Log::error("Error al cambiar el estatus", ['message' => $e->getMessage()]);
        }
    }
}
