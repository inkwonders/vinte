<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Property;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;


class DescripcionCasa extends Component
{

    public $id_prop;
    public $propiedad_datos;

    public $nombre, $telefono, $correo, $terminos;

    protected $rules = [

        'nombre'    =>   'required|min:6',

        'telefono'  =>   'required|numeric|min:10',

        'correo'    =>   'required|email',

        'terminos'  =>   'required',

    ];

    public function mount($id_prop)
    {

        $this->id_prop = $id_prop;

        // $this->propiedad_datos = Property::with('neighborhood','images')->where('slug', $id_prop)->sortByDesc('images.order','asc')->first();
        $this->propiedad_datos = Property::where('slug', $id_prop)
        ->with('neighborhood')
        ->with(['images' => function ($query) {
            return $query->orderBy('order','ASC');
        }])
        ->first();

    }

    public function submit()
    {

        $this->validate();

        $hubspot = \SevenShores\Hubspot\Factory::createWithOAuth2Token('pat-na1-429f062c-f3ab-49da-95a5-e57736e09173');
        // pat-na1-429f062c-f3ab-49da-95a5-e57736e09173 xante vende tu casa, este api key se usa para vende tu casa, ahi cae el lead
         // pat-na1-0a47eab6-274e-4f07-8bce-ab8d4d93652f   --> xante compra tu casa , este api key se usa para compra tu casa, ahi cae el lead

        Log::debug("PropiedadController@sendEmail conecion hubspot: ", ['conexion' => $hubspot]);
        // dd($hubspot);
        // dd($this->nombre);
        $contactos = array(

            array(
                'property' => 'email',
                'value' =>  $this->correo,
            ),
            array(
                'property' => 'firstname',
                'value' =>  $this->nombre,
            ),
            array(
                'property' => 'phone',
                'value' =>  $this->telefono,
            ),
            array(
                'value' =>  'Fase 2',
                'property'  => 'fase'
            ),
            // array(
            //     'property' => 'hubspot_owner_id',
            //      'value' =>  '196035747', //id de contacto (contacto@xante.mx)
            //     //  'value' =>  '251810094', //id de contacto (Ventas Xante (ventas@xante.mx))
            // )
            // array(
            //     'property' => 'account',
            //     //  'value' =>  '196035747', //id de contacto (contacto@xante.mx)
            //      'value' =>  '22369567', //id de contacto (vende@xante.com)
            // )

        );

        $hubspot->contacts()->createOrUpdate($this->correo, $contactos);

        // $to ="vende@xante.com";
        $to ="ventas@xante.mx";

        Mail::send('emails.contact_property_form', ['propiedad' => $this->id_prop, 'txt_nombre' => $this->nombre, 'txt_telefono' => $this->telefono, 'txt_correo' => $this->correo], function ($message) use ($to) {
            $message->from('contacto@xante.mx');
            $message->to($to)
                ->subject('Solicitud de información de propiedad en Xante.mx')
                ->cc('karla.ramirez@vinte.com');
                // >cc('contacto@xante.mx')
                // ->cc('natalia.vilchis@vinte.com')
                // ->bcc('eddy@inkwonders.com')
        });

        return redirect('thankyou');

    }


    public function render()
    {
        return view('livewire.descripcion-casa');
    }
}
