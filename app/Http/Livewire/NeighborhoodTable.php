<?php

namespace App\Http\Livewire;


use Exception;
use App\Models\User;
use App\Models\Propiedad;
use App\Models\Neighborhood;
use App\Models\Municipality;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\DataTableComponent;


use Illuminate\Support\Facades\Auth;


class NeighborhoodTable extends DataTableComponent
{

    public $table_headers = 'livewire.modal_new_neighborhood';

    public bool $showSearch = false;
    public bool $showPerPage = false;

    public $mostrar_info = false;
    public $editar_info = false;
    public $nuevo_usuario = false;
    public $redirect_to;

    public $new_municipality,$new_neighborhood,$new_cp,$new_type;
    public $edit_municipality,$edit_neighborhood,$edit_cp,$edit_type,$edit_id;
    public $municipalities=[];


    public $modal_confirm=false;
    public $modal_edit=false;


    protected $listeners = [
        'deleteNeighborhood'
    ];

    protected $rules = [
        'new_neighborhood' =>  'required|min:3',
        'new_municipality' =>  'required|exists:municipalities,id',
        'new_cp'           =>  'required|min:5',
        'new_type'         =>  'nullable'
    ];

    protected $messages = [
        'new_neighborhood.required'        =>  'El campo colonia es requerido',
        'new_neighborhood.min'             => 'El campo colonia debe tener al menos 3 caracteres',

        'new_municipality.required'        =>  'El campo municipio es requerido',
        'new_municipality.exists'          =>  'El campo municipio debe estar registrado en el sistema',

        'new_cp.required'        =>  'El campo cp es requerido',
        'new_cp.min'             => 'El campo cp debe tener al menos 5 caracteres',

    ];

      protected $rulesEditUser = [
        'edit_neighborhood' =>  'required|min:3',
        'edit_municipality' =>  'required|exists:municipalities,id',
        'edit_cp'           =>  'required|min:5',
        'edit_type'         =>  'nullable'
    ];

    protected $messagesEditUser = [
        'edit_neighborhood.required'        =>  'El campo colonia es requerido',
        'edit_neighborhood.min'             => 'El campo colonia debe tener al menos 3 caracteres',

        'edit_municipality.required'        =>  'El campo municipio es requerido',
        'edit_municipality.exists'          =>  'El campo municipio debe estar registrado en el sistema',

        'edit_cp.required'        =>  'El campo cp es requerido',
        'edit_cp.min'             => 'El campo cp debe tener al menos 5 caracteres',



    ];



    public function mount()
    {

        $this->municipalities=Municipality::whereNotNull('state_id')
                                          ->with('state')
                                          ->get()
                                          ->sortBy('state.description');

        // dd($this->municipalities);


    }


    //Creacion de las columnas de la tabla
    public function columns(): array
    {
        return [
            Column::make('ID', 'id')
                ->sortable(),
            Column::make(__('traduction.municipality'), 'description')
                ->sortable()
                ->searchable(function (Builder $query, $searchTerm) {
                    $query->orWhere('description', 'LIKE', "%$searchTerm%");
                }),
            Column::make(__('traduction.neighborhood'), 'neighborhood')
                ->sortable()
                ->searchable(function (Builder $query, $searchTerm) {
                    $query->orWhere('neighborhood', 'LIKE', "%$searchTerm%");
                }),
                Column::make(__('traduction.neighborhood_type')),
                Column::make(__('traduction.acciones')),
        ];
    }


    //Query de los registros
    public function query(): Builder
    {
        $query = Neighborhood::query()
        ->join('municipalities', 'municipalities.id', 'neighbourhoods.municipality_id')
        ->select(
            'neighbourhoods.id AS id',
            'municipalities.description AS description',
            'neighbourhoods.neighborhood AS neighborhood',
            'neighbourhoods.type AS neighborhood_type',

        )->orderBy('municipalities.description');

        return $query;
    }


    //Vista donde se muestran los registros
    public function rowView(): string
    {
        return 'livewire.neighborhood_table';
    }

    public function new_user(){

        $this->nuevo_usuario=true;

    }


    public function save(){


        Log::debug("NeighborhoodTable@save: crear nueva colonia", [ 'colonia'=> $this->new_neighborhood]);

        $this->validate();

        DB::beginTransaction();

        try{

           $colonia=new Neighborhood();
           $colonia->municipality_id=$this->new_municipality;
           $colonia->neighborhood=$this->new_neighborhood;
           $colonia->zip_code=$this->new_cp;
           $colonia->type=$this->new_type;
           $colonia->active=1;

           $colonia->save();

        //    $colonia=new Neighborhood();
        //    $colonia->setConnection('mysql_xante');
        //    $colonia->municipality_id=$this->new_municipality;
        //    $colonia->neighborhood=$this->new_neighborhood;
        //    $colonia->zip_code=$this->new_cp;
        //    $colonia->type=$this->new_type;
        //    $colonia->active=1;
        //    $colonia->save();

            $this->reset(['new_municipality','new_neighborhood','new_cp','new_type']);

            DB::commit();

            Log::debug("NeighborhoodTable@save: creado nueva colonia", $colonia->toArray());

            activity()
            ->performedOn($colonia)
            // ->causedBy(Auth::user()->id)
            ->log(json_encode(array(
                'message' => 'Se creó la colonia con id: '.$colonia->id.', '.$colonia->neighborhood,
                'data' => $colonia
            )));


            $this->redirect_to="neighborhoods";

            $this->emit('successAlert', [

                'title' => 'Colonia agregada correctamente',

                'icon' => 'success'

            ], $this->redirect_to);


        }catch(Exception $e){

            DB::rollBack();

            Log::error("NeighborhoodTable@save: ocurrió un error al crear la colonia: ". $e->getMessage());

            $this->emit('errorAlert', [
                'title' => 'Ocurrió un error',
                'text'  => 'No se pudo crear la colonia',
                'icon'  => 'error',

            ]);

        }
    }


    public function deleteConfirm(Neighborhood $neighborhood){

        //envia modal confirmacion
        $this->emit('confirmNeighborhood', $neighborhood);

    }

    public function deleteNeighborhood(Neighborhood $neighborhood){

        Log::debug("NeighborhoodTable@deleteNeighborhood: eliminar colonia", [ 'colonia'=> $neighborhood->id]);

        activity()
        ->performedOn($neighborhood)
        // ->causedBy(Auth::user()->id)
        ->log(json_encode(array(
            'message' => 'Se eliminó la colonia con id: '.$neighborhood->id.', '.$neighborhood->neighborhood,
            'data' => $neighborhood
        )));

        DB::beginTransaction();

        try{


            $neighborhood->delete();
            // dd($neighborhood);
            // $neighborhood->setConnection('mysql_xante')->where('id',$neighborhood->id)->delete();

            DB::commit();


            $this->redirect_to="neighborhoods";

            $this->emit('successAlert', [

                'title' => 'Colonia eliminada correctamente',

                'icon' => 'success'

            ], $this->redirect_to);


        }catch(Exception $e){

            DB::rollBack();

            Log::error("NeighborhoodTable@deleteNeighborhood: ocurrió un error al eliminar la colonia: ". $e->getMessage());

            $this->emit('errorAlert', [
                'title' => 'Ocurrió un error',
                'text'  => 'No se pudo crear la colonia',
                'icon'  => 'error',

            ]);

        }


    }

    public function modalEdit(Neighborhood $neighborhood){

        $this->modal_edit=true;

        try{

            $this->edit_id=$neighborhood->id;
            $this->edit_municipality=$neighborhood->municipality_id;
            $this->edit_neighborhood=$neighborhood->neighborhood;
            $this->edit_cp=$neighborhood->zip_code;
            $this->edit_type=$neighborhood->type;

        }catch(Exception $e){

            DB::rollBack();

            Log::error("NeighborhoodTable@modalEdit: ocurrió un error al buscar la colonia: ". $e->getMessage());

            $this->emit('errorAlert', [
                'title' => 'Ocurrió un error',
                'text'  => 'No se encontro la colonia',
                'icon'  => 'error',

            ]);

        }


    }

    public function edit(){


        Log::debug("NeighborhoodTable@edit: editar colonia", [ 'colonia'=> $this->edit_neighborhood]);

       $this->validate($this->rulesEditUser,$this->messagesEditUser);

        DB::beginTransaction();

        try{

            $id=$this->edit_id;
            $neighborhood=Neighborhood::find($id);

            if(!empty($this->edit_municipality)){   $neighborhood->municipality_id=$this->edit_municipality; }
            if(!empty($this->edit_neighborhood)){   $neighborhood->neighborhood=$this->edit_neighborhood; }
            if(!empty($this->edit_cp)){   $neighborhood->zip_code=$this->edit_cp; }
            if(!empty($this->edit_type)){   $neighborhood->type=$this->edit_type; }

            $neighborhood->save();

        //    $neighborhood=Neighborhood::find($id);

        //    $neighborhood->setConnection('mysql_xante');
        //     if(!empty($this->edit_municipality)){   $neighborhood->municipality_id=$this->edit_municipality; }
        //     if(!empty($this->edit_neighborhood)){   $neighborhood->neighborhood=$this->edit_neighborhood; }
        //     if(!empty($this->edit_cp)){   $neighborhood->zip_code=$this->edit_cp; }
        //     if(!empty($this->edit_type)){   $neighborhood->type=$this->edit_type; }

        //     $neighborhood->save();

            DB::commit();

            $this->reset(['edit_municipality','edit_neighborhood','edit_cp','edit_type']);


            Log::debug("NeighborhoodTable@edit: se edita la colonia", $neighborhood->toArray());

            activity()
            ->performedOn($neighborhood)
            // ->causedBy(Auth::user()->id)
            ->log(json_encode(array(
                'message' => 'Se editó la colonia con id: '.$neighborhood->id.', '.$neighborhood->neighborhood,
                'data' => $neighborhood
            )));

            $this->redirect_to="neighborhoods";

            $this->emit('successAlert', [

                'title' => 'Colonia editada correctamente',

                'icon' => 'success'

            ], $this->redirect_to);


        }catch(Exception $e){

            DB::rollBack();

            Log::error("NeighborhoodTable@edit: ocurrió un error al editar la colonia: ". $e->getMessage());

            $this->emit('errorAlert', [
                'title' => 'Ocurrió un error',
                'text'  => 'No se pudo crear la colonia',
                'icon'  => 'error',

            ]);

        }
    }


}
