<?php

namespace App\Http\Livewire;
use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Property;

class ComprarCasa extends Component
{
    use WithPagination;

    public $casasMovile;

    public function mount(){

        $this->casasMovile =  Property::all();

    }

    public function paginationView()
    {
        return 'custom-pagination-links-view';
    }

    public function render()
    {
        return view('livewire.comprar-casa',[
            // 'casas' => Property::where('status',"!=",0)->with('neighborhood','images')->orderBy('status', 'asc')->paginate(9),

        'casas' => Property::where('status',"!=",0)
        ->orderBy('status', 'asc')
        ->with('neighborhood')
        ->with(['images' => function ($query) {
            return $query->orderBy('order','ASC');
        }])
        ->paginate(9)
        ]);
    }
}
