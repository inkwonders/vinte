<?php

namespace App\Http\Livewire;


use Exception;
use App\Models\User;
use App\Models\Propiedad;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\DataTableComponent;


use Illuminate\Support\Facades\Auth;


class UsersTable extends DataTableComponent
{

    public $table_headers = 'livewire.modal_new_user';

    public bool $showSearch = false;
    public bool $showPerPage = false;
    public $inicio = '';
    public $fin = '';
    public $mostrar_info = false;
    public $editar_info = false;
    public $nuevo_usuario = false;
    public $redirect_to;
    public $new_name,$new_email,$new_role,$new_password,$new_password_confirmation;
    public $edit_id,$edit_name,$edit_email,$edit_role,$edit_password,$edit_password_confirmation;
    public $edit_role_user;
    public $modal_confirm=false;
    public $modal_edit=false;
    public $users_count;

    protected $listeners = [
        'inicio',
        'fin',
        'deleteUser'
    ];

    protected $rules = [
        'new_name'         =>  'required|min:3|regex:/^[a-zA-Z ]+$/',
        'new_email'        =>  'required|email|unique:users,email',
        'new_role'         =>  'required',
        'new_password'     =>  'required|min:8|confirmed'
    ];

    protected $messages = [
        'new_name.required'        =>  'El campo nombre es requerido',
        'new_name.min'             => 'El campo nombre debe tener al menos 3 caracteres',
        'new_name.regex'           => 'El campo nombre solamente puede almacenar  letras',

        'new_email.required'       =>  'El campo email es requerido',
        'new_email.email'          =>  'El campo email debe ser un correo electrónico válido',
        'new_email.unique'         => 'El email ya se encuentra registrado',

        'new_role.required'        => 'Debe seleccionar un tipo de usuario',

        'new_password.required'    => 'El campo contraseña es requerido',
        'new_password.min'         => 'El campo contraseña debe tener al menos 8 caracteres',
        'new_password.confirmed'   => 'Las contraseñas no coinciden',


    ];

      protected $rulesEditUser = [
        'edit_name'         =>  'required|min:3|regex:/^[a-zA-Z ]+$/',

        'edit_role'         =>  'sometimes',
        'edit_password'     =>  'nullable|min:8|confirmed'
    ];

    protected $messagesEditUser = [
        'edit_name.required'        =>  'El campo nombre es requerido',
        'edit_name.min'             => 'El campo nombre debe tener al menos 3 caracteres',
        'edit_name.regex'           => 'El campo nombre solamente puede almacenar  letras',

        // 'edit_role.sometimes'        => 'Debe seleccionar un tipo de usuario',

        // 'edit_password.sometimes'    => 'El campo contraseña es requerido',
        'edit_password.min'         => 'El campo contraseña debe tener al menos 8 caracteres',
        'edit_password.confirmed'   => 'Las contraseñas no coinciden',


    ];



    public function mount()
    {
        $date = date('Y-m-d');
        $this->inicio = date('Y-m-01');
        $this->fin = date('Y-m-d');

        $hubspot = \SevenShores\Hubspot\Factory::createWithOAuth2Token('pat-na1-a4969485-3da3-40e1-8393-d86881127130');
        // dd($hubspot->owners()->all());

        $this->users_count=User::all()->count();

    }

    public function inicio($inicio)
    {
        $this->inicio = $inicio;
    }

    public function fin($fin)
    {
        $this->fin = $fin;
    }

    //Creacion de las columnas de la tabla
    public function columns(): array
    {
        return [
            Column::make('ID', 'id')
                ->sortable(),
            Column::make(__('traduction.name'), 'name')
                ->sortable()
                ->searchable(function (Builder $query, $searchTerm) {
                    $query->orWhere('name', 'LIKE', "%$searchTerm%");
                }),
            Column::make(__('traduction.email'), 'email')
                ->sortable()
                ->searchable(function (Builder $query, $searchTerm) {
                    $query->orWhere('email', 'LIKE', "%$searchTerm%");
                }),
                Column::make(__('traduction.user_type')),
                 Column::make(__('traduction.registration_date'), 'created_at')
                ->sortable(),
                Column::make(__('traduction.acciones')),
        ];
    }


    //Query de los registros
    public function query(): Builder
    {
        $query = User::query()
            ->select(
                'id',
                'name',
                'email',
                'created_at'
            )->with('roles');

        //  dd($query);

        return $query;
    }


    //Vista donde se muestran los registros
    public function rowView(): string
    {
        return 'livewire.users_table';
    }

    public function new_user(){

        $this->nuevo_usuario=true;

    }


    public function save(){


        Log::debug("Userstable@save: crear nuevo usuario", [ 'usuario'=> $this->new_name]);

        $this->validate();

        DB::beginTransaction();

        try{

            $user=new User();
            $user->name=$this->new_name;
            $user->email=$this->new_email;
            $user->password=bcrypt($this->new_password);
            $user->save();

            $user->assignRole($this->new_role);

            // $user = new User();
            // $user->setConnection('mysql_xante');
            // $user->name=$this->new_name;
            // $user->email=$this->new_email;
            // $user->password=bcrypt($this->new_password);
            // $user->save();


            $this->reset(['new_name','new_email','new_role','new_password','new_password_confirmation']);

            DB::commit();

            Log::debug("Userstable@save: creado nuevo usuario", $user->toArray());

            activity()
            ->performedOn($user)
            // ->causedBy(Auth::user()->id)
            ->log(json_encode(array(
                'message' => 'Se creó el usuario con id: '.$user->id,
                'data' => $user
            )));


            $this->redirect_to="users";

            $this->emit('successAlert', [

                'title' => 'Usuario agregado correctamente',

                'icon' => 'success'

            ], $this->redirect_to);


        }catch(Exception $e){

            DB::rollBack();

            Log::error("Userstable@save: ocurrió un error al crear el usuario: ". $e->getMessage());

            $this->emit('errorAlert', [
                'title' => 'Ocurrió un error',
                'text'  => 'No se pudo crear el usuario',
                'icon'  => 'error',

            ]);

        }
    }


    public function deleteConfirm(User $user){

        //envia modal confirmacion
        $this->emit('confirmDeleteUser', $user);

    }

    public function deleteUser(User $user){

        Log::debug("Userstable@deleteUser: eliminar usuario", [ 'usuario'=> $user->id]);


        DB::beginTransaction();

        try{


            $user->delete();

            // $user = User::on('mysql_xante')->where('email',$user->email)->first();
            // $user->delete();

            DB::commit();

            activity()
            ->performedOn($user)
            // ->causedBy(Auth::user()->id)
            ->log(json_encode(array(
                'message' => 'Se eliminó el usuario con id: '.$user->id,
                'data' => $user
            )));


            $this->redirect_to="users";

            $this->emit('successAlert', [

                'title' => 'Usuario eliminado correctamente',

                'icon' => 'success'

            ], $this->redirect_to);


        }catch(Exception $e){

            DB::rollBack();

            Log::error("Userstable@deleteUser: ocurrió un error al eliminar usuario: ". $e->getMessage());

            $this->emit('errorAlert', [
                'title' => 'Ocurrió un error',
                'text'  => 'No se pudo crear el usuario',
                'icon'  => 'error',

            ]);

        }


    }

    public function modalEdit(User $user){

        $this->modal_edit=true;

        try{
            $this->edit_name=$user->name;
            $this->edit_email=$user->email;
            $this->edit_id=$user->id;
            $this->edit_role_user=$user->getRoleNames()->first();



        }catch(Exception $e){

            DB::rollBack();

            Log::error("Userstable@modalEdit: ocurrió un error al buscar el usuario: ". $e->getMessage());

            $this->emit('errorAlert', [
                'title' => 'Ocurrió un error',
                'text'  => 'No se encontro el usuario',
                'icon'  => 'error',

            ]);

        }


    }

    public function edit(){


        Log::debug("Userstable@save: editar usuario", [ 'usuario'=> $this->edit_name]);

       $this->validate($this->rulesEditUser,$this->messagesEditUser);

        DB::beginTransaction();

        try{

            $id=$this->edit_id;
            $user=User::find($id);

            if(!empty($this->edit_name)){   $user->name=$this->edit_name; }

            if(!empty($this->edit_password)){ $user->password=bcrypt($this->edit_password); }

            $user->save();


            DB::commit();

            if(!empty($this->edit_role)){ $user->syncRoles([$this->edit_role]);  }

            $this->reset(['edit_name','edit_email','edit_role','edit_password','edit_password_confirmation']);

            //  $user = User::on('mysql_xante')->where('id', $user->id)->first();
            // if(!empty($this->edit_name)){   $user->name=$this->edit_name; }
            // if(!empty($this->edit_password)){ $user->password=bcrypt($this->edit_password); }
            // $user->save();


            Log::debug("Userstable@save: se edita el usuario usuario", $user->toArray());

            activity()
            ->performedOn($user)
            // ->causedBy(Auth::user()->id)
            ->log(json_encode(array(
                'message' => 'Se editó el usuario con id: '.$user->id,
                'data' => $user
            )));

            $this->redirect_to="users";

            $this->emit('successAlert', [

                'title' => 'Usuario editado correctamente',

                'icon' => 'success'

            ], $this->redirect_to);


        }catch(Exception $e){

            DB::rollBack();

            Log::error("Userstable@save: ocurrió un error al editar el usuario: ". $e->getMessage());

            $this->emit('errorAlert', [
                'title' => 'Ocurrió un error',
                'text'  => 'No se pudo crear el usuario',
                'icon'  => 'error',

            ]);

        }
    }


}
