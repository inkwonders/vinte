<?php

namespace App\Http\Livewire;

use App\Models\Neighborhood;
use App\Models\Property;
use App\Models\Propiedad;
use App\Models\Multimedia;
use App\Models\Activity as registro_actividad;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\File;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Livewire\WithFileUploads;
use Spatie\Activitylog\Models\Activity;
use App\Models\User;
use Illuminate\Support\Facades\Auth;



use Illuminate\Support\Str;

use function PHPUnit\Framework\isNull;

class NewPropertiesTable extends DataTableComponent
{

    use WithFileUploads;

    public $table_headers = 'livewire.modal_new_properties';

    public bool $showSearch = false;
    public bool $showPerPage = false;
    public $inicio = '';
    public $fin = '';
    public $mostrar_info = false;
    public $modal_img_upload = false;
    public $modal_see_img = false;
    public $editar_info = false;
    public $propiedadEditar;
    public $btn_edita;
    public $redirect_to;
    public $propiedad;
    public $propiedadActualiza;
    public $nueva_propiedad;
    public $neigbhbourhood_busca;
    public $neighborhood;
    public $neigbhbourhood;
    public $busqueda;
    public $btn_guarda;
    public $propiedadNew;
    public $imagen_propiedad;
    public $btn_upload_nw_img;
    public $btn_guarda_img;
    public $propiedadNewImg;
    public $add_attention;
    public $propiedadSave;
    public $propiedad_img;
    public $order;


    public  $neigbhbourhood_id, $property_type_id, $xanteid, $year, $title, $title2, $description, $street, $no_ext, $no_int, $slug, $pet_friendly, $bathrooms, $location_description, $half_bathrooms, $bedrooms, $parking_slots, $m2, $m2_construction, $m2_garden, $floors, $lat, $lon, $number_rooms, $price, $map_url, $status, $neigbhbourhood_id_original, $outstanding, $city;

    // public  $neigbhbourhood_id_nva, $property_type_id_nva, $xanteid_nva, $year_nva, $title_nva, $title2_nva, $description_nva, $street_nva, $no_ext_nva, $no_int_nva, $slug_nva, $pet_friendly_nva, $bathrooms_nva, $location_description_nva, $half_bathrooms_nva, $bedrooms_nva, $parking_slots_nva, $m2_nva, $m2_construction_nva, $m2_garden_nva, $floors_nva, $lat_nva, $lon_nva, $number_rooms_nva, $price_nva, $map_url_nva, $status_nva;

    protected $listeners = [
        'inicio',
        'fin',
        'confirm_del_img',
        'deleteProperty'
    ];

    protected $rules = [

        'neigbhbourhood_id' => 'required',
        'property_type_id' => 'required',
        'xanteid' => 'required',
        'title' => 'required',
        'title2' => 'required',
        'description' => 'required',
        'street' => 'required',
        'no_ext' => 'required',
        'bathrooms' => 'required|numeric',
        'location_description' => 'required',
        'half_bathrooms' => 'required|numeric',
        'bedrooms' => 'required|numeric',
        'parking_slots' => 'required|numeric',
        'm2' => 'required|numeric',
        'm2_construction' => 'required|numeric',
        // 'm2_garden' => 'required|numeric',
        'floors' => 'required|numeric',

        'price' => 'required|numeric',
        'map_url' => 'required',
        'city' => 'required',


    ];

    protected $messages = [
        'neigbhbourhood_id.required' => 'Es necesario seleccionar una dirección',
        'property_type_id.required' => 'Es necesario seleccionar un tipo de propiedad',
        'xanteid.required' => 'Es necesario ingresar un código de Xante (XA-xxxx)',
        'title.required' => 'Es necesario ingresar un título',
        'title2.required' => 'Es necesario ingresar un subtitulo',
        'description.required' => 'Es necesario ingresar una descripción',
        'street.required' => 'Es necesario ingresar el nombre de la calle',
        'no_ext.required' => 'Es necesario ingresar el número exterior',
        'bathrooms.required' => 'Es necesario ingresar el número de baños',
        'bathrooms.numeric' => 'El número de baños debe ser solo números',
        'location_description.required' => 'Es necesario ingresar el código de google maps',
        'half_bathrooms.required' => 'Es necesario ingresar el número de medios baños',
        'half_bathrooms.numeric' => 'El número de medios baños debe ser solo números',
        'bedrooms.required' => 'Es necesario ingresar el número de dormitorios',
        'bedrooms.numeric' => 'El número de dormitorios debe ser solo números',
        'parking_slots.required' => 'Es necesario ingresar el número de estacionamientos',
        'parking_slots.numeric' => 'El número de estacionamientos debe ser solo números',
        'm2.required' => 'Es necesario ingresar los metros cuadrados de la propiedad',
        'm2.numeric' => 'Los metros cuadrados deben ser solo números',
        'm2_construction.required' => 'Es necesario ingresar los metros cuadrados de construcción de la propiedad',
        'm2_construction.numeric' => 'Los metros cuadrados de construcción deben ser solo números',
        // 'm2_garden.required' => 'Es necesario ingresar los metros cuadrados de jardín',
        // 'm2_garden.numeric' => 'Los metros cuadrados de jardín deben ser solo números',
        'floors.required' => 'Es necesario ingresar el número de pisos de la propiedad',
        'floors.numeric' => 'El número de pisos deben ser solo números',

        'price.required' => 'Es necesario ingresar el precio de la propiedad',
        'price.numeric' => 'El precio debe ser solo números',
        'map_url.required' => 'Es necesario ingresar el código corto de google maps',
        'city.required' => 'Es necesario ingresar la ciudad',

    ];

    public function mount()
    {
        $date = date('Y-m-d');
        $this->inicio = date('Y-m-01');
        $this->fin = date('Y-m-d');

        $hubspot = \SevenShores\Hubspot\Factory::createWithOAuth2Token('pat-na1-a4969485-3da3-40e1-8393-d86881127130');

        $this->propiedadSave = new Property;
    }

    public function inicio($inicio)
    {
        $this->inicio = $inicio;
    }

    // public function updatingNeigbhbourhood_id()
    // {
    //     $this->refreshPage();
    // }

    public function fin($fin)
    {
        $this->fin = $fin;
    }

    //Creacion de las columnas de la tabla
    public function columns(): array
    {
        return [
            Column::make('ID', 'id')
                ->sortable(),
            Column::make('Fecha de creación', 'created_at')
                ->sortable(),
            Column::make('Xante ID', 'xanteid')
                ->sortable(),
            Column::make('Dirección', 'neighborhood')
                ->sortable()
                ->searchable(function (Builder $query, $searchTerm) {
                    $query->orWhere('neighbourhoods.neighborhood', 'LIKE', "%$searchTerm%");
                }),
            Column::make('Tipo de propiedad', 'property_type')
                ->sortable()
                ->searchable(function (Builder $query, $searchTerm) {
                    $query->orWhere('property_types.name', 'LIKE', "%$searchTerm%");
                }),

            Column::make('Calle', 'street')
                ->sortable()
                ->searchable(function (Builder $query, $searchTerm) {
                    $query->orWhere('street', 'LIKE', "%$searchTerm%");
                }),

            Column::make('No exterior', 'no_ext')
                ->sortable()
                ->searchable(function (Builder $query, $searchTerm) {
                    $query->orWhere('no_ext', 'LIKE', "%$searchTerm%");
                }),



            Column::make(__('traduction.status'), 'estatus')
                ->sortable(),

            Column::make('Ver detalles'),
            Column::make('Imágenes'),
            Column::make(__('traduction.acciones')),
        ];
    }


    //Query de los registros
    public function query(): Builder
    {
        $query = Property::query()->where('properties.id','<>',1) // se quita la propiedad 1 ya que esta propiedad es el post solito que va al final de los post
            ->leftJoin('neighbourhoods', 'neighbourhoods.id', '=', 'properties.neigbhbourhood_id')
            ->join('property_types', 'property_types.id', 'properties.property_type_id')
            ->join('municipalities', 'municipalities.id', 'neighbourhoods.municipality_id')
            ->join('states', 'states.id', 'municipalities.state_id')
            // ->whereBetween('properties.created_at', ["{$this->inicio} 00:00:00", "{$this->fin} 23:59:59"])
            ->select(
                'properties.xanteid',
                'properties.id',
                'properties.street',
                'properties.no_ext',
                'properties.status',
                'properties.created_at',
                DB::raw('neighbourhoods.neighborhood as neighbourhood'),
                DB::raw('neighbourhoods.type as type'),
                DB::raw('neighbourhoods.zip_code as zip_code'),
                DB::raw('property_types.name as property_type'),
                DB::raw('municipalities.description as municipality'),
                DB::raw('states.description as state'),

            )->orderBy('id', 'DESC');

        // dd($query);

        return $query;
    }


    //Vista donde se muestran los registros
    public function rowView(): string
    {
        return 'livewire.new_properties_table';
    }

    public function buscar()
    {


        $this->busqueda = Neighborhood::whereRaw("neighborhood like '%" . $this->neigbhbourhood_busca . "%' ")
            ->with('municipality.state')
            ->get()->take(5);

    }
    public function asignar($n)
    {

        $propiedad_busca = Property::find($n);


        $this->neigbhbourhood_busca = $propiedad_busca->neighborhood->neighborhood . ' ' . $propiedad_busca->neighborhood->zip_code;


        $this->busqueda = false;
    }


    public function mostrarPropiedad($propiedad_id)
    {
        $this->mostrar_info = true;
        $this->propiedad = Property::with('neighborhood')->find($propiedad_id);
    }
    public function modalImg($propiedad_id)
    {
        $this->modal_img_upload = true;
        $this->propiedad_img = Property::with('neighborhood')->find($propiedad_id);
        $this->imagen_propiedad = null;
    }

    public function modalSeeImg($propiedad_id)
    {
        $this->modal_see_img = true;
        $this->propiedad_img = Property::with('neighborhood')->find($propiedad_id);
        $this->imagen_propiedad = null;
    }

    public function nuevaPropiedad()
    {
        $this->nueva_propiedad = true;
    }
    public function closeNuevaPropiedad()
    {
        $this->nueva_propiedad = false;
        $this->reset();
        $this->redirect('/upload_properties');
    }

    public function closeVisualizarPropiedad()
    {
        $this->mostrar_info = false;
        $this->reset();
        $this->redirect('/upload_properties');
    }

    public function closeEditarPropiedad()
    {
        $this->editar_info = false;
        $this->reset();
        $this->redirect('/upload_properties');
    }

    public function dehydrate() {
        $this->emit('initializeCkEditor');
   }


    public function deleteConfirm(Property $propiedad_id){

        //envia modal confirmacion
        // dd($propiedad_id);
        $this->emit('confirmDeleteProperty', $propiedad_id);

    }

    public function deleteProperty(Property $propiedad_id){
        // dd($propiedad_id);
        Log::debug("Propertytable@deleteProperty: eliminar propiedad", [ 'propiedad_id'=> $propiedad_id->id]);


        DB::beginTransaction();

        try{


            $propiedad_id->delete();

            // $propiedad_id->setConnection('mysql_xante')->where('xanteid',$propiedad_id->xanteid)->delete();

            DB::commit();

            activity()
            ->performedOn($propiedad_id)
            // ->causedBy(Auth::user()->id)
            ->log(json_encode(array(
                'message' => 'Se eliminó la propiedad con Xante Id: '.$propiedad_id->xanteid,
                'data' => $propiedad_id
            )));


            $this->redirect_to="upload_properties";

            $this->emit('successAlert', [

                'title' => 'Propiedad eliminada correctamente',

                'icon' => 'success'

            ], $this->redirect_to);


        }catch(Exception $e){

            DB::rollBack();

            Log::error("NewPropertyTable@deleteUser: ocurrió un error al eliminar la propiedad: ". $e->getMessage());

            $this->emit('errorAlert', [
                'title' => 'Ocurrió un error',
                'text'  => 'No se pudo eliminar la propiedad',
                'icon'  => 'error',

            ]);

        }


    }


    public function editarPropiedad($propiedad_id)
    {
        $this->mostrar_info = false;
        $this->editar_info = true;
        $this->propiedad = Property::find($propiedad_id);


        $this->property_type_id = $this->propiedad->property_type_id;
        $this->xanteid = $this->propiedad->xanteid;
        $this->year = $this->propiedad->year;
        $this->title = $this->propiedad->title;
        $this->title2 = $this->propiedad->title2;
        $this->description = $this->propiedad->description;
        $this->street = $this->propiedad->street;
        $this->no_ext = $this->propiedad->no_ext;
        $this->no_int = $this->propiedad->no_int;
        $this->slug = $this->propiedad->slug;
        $this->pet_friendly = $this->propiedad->pet_friendly;
        $this->bathrooms = $this->propiedad->bathrooms;
        $this->location_description = $this->propiedad->location_description;
        $this->half_bathrooms = $this->propiedad->half_bathrooms;
        $this->bedrooms = $this->propiedad->bedrooms;
        $this->parking_slots = $this->propiedad->parking_slots;
        $this->m2 = $this->propiedad->m2;
        $this->m2_construction = $this->propiedad->m2_construction;
        $this->m2_garden = $this->propiedad->m2_garden;
        $this->floors = $this->propiedad->floors;

        $this->price = $this->propiedad->price;
        $this->map_url = $this->propiedad->map_url;
        $this->status = $this->propiedad->status;
        $this->outstanding = $this->propiedad->outstanding;
        $this->add_attention = $this->propiedad->atention_hours;

        $this->city = $this->propiedad->city;
    }

    public function cambioEstatus($propiedad_id)
    {

        $propiedad = Propiedad::find($propiedad_id);


        DB::beginTransaction();

        try {
            if ($propiedad->estatus == 'Activo') {
                $propiedad->estatus = 'Descartado';
            } else {
                $propiedad->estatus = 'Activo';
            }

            $propiedad->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error("Error al cambiar el estatus", ['message' => $e->getMessage()]);
        }
    }

    public function edit()
    {

        $search  = array('width="600" height="450"');
        $replace = array('class="w-full h-full rounded-xl"');
        $subject = $this->location_description;
        $remplace_mapframe = str_replace($search, $replace, $subject);

        $this->propiedadSave = Property::where('id', $this->propiedad->id)->with('neighborhood')->first();


        if (empty($this->neigbhbourhood_id)) {
            $this->neigbhbourhood_id = $this->propiedadSave->neighborhood->id;
        }

        $this->propiedadSave->neigbhbourhood_id = $this->neigbhbourhood_id;
        $this->propiedadSave->property_type_id = $this->property_type_id;

        $this->propiedadSave->xanteid = $this->xanteid;
        $this->propiedadSave->title = $this->title;
        $this->propiedadSave->title2 = $this->title2;
        $this->propiedadSave->description = $this->description;
        $this->propiedadSave->street = $this->street;
        $this->propiedadSave->no_ext = $this->no_ext;
        $this->propiedadSave->no_int = $this->no_int;
        $this->propiedadSave->bathrooms = $this->bathrooms;
        $this->propiedadSave->location_description = $remplace_mapframe ;
        $this->propiedadSave->half_bathrooms = $this->half_bathrooms;
        $this->propiedadSave->bedrooms = $this->bedrooms;
        $this->propiedadSave->parking_slots = $this->parking_slots;
        $this->propiedadSave->m2 = $this->m2;
        $this->propiedadSave->m2_construction = $this->m2_construction;
        $this->propiedadSave->m2_garden = $this->m2_garden;
        $this->propiedadSave->floors = $this->floors;
        $this->propiedadSave->price = $this->price;
        $this->propiedadSave->map_url = $this->map_url;
        $this->propiedadSave->status = $this->status;
        $this->propiedadSave->outstanding = $this->outstanding;
        $this->propiedadSave->atention_hours = $this->add_attention;
        $this->propiedadSave->city = $this->city;


        $this->propiedadSave->save();


        //creamos el slug
            $neighborhood_search = Neighborhood::where('id',$this->neigbhbourhood_id)->first();
            $colonia = Str::slug($neighborhood_search->neighborhood , "_");
            $estado = Str::slug($neighborhood_search->municipality->state->description , "_");
            $slug = $colonia.'_'.$this->no_ext.'_'.$estado.'_'.$this->xanteid;
            $this->propiedadSave->slug = $slug;
            $this->propiedadSave->save();

            $accion = [
                'action' => 'Propiedad Editada',
                'titulo' => 'Se ha editado una propiedad con éxito',
                'propiedades' =>  $this->propiedadSave,
            ];

            // $user_admin = User::find(1);
            // $user_admin->SendAdminUserActionsNotification($accion);


            activity()
            ->performedOn($this->propiedadSave)
            ->log(json_encode(array(
                'message' => 'Editado la propiedad con id: ' . $this->propiedadSave->id,
                'data' => $this->propiedadSave->id
            )));
        //editamos server xante


        // $this->propiedadSave = Property::on('mysql_xante')->where('xanteid', $this->propiedad->xanteid)->with('neighborhood')->first();

        // $this->propiedadSave->neigbhbourhood_id = $this->neigbhbourhood_id;
        // $this->propiedadSave->property_type_id = $this->property_type_id;

        // $this->propiedadSave->xanteid = $this->xanteid;
        // $this->propiedadSave->title = $this->title;
        // $this->propiedadSave->title2 = $this->title2;
        // $this->propiedadSave->description = $this->description;
        // $this->propiedadSave->street = $this->street;
        // $this->propiedadSave->no_ext = $this->no_ext;
        // $this->propiedadSave->no_int = $this->no_int;
        // $this->propiedadSave->slug = $slug;
        // $this->propiedadSave->bathrooms = $this->bathrooms;
        // $this->propiedadSave->location_description = $remplace_mapframe;
        // $this->propiedadSave->half_bathrooms = $this->half_bathrooms;
        // $this->propiedadSave->bedrooms = $this->bedrooms;
        // $this->propiedadSave->parking_slots = $this->parking_slots;
        // $this->propiedadSave->m2 = $this->m2;
        // $this->propiedadSave->m2_construction = $this->m2_construction;
        // $this->propiedadSave->m2_garden = $this->m2_garden;
        // $this->propiedadSave->floors = $this->floors;
        // // $this->propiedadSave->lat = $this->lat;
        // // $this->propiedadSave->lon = $this->lon;
        // // $this->propiedadSave->number_rooms = $this->number_rooms;
        // $this->propiedadSave->price = $this->price;
        // $this->propiedadSave->map_url = $this->map_url;
        // $this->propiedadSave->status = $this->status;
        // $this->propiedadSave->outstanding = $this->outstanding;
        // $this->propiedadSave->atention_hours = $this->add_attention;


        // $this->propiedadSave->save();

        Log::debug("NewPropertiesTable@edit: editando propiedad", $this->propiedadSave->toArray());





        $this->emit('successAlert', [
            'title' => 'Se editó la propiedad',
            'icon' => 'success'
        ], redirect('/upload_properties'));
    }
    public function save()
    {


        $this->validate();

        $search  = array('width="600" height="450"');
        $replace = array('class="w-full h-full rounded-xl"');
        $subject = $this->location_description;
        $remplace_mapframe = str_replace($search, $replace, $subject);

        DB::beginTransaction();
        try {

            $this->propiedadNew = new Property();
            $this->propiedadNew->property_type_id = $this->property_type_id;
            $this->propiedadNew->neigbhbourhood_id = $this->neigbhbourhood_id;
            $this->propiedadNew->xanteid = $this->xanteid;
            $this->propiedadNew->title = $this->title;
            $this->propiedadNew->title2 = $this->title2;
            $this->propiedadNew->description = $this->description;
            $this->propiedadNew->street = $this->street;
            $this->propiedadNew->no_ext = $this->no_ext;
            $this->propiedadNew->no_int = $this->no_int;

            $this->propiedadNew->bathrooms = $this->bathrooms;
            $this->propiedadNew->location_description = $remplace_mapframe;
            $this->propiedadNew->half_bathrooms = $this->half_bathrooms;
            $this->propiedadNew->bedrooms = $this->bedrooms;
            $this->propiedadNew->parking_slots = $this->parking_slots;
            $this->propiedadNew->m2 = $this->m2;
            $this->propiedadNew->m2_construction = $this->m2_construction;
            $this->propiedadNew->m2_garden = $this->m2_garden;
            $this->propiedadNew->floors = $this->floors;
            $this->propiedadNew->price = $this->price;
            $this->propiedadNew->map_url = $this->map_url;
            $this->propiedadNew->status = 0;
            $this->propiedadNew->outstanding = 0;
            $this->propiedadNew->city = $this->city;
            $this->propiedadNew->save();

            $colonia = Str::slug($this->propiedadNew->neighborhood->neighborhood , "_");
            $estado = Str::slug($this->propiedadNew->neighborhood->municipality->state->description , "_");
            $slug = $colonia.'_'.$this->no_ext.'_'.$estado.'_'.$this->xanteid;
            $this->propiedadNew->slug = $slug;
            $this->propiedadNew->save();

            $accion = [
                'action' => 'Propiedad Creada',
                'titulo' => 'Se ha creado una nueva propiedad con éxito',
                'propiedades' =>  $this->propiedadNew,
            ];

            // $user_admin = User::find(1);
            // $user_admin->SendAdminUserActionsNotification($accion);

            activity()
            ->performedOn($this->propiedadNew)
            ->log(json_encode(array(
                'message' => 'Se dio de alta la propiedad con id: ' . $this->propiedadNew->id,
                'data' => $this->propiedadNew->id
            )));

            //se agrega en server xante

            // $this->propiedadNew = new Property();
            // $this->propiedadNew->setConnection('mysql_xante');
            // $this->propiedadNew->property_type_id = $this->property_type_id;
            // $this->propiedadNew->neigbhbourhood_id = $this->neigbhbourhood_id;
            // $this->propiedadNew->xanteid = $this->xanteid;
            // $this->propiedadNew->title = $this->title;
            // $this->propiedadNew->title2 = $this->title2;
            // $this->propiedadNew->description = $this->description;
            // $this->propiedadNew->street = $this->street;
            // $this->propiedadNew->no_ext = $this->no_ext;
            // $this->propiedadNew->no_int = $this->no_int;
            // $this->propiedadNew->slug = $slug;
            // $this->propiedadNew->bathrooms = $this->bathrooms;
            // $this->propiedadNew->location_description = $this->location_description;
            // $this->propiedadNew->half_bathrooms = $this->half_bathrooms;
            // $this->propiedadNew->bedrooms = $this->bedrooms;
            // $this->propiedadNew->parking_slots = $this->parking_slots;
            // $this->propiedadNew->m2 = $this->m2;
            // $this->propiedadNew->m2_construction = $this->m2_construction;
            // $this->propiedadNew->m2_garden = $this->m2_garden;
            // $this->propiedadNew->floors = $this->floors;

            // $this->propiedadNew->price = $this->price;
            // $this->propiedadNew->map_url = $this->map_url;
            // $this->propiedadNew->status = 0;
            // $this->propiedadNew->outstanding = 0;

            // $this->propiedadNew->save();

        DB::commit();
                } catch (Exception $e) {
                        DB::rollBack();
                        Log::error("Error al subir la nueva propiedad", ['message' => $e->getMessage()]);
                    }



        $this->emit('successAlert', [
            'title' => 'Se guardó la propiedad',
            'icon' => 'success'
        ], redirect('/upload_properties'));
    }

    public function cambioOrden($valor, $id){
// dd($id, $valor);


        // $niceNames = [
        //     'order.required' => 'El orden es requerido',
        // ];
        // $fields_validation = [
        //     'order.*' => 'required',
        // ];
        // $this->validate($fields_validation, null, $niceNames);

        $cambio_orden = Multimedia::where('id', $id)->first();
        $cambio_orden->order = $valor;
        $cambio_orden->save();


        $this->emit('notifAlert', [
            'title' => 'Se guardo el orden',
            'icon' => 'success',
            // 'text' => 'Cambio'
        ]);
    }


    public function save_img()
    {
        // entra la validacion de las 6 img solo cuando el modal de uplodad img está abierto
        if($this->modal_img_upload == true){
        $validatedData = $this->validate([
            'imagen_propiedad' => 'min:6|max:15',
            'imagen_propiedad.*' => 'mimes:jpg,jpeg,bmp,png',
        ]);
        }

        $this->propiedadNewImg = Property::where('id', $this->propiedad_img->id)->with('neighborhood')->first();

        $i = $this->propiedadNewImg->images->count() + 1;

        if( $this->order == null ||  $this->order ==""){
            $orden = $i;
        }
        else{
            $orden = $this->order;
        }
        // dd($orden);
        // $i = $this->propiedadNewImg->images->count() + 1;


        foreach ($this->imagen_propiedad as $images) {

            $nombre = strtolower(str_replace(' ', '_', $this->propiedadNewImg->neigbhbourhood_id));
            $subfamilia = strtolower(str_replace(' ', '_', $this->propiedadNewImg->property_type_id));

            $nombre_imagen = $this->propiedadNewImg->id . '-' . $i .'_'.date('Y-m-d_s_i'). '.' . $images->guessExtension();

            $path = $images->storeAs('properties_pics', $nombre_imagen);

            $imagen = new Multimedia;
            $imagen->name = $nombre_imagen;
            $imagen->mime = $images->getMimeType();
            $imagen->size = "M";
            $imagen->compresed = 0;
            $imagen->order = $orden;
            $imagen->alt = $nombre_imagen;
            $imagen->url = $path;
            $imagen->is_mobile = 0;

            $this->propiedadNewImg->images()->save($imagen);

            $i++;
        }

        activity()
        ->performedOn($this->propiedadNewImg)
        // ->causedBy(Auth::user()->id)
        ->log(json_encode(array(
            'message' => 'Se dieron de alta imágenes para la propiedad: '.$this->propiedadNewImg->id,
            'data' => $this->propiedadNewImg->images->count()
        )));

        $this->emit('successAlert', [
            'title' => 'Se guardaron las imágenes',
            'icon' => 'success'
        ], redirect('/upload_properties'));
    }

    public function delete_img(Multimedia $img){
        $this->modal_see_img = false;

        $this->emit('confirmDelete',$img,'confirm_del_img','¿Eliminar imagen? '.$img->name);

    }

    public function confirm_del_img(Multimedia $img){

        DB::beginTransaction();

        try {

            $propiedad = $img->media_id;

            File::delete($img->url);
            $img->delete();

            // $imagen = Multimedia::on('mysql_xante')->where('name',$img->name)->first();
            // $imagen->delete();

            DB::commit();

            activity()
            ->performedOn($img)
            // ->causedBy(Auth::user()->id)
            ->log(json_encode(array(
                'message' => 'Se eliminó la imágen: ' . $img->id,
                'data' => $img
            )));


            $this->emit('errorAlert', [
                'title' => 'Imagen eliminada',
                'icon' => 'success'
            ]);

            $this->modalSeeImg($propiedad);

        } catch (Exception $e) {

            DB::rollBack();

            $this->emit('errorAlert', [
                'title' => 'Ocurrió un error',
                'icon' => 'error',
                'text' => $e->getMessage()
            ]);

        }
    }
}
