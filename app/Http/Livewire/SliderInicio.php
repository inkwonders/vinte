<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Property;

class SliderInicio extends Component
{
    public function render()
    {
        return view('livewire.slider-inicio',[
            'casas' => Property::where('status',"=",1)->with('neighborhood','images')->orderBy('created_at', 'asc')->get(),
        ]);
    }
}
