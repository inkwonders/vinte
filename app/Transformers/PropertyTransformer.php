<?php

namespace App\Transformers;


use App\Models\Property;
use League\Fractal\TransformerAbstract;
use Carbon\Carbon;

class PropertyTransformer extends TransformerAbstract
{


     /**
     * List of resources possible to include
     *
     * @var array
     */


    public function transform(Property $property)
    {

        return [
            'id' => $property->id,
            'neigbhbourhood'  => $property->neighborhood->neighborhood,
            'zip_code' => $property->neighborhood->zip_code,
            'neigbhbourhood_type' => $property->neighborhood->type,
            'property_type'  => $property->property_type->name,
            'xanteid'  => $property->xanteid,
            'year'  => $property->year,
            'title'  => $property->title,
            'title2'  => $property->title2,
            'description'  => $property->description,
            'street'  => $property->street,
            'no_ext'  => $property->no_ext,
            'no_int'  => $property->no_int,
            'slug'  => $property->slug,
            'pet_friendly'  => $property->pet_friendly,
            'bathrooms'  => $property->bathrooms,
            'location_description'  => $property->location_description,
            'half_bathrooms'  => $property->half_bathrooms,
            'bedrooms'  => $property->bedrooms,
            'parking_slots'  => $property->parking_slots,
            'm2'  => $property->m2,
            'm2_construction'  => $property->m2_construction,
            'm2_garden'  => $property->m2_garden,
            'floors'  => $property->floors,
            'lat'  => $property->lat,
            'lon'  => $property->lon,
            // 'number_rooms'  => $property->number_rooms,
            'price'  => $property->price,
            'map_url'  => $property->map_url,
            'status'  => $property->status,



        ];
    }


}
