<?php
namespace App\Transformers;

use App\Models\Equipo;
use League\Fractal;

class EquipoTransformer extends Fractal\TransformerAbstract
{

	public function transform(Equipo $equipo)
	{
	    return [
	        'id'      => (int) $equipo->id,
	        'nombre'   => $equipo->nombre,
	    ];
	}

}
