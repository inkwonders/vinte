<?php
namespace App\Transformers;

use App\Models\Estado;
use League\Fractal;

class EstadoTransformer extends Fractal\TransformerAbstract
{

	public function transform(Estado $estado)
	{
	    return [
	        'id'      => (int) $estado->id,
	        'nombre'   => $estado->descripcion,
	    ];
	}

}
