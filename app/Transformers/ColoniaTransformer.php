<?php
namespace App\Transformers;

use App\Models\Colonia;
use League\Fractal;

class ColoniaTransformer extends Fractal\TransformerAbstract
{

	public function transform(Colonia $colonia)
	{
	    return [
	        'id'      => (int) $colonia->id,
	        'nombre'   => $colonia->nombre,
            'codigo_postal' => $colonia->codigo_postal,
            'tipo' => $colonia->tipo,
	    ];
	}

}
