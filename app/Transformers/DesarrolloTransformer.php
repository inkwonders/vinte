<?php
namespace App\Transformers;

use App\Models\Desarrollo;
use League\Fractal;

class DesarrolloTransformer extends Fractal\TransformerAbstract
{

	public function transform(Desarrollo $desarrollo)
	{
	    return [
	        'id'      => (int) $desarrollo->id,
	        'nombre'   => $desarrollo->nombre,
	    ];
	}

}
