<?php
namespace App\Transformers;

use App\Http\Controllers\API\DesarrolloAPIController;
use App\Models\Propiedad;
use League\Fractal;

class PropiedadTransformer extends Fractal\TransformerAbstract
{
        /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [

        'equipos'

    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [



    ];

    public function transform(Propiedad $propiedad){

        $numero_tel = sprintf("%s-%s-%s",
        substr($propiedad->telefono, 0, 3),
        substr($propiedad->telefono, 3, 3),
        substr($propiedad->telefono, 6));

        if($propiedad->propiedad_tipo->nombre == 'Casa'){
            $tipo = [
                'tipo' => $propiedad->propiedad_tipo->nombre,
                'num_niveles' => $propiedad->num_niveles_casa,
                'terraza_jardin' => $propiedad->terraza_jardin,
            ];
        }else{
            $tipo = [
                'tipo' => $propiedad->propiedad_tipo->nombre,
                'piso_departamento' => $propiedad->piso_departamento
            ];
        }

        if(empty($propiedad->desarrollo->nombre)){
            $desarrollo = $propiedad->otro_desarrollo;
        }else{
            $desarrollo = $propiedad->desarrollo->nombre;
        }

        return [
	        'id'      => (int) $propiedad->id,
            'fecha_registro' => date('d/m/Y', strtotime($propiedad->created_at)),
            'propietario' => $propiedad->nombre_propietario,
            'telefono' => $numero_tel,
            'correo' => $propiedad->correo,
            'desarrollo' => $desarrollo,
            'propiedad_tipo' => $tipo,
            'calle' => $propiedad->direccion,
            'num_exterior' => $propiedad->num_exterior,
            'colonia' => $propiedad->colonia->nombre,
            'codigo_postal' => $propiedad->colonia->codigo_postal,
            'estado' => $propiedad->estado->descripcion,
            'municipio' => $propiedad->colonia->municipio->descripcion,
            'precio' => '$ '.number_format($propiedad->propiedad_precio, 2, '.', ','),
            'superficie' => $propiedad->superficie_habitable,
            'recamaras' => $propiedad->num_recamaras,
            'banios' => $propiedad->num_bath,
            'med_banios' => $propiedad->num_med_bath,
            'estacionamientos' => $propiedad->num_estacionamientos,
            'antiguedad' => $propiedad->antiguedad,
            'hipoteca' => $propiedad->hipoteca,
            'hipoteca_tipo' => $propiedad->hipoteca_tipo,
            'motivo_venta' => $propiedad->motivo,
            'motivo_complemento' => $propiedad->motivo_comp,
            'equipado' => $propiedad->equipado,
            'imagenes' => $propiedad->images
	    ];
    }


    public function includeEquipos(Propiedad $propiedad)
    {
        return $this->collection($propiedad->equipos, new EquipoTransformer);
    }
}
