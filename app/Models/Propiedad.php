<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Propiedad extends Model
{
    use HasFactory;

    protected $table = 'propiedades';

    protected $fillable = [
        "estado_id",
        "desarrollo_id",
        "propiedad_tipo_id",
        "colonia_id",
        "direccion",
        "num_exterior",
        "superficie_habitable",
        "equipado",
        "piso_departamento",
        "num_niveles_casa",
        "terraza_jardin",
        "num_recamaras",
        "num_bath",
        "num_med_bath",
        "num_estacionamientos",
        "antiguedad",
        "propiedad_precio",
        "hipoteca",
        "hipoteca_tipo",
        "telefono",
        "correo",
        "nombre_propietario",
        "motivo",
        "motivo_comp",
    ];

    public function desarrollo(){
        return $this->belongsTo(Desarrollo::class);
    }

    public function estado(){
        return $this->belongsTo(Estado::class);
    }

    public function colonia(){
        return $this->belongsTo(Colonia::class);
    }

    public function propiedad_tipo(){
        return $this->belongsTo(PropiedadTipo::class);
    }

    public function equipos(){
        return $this->belongsToMany(Equipo::class, 'propiedad_equipo', "propiedad_id", "equipo_id");
    }

    public function images(){
        return $this->morphMany(PropiedadImagen::class, 'imageable');
    }
}
