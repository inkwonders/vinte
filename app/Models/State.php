<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    use HasFactory;

    public $table='states';

    public $fillable = [
        'description',
        'abbreviation',
        'created_at',
        'updated_at'
    ];


   protected $casts = [
       'id' => 'integer',
       'description' => 'string',
       'abbreviation' => 'string',
   ];


    public function municipalities()
    {

        return $this->hasMany(Municipality::class, 'state_id');
    }
}
