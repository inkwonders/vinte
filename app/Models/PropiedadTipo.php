<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropiedadTipo extends Model
{
    use HasFactory;

    protected $table = 'propiedad_tipos';

    protected $fillable = [
        'nombre'
    ];

    public function propiedades(){
        return $this->hasMany(Propiedad::class);
    }
    public function questionnaire(){
        return $this->hasMany(Questionnaire::class);
    }

}
