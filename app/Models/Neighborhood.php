<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Neighborhood extends Model
{
    use HasFactory;

    public $table='neighbourhoods';

    public $fillable = [
        'neighborhood',
        'slug',
        'zip_code',
        'type',
        'active',
        'municipality_id',
        'created_at',
        'updated_at'
    ];


   protected $casts = [
       'id' => 'integer',
       'neighborhood' => 'string',
       'slug' => 'string',
       'zip_code' => 'string',
       'type' => 'string',
       'active'=>'integer',
       'municipality_id'=>'integer',
   ];


    public function municipality()
    {
        return $this->belongsTo(\App\Models\Municipality::class);
    }


    public function properties()
    {
        return $this->hasMany(Property::class);
    }
}
