<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    use HasFactory;
    protected $table = 'activity_log';

    public function usuario() {
        return $this->morphInstanceTo(User::class, 'activity_log', 'causer_type', 'causer_id', 'id');
    }

    public function registrable() {
        return $this->morphTo('causer');
    }
}
