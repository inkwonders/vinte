<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Equipo extends Model
{
    use HasFactory;

    public function propiedades(){
        return $this->belongsToMany(Propiedad::class, 'propiedad_equipo', "propiedad_id", "equipo_id");
    }
}
