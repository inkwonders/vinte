<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Municipality extends Model
{
    use HasFactory;

    public $table='municipalities';


    public $fillable = [
        'description',
        'longitude',
        'latitude',
        'created_at',
        'updated_at',
        'state_id'
    ];

    protected $casts = [
        'id' => 'integer',
        'description' => 'string',
        'state_id' => 'integer',
        'longitude' => 'string',
        'latitude' => 'string',
    ];

    public function state()
    {

        return $this->belongsTo(State::class);
    }


    public function neighbourhoods()
    {

        return $this->hasMany(\App\Models\Neighborhood::class);
    }

}
