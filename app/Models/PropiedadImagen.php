<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropiedadImagen extends Model
{
    use HasFactory;

    protected $table = 'propiedad_imagenes';

    protected $fillable = [
        'url',
        'nombre',
    ];

    public function imageable(){
        return $this->morphTo();
    }
}
