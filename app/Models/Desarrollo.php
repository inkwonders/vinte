<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Desarrollo extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre'
    ];

    public function propiedades(){
        return $this->hasMany(Propiedad::class);
    }

    public function questionnaire(){
        return $this->hasMany(Questionnaire::class);
    }
}
