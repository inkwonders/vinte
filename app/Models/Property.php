<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;

    public $table = 'properties';

    public $fillable = [
        'id',
        'neigbhbourhood_id',
        'property_type_id',
        'xanteid',
        'title',
        'title2',
        'description',
        'street',
        'no_ext',
        'no_int',
        'slug',
        'bathrooms',
        'location_description',
        'half_bathrooms',
        'bedrooms',
        'parking_slots',
        'm2',
        'm2_construction',
        'm2_garden',
        'floors',
        'lat',
        'lon',
        // 'number_rooms',
        'price',
        'map_url',
        'city',
        'status',
        'outstanding',
        'atention_hours',
        'created_at',
        'updated_at'
    ];

     /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'neigbhbourhood_id' => 'integer',
        'property_type_id' => 'integer',
        'xanteid' => 'string',
        'title' => 'string',
        'title2' => 'string',
        'description' => 'string',
        'street'=> 'string',
        'no_ext'=> 'string',
        'no_int'=> 'string',
        'slug'=>    'string',
        'bathrooms' => 'integer',
        'location_description' => 'string',
        'half_bathrooms' => 'integer',
        'bedrooms' => 'integer',
        'parking_slots' => 'integer',
        'm2' => 'float',
        'm2_construction' => 'float',
        'm2_garden' => 'float',
        'floors' => 'integer',
        'lat' => 'string',
        'lon' => 'string',
        // 'number_rooms' => 'integer',
        'price' => 'integer',
        'map_url' => 'string',
        'city' => 'string',
        'status' => 'integer',
        'outstanding' =>'integer',
        'atention_hours'=> 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

      /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function neighborhood()
    {
        return $this->belongsTo(\App\Models\Neighborhood::class,'neigbhbourhood_id');
    }

       /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function property_type()
    {
        return $this->belongsTo(\App\Models\PropertyType::class, 'property_type_id');
    }


    public function images() {
        return $this->morphMany(Multimedia::class, 'media');
    }

}
