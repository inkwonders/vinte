<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Definition(
 *      definition="Municipio",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nombre",
 *          description="nombre",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="estado_id",
 *          description="estado_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Municipio extends Model
{

    use HasFactory;

    public $table = 'municipios';

    public $fillable = [
        'descripcion',
        'lon',
        'lat',
        'created_at',
        'updated_at',
        'estado_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'descripcion' => 'string',
        'estado_id' => 'integer',
        'lon' => 'string',
        'lat' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function estado()
    {
        return $this->belongsTo(Estado::class, 'estado_id');
    }

    public function propiedades(){
        return $this->belongsToMany(Propiedad::class);
    }

    public function colonias(){
        return $this->hasMany(Colonia::class);
    }

}
