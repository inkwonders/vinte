<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Multimedia extends Model
{
    use HasFactory;

    public $table = 'multimedia';

    public $fillable = [
        'multimedia_father_id',
        'name',
        'mime',
        'type',
        'size',
        'compresed',
        'order',
        'alt',
        'url',
        'is_mobile',
        'description',
    ];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                                => 'integer',
        'multimedia_father_id'            => 'integer',
        'name'                              => 'string',
        'mime'                              => 'string',
        'type'                              => 'string',
        'size'                              => 'string',
        'compresed'                         => 'integer',
        'order'                             => 'integer',
        'alt'                               => 'string',
        'url'                               => 'string',
        'is_mobile'                         => 'integer',
        'description'                       => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function media(){
        return $this->morphTo();
    }


    //relacion recursiva


    public function multimedia_father()

    {
        return $this->belongsTo(\App\Models\Multimedia::class);

    }


    public function multimedia()
    {
        return $this->hasMany(\App\Models\Multimedia::class,'multimedia_father_id');

    }


    public function allMultimedia()
    {
        return $this->multimedia()->with('allMultimedia');

    }

     //relacion recursiva


}
