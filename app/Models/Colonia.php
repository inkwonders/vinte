<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Colonia extends Model
{
    use HasFactory;

    public function municipio(){
        return $this->belongsTo(Municipio::class);
    }

    public function propiedades(){
        return $this->hasMany(Propiedad::class);
    }
}
