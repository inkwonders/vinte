<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    use HasFactory;

    public $table = 'questionnaire';

    public $fillable = [
        'id',
        "desarrollo_id",
        "propiedad_tipo_id",
        'street',
        'num_ext',
        'num_int',
        'google_direction',
        'property_type',
        'house_levels',
        'level_department',
        'bedroom',
        'bathroom',
        'half_bathrooms',
        'parking_slots',
        'years',
        'm2',
        'offer_money',
        'mortgage',
        'sale_reasons',
        'name',
        'whatsapp',
        'email',
        'comments',
        'status',
    ];

     /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        "desarrollo_id" => 'integer',
        "propiedad_tipo_id" => 'integer',
        // 'ubication' => 'string',
        'street' => 'string',
        'num_ext' => 'string',
        'num_int' => 'string',
        'google_direction' => 'string',
        'property_type' => 'string',
        'house_levels' => 'integer',
        'level_department' => 'integer',
        'bedroom' => 'integer',
        'bathroom' => 'integer',
        'half_bathrooms' => 'integer',
        'parking_slots' => 'integer',
        'years' => 'float',
        'm2' => 'float',
        'offer_money' => 'string',
        'mortgage' => 'string',
        'sale_reasons' => 'string',
        'name' => 'string',
        'whatsapp' => 'integer',
        'email' => 'string',
        'comments' => 'string',
        'status' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function desarrollo(){
        return $this->belongsTo(Desarrollo::class);
    }

    public function estado(){
        return $this->belongsTo(Estado::class);
    }
     public function propiedad_tipo(){
        return $this->belongsTo(PropiedadTipo::class);
    }
}
