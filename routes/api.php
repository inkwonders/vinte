<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Transformers\EstadoTransformer;
use App\Http\Controllers\PropiedadController;
use  App\Http\Controllers\API\EstadoAPIController;
use App\Http\Controllers\API\PropiedadAPIController;
use App\Http\Controllers\API\DesarrolloAPIController;
use App\Http\Controllers\DescargaController;
use App\Http\Controllers\QuestionnaireController;

use App\Http\Controllers\API\PropertyAPIController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Todos los equipos
Route::get('/equipos', [DesarrolloAPIController::class, 'getEquipos']);

//Desarrollos
Route::get('/desarrollos/{estado_id}', [DesarrolloAPIController::class, 'getDesarrollos']);

//Todos los desarrollos
Route::get('/desarrollos-all', [DesarrolloAPIController::class, 'getAllDesarrollos']);

//api google
Route::post('/google-places', [DesarrolloAPIController::class, 'getGooglePlaces']);

//Colonias
Route::get('/municipios/{municipio}/colonias', [EstadoAPIController::class, 'getColonias']);

Route::prefix('estados')->group(function () {

    //Estados
    Route::get('/', [EstadoAPIController::class, 'getEstados']);

    //Municipios
    Route::get('/{estado}/municipios', [EstadoAPIController::class, 'getMunicipios']);
});

Route::prefix('colonias')->group(function () {

    //lista colonias
    Route::post('/', [EstadoAPIController::class, 'getColoniasEstado']);

});

Route::prefix('propiedades')->group(function () {

    //Equipos propiedad
    Route::get('/{propiedad}/equipos', [PropiedadAPIController::class, 'getEquipoPropiedad']);

    //exportar tabla
    Route::get('/exportarTabla_old', [DescargaController::class, 'exportarPropiedadOld'])->name('exportar_tabla_old');
    Route::get('/exportarTabla', [DescargaController::class, 'exportarPropiedad'])->name('exportar_tabla');

    //exportar propiedad
    Route::get('/{propiedad}/exportar', [DescargaController::class, 'exportarPropiedadEspecifica']);

    //Propiedades
    Route::get('/', [PropiedadAPIController::class, 'getPropiedades']);

    //InfoPropiedad
    Route::get('/{propiedad}', [PropiedadAPIController::class, 'getPropiedadInfo']);

    //Guardar
    // Route::post('/guardar', [PropiedadController::class, 'store']);
     //Guardar
     Route::post('/guardar', [QuestionnaireController::class, 'store']);

    //Descargar_zip
    Route::get('/{propiedad}/descargar', [DescargaController::class, 'descargarZip']);

    //Verificar CP
    Route::post('/verificarcp', [PropiedadController::class,  'verificarEstado']);

});

Route::get('new_properties/{propiedad}/descargar', [DescargaController::class, 'descargarNewProperties']);


Route::get('/getProperties', [PropertyAPIController::class,  'index']);
Route::get('/getProperties', [PropertyAPIController::class,  'index_old']);
Route::get('/showProperty/{id}', [PropertyAPIController::class,  'show']);
Route::post('/storeProperty', [PropertyAPIController::class,  'store']);

Route::post('/getNeighborhood', [App\Http\Controllers\API\ApiController::class,  'getNeighborhood']);
Route::get('/getStates', [App\Http\Controllers\API\ApiController::class,  'getStates']);
