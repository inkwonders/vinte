<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PropiedadController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\NeighborhoodController;
use App\Http\Livewire\DescCasa;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\LogController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/descripcion', DescCasa::class)->name('descripcion');

 // Ruta logout
 Route::get('/logout', "App\Http\Controllers\AuthController@logOut")->name('logout');

 //ruta correo de perfil de propiedad
 Route::post('/sendEmail', "App\Http\Controllers\PropiedadController@sendEmail")->name('sendEmail');
 Route::get('/thankyou', function () {
    return view('thankyou');
})->name("thankyou");


Route::get('/sms', [PropiedadController::class, 'prueba']);

Route::get('/form', function () {
    return view('form.index');
});

Route::get('/', function () {
    return view('index_nuevo');
})->name("index.nuevo");

Route::get('/faqs', function () {
    return view('faqs');
})->name("faqs");

Route::get('/comprar/1', function () {
    return view('compra_tu_casa');
})->name("compra_tu_casa_1");

// Route::get('/comprar/2', function () {
//     return view('compra_tu_casa');
// })->name("compra_tu_casa");


Route::get('/vender', function () {
    // return view('vende_tu_casa');
    return redirect('/');
})->name("vende_tu_casa");

Route::get('/comprarnvo', function () {
    return view('comprar-casa');
})->name("comprarnvo");


// Route::get('/descripcion', function () {
//     return view('desc-casa');
// })->name("desc-casa");

Route::get('/propiedad/{id_prop}', function ($id_prop) {
    return view('desc-casa')->with('id_prop',$id_prop);

    // return $id_prop;
});

// Route::fallback(function () {
//     return view('index_nuevo');
// });

// Route::get('/csrf', function() {
//     //dd('hola');
//     return csrf_token();
// });

Route::group(['middleware' => 'auth'], function () {
    Route::middleware(['auth:sanctum', 'verified'])->group(function () {
        Route::get('/propiedades', [PropiedadController::class, 'index'])->name('admin.index');
        Route::get('/propiedades_old', [PropiedadController::class, 'index_old'])->name('admin.old');
        Route::get('/upload_properties', [PropiedadController::class, 'show_properties'])->name('admin.show_properties');
        Route::get('/users', [UserController::class, 'show_users'])->name('admin.show_users');
        Route::get('/neighborhoods', [NeighborhoodController::class, 'show_neighborhoods'])->name('admin.show_neighborhoods');
        Route::get('/log', [LogController::class, 'show_log'])->name('admin.show_log');
    });
});

Route::get('/{num}', function ($num) {
    if($num >= 1 && $num <= 27)
        return view('form.index');

    return redirect('/');
});

//Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'App\Http\Controllers\LanguageController@switchLang']);
Route::get('locale/{locale}', function($locale){
    session()->put('locale',$locale);
    return Redirect::back();
});






