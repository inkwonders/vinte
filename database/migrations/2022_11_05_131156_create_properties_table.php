<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('properties', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->foreignId('neigbhbourhood_id')->constrained('neighbourhoods')->onUpdate('cascade')->onDelete('restrict');
            $table->foreignId('property_type_id')->constrained('property_types')->onUpdate('cascade')->onDelete('restrict');
            // $table->foreignId('post_id')->nullable()->constrained('posts')->onUpdate('cascade')->onDelete('restrict');
            $table->string('xanteid')->nullable();
            $table->string('title')->nullable();
            $table->string('title2')->nullable();
            $table->text('description')->nullable();
            $table->string('street')->nullable();
            $table->string('no_ext')->nullable();
            $table->string('no_int')->nullable();
            $table->string('slug')->nullable();
            $table->integer('bathrooms')->unsigned()->nullable();
            $table->text('location_description')->nullable();
            $table->integer('half_bathrooms')->unsigned()->nullable();
            $table->integer('bedrooms')->unsigned()->nullable();
            $table->integer('parking_slots')->unsigned()->nullable();
            $table->float('m2')->nullable();
            $table->float('m2_construction')->nullable();
            $table->float('m2_garden')->nullable();
            $table->integer('floors')->unsigned()->nullable();
            $table->string('lat')->nullable();
            $table->string('lon')->nullable();
            $table->integer('price')->nullable();
            $table->string('map_url')->nullable();
            $table->integer('status')->nullable()->comment('0.- inactivo, 1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
