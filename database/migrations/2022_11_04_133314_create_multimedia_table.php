<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMultimediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('multimedia', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('name')->nullable();
            $table->string('mime')->nullable();
            $table->enum('size',['S', 'M', 'L']);
            $table->integer('compresed')->unsigned()->nullable();
            $table->integer('order')->unsigned()->nullable();
            $table->string('alt')->nullable();
            $table->string('url')->nullable();
            $table->string('is_mobile')->nullable();
            $table->morphs('media');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('multimedia');
    }
}
