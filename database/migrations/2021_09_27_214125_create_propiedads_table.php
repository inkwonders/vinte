<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropiedadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propiedades', function (Blueprint $table) {
            $table->id();
            $table->foreignId('estado_id')->constrained('estados','id')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('desarrollo_id')->nullable()->constrained('desarrollos','id')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('propiedad_tipo_id')->constrained('propiedad_tipos','id')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('colonia_id')->constrained('colonias','id')->onUpdate('cascade')->onDelete('cascade');
            $table->string('direccion');
            $table->string('num_exterior',10);
            $table->float('superficie_habitable', 15, 2);
            $table->string('equipado',2)->nullable();
            $table->integer('piso_departamento')->nullable();
            $table->integer('num_niveles_casa')->nullable();
            $table->string('terraza_jardin')->nullable();
            $table->integer('num_recamaras')->nullable();
            $table->integer('num_bath')->nullable();
            $table->integer('num_med_bath')->nullable();
            $table->integer('num_estacionamientos')->nullable();
            $table->string('antiguedad');
            $table->float('propiedad_precio', 15, 2);
            $table->string('hipoteca',2);
            $table->string('hipoteca_tipo')->nullable();
            $table->string('telefono');
            $table->string('correo');
            $table->string('nombre_propietario');
            $table->string('motivo');
            $table->string('motivo_comp')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propiedades');
    }
}
