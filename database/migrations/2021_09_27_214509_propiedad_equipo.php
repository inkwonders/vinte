<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PropiedadEquipo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propiedad_equipo', function (Blueprint $table) {
            $table->foreignId('propiedad_id')->constrained('propiedades','id')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('equipo_id')->constrained('equipos','id')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propiedad_equipo');
    }
}
