<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Questionnaire extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::disableForeignKeyConstraints();

        Schema::create('questionnaire', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();

            $table->foreignId('desarrollo_id')->nullable()->constrained('desarrollos','id')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('propiedad_tipo_id')->constrained('propiedad_tipos','id')->onUpdate('cascade')->onDelete('cascade');
            $table->string('street')->nullable();
            $table->string('num_ext')->nullable();
            $table->string('num_int')->nullable();
            $table->text('google_direction')->nullable();
            $table->enum('property_type',['Casa','Departamento'])->nullable();
            $table->integer('house_levels')->nullable();
            $table->integer('level_department')->nullable();
            $table->integer('bedroom')->unsigned()->nullable();
            $table->integer('bathroom')->nullable();
            $table->integer('half_bathrooms')->unsigned()->nullable();
            $table->integer('parking_slots')->unsigned()->nullable();
            $table->float('years')->nullable();
            $table->float('m2')->nullable();
            $table->string('offer_money')->nullable();
            $table->enum('mortgage',['No está hipotecada','INFONAVIT', 'FOVISSTE', 'Banco', 'COFINAVIT', 'Otro'])->nullable();
            $table->enum('sale_reasons',['Busco liquidéz','Cambiar de ciudad', 'Busco hogar con características diferentes', 'Otro'])->nullable();
            $table->string('name')->nullable();
            $table->bigInteger('whatsapp')->nullable();
            $table->string('email')->nullable();
            $table->string('comments')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('questionnaire');
    }
}
