<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHabilitadoFieldToEstadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('estados', function (Blueprint $table) {
            $table->boolean('habilitado')
            ->after('abreviacion')
            ->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('estados', function (Blueprint $table) {
            $table->dropColumn('habilitado');
        });

    }
}
