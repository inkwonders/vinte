<?php

namespace Database\Factories;

use App\Models\Propiedad;
use Illuminate\Database\Eloquent\Factories\Factory;

class PropiedadFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Propiedad::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre_apellidos' => $this->faker->name.' '.$this->faker->lastName,
            'telefono' => $this->faker->phoneNumber,
            'correo' => $this->faker->email,
            'desarrollo_id' => $this->faker->numberBetween(1,9),
            'calle' => $this->faker->streetName,
            'privada' => $this->faker->streetName,
            'propiedad_tipo_id' => $this->faker->numberBetween(1,2)
        ];
    }
}
