<?php

namespace Database\Factories;

use App\Models\Desarrollo;
use Illuminate\Database\Eloquent\Factories\Factory;

class DesarrolloFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Desarrollo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
