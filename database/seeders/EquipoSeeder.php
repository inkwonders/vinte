<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EquipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $equipos = array(
            array('nombre' => 'Cocina integral'),
            array('nombre' => 'Clósets'),
            array('nombre' => 'Cancel en el baño'),
            array('nombre' => 'Piso en toda la casa'),
            array('nombre' => 'Protecciones en puertas y ventanas')
        );

        DB::table('equipos')->insert($equipos);
    }
}
