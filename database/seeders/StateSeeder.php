<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
// use App\Models\Country;
use App\Models\State;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        State::truncate();

        $estados = [
			['id' => 1, 'description' => 'Aguascalientes', 'abbreviation' => 'Ags.'],
			['id' => 2, 'description' => 'Baja California', 'abbreviation' => 'BC'],
			['id' => 3, 'description' => 'Baja California Sur', 'abbreviation' => 'BCS'],
			['id' => 4, 'description' => 'Campeche', 'abbreviation' => 'Camp.'],
			['id' => 5, 'description' => 'Coahuila de Zaragoza', 'abbreviation' => 'Coah.'],
			['id' => 6, 'description' => 'Colima', 'abbreviation' => 'Col.'],
			['id' => 7, 'description' => 'Chiapas', 'abbreviation' => 'Chis.'],
			['id' => 8, 'description' => 'Chihuahua', 'abbreviation' => 'Chih.'],
			['id' => 9, 'description' => 'Ciudad de México', 'abbreviation' => 'CDMX'],
			['id' => 10, 'description' => 'Durango', 'abbreviation' => 'Dgo.'],
			['id' => 11, 'description' => 'Guanajuato', 'abbreviation' => 'Gto.'],
			['id' => 12, 'description' => 'Guerrero', 'abbreviation' => 'Gro.'],
			['id' => 13, 'description' => 'Hidalgo', 'abbreviation' => 'Hgo.'],
			['id' => 14, 'description' => 'Jalisco', 'abbreviation' => 'Jal.'],
			['id' => 15, 'description' => 'México', 'abbreviation' => 'Mex.'],
			['id' => 16, 'description' => 'Michoacán de Ocampo', 'abbreviation' => 'Mich.'],
			['id' => 17, 'description' => 'Morelos', 'abbreviation' => 'Mor.'],
			['id' => 18, 'description' => 'Nayarit', 'abbreviation' => 'Nay.'],
			['id' => 19, 'description' => 'Nuevo León', 'abbreviation' => 'NL'],
			['id' => 20, 'description' => 'Oaxaca', 'abbreviation' => 'Oax.'],
			['id' => 21, 'description' => 'Puebla', 'abbreviation' => 'Pue.'],
			['id' => 22, 'description' => 'Querétaro', 'abbreviation' => 'Qro.'],
			['id' => 23, 'description' => 'Quintana Roo', 'abbreviation' => 'Q. Roo'],
			['id' => 24, 'description' => 'San Luis Potosí', 'abbreviation' => 'SLP'],
			['id' => 25, 'description' => 'Sinaloa', 'abbreviation' => 'Sin.'],
			['id' => 26, 'description' => 'Sonora', 'abbreviation' => 'Son.'],
			['id' => 27, 'description' => 'Tabasco', 'abbreviation' => 'Tab.'],
			['id' => 28, 'description' => 'Tamaulipas', 'abbreviation' => 'Tamps.'],
			['id' => 29, 'description' => 'Tlaxcala', 'abbreviation' => 'Tlax.'],
			['id' => 30, 'description' => 'Veracruz de Ignacio de la Llave', 'abbreviation' => 'Ver.'],
			['id' => 31, 'description' => 'Yucatán', 'abbreviation' => 'Yuc.'],
			['id' => 32, 'description' => 'Zacatecas', 'abbreviation' => 'Zac.'],
        ];

        State::insert($estados);
        Schema::enableForeignKeyConstraints();
    }
}
