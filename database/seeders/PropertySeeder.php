<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Property;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class PropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Schema::disableForeignKeyConstraints();

        DB::table('properties')->truncate();

        $properties = array(

            array(
                'id' => '2',
                'neigbhbourhood_id' => '52419', // Real toledo Pachuca
                'property_type_id' => '1',
                'xanteid' => 'XA005',
                // 'year' => '',
                'title' => 'Casa equipada',
                'title2' => 'de 2 niveles',
                'description' => 'Cuenta con cocina integral y clósets en excelente estado.
                Está ubicada en Real Toledo, un desarrollo con acceso controlado, parques infantiles, multicanchas, ciclopista y comercio.
                A unos pasos de Explanada Pachuca, cerca de escuelas, universidades, hospitales y el Tuzobus.',
                'street' => 'Albarreal',
                'no_ext' => '102',
                'no_int' => '',
                'slug' => 'real-toledo-pachuca-XA005',
                // 'pet_friendly' => '',
                'bathrooms' => '1',
                'location_description' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3748.4465004461135!2d-98.7860391!3d20.03173225!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1a170ccd620b1%3A0xcef29b9790600b18!2sReal%20de%20Toledo%2C%2042119%20Pachuca%20de%20Soto%2C%20Hgo.!5e0!3m2!1ses-419!2smx!4v1667601847485!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>',
                'half_bathrooms' => '1',
                'bedrooms' => '2',
                'parking_slots' => '1',
                'm2' => '90',
                'm2_construction' => '69',
                'm2_garden' => '0',
                'floors' => '2',
                'lat' => '20.0317323',
                'lon' => '-98.7860391',
                // 'number_rooms' => '0',
                'price' => '990000',
                'map_url' => 'https://goo.gl/maps/Vit7eKeerNyhk9aM7',
                'status' => '3',  //'0.- inactivo, 1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada'
                'created_at' => '2022-11-04 15:41:37',
                'updated_at' => '2022-11-04 15:41:37',
                'deleted_at' => NULL,
                ),
                array(
                'id' => '3',
                'neigbhbourhood_id' => '70814', // Real del cid
                'property_type_id' => '1',
                'xanteid' => 'XA006',
                // 'year' => '',
                'title' => '¡Aprovecha preciosa casa!',
                'title2' => 'Con piso, cocina integral, clósets y más',
                'description' => 'Preciosa casa a un súper precio, en excelentes condiciones ¡lista para estrenarse!
                Cuenta con piso, cocina integral, cancel en el baño y protecciones. Está ubicada en Real del Cid, un desarrollo con acceso controlado, parques infantiles, multicanchas, comercio y ciclopista. En la mejor zona de Ojo de Agua, a minutos de centros comerciales, escuelas y a 45 min de CDMX.',
                'street' => 'Soliedra',
                'no_ext' => '19',
                'no_int' => '',
                'slug' => 'real-del-cid-tecamac-XA006',
                // 'pet_friendly' => '',
                'bathrooms' => '1',
                'location_description' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3757.5559076929057!2d-99.0230701!3d19.646274899999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1f3fb7d5ebe27%3A0xed931f1ac430834!2sReal%20del%20cid!5e0!3m2!1ses-419!2smx!4v1667604874927!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>',
                'half_bathrooms' => '1',
                'bedrooms' => '3',
                'parking_slots' => '2',
                'm2' => '117',
                'm2_construction' => '77',
                'm2_garden' => '0',
                'floors' => '2',
                'lat' => '19.6462749',
                'lon' => '-99.0230701',
                // 'number_rooms' => '0',
                'price' => '1200000',
                'map_url' => 'https://goo.gl/maps/iNpz2JEEPSJtRqpd6',
                'status' => '4',  //'0.- inactivo, 1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada'
                'created_at' => '2022-11-04 15:41:37',
                'updated_at' => '2022-11-04 15:41:37',
                'deleted_at' => NULL,
                ),
                array(
                'id' => '4',
                'neigbhbourhood_id' => '103814', // Real solare
                'property_type_id' => '1',
                'xanteid' => 'XA009',
                // 'year' => '',
                'title' => 'Casa equipada',
                'title2' => 'de 2 niveles',
                'description' => 'Cuenta con cocina integral, clósets en recámaras, cancel de baño y protecciones.
                Está ubicada en Real Solare, un desarrollo con acceso controlado, parques infantiles, dog park, multicanchas, gym al aire libre, ciclopista, escuelas y comercio.
                Muy cerca de parques industriales de la zona El Marqués y a 15 min del centro de Querétaro.',
                'street' => 'Aquari',
                'no_ext' => '19',
                'no_int' => '',
                'slug' => 'real-solare-queretaro-XA009',
                // 'pet_friendly' => '',
                'bathrooms' => '2',
                'location_description' => '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d59760.59182421701!2d-100.2899472!3d20.586547!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d342cc112abe65%3A0xcb0cd5063e7b5675!2sReal%20Solare%2C%2076246%20Qro.!5e0!3m2!1ses-419!2smx!4v1667605295291!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>',
                'half_bathrooms' => '1',
                'bedrooms' => '2',
                'parking_slots' => '2',
                'm2' => '92',
                'm2_construction' => '78',
                'm2_garden' => '0',
                'floors' => '2',
                'lat' => '20.586547',
                'lon' => '-100.2899472',
                // 'number_rooms' => '0',
                'price' => '1021000',
                'map_url' => 'https://goo.gl/maps/a5wXrdXGehBK4Uc38',
                'status' => '2',  //'0.- inactivo, 1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada'
                'created_at' => '2022-11-04 15:41:37',
                'updated_at' => '2022-11-04 15:41:37',
                'deleted_at' => NULL,
                ),
                array(
                'id' => '5',
                'neigbhbourhood_id' => '57721', // Real navarra
                'property_type_id' => '1',
                'xanteid' => 'XA0012',
                // 'year' => '',
                'title' => 'Casa equipada',
                'title2' => 'de 2 niveles',
                'description' => 'Cuenta con cocina integral y clósets en recámaras
                Está ubicada en Real Navarra, un desarrollo con acceso controlado, parques infantiles, dog park, multicanchas, gym al aire libre, ciclopista y comercio.
                Muy cerca del Tuzobus, asi como de escuelas y avenidas principales.',
                'street' => 'Villa Franca',
                'no_ext' => '217',
                'no_int' => '',
                'slug' => 'real-navarra-pachuca-XA0012',
                // 'pet_friendly' => '',
                'bathrooms' => '2',
                'location_description' => '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1874.5420645182717!2d-98.8026479!3d20.0049835!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1a187dbea2dbf%3A0x1f4009b1b0963009!2sReal%20Navarra!5e0!3m2!1ses-419!2smx!4v1667605512902!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>',
                'half_bathrooms' => '1',
                'bedrooms' => '3',
                'parking_slots' => '2',
                'm2' => '121',
                'm2_construction' => '134',
                'm2_garden' => '0',
                'floors' => '2',
                'lat' => '20.0049835',
                'lon' => '-98.8026479',
                // 'number_rooms' => '0',
                'price' => '1630000',
                'map_url' => 'https://g.page/RealNavarra?share',
                'status' => '2',  //'0.- inactivo, 1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada'
                'created_at' => '2022-11-04 15:41:37',
                'updated_at' => '2022-11-04 15:41:37',
                'deleted_at' => NULL,
                ),
                array(
                'id' => '6',
                'neigbhbourhood_id' => '70800', // Real verona
                'property_type_id' => '1',
                'xanteid' => 'XA0013',
                // 'year' => '',
                'title' => 'Casa',
                'title2' => 'de 2 niveles',
                'description' => 'Cuenta con 3 recámaras y 2 lugares de estacionamiento.
                Está ubicada en Real Verona, un desarrollo con acceso controlado, parques infantiles, parque canino, multicanchas, gimnasio al aire libre, comercio y escuelas desde jardin de niños hasta secundaria.
                Muy cerca de avenidas principales y plazas comerciales.',
                'street' => 'Lanciere',
                'no_ext' => '1',
                'no_int' => '',
                'slug' => 'real-verona-tecamac-XA0013',
                // 'pet_friendly' => '',
                'bathrooms' => '1',
                'location_description' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7514.85652875955!2d-99.02596534999999!3d19.651726150000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1f2135a3e68f7%3A0x2e64832cbc973d2c!2sFraccionamiento%20Real%20Verona%2C%20Ojo%20de%20Agua%2C%20M%C3%A9x.!5e0!3m2!1ses-419!2smx!4v1667605955808!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>',
                'half_bathrooms' => '1',
                'bedrooms' => '3',
                'parking_slots' => '1',
                'm2' => '54',
                'm2_construction' => '77',
                'm2_garden' => '0',
                'floors' => '2',
                'lat' => '19.6517262',
                'lon' => '-99.0259653',
                // 'number_rooms' => '0',
                'price' => '1115000',
                'map_url' => 'https://goo.gl/maps/yJDw8hB6udoHGhAW7',
                'status' => '3',  //'0.- inactivo, 1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada'
                'created_at' => '2022-11-04 15:41:37',
                'updated_at' => '2022-11-04 15:41:37',
                'deleted_at' => NULL,
                ),
                array(
                'id' => '7',
                'neigbhbourhood_id' => '100839', // Real segobia
                'property_type_id' => '2',
                'xanteid' => 'XA0014',
                // 'year' => '',
                'title' => 'Casa',
                'title2' => 'de 2 niveles',
                'description' => 'Cuenta con 2 recámaras y ampliación en parte trasera. Está ubicada en Real Segovia, un desarrollo con acceso controlado, parques infantiles, parque canino, multicanchas, gimnasio al aire libre y comercio.
                A solo 15 min se encuentra Cholula, ademas de estar rodeado de Parques Industriales.',
                'street' => 'Arcones',
                'no_ext' => '292',
                'no_int' => '',
                'slug' => 'real-segovia-huejotzingo-XA0013',
                // 'pet_friendly' => '',
                'bathrooms' => '1',
                'location_description' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7514.85652875955!2d-99.02596534999999!3d19.651726150000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1f2135a3e68f7%3A0x2e64832cbc973d2c!2sFraccionamiento%20Real%20Verona%2C%20Ojo%20de%20Agua%2C%20M%C3%A9x.!5e0!3m2!1ses-419!2smx!4v1667605955808!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>',
                'half_bathrooms' => '1',
                'bedrooms' => '3',
                'parking_slots' => '1',
                'm2' => '54',
                'm2_construction' => '77',
                'm2_garden' => '0',
                'floors' => '2',
                'lat' => '19.6517262',
                'lon' => '-99.0259653',
                // 'number_rooms' => '0',
                'price' => '1115000',
                'map_url' => 'https://goo.gl/maps/yJDw8hB6udoHGhAW7',
                'status' => '3',  //'0.- inactivo, 1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada'
                'created_at' => '2022-11-04 15:41:37',
                'updated_at' => '2022-11-04 15:41:37',
                'deleted_at' => NULL,
                ),

                array(
                'id' => '8',
                'neigbhbourhood_id' => '100839', // Real segobia
                'property_type_id' => '2',
                'xanteid' => 'XA0015',
                // 'year' => '',
                'title' => 'Departamento',
                'title2' => 'en Planta Baja',
                'description' => 'Cuenta con 2 recámaras y una alcoba. Está ubicada en Real Segovia, un desarrollo con acceso controlado, parques infantiles, parque canino, multicanchas, gimnasio al aire libre y comercio.
                A solo 15 min se encuentra Cholula, ademas de estar rodeado de Parques Industriales.',
                'street' => 'Carabia',
                'no_ext' => '116',
                'no_int' => '',
                'slug' => 'real-segovia-puebla-XA0015',
                // 'pet_friendly' => '',
                'bathrooms' => '1',
                'location_description' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4481.3085639294195!2d-98.38494963536509!3d19.18138217704836!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cfcde7f5db286f%3A0x235339753d402237!2sReal%20Segovia!5e0!3m2!1ses-419!2smx!4v1667840019277!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>',
                'half_bathrooms' => '0',
                'bedrooms' => '3',
                'parking_slots' => '1',
                'm2' => '41.71',
                'm2_construction' => '54',
                'm2_garden' => '0',
                'floors' => '1',
                'lat' => '19.1813822',
                'lon' => '-98.3849496',
                // 'number_rooms' => '0',
                'price' => '607000',
                'map_url' => 'https://g.page/RealSegoviaPuebla?share',
                'status' => '1',  //'0.- inactivo, 1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada'
                'created_at' => '2022-11-04 15:41:37',
                'updated_at' => '2022-11-04 15:41:37',
                'deleted_at' => NULL,
                ),
                array(
                'id' => '9',
                'neigbhbourhood_id' => '106695', // Real bilbao
                'property_type_id' => '2',
                'xanteid' => 'XA0017',
                // 'year' => '',
                'title' => 'Departamento',
                'title2' => 'en 2do piso',
                'description' => 'Departamento que cuenta con 3 recámaras y cocina integral. Está ubicada en Real Bilbao, un desarrollo con acceso controlado donde encontrarás parques infantiles, parque canino, casa club con alberca, multicanchas, gimnasio al aire libre y comercio.',
                'street' => 'Areatza',
                'no_ext' => '301',
                'no_int' => '',
                'slug' => 'real-bilbao-playa-del-carmen-XA0017',
                // 'pet_friendly' => '',
                'bathrooms' => '1',
                'location_description' => '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d17251.493668042767!2d-87.1113340737726!3d20.671629378241832!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f4e4365a8f0a989%3A0x6f97a25427373ba8!2sFRACC.%20REAL%20BILBAO!5e0!3m2!1ses-419!2smx!4v1667841749829!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>',
                'half_bathrooms' => '0',
                'bedrooms' => '3',
                'parking_slots' => '1',
                'm2' => '62',
                'm2_construction' => '62',
                'm2_garden' => '0',
                'floors' => '1',
                'lat' => '20.6716294',
                'lon' => '-87.1113341',
                // 'number_rooms' => '0',
                'price' => '1202000',
                'map_url' => 'https://goo.gl/maps/UZUuPp7DqwCAtNQd7',
                'status' => '1',  //'0.- inactivo, 1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada'
                'created_at' => '2022-11-04 15:41:37',
                'updated_at' => '2022-11-04 15:41:37',
                'deleted_at' => NULL,
                ),

                array(
                'id' => '10',
                'neigbhbourhood_id' => '146817', // Real ibiza plus
                'property_type_id' => '2',
                'xanteid' => 'XA0018',
                // 'year' => '',
                'title' => 'Departamento',
                'title2' => 'en 2do piso',
                'description' => 'Departamento que cuenta con 3 recámaras, cocina integral y clósets. Está ubicada en Real Ibiza Plus, un desarrollo con acceso controlado, alberca, parques, area de usos multiples, parque canino, multicanchas, gimnasio al aire libre. A 4km de la playa y cercanía con hospitales, escuelas y comercio.',
                'street' => 'Bermella',
                'no_ext' => '308',
                'no_int' => '',
                'slug' => 'real-ibiza-plus-playa-del-carmen-XA0018',
                // 'pet_friendly' => '',
                'bathrooms' => '1',
                'location_description' => '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3733.3757511697886!2d-87.09605736137694!3d20.65428659659355!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f4e42f0e2d09881%3A0x3115d8d185c8b4cd!2sReal%20Ibiza%20Plus%20Apartment!5e0!3m2!1ses-419!2smx!4v1667842339647!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>',
                'half_bathrooms' => '0',
                'bedrooms' => '2',
                'parking_slots' => '1',
                'm2' => '58',
                'm2_construction' => '52',
                'm2_garden' => '0',
                'floors' => '1',
                'lat' => '20.6542866',
                'lon' => '-87.0960574',
                // 'number_rooms' => '0',
                'price' => '1202000',
                'map_url' => 'https://goo.gl/maps/wNh8yxJCKwRTysDt5',
                'status' => '1',  //'0.- inactivo, 1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada'
                'created_at' => '2022-11-04 15:41:37',
                'updated_at' => '2022-11-04 15:41:37',
                'deleted_at' => NULL,
                ),
                array(
                'id' => '11',
                'neigbhbourhood_id' => '103814', // Real solare
                'property_type_id' => '1',
                'xanteid' => 'XA0021',
                // 'year' => '',
                'title' => 'Casa',
                'title2' => 'de 2 niveles',
                'description' => 'Cuenta con cocina integral, clósets en las 3 recámaras, cancel en baños y protecciones.
                Está ubicada en Real Solare, un desarrollo con acceso controlado, parques infantiles, dog park, multicanchas, gym al aire libre, ciclopista, escuelas y comercio.
                Muy cerca de parques industriales de la zona El Marqués y a 15 min del centro de Querétaro.',
                'street' => 'Carina',
                'no_ext' => '37',
                'no_int' => '',
                'slug' => 'real-solare-queretaro-XA0021',
                // 'pet_friendly' => '',
                'bathrooms' => '2',
                'location_description' => '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d59760.59182421701!2d-100.2899472!3d20.586547!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d342cc112abe65%3A0xcb0cd5063e7b5675!2sReal%20Solare%2C%2076246%20Qro.!5e0!3m2!1ses-419!2smx!4v1667842644229!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>',
                'half_bathrooms' => '1',
                'bedrooms' => '3',
                'parking_slots' => '2',
                'm2' => '85',
                'm2_construction' => '104',
                'm2_garden' => '0',
                'floors' => '2',
                'lat' => '20.586547',
                'lon' => '-100.2899472',
                // 'number_rooms' => '0',
                'price' => '1320000',
                'map_url' => 'https://goo.gl/maps/aegvphxsvUTEWuJk7',
                'status' => '1',  //'0.- inactivo, 1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada'
                'created_at' => '2022-11-04 15:41:37',
                'updated_at' => '2022-11-04 15:41:37',
                'deleted_at' => NULL,
                ),
                array(
                'id' => '12',
                'neigbhbourhood_id' => '70800', // Real verona
                'property_type_id' => '2',
                'xanteid' => 'XA0022',
                // 'year' => '',
                'title' => 'Departamento',
                'title2' => 'equipado en 2do nivel',
                'description' => 'Depto que cuenta con 2 recámaras y 1 lugares de estacionamiento.
                Está ubicada en Real Verona, un desarrollo con acceso controlado, parques infantiles, parque canino, multicanchas, gimnasio al aire libre, comercio y escuelas desde jardin de niños hasta secundaria.
                Muy cerca de avenidas principales y plazas comerciales.',
                'street' => 'Fadalto',
                'no_ext' => '301',
                'no_int' => '',
                'slug' => 'real-verona-tecamac-XA0022',
                // 'pet_friendly' => '',
                'bathrooms' => '1',
                'location_description' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7514.85652875955!2d-99.02596534999999!3d19.651726150000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1f2135a3e68f7%3A0x2e64832cbc973d2c!2sFraccionamiento%20Real%20Verona%2C%20Ojo%20de%20Agua%2C%20M%C3%A9x.!5e0!3m2!1ses-419!2smx!4v1667844912934!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>',
                'half_bathrooms' => '0',
                'bedrooms' => '2',
                'parking_slots' => '1',
                'm2' => '46',
                'm2_construction' => '46',
                'm2_garden' => '0',
                'floors' => '1',
                'lat' => '19.6517262',
                'lon' => '-99.0259653',
                // 'number_rooms' => '0',
                'price' => '650000',
                'map_url' => 'https://goo.gl/maps/NBfHPXC5crFDyY1F7',
                'status' => '1',  //'0.- inactivo, 1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada'
                'created_at' => '2022-11-04 15:41:37',
                'updated_at' => '2022-11-04 15:41:37',
                'deleted_at' => NULL,
                ),
                array(
                'id' => '13',
                'neigbhbourhood_id' => '54787', // Real castilla
                'property_type_id' => '1',
                'xanteid' => 'XA0023',
                // 'year' => '',
                'title' => 'Casa',
                'title2' => 'de 2 niveles',
                'description' => 'Casa que cuenta con 3 recámaras y 2 lugares de estacionamiento.
                Está ubicada en Real Castilla, un desarrollo con acceso controlado, parques infantiles, parque canino, multicanchas, gimnasio al aire libre, comercio. A 15min del Pueblo Mágico Tepotzotlán.',
                'street' => 'Quinea',
                'no_ext' => '112',
                'no_int' => '',
                'slug' => 'real-castilla-tula-XA0023',
                // 'pet_friendly' => '',
                'bathrooms' => '1',
                'location_description' => '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15003.303089558738!2d-99.2364512!3d19.9317455!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d228f64fb7bfed%3A0x9d85323db9e53de!2sOficinas%20Real%20Castilla!5e0!3m2!1ses-419!2smx!4v1667845926407!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>',
                'half_bathrooms' => '1',
                'bedrooms' => '3',
                'parking_slots' => '2',
                'm2' => '89',
                'm2_construction' => '156',
                'm2_garden' => '0',
                'floors' => '2',
                'lat' => '19.9317455',
                'lon' => '99.2364512',
                // 'number_rooms' => '0',
                'price' => '1300000',
                'map_url' => 'https://g.page/RealCastillaTula?share',
                'status' => '1',  //'0.- inactivo, 1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada'
                'created_at' => '2022-11-04 15:41:37',
                'updated_at' => '2022-11-04 15:41:37',
                'deleted_at' => NULL,
                ),
                // array(
                // 'id' => '13',
                // 'neigbhbourhood_id' => '54787', // Real castilla
                // 'property_type_id' => '1',
                // 'xanteid' => 'XA0023',
                // // 'year' => '',
                // 'title' => 'Casa',
                // 'title2' => 'de 2 niveles',
                // 'description' => 'Casa que cuenta con 3 recámaras y 2 lugares de estacionamiento.
                // Está ubicada en Real Castilla, un desarrollo con acceso controlado, parques infantiles, parque canino, multicanchas, gimnasio al aire libre, comercio. A 15min del Pueblo Mágico Tepotzotlán.',
                // 'street' => 'Quinea',
                // 'no_ext' => '112',
                // 'no_int' => '',
                // 'slug' => 'real-castilla-tula-XA0023',
                // // 'pet_friendly' => '',
                // 'bathrooms' => '1',
                // 'location_description' => '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15003.303089558738!2d-99.2364512!3d19.9317455!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d228f64fb7bfed%3A0x9d85323db9e53de!2sOficinas%20Real%20Castilla!5e0!3m2!1ses-419!2smx!4v1667845926407!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>',
                // 'half_bathrooms' => '1',
                // 'bedrooms' => '3',
                // 'parking_slots' => '2',
                // 'm2' => '89',
                // 'm2_construction' => '156',
                // 'm2_garden' => '0',
                // 'floors' => '2',
                // 'lat' => '19.9317455',
                // 'lon' => '99.2364512',
                // 'number_rooms' => '0',
                // 'price' => '1300000',
                // 'map_url' => 'https://g.page/RealCastillaTula?share',
                // 'status' => '1',  //'0.- inactivo, 1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada'
                // 'created_at' => '2022-11-04 15:41:37',
                // 'updated_at' => '2022-11-04 15:41:37',
                // 'deleted_at' => NULL,
                // ),
                array(
                'id' => '14',
                'neigbhbourhood_id' => '106597', // Real Valencia
                'property_type_id' => '2',
                'xanteid' => 'XA0024',
                // 'year' => '',
                'title' => 'Departamento',
                'title2' => 'equipado en 2do nivel',
                'description' => 'Departamento en 3er piso que cuenta con 1 recámara, estancia y comedor. Está ubicado en Real Valencia, un desarrollo con acceso controlado, casa club, dog park, multicancha y más. A 25 min de la zona hotelera y la playa.',
                'street' => 'Talaia',
                'no_ext' => '315',
                'no_int' => '',
                'slug' => 'real-valencia-cancun-XA0024',
                // 'pet_friendly' => '',
                'bathrooms' => '1',
                'location_description' => '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7442.185609125889!2d-86.9337164!3d21.1487047!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f4dd5c725dc8839%3A0xb9b3e711acc70bed!2sReal%20Valencia!5e0!3m2!1ses-419!2smx!4v1667846602056!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>',
                'half_bathrooms' => '0',
                'bedrooms' => '1',
                'parking_slots' => '1',
                'm2' => '35',
                'm2_construction' => '49',
                'm2_garden' => '0',
                'floors' => '1',
                'lat' => '21.1487047',
                'lon' => '-86.9337164',
                // 'number_rooms' => '0',
                'price' => '605000',
                'map_url' => 'https://g.page/RealValenciaCancun?share',
                'status' => '1',  //'0.- inactivo, 1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada'
                'created_at' => '2022-11-04 15:41:37',
                'updated_at' => '2022-11-04 15:41:37',
                'deleted_at' => NULL,
                ),
                array(
                'id' => '15',
                'neigbhbourhood_id' => '103814', // Real solare
                'property_type_id' => '1',
                'xanteid' => 'XA0025',
                // 'year' => '',
                'title' => 'Casa Dúplex',
                'title2' => 'Planta Alta',
                'description' => 'Casa en planta alta que cuenta con cocina integral, 2 recámaras y área de lavado.
                Está ubicada en Real Solare, un desarrollo con acceso controlado, parques infantiles, dog park, multicanchas, gym al aire libre, ciclopista, escuelas y comercio.
                Muy cerca de parques industriales de la zona El Marqués y a 15 min del centro de Querétaro.',
                'street' => 'Ganimedes',
                'no_ext' => '47',
                'no_int' => '',
                'slug' => 'real-solare-queretaro-XA0025',
                // 'pet_friendly' => '',
                'bathrooms' => '1',
                'location_description' => '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d59760.59182421701!2d-100.2899472!3d20.586547!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d342cc112abe65%3A0xcb0cd5063e7b5675!2sReal%20Solare%2C%2076246%20Qro.!5e0!3m2!1ses-419!2smx!4v1667847041637!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>',
                'half_bathrooms' => '0',
                'bedrooms' => '2',
                'parking_slots' => '1',
                'm2' => '47',
                'm2_construction' => '45',
                'm2_garden' => '0',
                'floors' => '1',
                'lat' => '20.586547',
                'lon' => '-100.2899472',
                // 'number_rooms' => '0',
                'price' => '815000',
                'map_url' => 'https://goo.gl/maps/LJsKr2tpZhuJkw9P9',
                'status' => '2',  //'0.- inactivo, 1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada'
                'created_at' => '2022-11-04 15:41:37',
                'updated_at' => '2022-11-04 15:41:37',
                'deleted_at' => NULL,
                ),
                array(
                'id' => '16',
                'neigbhbourhood_id' => '70800', // Real verona
                'property_type_id' => '1',
                'xanteid' => 'XA0026',
                // 'year' => '',
                'title' => 'Casa',
                'title2' => 'de 2 niveles',
                'description' => 'Casa que cuenta con 3 recámaras y 2 lugares de estacionamiento.
                Está ubicada en Real Verona, un desarrollo con acceso controlado, parques infantiles, parque canino, multicanchas, gimnasio al aire libre, comercio y escuelas desde jardin de niños hasta secundaria.
                Muy cerca de avenidas principales y plazas comerciales.',
                'street' => 'Caltania',
                'no_ext' => '22',
                'no_int' => '',
                'slug' => 'real-verona-tecamac-XA0026',
                // 'pet_friendly' => '',
                'bathrooms' => '1',
                'location_description' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7514.85652875955!2d-99.02596534999999!3d19.651726150000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1f2135a3e68f7%3A0x2e64832cbc973d2c!2sFraccionamiento%20Real%20Verona%2C%20Ojo%20de%20Agua%2C%20M%C3%A9x.!5e0!3m2!1ses-419!2smx!4v1667605955808!5m2!1ses-419!2smx" class="w-full h-full rounded-xl" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>',
                'half_bathrooms' => '1',
                'bedrooms' => '3',
                'parking_slots' => '2',
                'm2' => '78',
                'm2_construction' => '51',
                'm2_garden' => '0',
                'floors' => '2',
                'lat' => '19.6517262',
                'lon' => '-99.0259653',
                // 'number_rooms' => '0',
                'price' => '1250000',
                'map_url' => 'https://goo.gl/maps/yJDw8hB6udoHGhAW7',
                'status' => '1',  //'0.- inactivo, 1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada'
                'created_at' => '2022-11-04 15:41:37',
                'updated_at' => '2022-11-04 15:41:37',
                'deleted_at' => NULL,
                ),
                array(
                    //'este post es solo una img, el card final
                    'id' => '1',
                    'neigbhbourhood_id' => '1', // Real toledo Pachuca
                    'property_type_id' => '1',
                    'xanteid' => 'XA000',
                    // 'year' => '',
                    'title' => 'null',
                    'title2' => 'null',
                    'description' => 'null',
                    'street' => 'null',
                    'no_ext' => '1',
                    'no_int' => '1',
                    'slug' => '1',
                    // 'pet_friendly' => '',
                    'bathrooms' => '1',
                    'location_description' => 'null',
                    'half_bathrooms' => '1',
                    'bedrooms' => '1',
                    'parking_slots' => '1',
                    'm2' => '1',
                    'm2_construction' => '1',
                    'm2_garden' => '1',
                    'floors' => '1',
                    'lat' => '1',
                    'lon' => '1',
                    // 'number_rooms' => '1',
                    'price' => '1',
                    'map_url' => 'null',
                    'status' => '10',  //'0.- inactivo, 1.- Disponibles 2.- Apartadas, 3.- Vendidas, 4.-Escriturada, 5.- Entregada'
                    'created_at' => '2022-11-04 15:41:37',
                    'updated_at' => '2022-11-04 15:41:37',
                    'deleted_at' => NULL,
                    ),
        );

        $propiedades_collection = collect($properties);

          $total = $propiedades_collection->count();
          $this->command->info("Insertando {$total} registros");

          $output = $this->command->getOutput();

          $output->progressStart($total);

          $propiedades_collection
            ->chunk(1000)
            ->each(function($propiedades_chunk) use ($output) {
                DB::table('properties')->insert($propiedades_chunk->toArray());
                $output->progressAdvance($propiedades_chunk->count());
          });

          $output->progressFinish();

          $this->command->info("Registros insertados correctamente");

          Schema::enableForeignKeyConstraints();
        }

    }



