<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;

use App\Models\User;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Schema::disableForeignKeyConstraints();

        DB::statement("TRUNCATE users");
        DB::statement("TRUNCATE model_has_permissions");
        DB::statement("TRUNCATE model_has_roles");
        DB::statement("TRUNCATE permissions");
        DB::statement("TRUNCATE roles");
        DB::statement("TRUNCATE role_has_permissions");


        $role_admin = Role::create(['name' => 'Admin']);
        $role_editorcompra = Role::create(['name' => 'EditorCompra']);
        $role_editorvente = Role::create(['name' => 'EditorVende']);
        $role_client = Role::create(['name' => 'Viewer']);

        $admin = User::create(['name' => 'Admin', 'email' => 'admin@inkwonders.com','email_verified_at'=> date('Y-m-d H:i:s'),'password' => Hash::make('inkwonders')]);
        $admin->assignRole('Admin');

        $admin = User::create(['name' => 'Editor Compra', 'email' => 'editorcompra@inkwonders.com','email_verified_at'=> date('Y-m-d H:i:s'),'password' => Hash::make('inkwonders')]);
        $admin->assignRole('EditorCompra');

        $admin = User::create(['name' => 'Editor Vende', 'email' => 'editorvende@inkwonders.com','email_verified_at'=> date('Y-m-d H:i:s'),'password' => Hash::make('inkwonders')]);
        $admin->assignRole('EditorVende');

        $admin = User::create(['name' => 'Visor', 'email' => 'visor@inkwonders.com','email_verified_at'=> date('Y-m-d H:i:s'),'password' => Hash::make('inkwonders')]);
        $admin->assignRole('Viewer');

        $admin = User::create(['name' => 'inkwonders', 'email' => 'ink@inkwonders.com','email_verified_at'=> date('Y-m-d H:i:s'),'password' => Hash::make('inkwonders')]);
        $admin->assignRole('Admin');

        Schema::enableForeignKeyConstraints();
    }
}
