<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PropiedadTipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $tipos_propiedad = array(
            array('nombre' => 'Casa'),
            array('nombre' => 'Departamento')
        );

        DB::table('propiedad_tipos')->insert($tipos_propiedad);
    }
}
