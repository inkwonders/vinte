<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PropiedadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $propiedades_prueba = array(
            array('estado_id' => 1, 'desarrollo_id' => 1, 'propiedad_tipo_id' => 1, 'colonia_id' => 102936, 'direccion' => 'Av zaragoza, esquina con la piedad', 'num_exterior' => '150', 'superficie_habitable' => 200.50, 'equipado' => 'SI', 'num_niveles_casa' => 2, 'terraza_jardin' => 'NO', 'num_recamaras' => 3, 'num_bath' => 2, 'num_med_bath' => 0, 'num_estacionamientos' => 2, 'antiguedad' => '5 Años', 'propiedad_precio' => 2500000.00, 'hipoteca' => 'NO', 'hipoteca_tipo' => null, 'telefono' => '4425678945', 'correo' => 'correo@prueba.com', 'nombre_propietario' => 'Carlos Daniel Juarez', 'motivo' => 'Cambiar de casa o depto', 'motivo_comp' => 'Otra empresa' ),
            array('estado_id' => 1, 'desarrollo_id' => 1, 'propiedad_tipo_id' => 1, 'colonia_id' => 3, 'direccion' => 'Av Juarez', 'num_exterior' => '220B', 'superficie_habitable' => 150, 'equipado' => 'NO', 'num_niveles_casa' => 1, 'terraza_jardin' => 'SI', 'num_recamaras' => 2, 'num_bath' => 1, 'num_med_bath' => 0, 'num_estacionamientos' => 2, 'antiguedad' => '10 Años', 'propiedad_precio' => 1250000.00, 'hipoteca' => 'SI', 'hipoteca_tipo' => 'Infonavit', 'telefono' => '4414527896', 'correo' => 'correo@correo.com', 'nombre_propietario' => 'Martin Suarez', 'motivo' => 'Remate bancario', 'motivo_comp' => null)
        );

        DB::table('propiedades')->insert($propiedades_prueba);

        $departamentos = array(
            array('estado_id' => 1, 'desarrollo_id' => 2, 'propiedad_tipo_id' => 2, 'colonia_id' => 777, 'direccion' => 'Francisco I. Madero', 'num_exterior' => 'S/N', 'superficie_habitable' => 80, 'equipado' => 'NO', 'piso_departamento' => 2, 'num_recamaras' => 1, 'num_bath' => 1, 'num_med_bath' => 0, 'num_estacionamientos' => 0, 'antiguedad' => '1 Año', 'propiedad_precio' => 300000, 'hipoteca' => 'SI', 'hipoteca_tipo' => 'Fovisste', 'telefono' => '4493124578', 'correo' => 'correo@aguascalientes.com', 'nombre_propietario' => 'Silvia Oliva Martinez', 'motivo' => 'Busco liquidez', 'motivo_comp' => null),
            array('estado_id' => 1, 'desarrollo_id' => 2, 'propiedad_tipo_id' => 2, 'colonia_id' => 102936, 'direccion' => 'Francisco I. Madero', 'num_exterior' => '132', 'superficie_habitable' => 100, 'equipado' => 'SI', 'piso_departamento' => 2, 'num_recamaras' => 2, 'num_bath' => 1, 'num_med_bath' => 1, 'num_estacionamientos' => 2, 'antiguedad' => '50 Años', 'propiedad_precio' => 2530000, 'hipoteca' => 'NO', 'hipoteca_tipo' => null, 'telefono' => '4429236578', 'correo' => 'correo@centroqueretaro.com', 'nombre_propietario' => 'Angel Ramiro Solis', 'motivo' => 'Cambiar de casa o depto', 'motivo_comp' => 'Otro desarrollo Vinte')
        );

        DB::table('propiedades')->insert($departamentos);


        $relacion = array(
            array('propiedad_id' => 1, 'equipo_id' => 1),
            array('propiedad_id' => 1, 'equipo_id' => 2),
            array('propiedad_id' => 1, 'equipo_id' => 3),
            array('propiedad_id' => 2, 'equipo_id' => 1),
            array('propiedad_id' => 2, 'equipo_id' => 2),
            array('propiedad_id' => 2, 'equipo_id' => 3),
            array('propiedad_id' => 3, 'equipo_id' => 1),
            array('propiedad_id' => 3, 'equipo_id' => 2),
            array('propiedad_id' => 3, 'equipo_id' => 3),
            array('propiedad_id' => 4, 'equipo_id' => 1),
            array('propiedad_id' => 4, 'equipo_id' => 2),
            array('propiedad_id' => 4, 'equipo_id' => 3),
        );

        DB::table('propiedad_equipo')->insert($relacion);

    }
}
