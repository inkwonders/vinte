<?php

namespace Database\Seeders;

use App\Models\PropiedadTipo;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            //seeders de la ronda 1 cuando se creó xante
            StatesMunicipalitiesSeeder::class,
            DesarrolloSeeder::class,
            PropiedadTipoSeeder::class,
            EquipoSeeder::class,
            PropiedadSeeder::class,
            EstadosUpdateSeeder::class,
            MunicipalitiesSeeder::class,

            //seeders de la ronda 2, la mas actual, cuando se cambió compra tu casa
            //descomentar en una nueva corrida de seeders desde cero
            RoleSeeder::class,
            StateSeeder::class,
            StatesMunicipalitiesNeighbourhoodsSeeder::class,
            PropertyTypeSeeder::class,
            PropertySeeder::class,
            PropiedadImagenSeeder::class,


        ]);
    }
}
