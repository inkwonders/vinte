<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DesarrolloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $desarrollos = array(
        //     array('id'=> 1,'nombre' => 'Real del Sol', 'estado_id' => 9),
        //     array('id'=> 2,'nombre' => 'Real del Cid', 'estado_id' => 9),
        //     array('id'=> 3,'nombre' => 'Real Firenze', 'estado_id' => 9),
        //     array('id'=> 4,'nombre' => 'Real Castell', 'estado_id' => 9),
        //     array('id'=> 5,'nombre' => 'Real Verona', 'estado_id' => 9),
        //     array('id'=> 6,'nombre' => 'Real Toscana', 'estado_id' => 9),
        //     array('id'=> 7,'nombre' => 'Real Carrara', 'estado_id' => 9),
        //     array('id'=> 8,'nombre' => 'Real Vizcaya', 'estado_id' => 9),
        //     array('id'=> 9,'nombre' => 'Real Granada', 'estado_id' => 9),

        //     array('id'=> 10,'nombre' => 'Real Toledo', 'estado_id' => 13),
        //     array('id'=> 11,'nombre' => 'Real Navarra  ', 'estado_id' => 13),
        //     array('id'=> 12,'nombre' => 'Real Madeira', 'estado_id' => 13),
        //     array('id'=> 13,'nombre' => 'Real Castilla', 'estado_id' => 13),

        //     array('id'=> 14,'nombre' => 'Real Solare', 'estado_id' => 22),
        //     array('id'=> 15,'nombre' => 'Misión de Santiago', 'estado_id' => 22),
        //     array('id'=> 16,'nombre' => 'Livenza', 'estado_id' => 22),
        //     array('id'=> 17,'nombre' => 'Rivalta', 'estado_id' => 22),
        //     array('id'=> 18,'nombre' => 'Rivello', 'estado_id' => 22),

        //     array('id'=> 19,'nombre' => 'Real Segovia', 'estado_id' => 21),

        //     array('id'=> 20,'nombre' => 'Real Ibiza', 'estado_id' => 23),
        //     array('id'=> 21,'nombre' => 'Real Ibiza Plus', 'estado_id' => 23),
        //     array('id'=> 22,'nombre' => 'Real Lucerna', 'estado_id' => 23),
        //     array('id'=> 23,'nombre' => 'Real Amalfi', 'estado_id' => 23),
        //     array('id'=> 24,'nombre' => 'Jardines de Ciudad Mayakoba', 'estado_id' => 23),
        //     array('id'=> 25,'nombre' => 'Real Valencia', 'estado_id' => 23),
        //     array('id'=> 26,'nombre' => 'Catania Residencial', 'estado_id' => 23),

        //     array('id'=> 27,'nombre' => 'Montalto', 'estado_id' => 19),
        //     array('id'=> 999,'nombre' => 'Otro', 'estado_id' => 1),
        // );

        // DB::table('desarrollos')->truncate();

        $desarrollos = array(
            array('id'=> 1,'nombre' =>'Catania Residencial', 'estado_id' => 9),
            array('id'=> 2,'nombre' =>'Jardines de Ciudad Mayakoba', 'estado_id' => 9),
            array('id'=> 3,'nombre' =>'Livenza', 'estado_id' => 9),
            array('id'=> 4,'nombre' =>'Misión de Santiago', 'estado_id' => 9),
            array('id'=> 5,'nombre' =>'Montalto Residencial', 'estado_id' => 9),
            array('id'=> 6,'nombre' =>'Real Alcalá', 'estado_id' => 9),
            array('id'=> 7,'nombre' =>'Real Amalfi', 'estado_id' => 9),
            array('id'=> 8,'nombre' =>'Real Carrara', 'estado_id' => 9),
            array('id'=> 9,'nombre' =>'Real Castell', 'estado_id' => 9),
            array('id'=> 10,'nombre' =>'Real Castilla', 'estado_id' => 9),
            array('id'=> 11,'nombre' =>'Real del Cid', 'estado_id' => 9),
            array('id'=> 12,'nombre' =>'Real del Sol', 'estado_id' => 9),
            array('id'=> 13,'nombre' =>'Real Firenze', 'estado_id' => 9),
            array('id'=> 14,'nombre' =>'Real Granada', 'estado_id' => 9),
            array('id'=> 15,'nombre' =>'Real Ibiza', 'estado_id' => 9),
            array('id'=> 16,'nombre' =>'Real Ibiza Plus', 'estado_id' => 9),
            array('id'=> 17,'nombre' =>'Real Lucerna', 'estado_id' => 9),
            array('id'=> 18,'nombre' =>'Real Madeira', 'estado_id' => 9),
            array('id'=> 19,'nombre' =>'Real Navarra', 'estado_id' => 9),
            array('id'=> 20,'nombre' =>'Real Segovia', 'estado_id' => 9),
            array('id'=> 21,'nombre' =>'Real Solare', 'estado_id' => 9),
            array('id'=> 22,'nombre' =>'Real Toledo', 'estado_id' => 9),
            array('id'=> 23,'nombre' =>'Real Toscana', 'estado_id' => 9),
            array('id'=> 24,'nombre' =>'Real Valencia', 'estado_id' => 9),
            array('id'=> 25,'nombre' =>'Real Verona', 'estado_id' => 9),
            array('id'=> 26,'nombre' =>'Real Vizcaya', 'estado_id' => 9),
            array('id'=> 27,'nombre' =>'Rivalta', 'estado_id' => 9),
            array('id'=> 28,'nombre' =>'Rivello', 'estado_id' => 9),
        );

        DB::table('desarrollos')->insert($desarrollos);
    }
}
