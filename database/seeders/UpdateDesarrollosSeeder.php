<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use App\Models\Desarrollo;

class UpdateDesarrollosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // DB::statement("TRUNCATE desarrollos");

        DB::table('desarrollos')->where('id', 26)->update(['order' => 1]);
        DB::table('desarrollos')->where('id', 24)->update(['order' => 2]);
        DB::table('desarrollos')->where('id', 16)->update(['order' => 3]);
        DB::table('desarrollos')->where('id', 15)->update(['order' => 4]);
        DB::table('desarrollos')->where('id', 27)->update(['order' => 5]);

        Desarrollo::create(['id' => 30, 'nombre' => 'Real Alcalá','estado_id'=> 9,'order' => 6])->save();

        DB::table('desarrollos')->where('id', 23)->update(['order' => 7]);
        DB::table('desarrollos')->where('id', 7)->update(['order' => 8]);
        DB::table('desarrollos')->where('id', 4)->update(['order' => 9]);
        DB::table('desarrollos')->where('id', 13)->update(['order' => 10]);
        DB::table('desarrollos')->where('id', 2)->update(['order' => 11]);
        DB::table('desarrollos')->where('id', 1,)->update(['order' => 12]);
        DB::table('desarrollos')->where('id', 3,)->update(['order' => 13]);
        DB::table('desarrollos')->where('id', 9)->update(['order' => 14]);
        DB::table('desarrollos')->where('id', 20)->update(['order' => 15]);
        DB::table('desarrollos')->where('id', 21)->update(['order' => 16]);
        DB::table('desarrollos')->where('id', 22)->update(['order' => 17]);
        DB::table('desarrollos')->where('id', 12)->update(['order' => 18]);
        DB::table('desarrollos')->where('id', 11)->update(['order' => 19]);
        DB::table('desarrollos')->where('id', 19)->update(['order' => 20]);
        DB::table('desarrollos')->where('id', 14)->update(['order' => 21]);
        DB::table('desarrollos')->where('id', 10)->update(['order' => 22]);
        DB::table('desarrollos')->where('id', 6)->update(['order' => 23]);
        DB::table('desarrollos')->where('id', 25)->update(['order' => 24]);
        DB::table('desarrollos')->where('id', 5)->update(['order' => 25]);
        DB::table('desarrollos')->where('id', 8)->update(['order' => 26]);
        DB::table('desarrollos')->where('id', 17)->update(['order' => 27]);
        DB::table('desarrollos')->where('id', 18)->update(['order' => 28]);
        DB::table('desarrollos')->where('id', 999)->update(['order' => 29]);


    }
}
