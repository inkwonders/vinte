<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PropertyType;
use App\Models\Document;

class PropertyTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        PropertyType::create(['name' => 'Casa', 'description' => ''])->save();
        PropertyType::create(['name' => 'Departamento', 'description' => ''])->save();
    }
}
