<?php

namespace Database\Seeders;

use App\Models\Estado;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstadosUpdateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estados = Estado::all();

        $estados->each(function($estado, $key){

            // $activos = [
            //     'Estado de México',
            //     'Hidalgo',
            //     'Nuevo León',
            //     'Querétaro',
            //     'Quintana Roo',
            //     'Puebla'
            // ];
            $activos = [
                'Aguascalientes',
                'Baja California',
                'Baja California Sur',
                'Campeche',
                'Coahuila de Zaragoza',
                'Colima',
                'Chiapas',
                'Chihuahua',
                'Ciudad de México',
                'Durango',
                'Guanajuato',
                'Guerrero',
                'Hidalgo',
                'Jalisco',
                'México',
                'Michoacán de Ocampo',
                'Morelos',
                'Nayarit',
                'Nuevo León',
                'Oaxaca',
                'Puebla',
                'Querétaro',
                'Quintana Roo',
                'San Luis Potosí',
                'Sinaloa',
                'Sonora',
                'Tabasco',
                'Tamaulipas',
                'Tlaxcala',
                'Veracruz de Ignacio de la Llave',
                'Yucatán',
                'Zacatecas',
            ];

            if(in_array($estado->descripcion, $activos)){
                $estado->habilitado = true;
            }else{
                $estado->habilitado = false;
            }

            $estado->save();

        });
    }
}
