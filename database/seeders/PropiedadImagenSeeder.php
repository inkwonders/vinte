<?php

namespace Database\Seeders;
use App\Models\Property;
use App\Models\Multimedia;
use Illuminate\Database\Seeder;

class PropiedadImagenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $propiedades=Property::all()
        ->each(function($property)  {
                                if($property->id==1){

                                    $num_fotos=1;
                                    $folder="assets/images/jpg//card_cero";

                                    for ($j=0; $j <=$num_fotos ; $j++) {

                                    $image1 = new Multimedia();
                                    $image1->name = "Foto " . $j;
                                    $image1->mime = "image/png";
                                    $image1->size = "M";
                                    $image1->compresed = 0;
                                    $image1->order = $j;
                                    $image1->alt = "Foto " . $j;
                                    $image1->url = $folder. "//" . $j . ".png";
                                    $image1->is_mobile = 0;

                                    $property->images()->save($image1);

                                    }

                                }//card_cero


                             if($property->id==2){

                                $num_fotos=8;
                                $folder="assets/images/jpg//real_toledo_pachuca_52";

                                for ($j=0; $j <=$num_fotos ; $j++) {

                                $image1 = new Multimedia();
                                $image1->name = "Foto " . $j;
                                $image1->mime = "image/png";
                                $image1->size = "M";
                                $image1->compresed = 0;
                                $image1->order = $j;
                                $image1->alt = "Foto " . $j;
                                $image1->url = $folder. "//" . $j . ".png";
                                $image1->is_mobile = 0;

                                $property->images()->save($image1);

                                }

                             }// Real toledo Pachuca




                             if($property->id==3){

                                $num_fotos=4;
                                $folder="assets/images/jpg//real_del_cid_tecamac_7";
                                $extension='.jpeg';

                                for ($j=0; $j <=$num_fotos ; $j++) {

                                    if($j!=0){ $extension='.jpg'; }

                                $image1 = new Multimedia();
                                $image1->name = "Foto " . $j;
                                $image1->mime = "image/png";
                                $image1->size = "M";
                                $image1->compresed = 0;
                                $image1->order = $j;
                                $image1->alt = "Foto " . $j;
                                $image1->url = $folder."//" . $j . $extension;
                                $image1->is_mobile = 0;

                                $property->images()->save($image1);

                                }

                             }// Real del cid


                             if($property->id==4){

                                $num_fotos=8;
                                $folder="assets/images/jpg//real_solare_queretaro_8";

                                for ($j=0; $j <=$num_fotos ; $j++) {

                                $image1 = new Multimedia();
                                $image1->name = "Foto " . $j;
                                $image1->mime = "image/png";
                                $image1->size = "M";
                                $image1->compresed = 0;
                                $image1->order = $j;
                                $image1->alt = "Foto " . $j;
                                $image1->url = $folder. "//" . $j . ".png";
                                $image1->is_mobile = 0;

                                $property->images()->save($image1);

                                }

                             }// Real solare



                             if($property->id==5){

                                $num_fotos=10;
                                $folder="assets/images/jpg//xa0012";

                                for ($j=0; $j <=$num_fotos ; $j++) {

                                $image1 = new Multimedia();
                                $image1->name = "Foto " . $j;
                                $image1->mime = "image/png";
                                $image1->size = "M";
                                $image1->compresed = 0;
                                $image1->order = $j;
                                $image1->alt = "Foto " . $j;
                                $image1->url = $folder. "//" . $j . ".png";
                                $image1->is_mobile = 0;

                                $property->images()->save($image1);

                                }

                             }// Real navarra


                             if($property->id==6){

                                $num_fotos=11;
                                $folder="assets/images/jpg//real-verona-tecamac-13";

                                for ($j=1; $j <=$num_fotos ; $j++) {

                                $image1 = new Multimedia();
                                $image1->name = "Foto " . $j;
                                $image1->mime = "image/png";
                                $image1->size = "M";
                                $image1->compresed = 0;
                                $image1->order = $j;
                                $image1->alt = "Foto " . $j;
                                $image1->url = $folder. "//" . $j . ".png";
                                $image1->is_mobile = 0;

                                $property->images()->save($image1);

                                }

                             }// Real navarra


                             if($property->id==7){

                                $num_fotos=7;
                                $folder="assets/images/jpg//real-segovia-puebla-14";

                                for ($j=1; $j <=$num_fotos ; $j++) {

                                $image1 = new Multimedia();
                                $image1->name = "Foto " . $j;
                                $image1->mime = "image/png";
                                $image1->size = "M";
                                $image1->compresed = 0;
                                $image1->order = $j;
                                $image1->alt = "Foto " . $j;
                                $image1->url = $folder. "//" . $j . ".png";
                                $image1->is_mobile = 0;

                                $property->images()->save($image1);

                                }

                             }// Real segobia

                             if($property->id==8){

                                $num_fotos=9;
                                $folder="assets/images/jpg//xa0015";

                                for ($j=1; $j <=$num_fotos ; $j++) {

                                $image1 = new Multimedia();
                                $image1->name = "Foto " . $j;
                                $image1->mime = "image/png";
                                $image1->size = "M";
                                $image1->compresed = 0;
                                $image1->order = $j;
                                $image1->alt = "Foto " . $j;
                                $image1->url = $folder. "//" . $j . ".png";
                                $image1->is_mobile = 0;

                                $property->images()->save($image1);

                                }

                             }// Real segobia


                             if($property->id==9){

                                $num_fotos=6;
                                $folder="assets/images/jpg//xa0018";

                                for ($j=1; $j <=$num_fotos ; $j++) {

                                $image1 = new Multimedia();
                                $image1->name = "Foto " . $j;
                                $image1->mime = "image/png";
                                $image1->size = "M";
                                $image1->compresed = 0;
                                $image1->order = $j;
                                $image1->alt = "Foto " . $j;
                                $image1->url = $folder. "//" . $j . ".png";
                                $image1->is_mobile = 0;

                                $property->images()->save($image1);

                                }

                             }// Real bilbao


                             if($property->id==10){

                                $num_fotos=8;
                                $folder="assets/images/jpg//xa0017";

                                for ($j=1; $j <=$num_fotos ; $j++) {

                                $image1 = new Multimedia();
                                $image1->name = "Foto " . $j;
                                $image1->mime = "image/png";
                                $image1->size = "M";
                                $image1->compresed = 0;
                                $image1->order = $j;
                                $image1->alt = "Foto " . $j;
                                $image1->url = $folder. "//" . $j . ".png";
                                $image1->is_mobile = 0;

                                $property->images()->save($image1);

                                }

                             }// Real ibiza plus

                             if($property->id==11){

                                $num_fotos=9;
                                $folder="assets/images/jpg//real-solare-queretaro-21";

                                for ($j=1; $j <=$num_fotos ; $j++) {

                                $image1 = new Multimedia();
                                $image1->name = "Foto " . $j;
                                $image1->mime = "image/png";
                                $image1->size = "M";
                                $image1->compresed = 0;
                                $image1->order = $j;
                                $image1->alt = "Foto " . $j;
                                $image1->url = $folder. "//" . $j . ".png";
                                $image1->is_mobile = 0;

                                $property->images()->save($image1);

                                }

                             }// Real solare


                             if($property->id==12){

                                $num_fotos=5;
                                $folder="assets/images/jpg//real-verona-edo-mex-22";

                                for ($j=1; $j <=$num_fotos ; $j++) {

                                $image1 = new Multimedia();
                                $image1->name = "Foto " . $j;
                                $image1->mime = "image/png";
                                $image1->size = "M";
                                $image1->compresed = 0;
                                $image1->order = $j;
                                $image1->alt = "Foto " . $j;
                                $image1->url = $folder. "//" . $j . ".png";
                                $image1->is_mobile = 0;

                                $property->images()->save($image1);

                                }

                             }// Real verona

                             if($property->id==13){

                                $num_fotos=8;
                                $folder="assets/images/jpg//real-castilla-hidalgo-23";

                                for ($j=1; $j <=$num_fotos ; $j++) {

                                $image1 = new Multimedia();
                                $image1->name = "Foto " . $j;
                                $image1->mime = "image/png";
                                $image1->size = "M";
                                $image1->compresed = 0;
                                $image1->order = $j;
                                $image1->alt = "Foto " . $j;
                                $image1->url = $folder. "//" . $j . ".png";
                                $image1->is_mobile = 0;

                                $property->images()->save($image1);

                                }

                             }// Real castilla


                             if($property->id==13){

                                $num_fotos=8;
                                $folder="assets/images/jpg//real-castilla-hidalgo-23";

                                // for ($j=1; $j <=$num_fotos ; $j++) {

                                // $image1 = new Multimedia();
                                // $image1->name = "Foto " . $j;
                                // $image1->mime = "image/png";
                                // $image1->size = "M";
                                // $image1->compresed = 0;
                                // $image1->order = $j;
                                // $image1->alt = "Foto " . $j;
                                // $image1->url = $folder. "//" . $j . ".png";
                                // $image1->is_mobile = 0;

                                // $property->images()->save($image1);

                                // }

                             }// Real castilla


                             if($property->id==14){

                                $num_fotos=8;
                                $folder="assets/images/jpg//real-valencia-quintana-roo-24";

                                for ($j=1; $j <=$num_fotos ; $j++) {

                                $image1 = new Multimedia();
                                $image1->name = "Foto " . $j;
                                $image1->mime = "image/png";
                                $image1->size = "M";
                                $image1->compresed = 0;
                                $image1->order = $j;
                                $image1->alt = "Foto " . $j;
                                $image1->url = $folder. "//" . $j . ".png";
                                $image1->is_mobile = 0;

                                $property->images()->save($image1);

                                }

                             }// Real Valencia


                             if($property->id==15){

                                $num_fotos=6;
                                $folder="assets/images/jpg//real-solare-queretaro-25";

                                for ($j=1; $j <=$num_fotos ; $j++) {

                                $image1 = new Multimedia();
                                $image1->name = "Foto " . $j;
                                $image1->mime = "image/png";
                                $image1->size = "M";
                                $image1->compresed = 0;
                                $image1->order = $j;
                                $image1->alt = "Foto " . $j;
                                $image1->url = $folder. "//" . $j . ".png";
                                $image1->is_mobile = 0;

                                $property->images()->save($image1);

                                }

                             }// Real solare


                             if($property->id==16){

                                $num_fotos=9;
                                $folder="assets/images/jpg//real-verona-edo-mex-26";

                                for ($j=1; $j <=$num_fotos ; $j++) {

                                $image1 = new Multimedia();
                                $image1->name = "Foto " . $j;
                                $image1->mime = "image/png";
                                $image1->size = "M";
                                $image1->compresed = 0;
                                $image1->order = $j;
                                $image1->alt = "Foto " . $j;
                                $image1->url = $folder. "//" . $j . ".png";
                                $image1->is_mobile = 0;

                                $property->images()->save($image1);

                                }

                             }// Real verona
      });

    }
}
